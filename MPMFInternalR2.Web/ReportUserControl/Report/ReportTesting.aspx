﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportTesting.aspx.cs" Inherits="Report.ReportUserControl.Report.ReportTesting" %>

<%@ Register Src="~/ReportUserControl/Report/UCReportSearch.ascx" TagName="UCReportSearch" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" ID="smCust">
        </asp:ScriptManager>
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="AccMnt\Report\PdcBounceRpt"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">
                                PDC BOUNCE REPORT</label></span> <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />

            <div id="dPDCReceive" runat="server">
                <uc1:UCReportSearch id="ucReportSearch" runat="server" title="Report Type-Search" />
            </div>
        </div>
    </form>
</body>
</html>

