﻿using AdIns.Service;
using AdIns.Util.TextFormatter;
using Confins.BusinessService.Common.Reports;
using Confins.BusinessService.Common.ReportWebService;
using Confins.BusinessService.Common.Setting;
using Confins.BusinessService.RefCommon.Setting;
using Confins.Common.Exp;
using Confins.DataModel.Common.NCModel;
using Confins.DataModel.RefCommonModel;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportUserControl.Report
{
    public partial class UCReportSearch : UserControlBase
    {
        #region UserControl's Path
        private const string UCINPUTNUMBERPATH = "~/WebUserControl/UCInputNumber.ascx";
        private const string UCDATEPICKERPATH = "~/WebUserControl/UCDatePicker.ascx";
        private const string UCREFERENCEPATH = "~/WebUserControl/UCReference.ascx";
        #endregion

        #region Constanta
        private string ALL = "All";
        #endregion

        #region Delegate Events Handler
        // Delegate declaration
        [Serializable]
        public delegate void OnButtonClick();
        #endregion

        #region Delegate Properties
        // Event declaration
        public event OnButtonClick btnHandler;
        #endregion

        #region Properties
        //digunakan untuk membuat Main Parameters
        public Dictionary<string, object> Criteria
        {
            get { return _criteria; }
            set { _criteria = value; }
        }
        private Dictionary<string, object> _criteria
        {
            get { return ViewState["_criteria"] as Dictionary<string, object>; }
            set { ViewState["_criteria"] = value; }
        }

        //digunakan untuk mengambil Text pada Main Parameters yang akan digunakan sebagai parameters pada Template Parameters
        public Dictionary<string, string> TextOfMainParams
        {
            get { return _textOfMainParams; }
            set { _textOfMainParams = value; }
        }
        private Dictionary<string, string> _textOfMainParams
        {
            get { return ViewState["_textOfMainParams"] as Dictionary<string, string>; }
            set { ViewState["_textOfMainParams"] = value; }
        }

        //digunakan untuk mengambil Text pada Additional input yang akan digunakan sebagai parameters pada Template Parameters
        public Dictionary<string, string> TextOfAdditionalInput
        {
            get { return _textOfAdditionalInput; }
            set { _textOfAdditionalInput = value; }
        }
        private Dictionary<string, string> _textOfAdditionalInput
        {
            get { return ViewState["_textOfAdditionalInput"] as Dictionary<string, string>; }
            set { ViewState["_textOfAdditionalInput"] = value; }
        }

        //digunakan untuk menyimpan Code Template
        private string _reportTemplateCode
        {
            get { return ViewState["_reportTemplateCode"] as string; }
            set { ViewState["_reportTemplateCode"] = value; }
        }

        //di gunakan untuk membuat Template Parameters
        private Dictionary<string, string> _templateParam
        {
            get { return ViewState["_templateParam"] as Dictionary<string, string>; }
            set { ViewState["_templateParam"] = value; }
        }

        //digunakan untuk Inisialisasi Field dan Template Param dari User
        private ReportControlParams _scParam
        {
            get { return (ReportControlParams)ViewState["_scParam"]; }
            set { ViewState["_scParam"] = value; }
        }

        private TextFormatterHelper formatter { get; set; }
        #endregion

        #region Override LoadViewState Event
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            createFixedSearchAppearanceTable();
            createReportSeacrhAppearanceTable();
            createAdditionalInputAppearanceTable();

        }
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                createFixedSearchAppearanceTable();
                createReportSeacrhAppearanceTable();
                createAdditionalInputAppearanceTable();
            }

            formatter = UnityFactory.Resolve<TextFormatterHelper>();
        }
        #endregion

        #region Events
        protected void SyncOnClick(object sender, EventArgs e)
        {
            GenerateSearchCriteria();

            generateTextMainParameters();

            generateTextAdditionalInput();

            if (btnHandler != null)
                btnHandler();
    
        }

        protected void AsyncOnClick(object sender, EventArgs e)
        {
            RptListService rptListSvc = (RptListService)UnityFactory.Resolve<RptListService>();
            RefUserService refUserSvc = (RefUserService)UnityFactory.Resolve<RefUserService>();
            RefEmpService refEmpSvc = (RefEmpService)UnityFactory.Resolve<RefEmpService>();
            GenerateSearchCriteria();

            if (btnHandler != null)
                btnHandler();

            generateTextMainParameters();
            generateTextAdditionalInput();

            _templateParam = generateTemplateParams(_scParam.ListOfReportTemplateParam, _textOfMainParams);

            Merge(_templateParam, _textOfAdditionalInput);

            ReportServices rptSvc = new ReportServices();
            confReportInfo rptInfoMain = new confReportInfo();
            List<confReportInfo> rptInfoSubs = new List<confReportInfo>();
            List<UserRecipient> usrRecipients = new List<UserRecipient>();

            if (rptListSvc.GetRptList(_reportTemplateCode) != null)
            {
                string tmpltLoc = rptListSvc.GetRptList(_reportTemplateCode).RptTmpltPath;

                rptInfoMain.expFormat = getExportFormat();
                rptInfoMain.FullTmpltLocation = @tmpltLoc;
                rptInfoMain.DataProviderParameters = _criteria;
                rptInfoMain.TemplateParameters = _templateParam;

                RefUser refUser = refUserSvc.GetRefUser(Page.CurrentUserContext.RefUserId);
                RefEmp refEmp = refEmpSvc.GetRefEmpByRefUserId(Page.CurrentUserContext.RefUserId);

                rptInfoSubs = generateSubReportParams();

                usrRecipients.Add
                (
                    new UserRecipient()
                    {
                        RecpRefUserId = refUser.RefUserId,
                        RecpUserName = refUser.Username,
                        RecpEmailAddr = refEmp.Email1,
                        RecpMblPhnNo = refEmp.MobilePhn1
                    }
                );

                rptSvc.AsyncReportGen(rptInfoMain, rptInfoSubs, usrRecipients, Page.CurrentUserContext);

                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "showSaveNotification", " parent.parent.ShowSaveNotification('Report Has Sent To " + refEmp.Email1 + "');", true);
            }
            else
            {
                throw new Exception(_reportTemplateCode + ",Template Report Doesn't Exist");
            }
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            string url = Request.Url.ToString();

            Response.Redirect(url);

            //ResetClick();
        }
        #endregion

        #region Public Methods
        public void ResetClick()
        {
            phFixedSearch.Controls.Clear();
            phReportFeatureSearch.Controls.Clear();

            createFixedSearchAppearanceTable();
            createReportSeacrhAppearanceTable();
        }

        public void GenerateSearchCriteria()
        {
            _criteria = new Dictionary<string, object>();

            bool isContainAllCriteria = false;
            if (phFixedSearch.HasControls())
            {
                foreach (Control _tbl in phFixedSearch.Controls)
                {
                    foreach (Control tblRow in _tbl.Controls)
                    {
                        foreach (Control tblColl in tblRow.Controls)
                        {
                            foreach (Control ctrl in tblColl.Controls)
                            {
                                if (ctrl.GetType() == typeof(TextBox))
                                {
                                    TextBox txt = (TextBox)ctrl;
                                    string[] idSplit = txt.ID.Split('_');
                                    string text = idSplit[1];
                                    string propertyName = idSplit[0].Substring(3, idSplit[0].Length - 3);
                                    FixedSearchPropSpec fixedSearchPropSpec =
                                        _scParam.ListOfReportSearchInput.Find(a => a.PropName == propertyName && a.Text.Replace(" ", "").Replace("_", "") == text);

                                    if (txt.Text.Contains("%") && fixedSearchPropSpec.PropType != typeof(Int64))
                                        _criteria.Add(propertyName, txt.Text);

                                    else if (txt.Text != null && txt.Text != "" && !txt.Text.Contains("%"))
                                    {
                                        if (fixedSearchPropSpec.PropType == typeof(Int64))
                                            _criteria.Add(propertyName, Convert.ToInt64(txt.Text));
                                        else
                                            _criteria.Add(propertyName, txt.Text);
                                    }
                                    else if (txt.Text == string.Empty || txt.Text == "" || txt.Text == null)
                                    {
                                        _criteria.Add(propertyName, null);
                                    }
                                }
                                else if (ctrl.GetType() == typeof(DropDownList))
                                {
                                    DropDownList ddl = (DropDownList)ctrl;
                                    string[] idSplit = ddl.ID.Split('_');
                                    string text = idSplit[1];
                                    string propertyName = idSplit[0].Substring(3, idSplit[0].Length - 3);
                                    FixedSearchPropSpec fixedSearchPropSpec =
                                        _scParam.ListOfReportSearchInput.Find(a => a.PropName == propertyName && a.Text.Replace(" ", "").Replace("_", "") == text);

                                    if (ddl.SelectedValue == "All")
                                    {
                                        _criteria.Add(propertyName, null);
                                        isContainAllCriteria = true;
                                    }
                                    else if (ddl.SelectedValue != "")
                                    {
                                        if (fixedSearchPropSpec.PropType == typeof(Int64))
                                            _criteria.Add(propertyName, Convert.ToInt64(ddl.SelectedValue));
                                        else
                                            _criteria.Add(propertyName, ddl.SelectedValue);
                                    }
                                }
                                else if (ctrl.GetType().ToString().ToLower().Contains("ucinputnumber"))
                                {
                                    UCInputNumber txt = (UCInputNumber)ctrl;
                                    string[] idSplit = txt.ID.Split('_');
                                    string propertyName = idSplit[0].Substring(3, idSplit[0].Length - 3);
                                    string text = idSplit[1];
                                    FixedSearchPropSpec fixedSearchPropSpec =
                                        _scParam.ListOfReportSearchInput.Find(a => a.PropName == propertyName && a.Text.Replace(" ", "").Replace("_", "") == text);

                                    if (txt.Text.Contains("%") && fixedSearchPropSpec.PropType != typeof(Int64))
                                        _criteria.Add(propertyName, txt.Text);
                                    else if (txt.Text != null && (txt.Text != "0" || txt.Text != "0.00") && !txt.Text.Contains("%"))
                                    {
                                        _criteria.Add(propertyName, formatter.ParseFormString(fixedSearchPropSpec.PropType, txt.Text));
                                    }
                                    else if (txt.Text == string.Empty || txt.Text == "" || txt.Text == null)
                                    {
                                        _criteria.Add(propertyName, null);
                                    }
                                }
                                else if (ctrl.GetType().ToString().ToLower().Contains("ucdatepicker"))
                                {
                                    UCDatePicker txt = (UCDatePicker)ctrl;
                                    string[] idSplit = txt.ID.Split('_');
                                    string text = idSplit[1];
                                    string propertyName = idSplit[0].Substring(3, idSplit[0].Length - 3);
                                    FixedSearchPropSpec fixedSearchPropSpec =
                                        _scParam.ListOfReportSearchInput.Find(a => a.PropName == propertyName && a.Text.Replace(" ", "").Replace("_", "") == text);

                                    if (txt.Text.Contains("%"))
                                        _criteria.Add(propertyName, txt.Text);
                                    else if (txt.Text != null && txt.Text != "")
                                    {
                                        _criteria.Add(propertyName, txt.DateValue);
                                    }
                                    else if (txt.Text == string.Empty || txt.Text == "" || txt.Text == null)
                                    {
                                        _criteria.Add(propertyName, null);
                                    }
                                }
                                else if (ctrl.GetType().ToString().ToLower().Contains("ucreference"))
                                {
                                    UCReference txt = (UCReference)ctrl;
                                    string[] idSplit = txt.ID.Split('_');
                                    string text = idSplit[1];
                                    string propertyName = idSplit[0].Substring(3, idSplit[0].Length - 3);
                                    FixedSearchPropSpec fixedSearchPropSpec =
                                        _scParam.ListOfReportSearchInput.Find(a => a.PropName == propertyName && a.Text.Replace(" ", "").Replace("_", "") == text);

                                    object value = new object();
                                    if (txt.SelectedValue != ALL || txt.SelectedValue != "")
                                        value = formatter.ParseFormString(fixedSearchPropSpec.PropType, txt.SelectedValue);

                                    if (txt.SelectedValue == ALL)
                                    {
                                        _criteria.Add(propertyName, null);
                                        isContainAllCriteria = true;
                                    }
                                    else if (txt.SelectedValue != null && txt.SelectedValue != "" && txt.SelectedValue != ALL)
                                    {
                                        _criteria.Add(propertyName, value);
                                    }
                                    else if ((txt.SelectedValue == string.Empty || txt.SelectedValue == "" || txt.SelectedValue == null))
                                    {
                                        _criteria.Add(propertyName, null);
                                    }
                                }
                                else if (ctrl.GetType().ToString().ToLower().Contains("uclookup"))
                                {
                                    //FixedSearchPropSpec fixedSearchPropSpec = _scParam.FixedSearch.Find(a => a.PropName == propertyName);
                                    //string propertyName = txt.ID.Substring(3, txt.ID.Length - 3);
                                    //Convert.ChangeType(ctrl, 
                                    //UCLookupCust txt = (UCReference)ctrl;
                                    //string text = idSplit[1];
                                    //string propertyName = idSplit[0].Substring(3, idSplit[0].Length - 3);
                                    //FixedSearchPropSpec fixedSearchPropSpec = _scParam.FixedSearch.Find(a => a.PropName == propertyName && a.Text.Replace(" ", "") == text);

                                    //if (txt.SelectedText.Contains("%"))
                                    //    _criteria.Add(Restrictions.Like(propertyName, txt.SelectedValue));
                                    //else if (txt.SelectedText != null && txt.SelectedText != "")
                                    //    _criteria.Add(Restrictions.Eq(propertyName, txt.SelectedValue));
                                }
                            }
                        }
                    }
                }

                if (_criteria.Count == 0 && !isContainAllCriteria)
                    throw new CommonException(CommonExceptionType.InvalidCriteriaCount, "", null, null);
            }
        }

        public void UpdateItemEnabled(int row, int column, bool enabled)
        {
            Table tbl = (Table)phFixedSearch.FindControl("tblFixedSearch");
            Object control = tbl.Rows[row].Cells[column].Controls[0];
            String typeStr = control.GetType().ToString();

            if (typeStr.Contains("ucreference"))
            {
                UCReference objUCReference = (UCReference)control;
                objUCReference.Enabled = enabled;
            }
            else if (typeStr.Contains("ucdatepicker"))
            {
                UCDatePicker objDatePicker = (UCDatePicker)control;
                objDatePicker.Enabled = false;
            }
        }

        public void SetAttribute(ReportControlParams ScParam, string RptTmpltCode = "")
        {
            RptListService rptSvc = (RptListService)UnityFactory.Resolve<RptListService>();

            this._scParam = ScParam;
            this._reportTemplateCode = RptTmpltCode;

            RptList rpt = rptSvc.GetRptList(RptTmpltCode);

            if (rpt == null)
            {
                lb_Print_Async.Visible = false;
                lb_Print_Sync.Visible = true;
            }
            else
            {
                if (rpt.SynchronousType == RptList.RPT_TYPE_SYNHCRONOUS)
                {
                    lb_Print_Async.Visible = false;
                    lb_Print_Sync.Visible = true;
                }
                else if (rpt.SynchronousType == RptList.RPT_TYPE_ASYNCHRONOUS)
                {
                    lb_Print_Async.Visible = true;
                    lb_Print_Sync.Visible = false;
                }
                else
                {
                    lb_Print_Async.Visible = true;
                    lb_Print_Sync.Visible = true;
                }
            }

            if (_scParam.ListOfAdditionalReportInput.Count != 0)
                dAddInfo.Visible = true;

            upButton.Update();
        }

        public void SetAttribute(string RptTmpltCode)
        {
            RptListService rptSvc = (RptListService)UnityFactory.Resolve<RptListService>();

            this._reportTemplateCode = RptTmpltCode;

            RptList rpt = rptSvc.GetRptList(RptTmpltCode);

            if (rpt == null)
            {
                lb_Print_Async.Visible = false;
                lb_Print_Sync.Visible = true;
            }
            else
            {
                if (rpt.SynchronousType == RptList.RPT_TYPE_SYNHCRONOUS)
                {
                    lb_Print_Async.Visible = false;
                    lb_Print_Sync.Visible = true;
                }
                else if (rpt.SynchronousType == RptList.RPT_TYPE_ASYNCHRONOUS)
                {
                    lb_Print_Async.Visible = true;
                    lb_Print_Sync.Visible = false;
                }
                else
                {
                    lb_Print_Async.Visible = true;
                    lb_Print_Sync.Visible = true;
                }
            }

            upButton.Update();
        }
        #endregion

        #region Private Methods
        //untuk membuat table secara dinamis
        private void createFixedSearchAppearanceTable()
        {
            Table _tbl = new Table();
            _tbl.ID = "tblFixedSearch";
            _tbl.CssClass = "formTable";
            _tbl.EnableViewState = true;

            if (_scParam.ListOfReportSearchInput.Count > 0)
            {
                if (_scParam.ListOfReportSearchInput.Count < 4)
                {
                    for (int i = 0; i < _scParam.ListOfReportSearchInput.Count; i++)
                    {
                        FixedSearchPropSpec fixedSearchPropSpec = _scParam.ListOfReportSearchInput[i];
                        TableRow tRow = new TableRow();
                        TableCell tCell = new TableCell();
                        tCell.Width = Unit.Percentage(20);
                        tCell.CssClass = "tdDesc";

                        Literal ltlDescription = new Literal();
                        ltlDescription.ID = fixedSearchPropSpec.Text;
                        tCell.Controls.Add(ltlDescription);

                        if (fixedSearchPropSpec.IsShowSearchCondLabel)
                        {
                            Literal ltlSpace = new Literal();
                            ltlSpace.ID = "ltl_Search_Space_" + i;
                            tCell.Controls.Add(ltlSpace);

                            Literal ltlCondDescription = new Literal();
                            ltlCondDescription.ID = generateSearchCondDescriptionId(fixedSearchPropSpec.SearchCond);
                            tCell.Controls.Add(ltlCondDescription);
                        }

                        tRow.Cells.Add(tCell);

                        tCell = new TableCell();
                        tCell.Width = Unit.Percentage(80);
                        tCell.CssClass = "tdValue";

                        generateControlByType(fixedSearchPropSpec, ref tCell, i);
                        tRow.Cells.Add(tCell);
                        _tbl.Rows.Add(tRow);
                    }
                }
                else
                {
                    TableRow tRow = new TableRow();
                    for (int i = 0; i < _scParam.ListOfReportSearchInput.Count; i++)
                    {
                        FixedSearchPropSpec fixedSearchPropSpec = _scParam.ListOfReportSearchInput[i];
                        TableCell tCell = new TableCell();
                        tCell.Width = Unit.Percentage(20);
                        tCell.CssClass = "tdDesc";

                        Literal ltlDescription = new Literal();
                        ltlDescription.ID = fixedSearchPropSpec.Text;
                        tCell.Controls.Add(ltlDescription);

                        if (fixedSearchPropSpec.IsShowSearchCondLabel)
                        {
                            Literal ltlSpace = new Literal();
                            ltlSpace.ID = "ltl_Search_Space_" + i;
                            tCell.Controls.Add(ltlSpace);

                            Literal ltlCondDescription = new Literal();
                            ltlCondDescription.ID = generateSearchCondDescriptionId(fixedSearchPropSpec.SearchCond);
                            tCell.Controls.Add(ltlCondDescription);
                        }

                        tRow.Cells.Add(tCell);

                        tCell = new TableCell();
                        tCell.Width = Unit.Percentage(30);
                        tCell.CssClass = "tdValue";

                        generateControlByType(fixedSearchPropSpec, ref tCell, i);
                        tRow.Cells.Add(tCell);

                        if (i % 2 != 0 || i == _scParam.ListOfReportSearchInput.Count - 1)
                        {
                            _tbl.Rows.Add(tRow);
                            tRow = new TableRow();
                        }
                    }
                }

                phFixedSearch.Controls.Add(_tbl);
                upFixedSearch.Update();

                UIDataHelper helper = new UIDataHelper();
                helper.WriteMultilangText(this, Page.LanguageCode);
            }
        }

        private void createReportSeacrhAppearanceTable()
        {
            RptListService rptListSvc = (RptListService)UnityFactory.Resolve<RptListService>();
            List<KeyValuePair<string, string>> listOfReportType = rptListSvc.GetListOfReportFileType(_reportTemplateCode);

            _scParam.ListOfExportFeatureSearchInput = new List<ReportSearchPropSpec>();
            _scParam.AddExportFeatureSearchInput(new ReportSearchPropSpec[] 
                {
                    new ReportSearchPropSpec("ltl_RptList_ReportType", "exportTo", listOfReportType, "Key", "Value", typeof(string), false)
                });

            Table _tbl = new Table();
            _tbl.ID = "tblReportFeatureSearch";
            _tbl.CssClass = "formTable";
            _tbl.EnableViewState = true;

            if (_scParam.ListOfExportFeatureSearchInput.Count > 0)
            {
                if (_scParam.ListOfExportFeatureSearchInput.Count < 4)
                {
                    for (int i = 0; i < _scParam.ListOfExportFeatureSearchInput.Count; i++)
                    {
                        ReportSearchPropSpec reportSearchPropSpe = _scParam.ListOfExportFeatureSearchInput[i];
                        TableRow tRow = new TableRow();
                        TableCell tCell = new TableCell();
                        tCell.Width = Unit.Percentage(20);
                        tCell.CssClass = "tdDesc";

                        Literal ltlDescription = new Literal();
                        ltlDescription.ID = reportSearchPropSpe.Text;
                        tCell.Controls.Add(ltlDescription);

                        if (reportSearchPropSpe.IsShowSearchCondLabel)
                        {
                            Literal ltlSpace = new Literal();
                            ltlSpace.ID = "ltl_Search_Space_" + i;
                            tCell.Controls.Add(ltlSpace);

                            Literal ltlCondDescription = new Literal();
                            ltlCondDescription.ID = generateSearchCondDescriptionId(reportSearchPropSpe.SearchCond);
                            tCell.Controls.Add(ltlCondDescription);
                        }

                        tRow.Cells.Add(tCell);

                        tCell = new TableCell();
                        tCell.Width = Unit.Percentage(80);
                        tCell.CssClass = "tdValue";

                        generateReportFeatureControlByType(reportSearchPropSpe, ref tCell, i);
                        tRow.Cells.Add(tCell);
                        _tbl.Rows.Add(tRow);
                    }
                }
                else
                {
                    TableRow tRow = new TableRow();
                    for (int i = 0; i < _scParam.ListOfExportFeatureSearchInput.Count; i++)
                    {
                        ReportSearchPropSpec reportSearchPropSpe = _scParam.ListOfExportFeatureSearchInput[i];
                        TableCell tCell = new TableCell();
                        tCell.Width = Unit.Percentage(20);
                        tCell.CssClass = "tdDesc";

                        Literal ltlDescription = new Literal();
                        ltlDescription.ID = reportSearchPropSpe.Text;
                        tCell.Controls.Add(ltlDescription);

                        if (reportSearchPropSpe.IsShowSearchCondLabel)
                        {
                            Literal ltlSpace = new Literal();
                            ltlSpace.ID = "ltl_Search_Space_" + i;
                            tCell.Controls.Add(ltlSpace);

                            Literal ltlCondDescription = new Literal();
                            ltlCondDescription.ID = generateSearchCondDescriptionId(reportSearchPropSpe.SearchCond);
                            tCell.Controls.Add(ltlCondDescription);
                        }

                        tRow.Cells.Add(tCell);

                        tCell = new TableCell();
                        tCell.Width = Unit.Percentage(30);
                        tCell.CssClass = "tdValue";

                        generateReportFeatureControlByType(reportSearchPropSpe, ref tCell, i);
                        tRow.Cells.Add(tCell);

                        if (i % 2 != 0 || i == _scParam.ListOfReportSearchInput.Count - 1)
                        {
                            _tbl.Rows.Add(tRow);
                            tRow = new TableRow();
                        }
                    }
                }

                phReportFeatureSearch.Controls.Add(_tbl);
                upReportFeatureSearch.Update();

                UIDataHelper helper = new UIDataHelper();
                helper.WriteMultilangText(this, Page.LanguageCode);
            }
        }

        private void createAdditionalInputAppearanceTable()
        {
            Table _tbl = new Table();
            _tbl.ID = "tblAdditionalInput";
            _tbl.CssClass = "formTable";
            _tbl.EnableViewState = true;

            if (_scParam.ListOfAdditionalReportInput.Count > 0)
            {
                if (_scParam.ListOfAdditionalReportInput.Count < 4)
                {
                    for (int i = 0; i < _scParam.ListOfAdditionalReportInput.Count; i++)
                    {
                        FixedSearchPropSpec fixedSearchPropSpec = _scParam.ListOfAdditionalReportInput[i];
                        TableRow tRow = new TableRow();
                        TableCell tCell = new TableCell();
                        tCell.Width = Unit.Percentage(20);
                        tCell.CssClass = "tdDesc";

                        Literal ltlDescription = new Literal();
                        ltlDescription.ID = fixedSearchPropSpec.Text;
                        tCell.Controls.Add(ltlDescription);

                        if (fixedSearchPropSpec.IsShowSearchCondLabel)
                        {
                            Literal ltlSpace = new Literal();
                            ltlSpace.ID = "ltl_Search_Space_" + i;
                            tCell.Controls.Add(ltlSpace);

                            Literal ltlCondDescription = new Literal();
                            ltlCondDescription.ID = generateSearchCondDescriptionId(fixedSearchPropSpec.SearchCond);
                            tCell.Controls.Add(ltlCondDescription);
                        }

                        tRow.Cells.Add(tCell);

                        tCell = new TableCell();
                        tCell.Width = Unit.Percentage(80);
                        tCell.CssClass = "tdValue";

                        generateControlByType(fixedSearchPropSpec, ref tCell, i);
                        tRow.Cells.Add(tCell);
                        _tbl.Rows.Add(tRow);
                    }
                }
                else
                {
                    TableRow tRow = new TableRow();
                    for (int i = 0; i < _scParam.ListOfAdditionalReportInput.Count; i++)
                    {
                        FixedSearchPropSpec fixedSearchPropSpec = _scParam.ListOfAdditionalReportInput[i];
                        TableCell tCell = new TableCell();
                        tCell.Width = Unit.Percentage(20);
                        tCell.CssClass = "tdDesc";

                        Literal ltlDescription = new Literal();
                        ltlDescription.ID = fixedSearchPropSpec.Text;
                        tCell.Controls.Add(ltlDescription);

                        if (fixedSearchPropSpec.IsShowSearchCondLabel)
                        {
                            Literal ltlSpace = new Literal();
                            ltlSpace.ID = "ltl_Search_Space_" + i;
                            tCell.Controls.Add(ltlSpace);

                            Literal ltlCondDescription = new Literal();
                            ltlCondDescription.ID = generateSearchCondDescriptionId(fixedSearchPropSpec.SearchCond);
                            tCell.Controls.Add(ltlCondDescription);
                        }

                        tRow.Cells.Add(tCell);

                        tCell = new TableCell();
                        tCell.Width = Unit.Percentage(30);
                        tCell.CssClass = "tdValue";

                        generateControlByType(fixedSearchPropSpec, ref tCell, i);
                        tRow.Cells.Add(tCell);

                        if (i % 2 != 0 || i == _scParam.ListOfAdditionalReportInput.Count - 1)
                        {
                            _tbl.Rows.Add(tRow);
                            tRow = new TableRow();
                        }
                    }
                }

                phAdditonalInputReport.Controls.Add(_tbl);
                upAdditonalInputReport.Update();

                UIDataHelper helper = new UIDataHelper();
                helper.WriteMultilangText(this, Page.LanguageCode);
            }
        }

        //untuk me-generate control sesuai tipe yang didefinisikan
        private void generateControlByType(FixedSearchPropSpec fixedSearchPropSpec, ref TableCell cell, int itemIndex)
        {
            if (fixedSearchPropSpec.ValueOptionType == SearchPropSpec.ValOptType.Default)
            {
                #region TEXT BOX
                if (fixedSearchPropSpec.PropType == typeof(String))
                {
                    TextBox txtInput = new TextBox();


                    txtInput.ID = "txt" + fixedSearchPropSpec.PropName + "_" + fixedSearchPropSpec.Text.Replace(" ", "").Replace("_", "");

                    if (fixedSearchPropSpec.IsNotes)
                    {
                        txtInput.CssClass = "txtNotesStyle";
                        txtInput.TextMode = TextBoxMode.MultiLine;
                    }
                    else
                    {
                        txtInput.CssClass = "txtLongInputStyle";
                    }

                    if (fixedSearchPropSpec.DefaultValue != null)
                    {
                        string defValue = fixedSearchPropSpec.DefaultValue.ToString();
                        txtInput.Text = defValue;
                    }

                    if (fixedSearchPropSpec.isVisible == false)
                    {
                        txtInput.Visible = false;
                    }

                    cell.Controls.Add(txtInput);

                    if (fixedSearchPropSpec.IsRequired)
                    {
                        Label lblRequiredMark = new Label();
                        lblRequiredMark.Text = "*";
                        lblRequiredMark.CssClass = "mandatoryStyle";
                        cell.Controls.Add(lblRequiredMark);

                        RequiredFieldValidator rfvString = new RequiredFieldValidator();
                        rfvString.ErrorMessage = "This field is required";
                        rfvString.ControlToValidate = txtInput.ID;
                        rfvString.InitialValue = "";
                        rfvString.Display = ValidatorDisplay.Dynamic;
                        rfvString.SetFocusOnError = true;
                        rfvString.ForeColor = System.Drawing.Color.Red;
                        cell.Controls.Add(rfvString);
                    }
                }
                #endregion

                #region DDL YES/NO
                else if (fixedSearchPropSpec.PropType == typeof(Boolean))
                {
                    DropDownList ddl = new DropDownList();

                    if (fixedSearchPropSpec.AdditionalSelectionType == UCReference.AdditionalSelectionType.SelectOne || fixedSearchPropSpec.IsRequired)
                        ddl.Items.Add(new ListItem("Select One", ""));
                    else if (fixedSearchPropSpec.AdditionalSelectionType == UCReference.AdditionalSelectionType.All)
                        ddl.Items.Add(new ListItem("All", "All"));

                    ddl.Items.Add(new ListItem("Yes", "1"));
                    ddl.Items.Add(new ListItem("No", "0"));

                    ddl.ID = "ddl" + fixedSearchPropSpec.PropName + "_" + fixedSearchPropSpec.Text.Replace(" ", "").Replace("_", "");
                    ddl.CssClass = "ddlShortStyle";

                    if (fixedSearchPropSpec.DefaultValue != null)
                    {
                        bool defValue = (Boolean)fixedSearchPropSpec.DefaultValue;
                        if (defValue) ddl.SelectedValue = "1";
                        else ddl.SelectedValue = "0";
                    }

                    cell.Controls.Add(ddl);

                    if (fixedSearchPropSpec.IsRequired)
                    {
                        Label lblRequiredMark = new Label();
                        lblRequiredMark.Text = "*";
                        lblRequiredMark.CssClass = "mandatoryStyle";
                        cell.Controls.Add(lblRequiredMark);

                        RequiredFieldValidator rfvString = new RequiredFieldValidator();
                        rfvString.ID = "rfv" + fixedSearchPropSpec.PropName + "_" + fixedSearchPropSpec.Text.Replace(" ", "").Replace("_", "");
                        rfvString.ErrorMessage = "This field is required";
                        rfvString.ControlToValidate = ddl.ID;
                        rfvString.InitialValue = "";
                        rfvString.Display = ValidatorDisplay.Dynamic;
                        rfvString.SetFocusOnError = true;
                        rfvString.ForeColor = System.Drawing.Color.Red;
                        cell.Controls.Add(rfvString);
                    }

                }
                #endregion

                #region UCInputNumber
                else if (fixedSearchPropSpec.PropType == typeof(Decimal) || fixedSearchPropSpec.PropType == typeof(Int16)
                    || fixedSearchPropSpec.PropType == typeof(Int32) || fixedSearchPropSpec.PropType == typeof(Int64))
                {
                    UCInputNumber ucNumber = (UCInputNumber)Page.LoadControl(Page.ResolveUrl(UCINPUTNUMBERPATH));

                    ucNumber.ID = "txt" + fixedSearchPropSpec.PropName + "_" + fixedSearchPropSpec.Text.Replace(" ", "").Replace("_", "");

                    if (fixedSearchPropSpec.PropType == typeof(Int32) || fixedSearchPropSpec.PropType == typeof(Int64)) ucNumber.IsViewInteger = true;

                    if (fixedSearchPropSpec.DefaultValue != null)
                    {
                        string defValue = fixedSearchPropSpec.DefaultValue.ToString();
                        ucNumber.Text = defValue;
                    }

                    cell.Controls.Add(ucNumber);

                    if (fixedSearchPropSpec.IsRequired)
                    {
                        ucNumber.IsRequiredField = true;

                        //Label lblRequiredMark = new Label();
                        //lblRequiredMark.Text = "*";
                        //lblRequiredMark.CssClass = "mandatoryStyle";
                        //cell.Controls.Add(lblRequiredMark);

                    }
                }
                #endregion

                #region UCDatePicker
                else if (fixedSearchPropSpec.PropType == typeof(DateTime))
                {
                    UCDatePicker ucDatePicker = (UCDatePicker)Page.LoadControl(Page.ResolveUrl(UCDATEPICKERPATH));

                    ucDatePicker.ID = "txt" + fixedSearchPropSpec.PropName + "_" + fixedSearchPropSpec.Text.Replace(" ", "").Replace("_", "");

                    if (fixedSearchPropSpec.DefaultValue != null)
                    {
                        DateTime defValue = (DateTime)fixedSearchPropSpec.DefaultValue;
                        ucDatePicker.DateValue = defValue;
                    }

                    cell.Controls.Add(ucDatePicker);

                    if (fixedSearchPropSpec.IsRequired)
                    {
                        ucDatePicker.Required = true;

                        //Label lblRequiredMark = new Label();
                        //lblRequiredMark.Text = "*";
                        //lblRequiredMark.CssClass = "mandatoryStyle";
                        //cell.Controls.Add(lblRequiredMark);
                    }
                }
                #endregion
            }


            #region UCReference
            else if (fixedSearchPropSpec.ValueOptionType == SearchPropSpec.ValOptType.Reference)
            {
                UCReference ucReference = (UCReference)Page.LoadControl(Page.ResolveUrl(UCREFERENCEPATH));

                string defValue = "";

                if (fixedSearchPropSpec.DefaultValue != null)
                {
                    defValue = fixedSearchPropSpec.DefaultValue.ToString();
                }

                ucReference.ID = "ddl" + fixedSearchPropSpec.PropName + "_" + fixedSearchPropSpec.Text.Replace(" ", "").Replace("_", "");

                ucReference.BindingObject(fixedSearchPropSpec.Reference_DataSource, fixedSearchPropSpec.Reference_DataTextField,
                        fixedSearchPropSpec.Reference_DataValueField, fixedSearchPropSpec.IsRequired, fixedSearchPropSpec.AdditionalSelectionType, defValue);

                //if (fixedSearchPropSpec.CheckIfEventHandlerExisting())
                if (fixedSearchPropSpec.IsEventHandled)
                {
                    ucReference.AutoPostBack = true;
                    ucReference.SelectedIndexChanged -= new UCReference.SelectedIndexChangedEventHandler(OnSelected);
                    ucReference.SelectedIndexChanged += new UCReference.SelectedIndexChangedEventHandler(OnSelected);

                    PostBackTrigger trigger = new PostBackTrigger();
                    trigger.ControlID = ucReference.ID;
                    upFixedSearch.Triggers.Add(trigger);
                }

                cell.Controls.Add(ucReference);

                //if (fixedSearchPropSpec.IsRequired)
                //{
                //    Label lblRequiredMark = new Label();
                //    lblRequiredMark.Text = "*";
                //    lblRequiredMark.CssClass = "mandatoryStyle";
                //    cell.Controls.Add(lblRequiredMark);
                //}
            }
            #endregion

            #region Look  Up
            else if (fixedSearchPropSpec.ValueOptionType == SearchPropSpec.ValOptType.Lookup)
            {
                //UserControl ucLookup = (UserControl)Page.LoadControl(Page.ResolveUrl(fixedSearchPropSpec.LookupPath));
                //ucLookup.ID = "ucl" + fixedSearchPropSpec.PropName + "_" + fixedSearchPropSpec.Text.Replace(" ", "").Replace("_", "");
                //cell.Controls.Add(ucLookup);
            }
            #endregion
        }

        private void generateReportFeatureControlByType(ReportSearchPropSpec reportSearchPropSpec, ref TableCell cell, int itemIndex)
        {
            if (reportSearchPropSpec.ValueOptionType == SearchPropSpec.ValOptType.Reference)
            {
                UCReference ucReference = (UCReference)Page.LoadControl(Page.ResolveUrl(UCREFERENCEPATH));

                string defValue = "";

                if (reportSearchPropSpec.DefaultValue != null)
                {
                    defValue = reportSearchPropSpec.DefaultValue.ToString();
                }

                ucReference.ID = "ddl" + reportSearchPropSpec.PropName + "_" + reportSearchPropSpec.Text.Replace(" ", "").Replace("_", "");

                ucReference.BindingObject(reportSearchPropSpec.Reference_DataSource, reportSearchPropSpec.Reference_DataTextField,
                        reportSearchPropSpec.Reference_DataValueField, reportSearchPropSpec.IsRequired, reportSearchPropSpec.AdditionalSelectionType, defValue);

                //if (workflowSearchPropSpec.CheckIfEventHandlerExisting())
                if (reportSearchPropSpec.IsEventHandled)
                {
                    ucReference.AutoPostBack = true;
                    ucReference.SelectedIndexChanged -= new UCReference.SelectedIndexChangedEventHandler(OnSelected);
                    ucReference.SelectedIndexChanged += new UCReference.SelectedIndexChangedEventHandler(OnSelected);
                }

                cell.Controls.Add(ucReference);
            }

            #region OTHER CONTROL REMARKED

            //    if (reportSearchPropSpec.PropType == typeof(String))
            //    {
            //        TextBox txtInput = new TextBox();

            //        txtInput.ID = "txt" + reportSearchPropSpec.PropName + "_" + reportSearchPropSpec.Text.Replace(" ", "").Replace("_", "");
            //        txtInput.CssClass = "txtLongInputStyle";
            //        if (reportSearchPropSpec.DefaultValue != null)
            //        {
            //            string defValue = reportSearchPropSpec.DefaultValue.ToString();
            //            txtInput.Text = defValue;
            //        }

            //        cell.Controls.Add(txtInput);

            //        if (reportSearchPropSpec.IsRequired)
            //        {
            //            Label lblRequiredMark = new Label();
            //            lblRequiredMark.Text = "*";
            //            lblRequiredMark.CssClass = "mandatoryStyle";
            //            cell.Controls.Add(lblRequiredMark);

            //            RequiredFieldValidator rfvString = new RequiredFieldValidator();
            //            rfvString.ErrorMessage = "This field is required";
            //            rfvString.ControlToValidate = txtInput.ID;
            //            rfvString.InitialValue = "";
            //            rfvString.Display = ValidatorDisplay.Dynamic;
            //            rfvString.SetFocusOnError = true;
            //            cell.Controls.Add(rfvString);
            //        }
            //    }
            //    else if (reportSearchPropSpec.PropType == typeof(Boolean))
            //    {
            //        DropDownList ddl = new DropDownList();

            //        if (reportSearchPropSpec.AdditionalSelectionType == UCReference.AdditionalSelectionType.SelectOne || reportSearchPropSpec.IsRequired)
            //            ddl.Items.Add(new ListItem("Select One", ""));
            //        else if (reportSearchPropSpec.AdditionalSelectionType == UCReference.AdditionalSelectionType.All)
            //            ddl.Items.Add(new ListItem("All", "All"));

            //        ddl.Items.Add(new ListItem("Yes", "1"));
            //        ddl.Items.Add(new ListItem("No", "0"));

            //        ddl.ID = "ddl" + reportSearchPropSpec.PropName + "_" + reportSearchPropSpec.Text.Replace(" ", "").Replace("_", "");
            //        ddl.CssClass = "ddlShortStyle";

            //        if (reportSearchPropSpec.DefaultValue != null)
            //        {
            //            bool defValue = (Boolean)reportSearchPropSpec.DefaultValue;
            //            if (defValue) ddl.SelectedValue = "1";
            //            else ddl.SelectedValue = "0";
            //        }

            //        cell.Controls.Add(ddl);

            //        if (reportSearchPropSpec.IsRequired)
            //        {
            //            Label lblRequiredMark = new Label();
            //            lblRequiredMark.Text = "*";
            //            lblRequiredMark.CssClass = "mandatoryStyle";
            //            cell.Controls.Add(lblRequiredMark);

            //            RequiredFieldValidator rfvString = new RequiredFieldValidator();
            //            rfvString.ID = "rfv" + reportSearchPropSpec.PropName + "_" + reportSearchPropSpec.Text.Replace(" ", "").Replace("_", "");
            //            rfvString.ErrorMessage = "This field is required";
            //            rfvString.ControlToValidate = ddl.ID;
            //            rfvString.InitialValue = "";
            //            rfvString.Display = ValidatorDisplay.Dynamic;
            //            rfvString.SetFocusOnError = true;
            //            cell.Controls.Add(rfvString);
            //        }

            //    }
            //    else if (reportSearchPropSpec.PropType == typeof(Decimal) || reportSearchPropSpec.PropType == typeof(Int16)
            //        || reportSearchPropSpec.PropType == typeof(Int32) || reportSearchPropSpec.PropType == typeof(Int64))
            //    {
            //        UCInputNumber ucNumber = (UCInputNumber)Page.LoadControl(Page.ResolveUrl(UCINPUTNUMBERPATH));

            //        ucNumber.ID = "txt" + reportSearchPropSpec.PropName + "_" + reportSearchPropSpec.Text.Replace(" ", "").Replace("_", "");

            //        if (reportSearchPropSpec.IsRequired) ucNumber.IsRequiredField = true;
            //        if (reportSearchPropSpec.PropType == typeof(Int32) || reportSearchPropSpec.PropType == typeof(Int64)) ucNumber.IsViewInteger = true;

            //        if (reportSearchPropSpec.DefaultValue != null)
            //        {
            //            string defValue = reportSearchPropSpec.DefaultValue.ToString();
            //            ucNumber.Text = defValue;
            //        }

            //        cell.Controls.Add(ucNumber);
            //    }
            //    else if (reportSearchPropSpec.PropType == typeof(DateTime))
            //    {
            //        UCDatePicker ucDatePicker = (UCDatePicker)Page.LoadControl(Page.ResolveUrl(UCDATEPICKERPATH));

            //        ucDatePicker.ID = "txt" + reportSearchPropSpec.PropName + "_" + reportSearchPropSpec.Text.Replace(" ", "").Replace("_", "");

            //        if (reportSearchPropSpec.IsRequired) ucDatePicker.Required = true;

            //        if (reportSearchPropSpec.DefaultValue != null)
            //        {
            //            DateTime defValue = (DateTime)reportSearchPropSpec.DefaultValue;
            //            ucDatePicker.DateValue = defValue;
            //        }

            //        cell.Controls.Add(ucDatePicker);
            //    }
            //}
            //else 

            //else if (reportSearchPropSpec.ValueOptionType == SearchPropSpec.ValOptType.Lookup)
            //{
            //    UserControl ucLookup = (UserControl)Page.LoadControl(Page.ResolveUrl(reportSearchPropSpec.LookupPath));
            //    ucLookup.ID = "ucl" + reportSearchPropSpec.PropName + "_" + reportSearchPropSpec.Text.Replace(" ", "").Replace("_", "");
            //    cell.Controls.Add(ucLookup);
            //}
            #endregion

        }

        //untuk me-generate label yang memberi keterangan 
        private string generateSearchCondDescriptionId(SearchPropSpec.SearchCondition condition)
        {
            string id = string.Empty;

            if (condition == SearchPropSpec.SearchCondition.eq)
                id = "ltl_Search_EqualText";
            else if (condition == SearchPropSpec.SearchCondition.gt)
                id = "ltl_Search_GreaterThanText";
            else if (condition == SearchPropSpec.SearchCondition.gte)
                id = "ltl_Search_GreaterThanEqualText";
            else if (condition == SearchPropSpec.SearchCondition.lt)
                id = "ltl_Search_LowerThanText";
            else if (condition == SearchPropSpec.SearchCondition.lte)
                id = "ltl_Search_LowerThanEqualText";

            return id;
        }

        //untuk me-generate template Parameters
        private Dictionary<string, string> generateTemplateParams(List<ReportTemplateParam> listOfReportTemplateParam, Dictionary<string, string> textOfMainParams)
        {
            Dictionary<string, string> _templateParams = new Dictionary<string, string>();

            foreach (ReportTemplateParam _reportTemplateParam in _scParam.ListOfReportTemplateParam)
            {
                //string result = string.Format("{0}: {1:0.0} - {2:yyyy}",value1,value2,value3);

                if (_reportTemplateParam.RefMainParams.Count() != 0)
                {
                    List<string> listOfVal = new List<string>();

                    foreach (string _refMainParamId in _reportTemplateParam.RefMainParams)
                    {
                        listOfVal.Add(textOfMainParams.Where(a => a.Key == _refMainParamId).FirstOrDefault().Value);
                    }

                    _templateParams.Add(_reportTemplateParam.Key, string.Format(_reportTemplateParam.FormatText, listOfVal.ToArray()));
                }
                else
                {
                    _templateParams.Add(_reportTemplateParam.Key, _reportTemplateParam.FormatText);
                }
            }

            return _templateParams;
        }

        //untuk me-generate text dari Main Parameters
        private void generateTextMainParameters()
        {
            _textOfMainParams = new Dictionary<string, string>();

            if (phFixedSearch.HasControls())
            {
                foreach (Control _tbl in phFixedSearch.Controls)
                {
                    foreach (Control tblRow in _tbl.Controls)
                    {
                        foreach (Control tblColl in tblRow.Controls)
                        {
                            foreach (Control ctrl in tblColl.Controls)
                            {
                                if (ctrl.GetType() == typeof(TextBox))
                                {
                                    TextBox txt = (TextBox)ctrl;
                                    string[] idSplit = txt.ID.Split('_');
                                    string text = idSplit[1];
                                    string propertyName = idSplit[0].Substring(3, idSplit[0].Length - 3);
                                    FixedSearchPropSpec fixedSearchPropSpec =
                                        _scParam.ListOfReportSearchInput.Find(a => a.PropName == propertyName && a.Text.Replace(" ", "").Replace("_", "") == text);

                                    if (txt.Text.Contains("%") && fixedSearchPropSpec.PropType != typeof(Int64))
                                        _textOfMainParams.Add(propertyName, txt.Text);

                                    else if (txt.Text != null && txt.Text != "" && !txt.Text.Contains("%"))
                                    {
                                        _textOfMainParams.Add(propertyName, txt.Text);
                                    }
                                    else if (txt.Text == string.Empty || txt.Text == "")
                                    {
                                        _textOfMainParams.Add(propertyName, null);
                                    }
                                }
                                else if (ctrl.GetType() == typeof(DropDownList))
                                {
                                    DropDownList ddl = (DropDownList)ctrl;
                                    string[] idSplit = ddl.ID.Split('_');
                                    string text = idSplit[1];
                                    string propertyName = idSplit[0].Substring(3, idSplit[0].Length - 3);
                                    FixedSearchPropSpec fixedSearchPropSpec =
                                        _scParam.ListOfReportSearchInput.Find(a => a.PropName == propertyName && a.Text.Replace(" ", "").Replace("_", "") == text);

                                    if (ddl.SelectedValue == "All")
                                    {
                                        _textOfMainParams.Add(propertyName, "All");
                                    }
                                    else if (ddl.SelectedValue != "")
                                    {
                                        _textOfMainParams.Add(propertyName, ddl.SelectedItem.ToString());
                                    }
                                }
                                else if (ctrl.GetType().ToString().ToLower().Contains("ucinputnumber"))
                                {
                                    UCInputNumber txt = (UCInputNumber)ctrl;
                                    string[] idSplit = txt.ID.Split('_');
                                    string propertyName = idSplit[0].Substring(3, idSplit[0].Length - 3);
                                    string text = idSplit[1];
                                    FixedSearchPropSpec fixedSearchPropSpec =
                                        _scParam.ListOfReportSearchInput.Find(a => a.PropName == propertyName && a.Text.Replace(" ", "").Replace("_", "") == text);

                                    if (txt.Text.Contains("%") && fixedSearchPropSpec.PropType != typeof(Int64))
                                        _textOfMainParams.Add(propertyName, txt.Text);

                                    else if (txt.Text != null && (txt.Text != "0" || txt.Text != "0.00") && !txt.Text.Contains("%"))
                                    {
                                        _textOfMainParams.Add(propertyName, txt.Text);
                                    }
                                }
                                else if (ctrl.GetType().ToString().ToLower().Contains("ucdatepicker"))
                                {
                                    UCDatePicker txt = (UCDatePicker)ctrl;
                                    string[] idSplit = txt.ID.Split('_');
                                    string text = idSplit[1];
                                    string propertyName = idSplit[0].Substring(3, idSplit[0].Length - 3);
                                    FixedSearchPropSpec fixedSearchPropSpec =
                                        _scParam.ListOfReportSearchInput.Find(a => a.PropName == propertyName && a.Text.Replace(" ", "").Replace("_", "") == text);

                                    if (txt.Text.Contains("%"))
                                        _textOfMainParams.Add(propertyName, txt.Text);
                                    else if (txt.Text != null && txt.Text != "")
                                    {
                                        _textOfMainParams.Add(propertyName, DtFormatter.FormatToString(txt.DateValue));
                                    }
                                }
                                else if (ctrl.GetType().ToString().ToLower().Contains("ucreference"))
                                {
                                    UCReference txt = (UCReference)ctrl;
                                    string[] idSplit = txt.ID.Split('_');
                                    string text = idSplit[1];
                                    string propertyName = idSplit[0].Substring(3, idSplit[0].Length - 3);
                                    FixedSearchPropSpec fixedSearchPropSpec =
                                        _scParam.ListOfReportSearchInput.Find(a => a.PropName == propertyName && a.Text.Replace(" ", "").Replace("_", "") == text);

                                    object value = new object();

                                    if (txt.SelectedValue != ALL || txt.SelectedValue != "")
                                        value = txt.SelectedText;

                                    if (txt.SelectedValue == ALL)
                                    {
                                        _textOfMainParams.Add(propertyName, "All");
                                    }
                                    else if (txt.SelectedValue != null && txt.SelectedValue != "" && txt.SelectedValue != ALL)
                                    {
                                        _textOfMainParams.Add(propertyName, value.ToString());
                                    }
                                }
                                else if (ctrl.GetType().ToString().ToLower().Contains("uclookup"))
                                {
                                    //FixedSearchPropSpec fixedSearchPropSpec = _scParam.FixedSearch.Find(a => a.PropName == propertyName);
                                    //string propertyName = txt.ID.Substring(3, txt.ID.Length - 3);
                                    //Convert.ChangeType(ctrl, 
                                    //UCLookupCust txt = (UCReference)ctrl;
                                    //string text = idSplit[1];
                                    //string propertyName = idSplit[0].Substring(3, idSplit[0].Length - 3);
                                    //FixedSearchPropSpec fixedSearchPropSpec = _scParam.FixedSearch.Find(a => a.PropName == propertyName && a.Text.Replace(" ", "") == text);

                                    //if (txt.SelectedText.Contains("%"))
                                    //    _criteria.Add(Restrictions.Like(propertyName, txt.SelectedValue));
                                    //else if (txt.SelectedText != null && txt.SelectedText != "")
                                    //    _criteria.Add(Restrictions.Eq(propertyName, txt.SelectedValue));
                                }
                            }
                        }
                    }
                }

            }
        }

        //untuk me-generate text dari Additional Parameters
        private void generateTextAdditionalInput()
        {
            _textOfAdditionalInput = new Dictionary<string, string>();

            if (phAdditonalInputReport.HasControls())
            {
                foreach (Control _tbl in phAdditonalInputReport.Controls)
                {
                    foreach (Control tblRow in _tbl.Controls)
                    {
                        foreach (Control tblColl in tblRow.Controls)
                        {
                            foreach (Control ctrl in tblColl.Controls)
                            {
                                if (ctrl.GetType() == typeof(TextBox))
                                {
                                    TextBox txt = (TextBox)ctrl;
                                    string[] idSplit = txt.ID.Split('_');
                                    string text = idSplit[1];
                                    string propertyName = idSplit[0].Substring(3, idSplit[0].Length - 3);
                                    FixedSearchPropSpec fixedSearchPropSpec =
                                        _scParam.ListOfAdditionalReportInput.Find(a => a.PropName == propertyName && a.Text.Replace(" ", "").Replace("_", "") == text);

                                    if (txt.Text.Contains("%") && fixedSearchPropSpec.PropType != typeof(Int64))
                                        _textOfAdditionalInput.Add(propertyName, txt.Text);

                                    else if (txt.Text != null && txt.Text != "" && !txt.Text.Contains("%"))
                                    {
                                        _textOfAdditionalInput.Add(propertyName, txt.Text);
                                    }
                                    else if (txt.Text == string.Empty || txt.Text == "")
                                    {
                                        _textOfAdditionalInput.Add(propertyName, null);
                                    }
                                }
                                else if (ctrl.GetType() == typeof(DropDownList))
                                {
                                    DropDownList ddl = (DropDownList)ctrl;
                                    string[] idSplit = ddl.ID.Split('_');
                                    string text = idSplit[1];
                                    string propertyName = idSplit[0].Substring(3, idSplit[0].Length - 3);
                                    FixedSearchPropSpec fixedSearchPropSpec =
                                        _scParam.ListOfAdditionalReportInput.Find(a => a.PropName == propertyName && a.Text.Replace(" ", "").Replace("_", "") == text);

                                    if (ddl.SelectedValue == "All")
                                    {
                                        _textOfAdditionalInput.Add(propertyName, "All");
                                    }
                                    else if (ddl.SelectedValue != "")
                                    {
                                        _textOfAdditionalInput.Add(propertyName, ddl.SelectedItem.ToString());
                                    }
                                }
                                else if (ctrl.GetType().ToString().ToLower().Contains("ucinputnumber"))
                                {
                                    UCInputNumber txt = (UCInputNumber)ctrl;
                                    string[] idSplit = txt.ID.Split('_');
                                    string propertyName = idSplit[0].Substring(3, idSplit[0].Length - 3);
                                    string text = idSplit[1];
                                    FixedSearchPropSpec fixedSearchPropSpec =
                                        _scParam.ListOfAdditionalReportInput.Find(a => a.PropName == propertyName && a.Text.Replace(" ", "").Replace("_", "") == text);

                                    if (txt.Text.Contains("%") && fixedSearchPropSpec.PropType != typeof(Int64))
                                        _textOfAdditionalInput.Add(propertyName, txt.Text);

                                    else if (txt.Text != null && (txt.Text != "0" || txt.Text != "0.00") && !txt.Text.Contains("%"))
                                    {
                                        _textOfAdditionalInput.Add(propertyName, txt.Text);
                                    }
                                }
                                else if (ctrl.GetType().ToString().ToLower().Contains("ucdatepicker"))
                                {
                                    UCDatePicker txt = (UCDatePicker)ctrl;
                                    string[] idSplit = txt.ID.Split('_');
                                    string text = idSplit[1];
                                    string propertyName = idSplit[0].Substring(3, idSplit[0].Length - 3);
                                    FixedSearchPropSpec fixedSearchPropSpec =
                                        _scParam.ListOfAdditionalReportInput.Find(a => a.PropName == propertyName && a.Text.Replace(" ", "").Replace("_", "") == text);

                                    if (txt.Text.Contains("%"))
                                        _textOfAdditionalInput.Add(propertyName, txt.Text);
                                    else if (txt.Text != null && txt.Text != "")
                                    {
                                        _textOfAdditionalInput.Add(propertyName, DtFormatter.FormatToString(txt.DateValue));
                                    }
                                }
                                else if (ctrl.GetType().ToString().ToLower().Contains("ucreference"))
                                {
                                    UCReference txt = (UCReference)ctrl;
                                    string[] idSplit = txt.ID.Split('_');
                                    string text = idSplit[1];
                                    string propertyName = idSplit[0].Substring(3, idSplit[0].Length - 3);
                                    FixedSearchPropSpec fixedSearchPropSpec =
                                        _scParam.ListOfAdditionalReportInput.Find(a => a.PropName == propertyName && a.Text.Replace(" ", "").Replace("_", "") == text);

                                    object value = new object();

                                    if (txt.SelectedValue != ALL || txt.SelectedValue != "")
                                        value = txt.SelectedText;

                                    if (txt.SelectedValue == ALL)
                                    {
                                        _textOfAdditionalInput.Add(propertyName, "All");
                                    }
                                    else if (txt.SelectedValue != null && txt.SelectedValue != "" && txt.SelectedValue != ALL)
                                    {
                                        _textOfAdditionalInput.Add(propertyName, value.ToString());
                                    }
                                }
                                else if (ctrl.GetType().ToString().ToLower().Contains("uclookup"))
                                {
                                    //FixedSearchPropSpec fixedSearchPropSpec = _scParam.FixedSearch.Find(a => a.PropName == propertyName);
                                    //string propertyName = txt.ID.Substring(3, txt.ID.Length - 3);
                                    //Convert.ChangeType(ctrl, 
                                    //UCLookupCust txt = (UCReference)ctrl;
                                    //string text = idSplit[1];
                                    //string propertyName = idSplit[0].Substring(3, idSplit[0].Length - 3);
                                    //FixedSearchPropSpec fixedSearchPropSpec = _scParam.FixedSearch.Find(a => a.PropName == propertyName && a.Text.Replace(" ", "") == text);

                                    //if (txt.SelectedText.Contains("%"))
                                    //    _criteria.Add(Restrictions.Like(propertyName, txt.SelectedValue));
                                    //else if (txt.SelectedText != null && txt.SelectedText != "")
                                    //    _criteria.Add(Restrictions.Eq(propertyName, txt.SelectedValue));
                                }
                            }
                        }
                    }
                }

            }
        }

        //untuk me-generate Sub Report Parameters
        private List<confReportInfo> generateSubReportParams()
        {
            List<confReportInfo> listOfRptInfoSubs = new List<confReportInfo>();
            RptListService rptListSvc = (RptListService)UnityFactory.Resolve<RptListService>();
            Dictionary<string, object> subParams = new Dictionary<string, object>();

            foreach (SubReportControlParams _subReportTemplateParam in _scParam.ListOfSubReportControlParams)
            {
                string subTmpltLocation = @rptListSvc.GetRptList(_subReportTemplateParam.SubRptTmpltCode).RptTmpltPath;

                foreach (string key in _subReportTemplateParam.RefMainParams)
                {
                    object val = _criteria.Where(a => a.Key == key).FirstOrDefault().Value;

                    subParams.Add("key", val);
                }

                listOfRptInfoSubs.Add(new confReportInfo() { FullTmpltLocation = subTmpltLocation, expFormat = getExportFormat(), DataProviderParameters = subParams });
            }

            return listOfRptInfoSubs;
        }

        private ExportFormat getExportFormat()
        {
            ExportFormat exportFormat = ExportFormat.PDF;

            if (phReportFeatureSearch.HasControls())
            {
                foreach (Control _tbl in phReportFeatureSearch.Controls)
                {
                    foreach (Control tblRow in _tbl.Controls)
                    {
                        foreach (Control tblColl in tblRow.Controls)
                        {
                            foreach (Control ctrl in tblColl.Controls)
                            {
                                if (ctrl.GetType().ToString().ToLower().Contains("ucreference"))
                                {
                                    UCReference txt = (UCReference)ctrl;
                                    string[] idSplit = txt.ID.Split('_');
                                    string text = idSplit[1];
                                    string propertyName = idSplit[0].Substring(3, idSplit[0].Length - 3);

                                    if (txt.SelectedValue == RptList.RPT_FILE_TYPE_PDF)
                                        exportFormat = ExportFormat.PDF;

                                    else if (txt.SelectedValue == RptList.RPT_FILE_TYPE_DOC)
                                        exportFormat = ExportFormat.DOC;

                                    //else if (txt.SelectedValue == RptList.RPT_FILE_TYPE_HTML)
                                    //    exportFormat = ExportFormat.HTML;

                                    else if (txt.SelectedValue == RptList.RPT_FILE_TYPE_TIFF)
                                        exportFormat = ExportFormat.TIFF;

                                    else if (txt.SelectedValue == RptList.RPT_FILE_TYPE_XLS)
                                        exportFormat = ExportFormat.XLS;

                                }
                            }
                        }
                    }
                }
            }

            return exportFormat;
        }
        #endregion

        public void OnSelected(object sender, EventArgs e)
        {
            UCReference ucRef = (UCReference)((DropDownList)sender).Parent;
            string[] idArray = ucRef.ID.Split('_');

            FixedSearchPropSpec fixedSearchPropSpec = _scParam.ListOfReportSearchInput.Where(a => "ddl" + a.PropName == idArray[0] && a.Text.Replace(" ", "").Replace("_", "") == idArray[1]).FirstOrDefault();

            fixedSearchPropSpec.SelectedText = ucRef.SelectedText;
            fixedSearchPropSpec.SelectedValue = ucRef.SelectedValue;

            fixedSearchPropSpec.SetEventHandlerBroadcaster(fixedSearchPropSpec);

            ScriptManager.RegisterStartupScript(upFixedSearch, upFixedSearch.GetType(), "postBackDonk", "__doPostBack('" + upFixedSearch.ClientID + "', '');", true);
        }

        private void Merge<TKey, TValue>(IDictionary<TKey, TValue> first, IDictionary<TKey, TValue> second)
        {
            if (first != null && second != null)
                foreach (var item in second)
                    if (!first.ContainsKey(item.Key))
                        first.Add(item.Key, item.Value);
        }
    }
}