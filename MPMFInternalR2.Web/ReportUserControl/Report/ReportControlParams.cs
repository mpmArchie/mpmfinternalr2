﻿using Confins.Web.WebUserControl.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MPMFInternalR2.Web.ReportUserControl.Report
{
    [Serializable]
    public class ReportControlParams
    {
        public List<FixedSearchPropSpec> ListOfReportSearchInput;
        public List<FixedSearchPropSpec> ListOfAdditionalReportInput;
        public List<ReportTemplateParam> ListOfReportTemplateParam;
        public List<SubReportControlParams> ListOfSubReportControlParams;
        public List<ReportSearchPropSpec> ListOfExportFeatureSearchInput;

        public ReportControlParams()
        {
            ListOfReportSearchInput = new List<FixedSearchPropSpec>();
            ListOfReportTemplateParam = new List<ReportTemplateParam>();
            ListOfSubReportControlParams = new List<SubReportControlParams>();
            ListOfAdditionalReportInput = new List<FixedSearchPropSpec>();
            ListOfExportFeatureSearchInput = new List<ReportSearchPropSpec>();
        }

        public ReportControlParams AddExportFeatureSearchInput(ReportSearchPropSpec[] reportSearchPropSpecs)
        {
            foreach (ReportSearchPropSpec reportSearchPropSpec in reportSearchPropSpecs)
            {
                this.ListOfExportFeatureSearchInput.Add(reportSearchPropSpec);
            }
            return this;
        }

        public ReportControlParams AddReportSearchInputParams(params FixedSearchPropSpec[] ReportSearchInputParams)
        {
            foreach (FixedSearchPropSpec reportSearchInputParam in ReportSearchInputParams)
            {
                this.ListOfReportSearchInput.Add(reportSearchInputParam);
            }
            return this;
        }

        //untuk menambahkan additional report input
        public ReportControlParams AddAdditionalReportInputParams(params FixedSearchPropSpec[] ReportSearchInputParams)
        {
            foreach (FixedSearchPropSpec reportSearchInputParam in ReportSearchInputParams)
            {
                this.ListOfAdditionalReportInput.Add(reportSearchInputParam);
            }
            return this;
        }

        public ReportControlParams AddReportTemplateParams(params ReportTemplateParam[] ReportTemplateParams)
        {
            foreach (ReportTemplateParam reportTemplateParam in ReportTemplateParams)
            {
                this.ListOfReportTemplateParam.Add(reportTemplateParam);
            }

            return this;
        }

        public ReportControlParams AddSubReportSearchInputParams(params SubReportControlParams[] SubReportSearchInputParams)
        {
            foreach (SubReportControlParams subReportSearchInputParam in SubReportSearchInputParams)
            {
                this.ListOfSubReportControlParams.Add(subReportSearchInputParam);
            }

            return this;
        }

        public FixedSearchPropSpec GetFixedSearchPropSpec(string text, string propName)
        {
            FixedSearchPropSpec fxSearchPropSpec = null;

            foreach (FixedSearchPropSpec item in ListOfReportSearchInput)
            {
                if (item.PropName == propName && item.Text == text)
                {
                    fxSearchPropSpec = item;
                    break;
                }
            }

            return fxSearchPropSpec;
        }
    }
}