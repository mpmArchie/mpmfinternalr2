﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MPMFInternalR2.Web.ReportUserControl.Report
{
    [Serializable]
    public class ReportTemplateParam
    {
        public string Key { get { return _key; } }
        private string _key { get; set; }

        public string FormatText { get { return _formatText; } }
        private string _formatText { get; set; }

        public string[] RefMainParams { get { return _refMainParams; } }
        private string[] _refMainParams { get; set; }

        public ReportTemplateParam(string Key, string FormatText, params string[] RefMainParams)
        {
            this._key = Key;
            this._formatText = FormatText;
            this._refMainParams = RefMainParams;
        }
    }
}