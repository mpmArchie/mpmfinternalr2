﻿using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MPMFInternalR2.Web.ReportUserControl.Report
{
    [Serializable]
    public class ReportSearchPropSpec : SearchPropSpec
    {
        #region "PROPERTIES"
        public bool IsRequired { get; set; }
        public bool IsShowSearchCondLabel { get; set; }
        public bool IsEventHandled { get; set; }
        public string SelectedText { get; set; }
        public string SelectedValue { get; set; }
        public bool isVisible { get; set; }
        public UCReference.AdditionalSelectionType AdditionalSelectionType { get; set; }

        [Serializable]
        public delegate void SelectedIndexChangedEventHandler(FixedSearchPropSpec fixedSearchPropSpec);

        [field: NonSerialized]
        public event SelectedIndexChangedEventHandler SelectedIndexChangedBroadcaster;

        public bool IsSelectedIndexChangedEnabled { get; set; }
        #endregion

        #region "CONSTRUCTOR"
        //common constructor
        //public ReportSearchPropSpec(string Text, string PropName, Type Type, bool IsRequired = false, SearchCondition SearchCondition = SearchCondition.eq, bool isShowSearchCondLabel = false, ValOptType ValOptType = ValOptType.Default)
        //{
        //    this.Text = Text;
        //    this.PropName = PropName;
        //    this.PropType = Type;
        //    this.IsRequired = IsRequired;
        //    this.SearchCond = SearchCondition;
        //    this.IsShowSearchCondLabel = isShowSearchCondLabel;
        //    this.ValueOptionType = SearchPropSpec.ValOptType.Default;
        //    this.isVisible = true;
        //}

        //constructor for Text Box
        //public ReportSearchPropSpec(string Text, string PropName, Type Type) : this(Text, PropName, Type, false) { }
        //public ReportSearchPropSpec(string Text, string PropName) : this(Text, PropName, typeof(string), false) { }

        //constructor for Reference Type  
        public ReportSearchPropSpec(string Text, string PropName, IList Reference_DataSource,
            string Reference_DataTextField, string Reference_DataValueField, Type Type,
            bool IsRequired = false, UCReference.AdditionalSelectionType addSelectionType = UCReference.AdditionalSelectionType.None,
            SearchCondition SearchCondition = SearchCondition.eq, object DefaultValue = null, bool isShowSearchCondLabel = false,
            ValOptType ValOptType = ValOptType.Reference)
        {
            this.Text = Text;
            this.PropName = PropName;
            this.IsRequired = IsRequired;
            this.PropType = Type;
            this.SearchCond = SearchCondition;
            this.Reference_DataSource = Reference_DataSource;
            this.Reference_DataTextField = Reference_DataTextField;
            this.Reference_DataValueField = Reference_DataValueField;
            this.ValueOptionType = SearchPropSpec.ValOptType.Reference;
            this.AdditionalSelectionType = addSelectionType;
            this.IsShowSearchCondLabel = isShowSearchCondLabel;
        }

        //constructor for LookupType
        //public ReportSearchPropSpec(string Text, string PropName, Type Type, object DefaultValue, bool IsRequired, string LookupPath, ValOptType ValOptType = ValOptType.Lookup)
        //{
        //    this.Text = Text;
        //    this.PropName = PropName;
        //    this.PropType = Type;
        //    this.IsRequired = IsRequired;
        //    this.LookupPath = LookupPath;
        //    this.ValueOptionType = SearchPropSpec.ValOptType.Lookup;
        //}
        #endregion

        public void SetSelectedIndexChangedEventHandler(SelectedIndexChangedEventHandler selIdxEventHandler)
        {
            this.SelectedIndexChangedBroadcaster = null;
            //this.SelectedIndexChangedBroadcaster -= new SelectedIndexChangedEventHandler(selIdxEventHandler);
            this.SelectedIndexChangedBroadcaster += new SelectedIndexChangedEventHandler(selIdxEventHandler);
            IsEventHandled = true;
        }
        public bool CheckIfEventHandlerExisting()
        {
            bool IsExisted = false;

            if (SelectedIndexChangedBroadcaster != null) IsExisted = true;

            return IsExisted;
        }
        public void SetEventHandlerBroadcaster(FixedSearchPropSpec ficSearchPropSpec)
        {
            SelectedIndexChangedBroadcaster(ficSearchPropSpec);
        }
    }
}