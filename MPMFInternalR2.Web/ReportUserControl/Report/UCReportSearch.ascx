﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCReportSearch.ascx.cs"
    Inherits="RefCommon.UserControl.Report.UCReportSearch" %>

<%@ Register Src="~/WebUserControl/UCSubSectionContainer.ascx" TagName="UCSubSectionContainer"
    TagPrefix="uc1" %>

<%--<div class="subSection">
    <asp:Label runat="server" ID="subSectionID" Text="Report Parameters"></asp:Label>
</div>--%>
<uc1:UCSubSectionContainer ID="UCSubSectionContainer1" toggleID="minSearchForm" affectedID="dSearchForm"
    runat="server" subSectionTitle="Report Parameters" />
<div id="dSearchForm">
    <asp:UpdatePanel runat="server" ID="upFixedSearch" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <asp:PlaceHolder runat="server" ID="phFixedSearch"></asp:PlaceHolder>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel runat="server" ID="upReportFeatureSearch" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <asp:PlaceHolder runat="server" ID="phReportFeatureSearch"></asp:PlaceHolder>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

<asp:UpdatePanel runat="server" ID="upAdditonalInputReport" UpdateMode="Conditional" ChildrenAsTriggers="false">
    <ContentTemplate>
        <div runat="server" id="dAddInfo" visible="false">
            <uc1:UCSubSectionContainer ID="ucToogleAddInfo" toggleID="minAddInfo" affectedID="dAddInfoUC"
                runat="server" subSectionTitle="Template Parameters" />
            <div id="dAddInfoUC">
                <asp:PlaceHolder runat="server" ID="phAdditonalInputReport"></asp:PlaceHolder>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<br />
<table class="formTable">
    <tr>
        <td align="right">
            <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                <ContentTemplate>
                    <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                        OnClick="SyncOnClick"></asp:LinkButton>
                    <asp:LinkButton runat="server" ID="lb_Print_Async" CssClass="btnForm" Text="Print Asynchronously"
                        OnClick="AsyncOnClick"></asp:LinkButton>
                    <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                        CausesValidation="false"></asp:LinkButton>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="lb_Print_Sync" />
                    <asp:PostBackTrigger ControlID="lb_Print_Async" />
                </Triggers>
            </asp:UpdatePanel>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="lbErrorMessage" runat="server" BackColor="Red" />
        </td>
    </tr>
</table>

