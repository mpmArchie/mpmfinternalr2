﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdIns.Service;
using Confins.BusinessService.Common.Org;
using Confins.BusinessService.RefCommon.Setting;
using Confins.DataModel.RefCommonModel;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using MPMFInternalR2.Web.ReportUserControl.Report;

namespace MPMFInternalR2.Web.ReportUserControl.Report
{
    public partial class ReportTesting : WebFormBase
    {
        #region PAGECONTROL
        protected global::MPMFInternalR2.Web.ReportUserControl.Report.UCReportSearch ucReportSearch;
        #endregion
        #region PROPERTIES
        private ReportControlParams scParam
        {
            get { return (ReportControlParams)ViewState["scParam"]; }
            set { ViewState["scParam"] = value; }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                scParam = new ReportControlParams();
                string mainTmpltCode = RptList.RPT_PDC_BOUNCE_CODE;
                //inisialisasi Field-Field yang akan di tampilkan di ASPX, format sama seperti penggunaan UCSearch
                //parameter di SP
                #region "INIT FIXED SEARCH VALUE"
                scParam.AddReportSearchInputParams(new FixedSearchPropSpec[] { 
            new FixedSearchPropSpec("ltl_RefOffice_OfficeName_Search","RefOfficeId",fddlArea(),"OfficeName","RefOfficeId",typeof(Int64), false,UCReference.AdditionalSelectionType.All),
            new FixedSearchPropSpec("ltl_OfficeBankAcc_OfficeBankAccId_Search","OfficeBankAccId",fddlBank(),"Key","Value",typeof(Int64),  false,UCReference.AdditionalSelectionType.All),
            new FixedSearchPropSpec("ltl_Pdc_BounceDtGte_Search", "PeriodFrom", typeof(DateTime), true), 
            new FixedSearchPropSpec("ltl_Pdc_BounceDtLte_Search", "PeriodTo", typeof(DateTime),true)

            });

                scParam.AddAdditionalReportInputParams(new FixedSearchPropSpec[] { 
             new FixedSearchPropSpec("ltl_RefOffice_OfficeName2_Search","OfficeName",fddlArea(),"OfficeName","RefOfficeId",typeof(Int64), false,UCReference.AdditionalSelectionType.All),
             new FixedSearchPropSpec("ltl_Pdc_AdminFeeAmt_Search", "AdminFeeAmt", typeof(Decimal),true), 
             new FixedSearchPropSpec("ltl_Pdc_RequestDt_Search", "RequestDt", typeof(DateTime),true), 
             new FixedSearchPropSpec("ltl_Pdc_No_Search", "PDCNo", typeof(string),true),
              new FixedSearchPropSpec("ltl_Pdc_Notes_Search", "Notes", typeof(string), IsNotes:true)
            });
                #endregion
                //parameter di rdlc
                #region "INIT TEMPLATE REPORT VALUE"
                string filter = "Office Area : {0} And Deposit To Bank Account  : {1}  And Bounce date >= {2}   And Bounce date <= {3}";

                scParam.AddReportTemplateParams(new ReportTemplateParam[]{
                    new ReportTemplateParam("CoyName", this.GetCompanyName()),
                    new ReportTemplateParam("OfficeName", CurrentUserContext.OfficeName),
                    new ReportTemplateParam("PeriodFrom","{0}", new string[] {"PeriodFrom"}),
                    new ReportTemplateParam("PeriodTo","{0}", new string[] {"PeriodTo"}),
                    new ReportTemplateParam("FilterBy", filter,new string[] {"RefOfficeId","OfficeBankAccId", "PeriodFrom", "PeriodTo"} ),
                    new ReportTemplateParam("UserName",CurrentUserContext.FullName),
                    new ReportTemplateParam("SystemDate", DtFormatter.FormatToString(BusinessDate))
                });
                #endregion

                ucReportSearch.SetAttribute(scParam, mainTmpltCode);

                UIDataHelper helper = new UIDataHelper();
                helper.WriteMultilangText(this, base.LanguageCode);
            }
            FixedSearchPropSpec refOffice = scParam.GetFixedSearchPropSpec("ltl_RefOffice_OfficeName_Search", "RefOfficeId");
            refOffice.SetSelectedIndexChangedEventHandler(ReferenceSelectedChanged);
        }

        #region FUNCTION
        private IList fddlArea()
        {
            if (CurrentUserContext.IsHeadOffice)
            {
                MFOfficeService ros = UnityFactory.Resolve<MFOfficeService>() as MFOfficeService;

                IList listOfArea = ros.GetAllRefOffice();
                return listOfArea;
            }
            else
            {
                MFOfficeService ros = UnityFactory.Resolve<MFOfficeService>() as MFOfficeService;

                IList listOfArea = ros.GetRefOfficeById(CurrentUserContext.RefOfficeId);
                return listOfArea;
            }


        }
        private IList fddlBank()
        {
            OfficeBankAccService rbs = UnityFactory.Resolve<OfficeBankAccService>() as OfficeBankAccService;
            //IList ListOfOfficeBankAcc = rbs.GetOfficeBankAccKeyValuePair(CurrentUserContext.RefOfficeId);

            if (!CurrentUserContext.IsHeadOffice)
            {
                IList ListOfOfficeBankAcc = rbs.GetOfficeBankAccKeyValuePair(CurrentUserContext.RefOfficeId, "");
                return ListOfOfficeBankAcc;
            }
            else
            {
                IList ListOfOfficeBankAcc = rbs.GetAllOfficeBankAccKeyValuePair();
                return ListOfOfficeBankAcc;
            }


        }
        protected void ReferenceSelectedChanged(FixedSearchPropSpec fixedSearchPropSpec)
        {
            FixedSearchPropSpec ddlBankAcc = scParam.GetFixedSearchPropSpec("ltl_OfficeBankAcc_OfficeBankAccId_Search", "OfficeBankAccId");
            OfficeBankAccService rbs = (OfficeBankAccService)UnityFactory.Resolve<OfficeBankAccService>();

            if (fixedSearchPropSpec.SelectedValue == "All")
            {
                IList listOfBank = rbs.GetAllOfficeBankAccKeyValuePair();
                ddlBankAcc.Reference_DataSource = listOfBank;

            }
            else
            {
                IList listOfBank = rbs.GetAllOfficeBankAccKeyValuePair(Convert.ToInt64(fixedSearchPropSpec.SelectedValue));
                ddlBankAcc.Reference_DataSource = listOfBank;
            }
        }
        #endregion
    }
}