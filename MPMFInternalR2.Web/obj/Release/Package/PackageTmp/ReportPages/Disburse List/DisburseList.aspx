﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DisburseList.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.Disburse_List.DisburseList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
        </asp:ScriptManager>
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="Report/ DisburseList"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">Disburse List </label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />



            <div id="dPDCReceive">
                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">Search</span>
                </div>
                <div id="dSearchForm">
                    <table id="ucReportSearch_tblFixedSearch" class="formTable">
                        <tr>
                            <td class="tdDesc" style="width: 20%;">AP Type</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="cboAptype" runat="server">
                                    <asp:ListItem Value="0">Select One</asp:ListItem>
                                    <asp:ListItem Value="1">All</asp:ListItem>
                                    <asp:ListItem Value="2">Not Write Off</asp:ListItem>
                                    <asp:ListItem Value="3">Insurance</asp:ListItem>
                                    <asp:ListItem Value="4">Commission Supplier Employee and Referantor</asp:ListItem>
                                    <asp:ListItem Value="5">AP Fiducia to Notary</asp:ListItem>
                                    <asp:ListItem Value="6">AP Fiducia to DepKumHam</asp:ListItem>
                                    <asp:ListItem Value="7">Life Insurance</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblwar" runat="server" Font-Names="verdana"
                                    Font-Size="X-Small" ForeColor="Red"
                                    Style="font-family: Verdana, Arial, Tahoma, sans-serif; font-size: 11px"
                                    Visible="False">*) Required Field</asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                            OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
