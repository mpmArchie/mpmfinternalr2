﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RptReportPayment.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.ReportPayment.RptReportPayment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({
                dateFormat: 'dd/mm/yy'
            });
        });
    </script>
</head>
<body>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="Report/Report Payment"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">Payment Report </label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />

            <div id="dPDCReceive">
                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">Search</span>
                </div>
                <div id="dSearchForm">
                    <table id="ucReportSearch_tblFixedSearch" class="formTable">
                        <tr>
                            <td class="tdDesc">Branch</td>
                            <td class="tdValue">
                                <asp:DropDownList ID="ddlbranch" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc">Period Type</td>
                            <td class="tdValue">
                                <asp:DropDownList ID="ddlperiodtype" runat="server" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlperiodtype_SelectedIndexChanged">
                                    <asp:ListItem Value="00">Select One</asp:ListItem>
                                    <asp:ListItem Value="Daily">Daily</asp:ListItem>
                                    <asp:ListItem Value="Monthly">Monthly</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <asp:Panel runat="server" ID="Daily" Visible="False">
                                <td class="tdDesc">Period</td>
                                <td class="tdValue">
                                    <asp:TextBox runat="server" AutoCompleteType="Enabled" ID="date1" CssClass="datepicker"></asp:TextBox>
                                </td>
                                <td class="tdDesc">To</td>
                                <td class="tdValue">
                                    <asp:TextBox runat="server" AutoCompleteType="Enabled" ID="date2" CssClass="datepicker"></asp:TextBox>
                                    <asp:Label ID="lblwardate" runat="server" Font-Names="verdana"
                                        Font-Size="X-Small" ForeColor="Red"
                                        Style="font-family: verdana, Arial, tahoma, san-serif; font-size: 11px;"
                                        Visible="False">* Please cek date awal dan akhir</asp:Label>
                                </td>
                            </asp:Panel>
                            <asp:Panel ID="Monthly" runat="server" Visible="False">
                                <td class="tdDesc">Period</td>
                                <td class="tdValue">
                                    <asp:DropDownList ID="month" runat="server">
                                        <asp:ListItem Value="00">Select One</asp:ListItem>
                                        <asp:ListItem Value="1">Januari</asp:ListItem>
                                        <asp:ListItem Value="2">Februari</asp:ListItem>
                                        <asp:ListItem Value="3">Maret</asp:ListItem>
                                        <asp:ListItem Value="4">April</asp:ListItem>
                                        <asp:ListItem Value="5">Mei</asp:ListItem>
                                        <asp:ListItem Value="6">Juni</asp:ListItem>
                                        <asp:ListItem Value="7">Juli</asp:ListItem>
                                        <asp:ListItem Value="8">Agustus</asp:ListItem>
                                        <asp:ListItem Value="9">September</asp:ListItem>
                                        <asp:ListItem Value="10">Oktober</asp:ListItem>
                                        <asp:ListItem Value="11">November</asp:ListItem>
                                        <asp:ListItem Value="12">Desember</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="year" runat="server">
                                        <asp:ListItem Value="00">Select One</asp:ListItem>
                                        <asp:ListItem>2010</asp:ListItem>
                                        <asp:ListItem>2011</asp:ListItem>
                                        <asp:ListItem>2012</asp:ListItem>
                                        <asp:ListItem>2013</asp:ListItem>
                                        <asp:ListItem>2014</asp:ListItem>
                                        <asp:ListItem>2015</asp:ListItem>
                                        <asp:ListItem>2016</asp:ListItem>
                                        <asp:ListItem>2017</asp:ListItem>
                                        <asp:ListItem>2018</asp:ListItem>
                                        <asp:ListItem>2019</asp:ListItem>
                                        <asp:ListItem>2020</asp:ListItem>
                                        <asp:ListItem>2021</asp:ListItem>
                                        <asp:ListItem>2022</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Label ID="lblwarddlperiod" runat="server" Font-Names="verdana"
                                        Font-Size="X-Small" ForeColor="Red"
                                        Style="font-family: Verdana, Arial, Tahoma, sans-serif; font-size: 11px"
                                        Visible="False">*) Required Field</asp:Label>
                                </td>
                            </asp:Panel>
                        </tr>
                        <tr>
                            <td class="tdDesc">Report Type</td>
                            <td class="tdValue">
                                <asp:DropDownList ID="reporttype" runat="server">
                                    <asp:ListItem Selected="True" Value="Summary">Summary</asp:ListItem>
                                    <asp:ListItem Value="Detail">Detail</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc">Asset Type</td>
                            <td class="tdValue">
                                <asp:DropDownList ID="ddlassettype" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                            OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
