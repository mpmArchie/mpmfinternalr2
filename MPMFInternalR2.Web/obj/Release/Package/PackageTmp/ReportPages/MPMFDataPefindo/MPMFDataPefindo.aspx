﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MPMFDataPefindo.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.MPMFDataPefindo.MPMFDataPefindo" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({
                dateFormat: 'dd/mm/yy'
            });
        });
    </script>

    <script>
        function isnumerickey(e) {

            var txt = window.event.keyCode;
            if (txt > 31 && (txt < 48 || txt > 57)) {
                if (txt == 46) {
                    return true;
                }
                else {
                    return false;
                }
            }
            return true;
        }
    </script>
    </head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    Report Pefindo
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">Report Pefindo</label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />

            <div id="dPDCReceive">
                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">Report Pefindo</span>
                </div>
                <div id="dSearchForm">
                    <table id="ucReportSearch_tblFixedSearch" class="formTable">
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Pefindo Date</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:TextBox runat="server" AutoCompleteType="Enabled" ID="dtStart" CssClass="datepicker"></asp:TextBox>
                                 &nbsp;s/d
                            <%--</td>
                            <td class="tdDesc" style="width: 20%;">s/d</td>--%>
                            <%--<td class="tdValue" style="width: 30%;">--%>
                                <asp:TextBox runat="server" AutoCompleteType="Enabled" ID="dtEnd" CssClass="datepicker"></asp:TextBox>
                                <asp:Label ID="lblwardate" runat="server" Font-Names="verdana"
                                    Font-Size="X-Small" ForeColor="Red"
                                    Style="font-family: verdana, Arial, tahoma, san-serif; font-size: 11px;"
                                    Visible="False">* Please cek date awal dan akhir</asp:Label>
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="tdDesc" style="width: 20%;">NO KTP</td>
                            <td class="tdValue" style="width: 80%;">
                                <asp:TextBox runat="server" ID="txtKtp" onkeypress="return isnumerickey(event);" MaxLength="16"></asp:TextBox>
                                <asp:Label ID="lblwartxtKtp" runat="server" Font-Size="X-Small" ForeColor="Red" Visible="False">*) Required field</asp:Label>
                            </td>
                        </tr>

                         <tr>
                            <td class="tdDesc" style="width: 20%;">Cust No</td>
                            <td class="tdValue" style="width: 80%;">
                                <asp:TextBox runat="server" ID="txtCustId" onkeypress="return isnumerickey(event);" MaxLength="16"></asp:TextBox>
                                <asp:Label ID="lblwartxtCustId" runat="server" Font-Size="X-Small" ForeColor="Red" Visible="False">*) Required field</asp:Label>
                            </td>
                        </tr>

                         <tr>
                            <td class="tdDesc" style="width: 20%;">Cust Name</td>
                            <td class="tdValue" style="width: 80%;">
                                <asp:TextBox runat="server" ID="txtCustName" ></asp:TextBox>
                                <asp:Label ID="lblwartxtCustName" runat="server" Font-Size="X-Small" ForeColor="Red" Visible="False">*) Required field</asp:Label>
                            </td>
                        </tr>
                         
                         <tr>
                            <td class="tdDesc" style="width: 20%;">App No</td>
                            <td class="tdValue" style="width: 80%;">
                                <asp:TextBox runat="server" ID="txtAppNo"></asp:TextBox>
                                <asp:Label ID="Label1" runat="server" Font-Size="X-Small" ForeColor="Red" Visible="False">*) Required field</asp:Label>
                            </td>
                        </tr>

                    </table>
                    <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                            OnClick="lb_Print_Sync_OnClick"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_OnClick"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
