﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VisitFeeLCAssetRecoveryReportView.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.VisitFeeLCAssetRecovery.VisitFeeLCAssetRecoveryReportView" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({
                dateFormat: 'dd/mm/yy'
            });
        });
    </script>
</head>
<body>
   <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False"></asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <rsweb:ReportViewer ID="ReportViewer1" Height="800px" Width="100%" runat="server"></rsweb:ReportViewer>
    </div>
    </form>
</body>
</html>