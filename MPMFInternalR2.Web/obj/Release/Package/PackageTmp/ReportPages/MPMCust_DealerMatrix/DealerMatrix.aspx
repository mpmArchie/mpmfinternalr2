﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DealerMatrix.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.MPMCust_DealerMatrix.DealerMatrix" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <div>
                <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="linkPath">
                            <asp:Label runat="server" ID="lblPath" Text="Report/ReportDealerMatrix"></asp:Label>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="toolbar">
                            <span>
                                <label id="pageTitle">Report Dealer Matrix </label>
                            </span>
                            <span id="toolMenuContainer"></span>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <br />
                <br />
                <br />

                <div id="dPDCReceive">
                    <div class="subSection">
                        <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                        <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">Search</span>
                    </div>
                    <div id="dSearchForm">
                        <table id="ucReportSearch_tblFixedSearch" class="formTable">
                            <tr>
                                <td class="tdDesc" style="width: 20%;">Tipe Matrix
                                </td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:DropDownList ID="ddltipematrix" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblwartipe" Visible="false" ForeColor="Red" Text="*) Required" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdDesc" style="width: 20%;">Tahun
                                </td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:DropDownList ID="ddltahun" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblwartahun" Visible="false" ForeColor="Red" Text="*) Required" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdDesc" style="width: 20%;">Bulan
                                </td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:DropDownList ID="ddlbulan" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblwarbulan" Visible="false" ForeColor="Red" Text="*) Required" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                            OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
