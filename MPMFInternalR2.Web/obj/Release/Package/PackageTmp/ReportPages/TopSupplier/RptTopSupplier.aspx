﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RptTopSupplier.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.TopSupplier.RptTopSupplier" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="Report/ReportTopSupplier"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">Report Top Supplier</label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />

            <div id="dPDCReceive">
                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">Search</span>
                </div>
                <div id="dSearchForm">
                    <table id="ucReportSearch_tblFixedSearch" class="formTable">
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Branch</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlbranch" runat="server" OnSelectedIndexChanged="ddlbranch_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">POS</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlPos" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Periode</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="cboPeriodMonthS" runat="server">
                                    <asp:ListItem Value="1">January</asp:ListItem>
                                    <asp:ListItem Value="2">February</asp:ListItem>
                                    <asp:ListItem Value="3">March</asp:ListItem>
                                    <asp:ListItem Value="4">April</asp:ListItem>
                                    <asp:ListItem Value="5">May</asp:ListItem>
                                    <asp:ListItem Value="6">June</asp:ListItem>
                                    <asp:ListItem Value="7">July</asp:ListItem>
                                    <asp:ListItem Value="8">August</asp:ListItem>
                                    <asp:ListItem Value="9">September</asp:ListItem>
                                    <asp:ListItem Value="10">October</asp:ListItem>
                                    <asp:ListItem Value="11">November</asp:ListItem>
                                    <asp:ListItem Value="12">Desember</asp:ListItem>
                                </asp:DropDownList>&nbsp;&nbsp;&nbsp;
                                <asp:TextBox ID="txtPeriodYearS" Width="56px" runat="server" MaxLength="4" CssClass="inptype"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvPeriodYearS" runat="server" Display="Dynamic" ErrorMessage="Please Insert Year Start!"
                                    ControlToValidate="txtPeriodYearS"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revPeriodYears" runat="server" Display="Dynamic" ErrorMessage="Period Year Start is not Valid!"
                                    ControlToValidate="txtPeriodYearS" ValidationExpression="\d{4}"></asp:RegularExpressionValidator>
                                To
                                <asp:DropDownList ID="cboPeriodMonthE" runat="server">
                                    <asp:ListItem Value="1">January</asp:ListItem>
                                    <asp:ListItem Value="2">February</asp:ListItem>
                                    <asp:ListItem Value="3">March</asp:ListItem>
                                    <asp:ListItem Value="4">April</asp:ListItem>
                                    <asp:ListItem Value="5">May</asp:ListItem>
                                    <asp:ListItem Value="6">June</asp:ListItem>
                                    <asp:ListItem Value="7">July</asp:ListItem>
                                    <asp:ListItem Value="8">August</asp:ListItem>
                                    <asp:ListItem Value="9">September</asp:ListItem>
                                    <asp:ListItem Value="10">October</asp:ListItem>
                                    <asp:ListItem Value="11">November</asp:ListItem>
                                    <asp:ListItem Value="12">Desember</asp:ListItem>
                                </asp:DropDownList>&nbsp;&nbsp;&nbsp;
                                <asp:TextBox ID="txtPeriodYearE" Width="56px" runat="server" MaxLength="4" CssClass="inptype"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvPeriodYearE" runat="server" Display="Dynamic" ErrorMessage="Please Insert Year End !"
                                    ControlToValidate="txtPeriodYearE"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revPeriodYearE" runat="server" Display="Dynamic" ErrorMessage="Period Year End is not Valid!"
                                    ControlToValidate="txtPeriodYearE" ValidationExpression="\d{4}"></asp:RegularExpressionValidator>
                                &nbsp; <font color="#ff0033">*)</font>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Currency</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="cboCurrency" runat="server">
                                    <asp:ListItem Value="IDR">Rupiah</asp:ListItem>
                                    <asp:ListItem Value="USD">USD</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Product Type</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="cboProductType" runat="server">
                                    <asp:ListItem Value="CF">Consumer Finance</asp:ListItem>
                                    <asp:ListItem Value="LS">Leasing Finance</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Select For Top</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:TextBox ID="txtNTop" Width="56px" runat="server" CssClass="inptype"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                            OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
