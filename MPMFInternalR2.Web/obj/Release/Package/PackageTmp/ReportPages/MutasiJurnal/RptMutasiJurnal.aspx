﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RptMutasiJurnal.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.MutasiJurnal.RptMutasiJurnal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="Report/MutasiJurnal"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">Mutasi Jurnal </label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />

            <div id="dPDCReceive">
                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">MUTASI JURNAL</span>
                </div>
                <div id="dSearchForm">
                    <table id="ucReportSearch_tblFixedSearch" class="formTable">
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Branch</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlbranch" runat="server">
                                </asp:DropDownList>
                                <asp:Label ID="lblwarddlbranch" runat="server"
                                    Font-Size="X-Small" ForeColor="Red"
                                    Visible="False">*) Required Field</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Periode</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlBulan" runat="server">
                                    <asp:ListItem Value="00">Bulan</asp:ListItem>
                                    <asp:ListItem Value="01">Januari</asp:ListItem>
                                    <asp:ListItem Value="02">Februari</asp:ListItem>
                                    <asp:ListItem Value="03">Maret</asp:ListItem>
                                    <asp:ListItem Value="04">April</asp:ListItem>
                                    <asp:ListItem Value="05">Mei</asp:ListItem>
                                    <asp:ListItem Value="06">Juni</asp:ListItem>
                                    <asp:ListItem Value="07">Juli</asp:ListItem>
                                    <asp:ListItem Value="08">Agustus</asp:ListItem>
                                    <asp:ListItem Value="09">September</asp:ListItem>
                                    <asp:ListItem Value="10">Oktober</asp:ListItem>
                                    <asp:ListItem Value="11">November</asp:ListItem>
                                    <asp:ListItem Value="12">Desember</asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlTahun" runat="server">
                                    <asp:ListItem Value="00">Tahun</asp:ListItem>
                                    <asp:ListItem Value="2011">2011</asp:ListItem>
                                    <asp:ListItem Value="2012">2012</asp:ListItem>
                                    <asp:ListItem Value="2013">2013</asp:ListItem>
                                    <asp:ListItem Value="2014">2014</asp:ListItem>
                                    <asp:ListItem Value="2015">2015</asp:ListItem>
                                    <asp:ListItem Value="2016">2016</asp:ListItem>
                                    <asp:ListItem Value="2017">2017</asp:ListItem>
                                    <asp:ListItem Value="2018">2018</asp:ListItem>
                                    <asp:ListItem Value="2019">2019</asp:ListItem>
                                    <asp:ListItem Value="2020">2020</asp:ListItem>
                                </asp:DropDownList>

                                <asp:Label ID="lblwarddlbulan" runat="server"
                                    Font-Size="X-Small" ForeColor="Red"
                                    Visible="False">*) Required Field</asp:Label>
                                <asp:Label ID="lblwarddltahun" runat="server"
                                    Font-Size="X-Small" ForeColor="Red"
                                    Visible="False">*) Required Field</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Report</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlReport" runat="server">
                                    <asp:ListItem Value="00">Select One</asp:ListItem>
                                    <asp:ListItem Value="Branch">PerBranch</asp:ListItem>
                                    <asp:ListItem Value="Coa">PerCOA</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblwarddlreport" runat="server"
                                    Font-Size="X-Small" ForeColor="Red"
                                    Visible="False">*) Required Field</asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                            OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
