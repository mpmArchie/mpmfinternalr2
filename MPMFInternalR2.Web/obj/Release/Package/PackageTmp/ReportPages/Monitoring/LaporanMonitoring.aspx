﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LaporanMonitoring.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.Monitoring.LaporanMonitoring" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({
                dateFormat: 'dd/mm/yy'
            });
        });
    </script>
</head>
<body>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="Report/LaporanMonitoring"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">LAPORAN MONITORING</label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />


            <div id="dPDCReceive">
                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">SEARCH</span>
                </div>
                <div id="dSearchForm">
                    <table id="ucReportSearch_tblFixedSearch" class="formTable">
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Pilihan Cabang :</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlPilihan" runat="server" OnSelectedIndexChanged="ddlPilihan_SelectedIndexChanged" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlCabang" runat="server" Visible="False" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlRegional" runat="server" Visible="False" OnSelectedIndexChanged="ddlRegional_SelectedIndexChanged" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:Label ID="lblWarPilih" runat="server" ForeColor="Red" Text="*) Pilihan Cabang wajib dipilih" Visible="False" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Pilihan Laporan :</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlPilihRpt" runat="server" OnSelectedIndexChanged="ddlPilihRpt_SelectedIndexChanged"
                                    AutoPostBack="True">
                                    <asp:ListItem Value="0">Select One...</asp:ListItem>
                                    <asp:ListItem Value="1">Laporan Monitoring Proses Fidusia</asp:ListItem>
                                    <asp:ListItem Value="2">Laporan Monitoring Proses Asuransi</asp:ListItem>
                                    <asp:ListItem Value="3">Laporan Monitoring Tanda Terima Resmi</asp:ListItem>
                                    <asp:ListItem Value="4">Laporan Monitoring Pencetakan Dokumen Kontrak</asp:ListItem>
                                    <asp:ListItem Value="5">Laporan Monitoring Claim Asuransi</asp:ListItem>
                                    <asp:ListItem Value="6">Laporan Monitoring Suspend</asp:ListItem>
                                    <asp:ListItem Value="7">Laporan Monitoring Advance Voucher</asp:ListItem>
                                    <asp:ListItem Value="8">Laporan Monitoring Aplikasi In</asp:ListItem>
                                    <asp:ListItem Value="9">Laporan Monitoring BPKB Borrow</asp:ListItem>
                                    <asp:ListItem Value="10">Laporan Monitoring SP BPKB</asp:ListItem>
                                    <asp:ListItem Value="11">Laporan Monitoring SP Printing</asp:ListItem>
                                    <asp:ListItem Value="12">Laporan Monitoring Pencetakan SP Collection</asp:ListItem>
                                    <asp:ListItem Value="13">Laporan Monitoring Surat Tugas/Kuasa</asp:ListItem>
                                    <asp:ListItem Value="14">Laporan Monitoring STNK Proses</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblWarPilihRpt" runat="server" ForeColor="Red"
                                    Text="*) Pilihan Jenis Laporan wajib dipilih" Visible="False" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;"><asp:Label ID="lblstatustd" runat="server" Text="Pilihan Status :" /></td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlStatus" runat="server" Visible="False" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:Label ID="lblWarPilihStatus" runat="server" ForeColor="Red" Text="*) Pilihan Status wajib dipilih" Visible="False" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Dari Tanggal :</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:TextBox runat="server" AutoCompleteType="Enabled" ID="dtStart" CssClass="datepicker"></asp:TextBox>
                                Sampai Tanggal :
                                <asp:TextBox runat="server" AutoCompleteType="Enabled" ID="dtEnd" CssClass="datepicker"></asp:TextBox>
                                <asp:Label ID="lblwardate" runat="server" Font-Names="verdana"
                                    Font-Size="X-Small" ForeColor="Red"
                                    Style="font-family: verdana, Arial, tahoma, san-serif; font-size: 11px;"
                                    Visible="False">* Please cek date awal dan akhir</asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                            OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <asp:Label ID="lblPilihRpt" runat="server" Font-Names="Calibri" Font-Size="10pt" Visible="False"></asp:Label>
            <asp:Label ID="lblStatus" runat="server" Font-Names="Calibri" Font-Size="10pt" Visible="False"></asp:Label>
            <asp:Label ID="lblStatusID" runat="server" Font-Names="Calibri" Font-Size="10pt" Visible="False"></asp:Label>
            <asp:Label ID="lbltglfrom" runat="server" Font-Names="Calibri" Font-Size="10pt" Visible="false" />
            <asp:Label ID="lbltglto" runat="server" Font-Names="Calibri" Font-Size="10pt" Visible="false" />
            <asp:Label ID="lblpilihan" runat="server" Font-Names="Calibri" Font-Size="10pt" Visible="false" />
            <asp:Label ID="lblpilihn" runat="server" Font-Names="Calibri" Font-Size="10pt" Visible="false" />
            <asp:Label ID="lblreport" runat="server" Font-Names="Calibri" Font-Size="10pt" Visible="false" />
            <asp:Label ID="txtreport" runat="server" Font-Names="Calibri" Font-Size="10pt" Visible="false" />
            <asp:Label ID="lblstatusname" runat="server" Font-Names="Calibri" Font-Size="10pt" Visible="false" />
            <asp:Label ID="lblchoose" runat="server" Font-Names="Calibri" Font-Size="10pt" Visible="false" />
            <asp:Label ID="lblRegional" runat="server" Font-Names="Calibri" Font-Size="10pt" Visible="false" />
        </div>
    </form>
</body>
</html>
