﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RptPrintTabelAmortisasi.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.MPMFCust_PrintTabelAmortisasi.RptPrintTabelAmortisasi" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({
                dateFormat: 'dd/mm/yy'
            });
        });
    </script>
</head>
<body>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">Tabel Amortisasi</label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />
            <div id="dPDCReceive">
                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">Search</span>
                </div>
                <div id="dSearchForm">
                    <table id="ucReportSearch_tblFixedSearch" class="formTable">
                        <tr>
                            <td class="tdDesc">Branch</td>
                            <td class="tdValue">
                                <asp:DropDownList ID="ddlbranch" runat="server">
                                </asp:DropDownList>
                            </td>
                            <asp:Label ID="lblwarddlbranch" runat="server" Font-Names="verdana"
                                Font-Size="X-Small" ForeColor="Red"
                                Style="font-family: Verdana, Arial, Tahoma, sans-serif; font-size: 11px"
                                Visible="False">*) Required Field</asp:Label>
                        </tr>
                        <tr>
                            <td class="tdDesc">Cari</td>
                            <td class="tdValue">
                                <asp:TextBox ID="txtcari" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtcari" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Search TextBox Must be Filled!" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                    <table class="viewsearchtable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_view_Sync" CssClass="btnForm" Text="View Synchronously"
                                            OnClick="lb_view_Sync_Click"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>

                    <br />

                    <asp:UpdatePanel runat="server" ID="upGrid" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div runat="server" id="dGridSection" class="form">
                                <%--<uc1:ucsubsectioncontainer id="ucToggleGrid" toggleid="minGrid" affectedid="dGrid" subsectiontitle="TabelAmortisasi" runat="server" />--%>
                                <div id="dGrid" style="width: 100%">
                                    <%--<uc3:ucgridheader id="ucGridHeader" runat="server" />--%>
                                    <asp:GridView runat="server" ID="gvRefTableAmortisasi" AutoGenerateColumns="False" GridLines="None"
                                        CssClass="mGrid" AlternatingRowStyle-CssClass="alt" ShowHeaderWhenEmpty="True" 
                                        EmptyDataText="No records Found" PageSize="10" AllowPaging="True" OnPageIndexChanging="gvRefTableAmortisasi_PageIndexChanging"
                                        OnRowCommand="gvRefTableAmortisasi_RowCommand">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Agreement NO" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblAgrementNo" Text='<%# Eval("agreementno") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Name" HeaderStyle-Width="100" ItemStyle-Width="300" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblName" Text='<%# Eval("name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Installment Amount" HeaderStyle-Width="100" ItemStyle-Width="200" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblInstallmentAmount" Text='<%# Eval("InstallmentAmount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Currency" HeaderStyle-Width="50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblcurrencyid" Text='<%# Eval(("currencyid")) %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Next Due Date" HeaderStyle-Width="150" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center">
                                               <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblNextInstallmentDueDate" Text='<%# Eval(("NextInstallmentDueDate")) %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Contact Status" HeaderStyle-Width="150" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblcontractstatus" Text='<%# Eval(("contractstatus")) %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="STEP" HeaderStyle-Width="50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center">
                                               <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblapplicationstep" Text='<%# Eval(("applicationstep")) %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>
                                                    <asp:Literal runat="server" ID="ltl_Grid_Action" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnPrint" runat="server" CommandName="Print" Text="Print" CommandArgument='<%# Eval("applicationid") %>' ></asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <%--<uc2:ucgridfooter id="ucGridFooter" runat="server" />--%>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <!-- #region Comment R1 -->

                    <%--                    <tr>
                        <td align="right" class="tdValue" colspan="2">
                            <asp:GridView ID="gdAgr" runat="server" AutoGenerateColumns="False"
                                Font-Names="verdana,arial,tahoma,sans-serif"
                                PageSize="20" AllowPaging="True" BorderColor="LightSkyBlue"
                                BorderStyle="Solid" BorderWidth="1px" CellPadding="4"
                                EmptyDataText="Data tidak ditemukan" ForeColor="Black" Width="100%"
                                OnPageIndexChanging="gdAgr_PageIndexChanging">
                                <HeaderStyle BackColor="#D7D7FF" BorderColor="Gray" BorderStyle="Solid" BorderWidth="1px"
                                    Font-Bold="False" Font-Names="verdana,arial,tahoma,sans-serif" Font-Overline="False"
                                    Font-Size="11px" Font-Strikeout="False" ForeColor="Black" HorizontalAlign="Center"
                                    VerticalAlign="Middle" />
                                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"
                                    NextPageText="Next" PageButtonCount="5" PreviousPageText="Previous" />

                                <Columns>
                                    <asp:BoundField HeaderText="AGREEMENT NO" DataField="agreementno">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="NAME" DataField="name">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="INSTALLMENT AMOUNT" DataField="InstallmentAmount"
                                        DataFormatString="{0:N2}">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="CURRENCY" DataField="currencyid">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="NEXT DUE DATE" DataField="NextInstallmentDueDate">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="CONTRACT STATUS" DataField="contractstatus">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="STEP" DataField="applicationstep">

                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>

                                    <asp:HyperLinkField DataNavigateUrlFields="applicationid"
                                        DataNavigateUrlFormatString="~/TrainingReport/MPMFCust_PrintTabelAmortisasi/RptPrintTabelAmortisasiView.aspx?a=c&amp;applicationid={0}"
                                        HeaderText="Action" NavigateUrl="~/TrainingReport/MPMFCust_PrintTabelAmortisasi/RptPrintTabelAmortisasiView.aspx"
                                        Text="Print">
                                        <ItemStyle HorizontalAlign="Center" Width="75px" />
                                    </asp:HyperLinkField>

                                </Columns>
                                <PagerStyle BackColor="#D7D7FF" VerticalAlign="Middle" />
                            </asp:GridView>
                        </td>
                    </tr>
                    </table>--%>


                    <!-- #endregion -->


                </div>
            </div>
        </div>
    </form>
</body>
</html>
