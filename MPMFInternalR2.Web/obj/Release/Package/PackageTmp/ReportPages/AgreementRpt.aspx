﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AgreementRpt.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.AgreementRpt" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%@ Register Src="~/RefCommon/UserControl/Report/UCReportSearch.ascx" TagName="UCReportSearch" TagPrefix="uc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="HeadCth" runat="server">
    <title></title>
</head>
<body>
    <form id="formCth" runat="server">
        <asp:ScriptManager runat="server" ID="smCth">
        </asp:ScriptManager>
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="TrainingReport/AgrementReport"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">Agreement Report </label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />

            <div id="dReportCth" runat="server">
                <uc1:UCReportSearch id="ucReportSearch" runat="server" title="Report Type-Search" />
            </div>
            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="976px"></rsweb:ReportViewer>
        </div>
    </form>
</body>
</html>

