﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LaporanProduksiAsuransi.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.LaporanProduksiAsuransi.LaporanProduksiAsuransi" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="Report/InsuranceProductionReport"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">INSURANCE PRODUCTION REPORT</label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />

            <div id="dPDCReceive">
                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">Insurance Production Report</span>
                </div>
                <div id="dSearchForm">
                    <table id="ucReportSearch_tblFixedSearch" class="formTable">
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Branch</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlbranch" runat="server">
                                </asp:DropDownList>
                                <asp:Label ID="lblWarBranch" runat="server" Font-Names="Calibri" Font-Size="10pt" ForeColor="Red"
                                    Text="*) Pilihan Cabang wajib dipilih" Visible="False" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Year</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlYear" runat="server">
                                </asp:DropDownList>
                                <asp:Label ID="lblYear" runat="server" Font-Names="Calibri" Font-Size="10pt" ForeColor="Red" Text="*) Pilihan tahun wajib dipilih" Visible="False" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Report Type</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlReportType" runat="server">
                                    <asp:ListItem Value="0">Select One</asp:ListItem>
                                    <asp:ListItem Value="1">Automotive</asp:ListItem>
                                    <asp:ListItem Value="2">Heavy Equipment - IDR</asp:ListItem>
                                    <asp:ListItem Value="3">Heavy Equipment - US$</asp:ListItem>
                                    <asp:ListItem Value="4">Life Insurance</asp:ListItem>
                                    <asp:ListItem Value="5">PAR - Pembiayaan Berbasis Sertifikat</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblWarReportType" runat="server" Font-Names="Calibri" Font-Size="10pt" ForeColor="Red"
                                    Text="*) Pilihan Report Type wajib dipilih" Visible="False" />
                            </td>
                        </tr>
                    </table>
                    <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                            OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
