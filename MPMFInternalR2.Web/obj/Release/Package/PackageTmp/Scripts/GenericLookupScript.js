function GenericLookupTest1(txtcolumn1,hdncolumn1,valcolumn1,hdncolumn2,valcolumn2,hdncolumn3,valcolumn3,hdncolumn4,valcolumn4,hdncolumn5,valcolumn5,hdncolumn6,valcolumn6){
document.getElementById(txtcolumn1).value = valcolumn1;
document.getElementById(hdncolumn1).value = valcolumn1;
document.getElementById(hdncolumn2).value = valcolumn2;
document.getElementById(hdncolumn3).value = valcolumn3;
document.getElementById(hdncolumn4).value = valcolumn4;
document.getElementById(hdncolumn5).value = valcolumn5;
document.getElementById(hdncolumn6).value = valcolumn6;
}
function GenericLookupTest2(txtcolumn1,hdncolumn1,valcolumn1,hdncolumn2,valcolumn2,hdncolumn3,valcolumn3,hdncolumn4,valcolumn4,hdncolumn5,valcolumn5,hdncolumn6,valcolumn6){
document.getElementById(txtcolumn1).value = valcolumn1;
document.getElementById(hdncolumn1).value = valcolumn1;
document.getElementById(hdncolumn2).value = valcolumn2;
document.getElementById(hdncolumn3).value = valcolumn3;
document.getElementById(hdncolumn4).value = valcolumn4;
document.getElementById(hdncolumn5).value = valcolumn5;
document.getElementById(hdncolumn6).value = valcolumn6;
}
function ApprovalUser(txtUsername,hdnUsername,valUsername){
document.getElementById(txtUsername).value = valUsername;
document.getElementById(hdnUsername).value = valUsername;
}
function ZipCode(txtZipCode,hdnZipCode,valZipCode,hdnKelurahan,valKelurahan,hdnKecamatan,valKecamatan,hdnCity,valCity,hdnPhnArea,valPhnArea,hdnDistrictName,valDistrictName,hdnRefZipCodeId,valRefZipCodeId){
document.getElementById(txtZipCode).value = valZipCode;
document.getElementById(hdnZipCode).value = valZipCode;
document.getElementById(hdnKelurahan).value = valKelurahan;
document.getElementById(hdnKecamatan).value = valKecamatan;
document.getElementById(hdnCity).value = valCity;
document.getElementById(hdnPhnArea).value = valPhnArea;
document.getElementById(hdnDistrictName).value = valDistrictName;
document.getElementById(hdnRefZipCodeId).value = valRefZipCodeId;
}
function EconomicSectorLookup(txtEconomicSectorCode,hdnEconomicSectorCode,valEconomicSectorCode,hdnName,valName,hdnSandiBICF,valSandiBICF,hdnSandiBILS,valSandiBILS,hdnRefEconomicSectorId,valRefEconomicSectorId){
document.getElementById(txtEconomicSectorCode).value = valEconomicSectorCode;
document.getElementById(hdnEconomicSectorCode).value = valEconomicSectorCode;
document.getElementById(hdnName).value = valName;
document.getElementById(hdnSandiBICF).value = valSandiBICF;
document.getElementById(hdnSandiBILS).value = valSandiBILS;
document.getElementById(hdnRefEconomicSectorId).value = valRefEconomicSectorId;
}
function PaymentAlloc(hdnPaymentAllocCode,valPaymentAllocCode,txtPaymentAllocationName,hdnPaymentAllocationName,valPaymentAllocationName,hdnIsScheme,valIsScheme,hdnIsActive,valIsActive,hdnBaseCOA,valBaseCOA,hdnIsPaymentReceive,valIsPaymentReceive,hdnIsChargeRcv,valIsChargeRcv,hdnIsSystem,valIsSystem,hdnIsPettyCash,valIsPettyCash,hdnRefPaymentAllocId,valRefPaymentAllocId,hdnIsHoTransaction,valIsHoTransaction,hdnIsFeeBases,valIsFeeBases){
document.getElementById(hdnPaymentAllocCode).value = valPaymentAllocCode;
document.getElementById(txtPaymentAllocationName).value = valPaymentAllocationName;
document.getElementById(hdnPaymentAllocationName).value = valPaymentAllocationName;
document.getElementById(hdnIsScheme).value = valIsScheme;
document.getElementById(hdnIsActive).value = valIsActive;
document.getElementById(hdnBaseCOA).value = valBaseCOA;
document.getElementById(hdnIsPaymentReceive).value = valIsPaymentReceive;
document.getElementById(hdnIsChargeRcv).value = valIsChargeRcv;
document.getElementById(hdnIsSystem).value = valIsSystem;
document.getElementById(hdnIsPettyCash).value = valIsPettyCash;
document.getElementById(hdnRefPaymentAllocId).value = valRefPaymentAllocId;
document.getElementById(hdnIsHoTransaction).value = valIsHoTransaction;
document.getElementById(hdnIsFeeBases).value = valIsFeeBases;
}
function BankLookup(txtBankName,hdnBankName,valBankName,hdnRefBankId,valRefBankId){
document.getElementById(txtBankName).value = valBankName;
document.getElementById(hdnBankName).value = valBankName;
document.getElementById(hdnRefBankId).value = valRefBankId;
}
function InsCoBranchLookup(txtInscoBranchName,hdnInscoBranchName,valInscoBranchName,hdnCntctPersonName,valCntctPersonName,hdnPhone,valPhone,hdnInscoBranchId,valInscoBranchId){
document.getElementById(txtInscoBranchName).value = valInscoBranchName;
document.getElementById(hdnInscoBranchName).value = valInscoBranchName;
document.getElementById(hdnCntctPersonName).value = valCntctPersonName;
document.getElementById(hdnPhone).value = valPhone;
document.getElementById(hdnInscoBranchId).value = valInscoBranchId;
}
function SuppLookup(txtSupplBranchName,hdnSupplBranchName,valSupplBranchName,hdnSupplBranchAddr,valSupplBranchAddr,hdnSupplBranchCity,valSupplBranchCity,hdnSupplBranchId,valSupplBranchId){
document.getElementById(txtSupplBranchName).value = valSupplBranchName;
document.getElementById(hdnSupplBranchName).value = valSupplBranchName;
document.getElementById(hdnSupplBranchAddr).value = valSupplBranchAddr;
document.getElementById(hdnSupplBranchCity).value = valSupplBranchCity;
document.getElementById(hdnSupplBranchId).value = valSupplBranchId;
}
function CountryLookup(txtCountryName,hdnCountryName,valCountryName,hdnRefCountryId,valRefCountryId){
document.getElementById(txtCountryName).value = valCountryName;
document.getElementById(hdnCountryName).value = valCountryName;
document.getElementById(hdnRefCountryId).value = valRefCountryId;
}
function SupplHolding(txtSupplHoldingName,hdnSupplHoldingName,valSupplHoldingName,hdnSupplHoldingShortName,valSupplHoldingShortName,hdnSupplHoldingAddr,valSupplHoldingAddr,hdnSupplHoldingId,valSupplHoldingId){
document.getElementById(txtSupplHoldingName).value = valSupplHoldingName;
document.getElementById(hdnSupplHoldingName).value = valSupplHoldingName;
document.getElementById(hdnSupplHoldingShortName).value = valSupplHoldingShortName;
document.getElementById(hdnSupplHoldingAddr).value = valSupplHoldingAddr;
document.getElementById(hdnSupplHoldingId).value = valSupplHoldingId;
}
function IndustryEco(txtIndustryTypeName,hdnIndustryTypeName,valIndustryTypeName,hdnIndustryTypeCode,valIndustryTypeCode,hdnName,valName,hdnEconomicSectorCode,valEconomicSectorCode,hdnIsActiveRes,valIsActiveRes,hdnIsActiveRit,valIsActiveRit,hdnRefEconomicSectorId,valRefEconomicSectorId,hdnRefIndustryTypeId,valRefIndustryTypeId){
document.getElementById(txtIndustryTypeName).value = valIndustryTypeName;
document.getElementById(hdnIndustryTypeName).value = valIndustryTypeName;
document.getElementById(hdnIndustryTypeCode).value = valIndustryTypeCode;
document.getElementById(hdnName).value = valName;
document.getElementById(hdnEconomicSectorCode).value = valEconomicSectorCode;
document.getElementById(hdnIsActiveRes).value = valIsActiveRes;
document.getElementById(hdnIsActiveRit).value = valIsActiveRit;
document.getElementById(hdnRefEconomicSectorId).value = valRefEconomicSectorId;
document.getElementById(hdnRefIndustryTypeId).value = valRefIndustryTypeId;
}
function CustLookup(txtCustName,hdnCustName,valCustName,hdnCustNo,valCustNo,hdnCustType,valCustType,hdnCustTypeText,valCustTypeText,hdnMrNegativeType,valMrNegativeType,hdnAddress,valAddress,hdnCustId,valCustId,hdnCustGroupId,valCustGroupId){
document.getElementById(txtCustName).value = valCustName;
document.getElementById(hdnCustName).value = valCustName;
document.getElementById(hdnCustNo).value = valCustNo;
document.getElementById(hdnCustType).value = valCustType;
document.getElementById(hdnCustTypeText).value = valCustTypeText;
document.getElementById(hdnMrNegativeType).value = valMrNegativeType;
document.getElementById(hdnAddress).value = valAddress;
document.getElementById(hdnCustId).value = valCustId;
document.getElementById(hdnCustGroupId).value = valCustGroupId;
}
function Master(txtAssetJoin,hdnAssetJoin,valAssetJoin,hdnAssetName,valAssetName,hdnAssetCategoryId,valAssetCategoryId,hdnAssetTypeId,valAssetTypeId,hdnAssetMasterId,valAssetMasterId){
document.getElementById(txtAssetJoin).value = valAssetJoin;
document.getElementById(hdnAssetJoin).value = valAssetJoin;
document.getElementById(hdnAssetName).value = valAssetName;
document.getElementById(hdnAssetCategoryId).value = valAssetCategoryId;
document.getElementById(hdnAssetTypeId).value = valAssetTypeId;
document.getElementById(hdnAssetMasterId).value = valAssetMasterId;
}
function AssetSchm(txtAssetJoin,hdnAssetJoin,valAssetJoin,hdnAssetName,valAssetName,hdnAssetCategoryId,valAssetCategoryId,hdnAssetTypeId,valAssetTypeId,hdnAssetMasterId,valAssetMasterId){
document.getElementById(txtAssetJoin).value = valAssetJoin;
document.getElementById(hdnAssetJoin).value = valAssetJoin;
document.getElementById(hdnAssetName).value = valAssetName;
document.getElementById(hdnAssetCategoryId).value = valAssetCategoryId;
document.getElementById(hdnAssetTypeId).value = valAssetTypeId;
document.getElementById(hdnAssetMasterId).value = valAssetMasterId;
}
function AssetSchm(txtAssetJoin,hdnAssetJoin,valAssetJoin,hdnAssetName,valAssetName,hdnAssetCategoryId,valAssetCategoryId,hdnAssetTypeId,valAssetTypeId,hdnAssetMasterId,valAssetMasterId){
document.getElementById(txtAssetJoin).value = valAssetJoin;
document.getElementById(hdnAssetJoin).value = valAssetJoin;
document.getElementById(hdnAssetName).value = valAssetName;
document.getElementById(hdnAssetCategoryId).value = valAssetCategoryId;
document.getElementById(hdnAssetTypeId).value = valAssetTypeId;
document.getElementById(hdnAssetMasterId).value = valAssetMasterId;
}
function ProfessionLookup(txtProfessionName,hdnProfessionName,valProfessionName,hdnProfessionCategoryName,valProfessionCategoryName,hdnRefProfessionId,valRefProfessionId){
document.getElementById(txtProfessionName).value = valProfessionName;
document.getElementById(hdnProfessionName).value = valProfessionName;
document.getElementById(hdnProfessionCategoryName).value = valProfessionCategoryName;
document.getElementById(hdnRefProfessionId).value = valRefProfessionId;
}
function SupplEmpLookup(hdnSupplName,valSupplName,hdnSupplId,valSupplId,txtSupplEmpName,hdnSupplEmpName,valSupplEmpName,hdnSupplEmpId,valSupplEmpId,hdnSupplEmpPositionName,valSupplEmpPositionName){
document.getElementById(hdnSupplName).value = valSupplName;
document.getElementById(hdnSupplId).value = valSupplId;
document.getElementById(txtSupplEmpName).value = valSupplEmpName;
document.getElementById(hdnSupplEmpName).value = valSupplEmpName;
document.getElementById(hdnSupplEmpId).value = valSupplEmpId;
document.getElementById(hdnSupplEmpPositionName).value = valSupplEmpPositionName;
}
function BranchEmpLookup(txtSupplBranchEmpName,hdnSupplBranchEmpName,valSupplBranchEmpName,hdnSupplEmpPositionName,valSupplEmpPositionName,hdnSupplBranchName,valSupplBranchName,hdnSupplBranchEmpId,valSupplBranchEmpId,hdnSupplBranchId,valSupplBranchId,hdnSuppId,valSuppId){
document.getElementById(txtSupplBranchEmpName).value = valSupplBranchEmpName;
document.getElementById(hdnSupplBranchEmpName).value = valSupplBranchEmpName;
document.getElementById(hdnSupplEmpPositionName).value = valSupplEmpPositionName;
document.getElementById(hdnSupplBranchName).value = valSupplBranchName;
document.getElementById(hdnSupplBranchEmpId).value = valSupplBranchEmpId;
document.getElementById(hdnSupplBranchId).value = valSupplBranchId;
document.getElementById(hdnSuppId).value = valSuppId;
}
function SupplBranchLookup(txtSupplBranchName,hdnSupplBranchName,valSupplBranchName,hdnRefOfficeId,valRefOfficeId,hdnSupplBranchId,valSupplBranchId,hdnSupplName,valSupplName,hdnSupplBranchAddr,valSupplBranchAddr){
document.getElementById(txtSupplBranchName).value = valSupplBranchName;
document.getElementById(hdnSupplBranchName).value = valSupplBranchName;
document.getElementById(hdnRefOfficeId).value = valRefOfficeId;
document.getElementById(hdnSupplBranchId).value = valSupplBranchId;
document.getElementById(hdnSupplName).value = valSupplName;
document.getElementById(hdnSupplBranchAddr).value = valSupplBranchAddr;
}
function SupplBranchSchmLookup(txtSupplBranchName,hdnSupplBranchName,valSupplBranchName,hdnRefOfficeId,valRefOfficeId,hdnSupplBranchId,valSupplBranchId,hdnSupplName,valSupplName,hdnSupplBranchAddr,valSupplBranchAddr){
document.getElementById(txtSupplBranchName).value = valSupplBranchName;
document.getElementById(hdnSupplBranchName).value = valSupplBranchName;
document.getElementById(hdnRefOfficeId).value = valRefOfficeId;
document.getElementById(hdnSupplBranchId).value = valSupplBranchId;
document.getElementById(hdnSupplName).value = valSupplName;
document.getElementById(hdnSupplBranchAddr).value = valSupplBranchAddr;
}
function AccessoriesLookup(hdnAssetAccessoryId,valAssetAccessoryId,hdnAssetAccessoryCode,valAssetAccessoryCode,txtAssetAccessoryName,hdnAssetAccessoryName,valAssetAccessoryName){
document.getElementById(hdnAssetAccessoryId).value = valAssetAccessoryId;
document.getElementById(hdnAssetAccessoryCode).value = valAssetAccessoryCode;
document.getElementById(txtAssetAccessoryName).value = valAssetAccessoryName;
document.getElementById(hdnAssetAccessoryName).value = valAssetAccessoryName;
}
function CustPersonalLookup(txtCustName,hdnCustName,valCustName,hdnCustType,valCustType,hdnAddress,valAddress,hdnCustId,valCustId,hdnCustGroupId,valCustGroupId,hdnMrIdType,valMrIdType,hdnIdNumber,valIdNumber,hdnIdExpiredDt,valIdExpiredDt,hdnMrGender,valMrGender,hdnBirthPlace,valBirthPlace,hdnBirthDt,valBirthDt,hdnNpwp,valNpwp,hdnMotherMaidenName,valMotherMaidenName){
document.getElementById(txtCustName).value = valCustName;
document.getElementById(hdnCustName).value = valCustName;
document.getElementById(hdnCustType).value = valCustType;
document.getElementById(hdnAddress).value = valAddress;
document.getElementById(hdnCustId).value = valCustId;
document.getElementById(hdnCustGroupId).value = valCustGroupId;
document.getElementById(hdnMrIdType).value = valMrIdType;
document.getElementById(hdnIdNumber).value = valIdNumber;
document.getElementById(hdnIdExpiredDt).value = valIdExpiredDt;
document.getElementById(hdnMrGender).value = valMrGender;
document.getElementById(hdnBirthPlace).value = valBirthPlace;
document.getElementById(hdnBirthDt).value = valBirthDt;
document.getElementById(hdnNpwp).value = valNpwp;
document.getElementById(hdnMotherMaidenName).value = valMotherMaidenName;
}
function CustCoyLookup(txtCustName,hdnCustName,valCustName,hdnCustType,valCustType,hdnAddress,valAddress,hdnCustId,valCustId,hdnCustGroupId,valCustGroupId,hdnNpwp,valNpwp,hdnMrCoyType,valMrCoyType){
document.getElementById(txtCustName).value = valCustName;
document.getElementById(hdnCustName).value = valCustName;
document.getElementById(hdnCustType).value = valCustType;
document.getElementById(hdnAddress).value = valAddress;
document.getElementById(hdnCustId).value = valCustId;
document.getElementById(hdnCustGroupId).value = valCustGroupId;
document.getElementById(hdnNpwp).value = valNpwp;
document.getElementById(hdnMrCoyType).value = valMrCoyType;
}
function ProductOfferingLookup(txtProdOfferingName,hdnProdOfferingName,valProdOfferingName,hdnProdOfferingId,valProdOfferingId,hdnProdOfferingCode,valProdOfferingCode){
document.getElementById(txtProdOfferingName).value = valProdOfferingName;
document.getElementById(hdnProdOfferingName).value = valProdOfferingName;
document.getElementById(hdnProdOfferingId).value = valProdOfferingId;
document.getElementById(hdnProdOfferingCode).value = valProdOfferingCode;
}
function AgreementLookup(hdnAgrmntDt,valAgrmntDt,hdnDaysOverDue,valDaysOverDue,hdnAppId,valAppId,txtAgrmntNo,hdnAgrmntNo,valAgrmntNo,hdnAgrmntId,valAgrmntId,hdnRefOfficeId,valRefOfficeId,hdnCustId,valCustId,hdnAppNo,valAppNo,hdnContractStat,valContractStat,hdnDefaultStat,valDefaultStat,hdnOfficeName,valOfficeName,hdnCustName,valCustName,hdnSpouseName,valSpouseName,hdnCustType,valCustType){
document.getElementById(hdnAgrmntDt).value = valAgrmntDt;
document.getElementById(hdnDaysOverDue).value = valDaysOverDue;
document.getElementById(hdnAppId).value = valAppId;
document.getElementById(txtAgrmntNo).value = valAgrmntNo;
document.getElementById(hdnAgrmntNo).value = valAgrmntNo;
document.getElementById(hdnAgrmntId).value = valAgrmntId;
document.getElementById(hdnRefOfficeId).value = valRefOfficeId;
document.getElementById(hdnCustId).value = valCustId;
document.getElementById(hdnAppNo).value = valAppNo;
document.getElementById(hdnContractStat).value = valContractStat;
document.getElementById(hdnDefaultStat).value = valDefaultStat;
document.getElementById(hdnOfficeName).value = valOfficeName;
document.getElementById(hdnCustName).value = valCustName;
document.getElementById(hdnSpouseName).value = valSpouseName;
document.getElementById(hdnCustType).value = valCustType;
}
function ApprovalNextPersons(txtMainUserId,hdnMainUserId,valMainUserId){
document.getElementById(txtMainUserId).value = valMainUserId;
document.getElementById(hdnMainUserId).value = valMainUserId;
}
function ApprovalNextPerson(txtMainUserId,hdnMainUserId,valMainUserId,hdnApvNodeSId,valApvNodeSId,hdnApvMemberSId,valApvMemberSId){
document.getElementById(txtMainUserId).value = valMainUserId;
document.getElementById(hdnMainUserId).value = valMainUserId;
document.getElementById(hdnApvNodeSId).value = valApvNodeSId;
document.getElementById(hdnApvMemberSId).value = valApvMemberSId;
}
function QryPagingTobyNextPerson(txtMainUserId,hdnMainUserId,valMainUserId){
document.getElementById(txtMainUserId).value = valMainUserId;
document.getElementById(hdnMainUserId).value = valMainUserId;
}
function ProdHdrLookup(txtProductName,hdnProductName,valProductName,hdnProdHdrId,valProdHdrId,hdnProductCode,valProductCode){
document.getElementById(txtProductName).value = valProductName;
document.getElementById(hdnProductName).value = valProductName;
document.getElementById(hdnProdHdrId).value = valProdHdrId;
document.getElementById(hdnProductCode).value = valProductCode;
}
function District(txtName,hdnName,valName,hdnDistrictSandiBI,valDistrictSandiBI,hdnRefProvDistrictId,valRefProvDistrictId){
document.getElementById(txtName).value = valName;
document.getElementById(hdnName).value = valName;
document.getElementById(hdnDistrictSandiBI).value = valDistrictSandiBI;
document.getElementById(hdnRefProvDistrictId).value = valRefProvDistrictId;
}
function CLRecipient(txtUsername,hdnUsername,valUsername,hdnRefUserId,valRefUserId){
document.getElementById(txtUsername).value = valUsername;
document.getElementById(hdnUsername).value = valUsername;
document.getElementById(hdnRefUserId).value = valRefUserId;
}
function UCEmpLookup(txtEmpName,hdnEmpName,valEmpName,hdnEmpNo,valEmpNo,hdnRefEmpId,valRefEmpId){
document.getElementById(txtEmpName).value = valEmpName;
document.getElementById(hdnEmpName).value = valEmpName;
document.getElementById(hdnEmpNo).value = valEmpNo;
document.getElementById(hdnRefEmpId).value = valRefEmpId;
}
function UCUserLookup(txtUsername,hdnUsername,valUsername,hdnEmpNo,valEmpNo,hdnEmpName,valEmpName,hdnRefEmpId,valRefEmpId,hdnRefUserId,valRefUserId){
document.getElementById(txtUsername).value = valUsername;
document.getElementById(hdnUsername).value = valUsername;
document.getElementById(hdnEmpNo).value = valEmpNo;
document.getElementById(hdnEmpName).value = valEmpName;
document.getElementById(hdnRefEmpId).value = valRefEmpId;
document.getElementById(hdnRefUserId).value = valRefUserId;
}
function RefPaymentAlloc(txtPaymentAllocationName,hdnPaymentAllocationName,valPaymentAllocationName,hdnPaymentAllocCode,valPaymentAllocCode,hdnRefPaymentAllocId,valRefPaymentAllocId){
document.getElementById(txtPaymentAllocationName).value = valPaymentAllocationName;
document.getElementById(hdnPaymentAllocationName).value = valPaymentAllocationName;
document.getElementById(hdnPaymentAllocCode).value = valPaymentAllocCode;
document.getElementById(hdnRefPaymentAllocId).value = valRefPaymentAllocId;
}
function Surveyor(hdnSurveyorNo,valSurveyorNo,txtSurveyorName,hdnSurveyorName,valSurveyorName,hdnUsername,valUsername,hdnAddr,valAddr,hdnSurveyorId,valSurveyorId){
document.getElementById(hdnSurveyorNo).value = valSurveyorNo;
document.getElementById(txtSurveyorName).value = valSurveyorName;
document.getElementById(hdnSurveyorName).value = valSurveyorName;
document.getElementById(hdnUsername).value = valUsername;
document.getElementById(hdnAddr).value = valAddr;
document.getElementById(hdnSurveyorId).value = valSurveyorId;
}
function UCReceiptForm(txtReceiptFormNo,hdnReceiptFormNo,valReceiptFormNo,hdnEmpName,valEmpName,hdnReceiptFormId,valReceiptFormId){
document.getElementById(txtReceiptFormNo).value = valReceiptFormNo;
document.getElementById(hdnReceiptFormNo).value = valReceiptFormNo;
document.getElementById(hdnEmpName).value = valEmpName;
document.getElementById(hdnReceiptFormId).value = valReceiptFormId;
}
function OfficeEmpLookup(txtEmpName,hdnEmpName,valEmpName,hdnJobTitleName,valJobTitleName,hdnOfficeName,valOfficeName,hdnRefEmpId,valRefEmpId,hdnOrgJobTitleId,valOrgJobTitleId,hdnRefOfficeId,valRefOfficeId){
document.getElementById(txtEmpName).value = valEmpName;
document.getElementById(hdnEmpName).value = valEmpName;
document.getElementById(hdnJobTitleName).value = valJobTitleName;
document.getElementById(hdnOfficeName).value = valOfficeName;
document.getElementById(hdnRefEmpId).value = valRefEmpId;
document.getElementById(hdnOrgJobTitleId).value = valOrgJobTitleId;
document.getElementById(hdnRefOfficeId).value = valRefOfficeId;
}
function CoyCommissionerLookup(txtName,hdnName,valName,hdnJobTitle,valJobTitle,hdnAddress,valAddress,hdnFullName,valFullName,hdnCoyCommissionerId,valCoyCommissionerId,hdnRefCoyId,valRefCoyId){
document.getElementById(txtName).value = valName;
document.getElementById(hdnName).value = valName;
document.getElementById(hdnJobTitle).value = valJobTitle;
document.getElementById(hdnAddress).value = valAddress;
document.getElementById(hdnFullName).value = valFullName;
document.getElementById(hdnCoyCommissionerId).value = valCoyCommissionerId;
document.getElementById(hdnRefCoyId).value = valRefCoyId;
}
function RefTcLookup(hdnTcCode,valTcCode,txtTcName,hdnTcName,valTcName,hdnRefTcId,valRefTcId){
document.getElementById(hdnTcCode).value = valTcCode;
document.getElementById(txtTcName).value = valTcName;
document.getElementById(hdnTcName).value = valTcName;
document.getElementById(hdnRefTcId).value = valRefTcId;
}
function FilingMapper(txtFiling,hdnFiling,valFiling,hdnRack,valRack,hdnCabinet,valCabinet,hdnCabinetName,valCabinetName,hdnFilingId,valFilingId,hdnRackId,valRackId,hdnCabinetId,valCabinetId,hdnRefOfficeId,valRefOfficeId){
document.getElementById(txtFiling).value = valFiling;
document.getElementById(hdnFiling).value = valFiling;
document.getElementById(hdnRack).value = valRack;
document.getElementById(hdnCabinet).value = valCabinet;
document.getElementById(hdnCabinetName).value = valCabinetName;
document.getElementById(hdnFilingId).value = valFilingId;
document.getElementById(hdnRackId).value = valRackId;
document.getElementById(hdnCabinetId).value = valCabinetId;
document.getElementById(hdnRefOfficeId).value = valRefOfficeId;
}
function AssetLookup(txtAssetSeqNo,hdnAssetSeqNo,valAssetSeqNo,hdnAssetName,valAssetName,hdnAgrmntAssetId,valAgrmntAssetId,hdnAgrmntNo,valAgrmntNo,hdnCustName,valCustName,hdnAssetStatName,valAssetStatName){
document.getElementById(txtAssetSeqNo).value = valAssetSeqNo;
document.getElementById(hdnAssetSeqNo).value = valAssetSeqNo;
document.getElementById(hdnAssetName).value = valAssetName;
document.getElementById(hdnAgrmntAssetId).value = valAgrmntAssetId;
document.getElementById(hdnAgrmntNo).value = valAgrmntNo;
document.getElementById(hdnCustName).value = valCustName;
document.getElementById(hdnAssetStatName).value = valAssetStatName;
}
function AssetDataLookup(hdnAgrmntNo,valAgrmntNo,hdnCustName,valCustName,txtAssetSeqNo,hdnAssetSeqNo,valAssetSeqNo,hdnAssetName,valAssetName,hdnAgrmntAssetId,valAgrmntAssetId,hdnAssetStatName,valAssetStatName){
document.getElementById(hdnAgrmntNo).value = valAgrmntNo;
document.getElementById(hdnCustName).value = valCustName;
document.getElementById(txtAssetSeqNo).value = valAssetSeqNo;
document.getElementById(hdnAssetSeqNo).value = valAssetSeqNo;
document.getElementById(hdnAssetName).value = valAssetName;
document.getElementById(hdnAgrmntAssetId).value = valAgrmntAssetId;
document.getElementById(hdnAssetStatName).value = valAssetStatName;
}
function CollectorLookup(txtCollectorName,hdnCollectorName,valCollectorName,hdnMaxWorkloadAmt,valMaxWorkloadAmt,hdnWorkloadAmt,valWorkloadAmt,hdnCollectorId,valCollectorId){
document.getElementById(txtCollectorName).value = valCollectorName;
document.getElementById(hdnCollectorName).value = valCollectorName;
document.getElementById(hdnMaxWorkloadAmt).value = valMaxWorkloadAmt;
document.getElementById(hdnWorkloadAmt).value = valWorkloadAmt;
document.getElementById(hdnCollectorId).value = valCollectorId;
}
function BrandLookup(txtBrandName,hdnBrandName,valBrandName,hdnBrandCode,valBrandCode,hdnAssetTypeId,valAssetTypeId,hdnAssetHierarchyL1Id,valAssetHierarchyL1Id){
document.getElementById(txtBrandName).value = valBrandName;
document.getElementById(hdnBrandName).value = valBrandName;
document.getElementById(hdnBrandCode).value = valBrandCode;
document.getElementById(hdnAssetTypeId).value = valAssetTypeId;
document.getElementById(hdnAssetHierarchyL1Id).value = valAssetHierarchyL1Id;
}
function DeviationCriteriaLookup(hdnDeviationCode,valDeviationCode,hdnRefDeviationId,valRefDeviationId,hdnDeviationName,valDeviationName,txtDeviationCriteria,hdnDeviationCriteria,valDeviationCriteria,hdnApvSchmCode,valApvSchmCode){
document.getElementById(hdnDeviationCode).value = valDeviationCode;
document.getElementById(hdnRefDeviationId).value = valRefDeviationId;
document.getElementById(hdnDeviationName).value = valDeviationName;
document.getElementById(txtDeviationCriteria).value = valDeviationCriteria;
document.getElementById(hdnDeviationCriteria).value = valDeviationCriteria;
document.getElementById(hdnApvSchmCode).value = valApvSchmCode;
}
function AgencyLookup(hdnAgencyCode,valAgencyCode,txtAgencyName,hdnAgencyName,valAgencyName,hdnAgencyAddr,valAgencyAddr,hdnCooperationEndDt,valCooperationEndDt,hdnAgencyId,valAgencyId){
document.getElementById(hdnAgencyCode).value = valAgencyCode;
document.getElementById(txtAgencyName).value = valAgencyName;
document.getElementById(hdnAgencyName).value = valAgencyName;
document.getElementById(hdnAgencyAddr).value = valAgencyAddr;
document.getElementById(hdnCooperationEndDt).value = valCooperationEndDt;
document.getElementById(hdnAgencyId).value = valAgencyId;
}
function NotaryLookup(hdnNotaryCode,valNotaryCode,txtNotaryName,hdnNotaryName,valNotaryName,hdnNotaryAddr,valNotaryAddr,hdnNotaryId,valNotaryId){
document.getElementById(hdnNotaryCode).value = valNotaryCode;
document.getElementById(txtNotaryName).value = valNotaryName;
document.getElementById(hdnNotaryName).value = valNotaryName;
document.getElementById(hdnNotaryAddr).value = valNotaryAddr;
document.getElementById(hdnNotaryId).value = valNotaryId;
}
function LifeInsBranchLookup(txtLifeInscoBrcName,hdnLifeInscoBrcName,valLifeInscoBrcName,hdnLifeInscoBrcId,valLifeInscoBrcId,hdnLifeInscoId,valLifeInscoId,hdnLifeInscoBrcCode,valLifeInscoBrcCode){
document.getElementById(txtLifeInscoBrcName).value = valLifeInscoBrcName;
document.getElementById(hdnLifeInscoBrcName).value = valLifeInscoBrcName;
document.getElementById(hdnLifeInscoBrcId).value = valLifeInscoBrcId;
document.getElementById(hdnLifeInscoId).value = valLifeInscoId;
document.getElementById(hdnLifeInscoBrcCode).value = valLifeInscoBrcCode;
}
function SupplBranchBasedOnAppLookup(txtSupplBranchName,hdnSupplBranchName,valSupplBranchName,hdnSupplBranchId,valSupplBranchId,hdnSupplName,valSupplName,hdnSupplBranchAddr,valSupplBranchAddr){
document.getElementById(txtSupplBranchName).value = valSupplBranchName;
document.getElementById(hdnSupplBranchName).value = valSupplBranchName;
document.getElementById(hdnSupplBranchId).value = valSupplBranchId;
document.getElementById(hdnSupplName).value = valSupplName;
document.getElementById(hdnSupplBranchAddr).value = valSupplBranchAddr;
}
function AssetPolicyLookup(txtAssetName,hdnAssetName,valAssetName,hdnRefOfficeId,valRefOfficeId,hdnAgrmntAssetId,valAgrmntAssetId,hdnInscoBranchId,valInscoBranchId,hdnAgrmntNo,valAgrmntNo,hdnMainCvgTypeName,valMainCvgTypeName,hdnInsLength,valInsLength){
document.getElementById(txtAssetName).value = valAssetName;
document.getElementById(hdnAssetName).value = valAssetName;
document.getElementById(hdnRefOfficeId).value = valRefOfficeId;
document.getElementById(hdnAgrmntAssetId).value = valAgrmntAssetId;
document.getElementById(hdnInscoBranchId).value = valInscoBranchId;
document.getElementById(hdnAgrmntNo).value = valAgrmntNo;
document.getElementById(hdnMainCvgTypeName).value = valMainCvgTypeName;
document.getElementById(hdnInsLength).value = valInsLength;
}
function AppLookup(hdnAppId,valAppId,txtAppNo,hdnAppNo,valAppNo,hdnRefOfficeId,valRefOfficeId,hdnCustId,valCustId,hdnCustName,valCustName,hdnCustType,valCustType,hdnProdOfferingName,valProdOfferingName){
document.getElementById(hdnAppId).value = valAppId;
document.getElementById(txtAppNo).value = valAppNo;
document.getElementById(hdnAppNo).value = valAppNo;
document.getElementById(hdnRefOfficeId).value = valRefOfficeId;
document.getElementById(hdnCustId).value = valCustId;
document.getElementById(hdnCustName).value = valCustName;
document.getElementById(hdnCustType).value = valCustType;
document.getElementById(hdnProdOfferingName).value = valProdOfferingName;
}
function CustCoyShareHolderLookup(txtCustName,hdnCustName,valCustName,hdnCustId,valCustId,hdnRefOfficeId,valRefOfficeId,hdnShareholderId,valShareholderId,hdnCustCoyShareholderId,valCustCoyShareholderId,hdnAddress,valAddress,hdnSharePrcnt,valSharePrcnt,hdnJobTitle,valJobTitle){
document.getElementById(txtCustName).value = valCustName;
document.getElementById(hdnCustName).value = valCustName;
document.getElementById(hdnCustId).value = valCustId;
document.getElementById(hdnRefOfficeId).value = valRefOfficeId;
document.getElementById(hdnShareholderId).value = valShareholderId;
document.getElementById(hdnCustCoyShareholderId).value = valCustCoyShareholderId;
document.getElementById(hdnAddress).value = valAddress;
document.getElementById(hdnSharePrcnt).value = valSharePrcnt;
document.getElementById(hdnJobTitle).value = valJobTitle;
}
function TaxpayerLookup(txtName,hdnName,valName,hdnTaxpayerId,valTaxpayerId,hdnIdentityNo,valIdentityNo,hdnNpwpNo,valNpwpNo,hdnMrTaxpayerTypeCode,valMrTaxpayerTypeCode,hdnMrTaxKind,valMrTaxKind,hdnIsPaidByCoy,valIsPaidByCoy){
document.getElementById(txtName).value = valName;
document.getElementById(hdnName).value = valName;
document.getElementById(hdnTaxpayerId).value = valTaxpayerId;
document.getElementById(hdnIdentityNo).value = valIdentityNo;
document.getElementById(hdnNpwpNo).value = valNpwpNo;
document.getElementById(hdnMrTaxpayerTypeCode).value = valMrTaxpayerTypeCode;
document.getElementById(hdnMrTaxKind).value = valMrTaxKind;
document.getElementById(hdnIsPaidByCoy).value = valIsPaidByCoy;
}
function MainProdOffGrpMainLookup(hdnGroupCode,valGroupCode,hdnGroupId,valGroupId,txtGroupName,hdnGroupName,valGroupName,hdnDesc,valDesc){
document.getElementById(hdnGroupCode).value = valGroupCode;
document.getElementById(hdnGroupId).value = valGroupId;
document.getElementById(txtGroupName).value = valGroupName;
document.getElementById(hdnGroupName).value = valGroupName;
document.getElementById(hdnDesc).value = valDesc;
}
function ProductOfferingLookupFixed(txtProdOfferingName,hdnProdOfferingName,valProdOfferingName,hdnProdOfferingId,valProdOfferingId,hdnProdOfferingCode,valProdOfferingCode){
document.getElementById(txtProdOfferingName).value = valProdOfferingName;
document.getElementById(hdnProdOfferingName).value = valProdOfferingName;
document.getElementById(hdnProdOfferingId).value = valProdOfferingId;
document.getElementById(hdnProdOfferingCode).value = valProdOfferingCode;
}
