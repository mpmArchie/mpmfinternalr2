﻿function showErrorMessage(errorMessage) {
    var indexOfNewArray = errorMessage.indexOf('new Array(');
    if (indexOfNewArray == -1 || indexOfNewArray > 0)
        errorMessage = 'new Array("' + errorMessage + '")';

    if (parent.ShowErrorList)
        parent.ShowErrorList(eval(errorMessage));
}

$(
    function () {
        if (parent.CloseErrorList)
            parent.CloseErrorList();
    }

);

function deleteRecordConfirm() {
    return confirm('Are you sure want to delete this record?');
}

window.onclick = function () {
    if (parent.formClick)
        parent.formClick();
}

function resizeUCRightPanel() {
    var htmlheight = document.body.parentNode.clientHeight - 90;
    $('.rightPanelContent').height(htmlheight);
    $('.rightPanelFrame').height(htmlheight);
}

//function pageLoad() {
$(document).ready(function () {
    Sys.WebForms.PageRequestManager.getInstance().add_initializeRequest(cancelPostBack);

    //Jika page halaman didaftarkan pada array dibawah ini, maka jquery tidak akan mengubah hasil ketikan menjadi uppercase
    // halaman yang di daftarkan harus menggunakan lowmercase semua

    var arr = ['/jrmedia/setting/jrmediasettingpaging.aspx',
        '/cietratesting/mandatorytest.aspx',
        '/menu/menumanagement.aspx',
        '/jrmedia/setting/jrmediaentitysetting.aspx',
        '/jrmedia/setting/jrmediaheaderfactsetting.aspx',
        '/webform1.aspx',
        '/jrmedia/setting/jrmediagroupfactsetting.aspx',
        '/jrmedia/setting/jrmediaitemvaluesetting.aspx',
        '/jrmedia/setting/jrmediagroupsetting.aspx'
    ];

    if ($.inArray(window.location.pathname.toLowerCase(), arr) < 0) {
        $("input[type=text]").css("text-transform", "uppercase");
        $("textarea").css("text-transform", "uppercase");

        $("input[type=text]").focusout(function () {
            var lowerCase = new RegExp('[a-z]');
            if (this.value.match(lowerCase)) {
                this.value = this.value.toUpperCase();
            }
        });

        $("textarea").focusout(function () {
            var lowerCase = new RegExp('[a-z]');
            if (this.value.match(lowerCase)) {
                this.value = this.value.toUpperCase();
            }
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            $("input[type=text]").css("text-transform", "uppercase");
            $("textarea").css("text-transform", "uppercase");
            //  Sys.WebForms.PageRequestManager.getInstance().add_initializeRequest(cancelPostBack);
            $("input[type=text]").focusout(function () {
                var lowerCase = new RegExp('[a-z]');
                if (this.value.match(lowerCase)) {
                    this.value = this.value.toUpperCase();
                }
            });

            $("textarea").focusout(function () {
                var lowerCase = new RegExp('[a-z]');
                if (this.value.match(lowerCase)) {
                    this.value = this.value.toUpperCase();
                }
            });
        });

    }

});



function cancelPostBack(sender, args) {
    if (Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack()) {
        alert('Your request is being processed. Please wait a moment');
        args.set_cancel(true);
    }
}