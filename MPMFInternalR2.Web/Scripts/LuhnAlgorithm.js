//Function for ASP.NET Custom Validator
function validateNumber(source, args) {
    var TransNo = document.getElementById(source.controltovalidate.toString()).value;
    args.IsValid = luhnValidation(TransNo);
    return;
}

// LUHN Algorithm untuk Check Digit
function luhnValidation(TransNo) {
    var lastDigitIndex = TransNo.length - 1;
    var chkDigitVal = TransNo.slice(-1);
    var oriTransNo = TransNo.substring(0, lastDigitIndex);

    var sum = 0;
    var isValid = false;

    sum = calcSum(oriTransNo);

    if (sum > -1) {
        var finalSum = parseInt(sum, 10) + parseInt(chkDigitVal, 10);
        var sumMod10 = parseInt(finalSum, 10) % 10;
        if (sumMod10 == 0) isValid = true;
    }

    return isValid;
}

function calcSum(oriTransNo) {
    var sum = 0;
    var rvsOriTransNo = reverseString(oriTransNo);
    var alphabetArr = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
    var factor = 2;

    for (var index = 0; index < rvsOriTransNo.length; index++) {
        var pos = index + 1;
        var value = 0;
        var calcValue = 0;

        if (isNumber(rvsOriTransNo[index])) value = rvsOriTransNo[index];
        else 
        {
            value = alphabetArr.indexOf(rvsOriTransNo[index].toString().toUpperCase());
        }

        if (value > -1) {
            calcValue = value * factor;

            var calcValueArr = calcValue.toString().split('');

            while (calcValueArr.length > 1) {
                calcValue = summValue(calcValueArr);
                calcValueArr = calcValue.toString().split('');
            }

            sum = parseInt(sum, 10) + parseInt(calcValue, 10);

            if (factor == 2) factor = 1;
            else factor = 2;
        }
        else {
            sum = -1;
        }
    }

    return sum;
}

function reverseString(TransNo) {
    return TransNo.split('').reverse();
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function summValue(valArr) {
    var arrVal = 0;

    for (var index = 0; index < valArr.length; index++) {
        arrVal = parseInt(arrVal, 10) + parseInt(valArr[index],10);
    }

    return arrVal;
}
