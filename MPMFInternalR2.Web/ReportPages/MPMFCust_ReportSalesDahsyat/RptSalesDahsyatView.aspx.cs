﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.MPMFCust_ReportSalesDahsyat
{
    public partial class RptSalesDahsyatView : System.Web.UI.Page
    {
        private string branchid, initialproductcode, startDate, endDate;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branchid = Request.QueryString["branchid"].ToString().Trim();
            initialproductcode = Request.QueryString["initialproductcode"].ToString().Trim();
            startDate = Request.QueryString["StartDate"].ToString().Trim();
            endDate = Request.QueryString["EndDate"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryReportspMPMFSalesDahsyat(branchid, initialproductcode, startDate, endDate, "spMPMFSalesDahsyat");

            if (initialproductcode.ToString() == "07")
            {
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/SalesDahsyat2W.rdlc");
            }
            else
            {
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/SalesDahsyat4W.rdlc");
            }            

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            //ReportParameter p1 = new ReportParameter("Branch", branchname);
            //ReportParameter p2 = new ReportParameter("date1", date1);
            //ReportParameter p3 = new ReportParameter("date2", date2);

            //ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}