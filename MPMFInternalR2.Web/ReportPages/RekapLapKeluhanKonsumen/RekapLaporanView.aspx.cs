﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.RekapLapKeluhanKonsumen
{
    public partial class RekapLaporanView : System.Web.UI.Page
    {
        private string BranchID, FromDate, EndDate, status, problemid;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            FromDate = Request.QueryString["FromDate"].ToString().Trim();
            EndDate = Request.QueryString["EndDate"].ToString().Trim();
            status = Request.QueryString["status"].ToString().Trim();
            problemid = Request.QueryString["problemid"].ToString().Trim();
            BranchID = "000";

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            dt = qry.QueryspMPMFCustRekapLaporanKeluhanCS(BranchID, FromDate, EndDate, status, problemid, "spMPMFCustRekapLaporanKeluhanCS");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/COMRptRekapanKeluhanKonsumen.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("FromDate", FromDate);
            ReportParameter p2 = new ReportParameter("EndDate", EndDate);
            ReportParameter p3 = new ReportParameter("status", status);
            ReportParameter p4 = new ReportParameter("problemid", problemid);
            ReportParameter p5 = new ReportParameter("BranchID", BranchID);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4,p5});
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}