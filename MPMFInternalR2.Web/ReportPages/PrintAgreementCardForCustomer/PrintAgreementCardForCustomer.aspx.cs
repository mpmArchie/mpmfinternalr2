﻿using Confins.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.PrintAgreementCardForCustomer
{
    //public partial class PrintAgreementCardForCustomer : System.Web.UI.Page
    public partial class PrintAgreementCardForCustomer : WebFormBase
    {
        DataTable objdt = new DataTable();
        Decimal TotalINSTAMT = 0;
        Decimal TotalPaidAmount = 0;
        Decimal TotalOutstanding = 0;
        Decimal TotalLCAmount = 0;
        Decimal TotalAmount = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {                
                pnlSearch.Visible = true;
                btnKembali.Visible = false;
                pnlPrint.Visible = false;
                LoadBranch();                
            }
        }

        private void LoadBranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            //ds = qry.QueryBranch(Session["sesBranchId"].ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if (Session["sesBranchId"].ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            BindDataGrid();
        }

        private void BindDataGrid()
        {
            if (ddlbranch.SelectedValue.ToString() == "000")
            {
                lblwarddlbranch.Visible = true;
                ddlbranch.Focus();
                return;
            }
            else
            {
                lblwarddlbranch.Visible = false;
            }

            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryAgreementCardForCustomerListSet(ddlbranch.SelectedValue.ToString().Trim(), txtcari.Text.ToString().Trim(), txtCustomerName.Text.Trim(), "spMPMFCustPrintAgrementCardList");
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                gvAgreementCard.DataSource = dt;
                gvAgreementCard.DataBind();
            }
            else
            {
                gvAgreementCard.DataSource = null;
                gvAgreementCard.DataBind();
            }
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("PrintAgreementCardForCustomer.aspx");
        }

        protected void gvAgreementCard_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAgreementCard.PageIndex = e.NewPageIndex;
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryAgreementCardForCustomerListSet(ddlbranch.SelectedValue.ToString().Trim(), txtcari.Text.ToString().Trim(), txtCustomerName.Text.Trim(), "spMPMFCustPrintAgrementCardList");
            gvAgreementCard.DataSource = ds;
            gvAgreementCard.DataBind();
        }

        protected void gvAgreementCard_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Print")
            {
                string agrmntid = Convert.ToString(e.CommandArgument.ToString());     
                           
                pnlSearch.Visible = false;
                btnKembali.Visible = true;
                pnlPrint.Visible = true;

                DataSet ds = new DataSet();
                QueryConnection qry = new QueryConnection();
                ds = qry.QueryAgreementCardForCustomer(agrmntid, "spMPMF_JadwalAngsuranToCustomer");                
                gvPrintAgreementCard.DataSource = ds;
                gvPrintAgreementCard.DataBind();


                lblBranchName.Text = ds.Tables[0].Rows[0].Field<string>("Office_name");
                lblAgreementNo.Text = ds.Tables[0].Rows[0].Field<string>("AGRMNT_NO");
                lblCustomerName.Text = ds.Tables[0].Rows[0].Field<string>("CUST_NAME");
                lblContractStatus.Text = ds.Tables[0].Rows[0].Field<string>("CONTRACT_STAT");
                lblDenda.Text = ds.Tables[0].Rows[0].Field<decimal>("OS_LC_INST").ToString("N2");
                lblContractPrepaid.Text = ds.Tables[0].Rows[0].Field<decimal>("PREPAID_AMT").ToString("N2");
                lblLCPaid.Text = ds.Tables[0].Rows[0].Field<decimal>("LC_INST_PAID_AMT_AAM").ToString("N2");
                lblCurrency.Text = ds.Tables[0].Rows[0].Field<string>("CURRNAME");
                lblChargeReceivable.Text = ds.Tables[0].Rows[0].Field<decimal>("VISIT_FEE").ToString("N2");

                BindChangeReceivable(agrmntid);
            }
        }

        private void BindChangeReceivable(string agrmntid)
        {

            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryAgreementCardForCustomer(agrmntid, "spMPMF_InfoChangeReceivable");
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                gvReceivable.Visible = true;
                gvReceivable.DataSource = dt;
                gvReceivable.DataBind();
            }
            else
            {
                gvReceivable.Visible = false;
                gvReceivable.DataSource = null;
                gvReceivable.DataBind();
            }
        }

        protected void btnKembali_Click(object sender, EventArgs e)
        {
            pnlSearch.Visible = true;
            btnKembali.Visible = false;
            pnlPrint.Visible = false;
        }

        protected void gvPrintAgreementCard_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblInsAmount = (Label)e.Row.FindControl("lblInsAmount");
                TotalINSTAMT = TotalINSTAMT + Convert.ToDecimal(lblInsAmount.Text);

                //Paid Amount
                Label lblPaidAmount = (Label)e.Row.FindControl("lblPaidAmount");
                TotalPaidAmount = TotalPaidAmount + Convert.ToDecimal(lblPaidAmount.Text);

                //Outstanding
                Label lblOutstanding = (Label)e.Row.FindControl("lblOutstanding");
                TotalOutstanding = TotalOutstanding + Convert.ToDecimal(lblOutstanding.Text);

                //LCAmonth
                Label lblLCAmount = (Label)e.Row.FindControl("lblLCAmount");
                TotalLCAmount = TotalLCAmount + Convert.ToDecimal(lblLCAmount.Text);
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblTotalInsAmount = (Label)e.Row.FindControl("lblTotalInsAmount");
                lblTotalInsAmount.Text = TotalINSTAMT.ToString("N2");

                Label lblTotalPaidAmount = (Label)e.Row.FindControl("lblTotalPaidAmount");
                lblTotalPaidAmount.Text = TotalPaidAmount.ToString("N2");

                Label lblTotalOutstanding = (Label)e.Row.FindControl("lblTotalOutstanding");
                lblTotalOutstanding.Text = TotalOutstanding.ToString("N2");

                Label lblTotaLCAmount = (Label)e.Row.FindControl("lblTotaLCAmount");
                lblTotaLCAmount.Text = TotalLCAmount.ToString("N2");
            }
            
        }

        protected void gvReceivable_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblAmount = (Label)e.Row.FindControl("lblAmount");
                TotalAmount = TotalAmount + Convert.ToDecimal(lblAmount.Text);
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblTotalAmount = (Label)e.Row.FindControl("lblTotalAmount");
                lblTotalAmount.Text = TotalAmount.ToString("N2");
            }
        }
    }
}