﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintAgreementCardForCustomer.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.PrintAgreementCardForCustomer.PrintAgreementCardForCustomer" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({
                dateFormat: 'dd/mm/yy'
            });
        });
    </script>
</head>
<body>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="Report/PrintAgreementCardForCustomer"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">Print Agreement Card For Customer</label>
                        </span>
                        <span id="toolMenuContainer">
                            <asp:LinkButton runat="server" ID="btnKembali" OnClick="btnKembali_Click" Visible="false">Back</asp:LinkButton>
                        </span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />

            <asp:Panel runat="server" ID="pnlSearch">
                <div id="dPDCReceive">
                    <div class="subSection">
                        <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                        <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">SEARCH</span>
                    </div>
                    <div id="dSearchForm">
                        <table id="ucReportSearch_tblFixedSearch" class="formTable">
                            <tr>
                                <td class="tdDesc" style="width: 20%;">Branch</td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:DropDownList ID="ddlbranch" runat="server">
                                    </asp:DropDownList><asp:Label ID="lblwarddlbranch" runat="server" Font-Names="verdana"
                                        Font-Size="X-Small" ForeColor="Red"
                                        Style="font-family: Verdana, Arial, Tahoma, sans-serif; font-size: 11px"
                                        Visible="False">*) Required Field</asp:Label>
                                </td>
                                <td class="tdDesc" style="width: 20%;">Customer Name</td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:TextBox ID="txtCustomerName" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdDesc" style="width: 20%;">Agreement No</td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:TextBox ID="txtcari" runat="server"></asp:TextBox>
                                </td>
                                <td class="tdDesc" style="width: 20%;"></td>
                                <td class="tdValue" style="width: 30%;"></td>
                            </tr>
                        </table>
                        <table class="formTable">
                            <tr>
                                <td align="right">
                                    <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                        <ContentTemplate>
                                            <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="SEARCH"
                                                OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                                CausesValidation="false"></asp:LinkButton>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>

                        <br />

                        <asp:UpdatePanel runat="server" ID="upGrid" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div runat="server" id="dGridSection" class="form">
                                    <div id="dGrid" style="width: 100%">
                                        <asp:GridView runat="server" ID="gvAgreementCard" AutoGenerateColumns="False" GridLines="None"
                                            CssClass="mGrid" AlternatingRowStyle-CssClass="alt" ShowHeaderWhenEmpty="True"
                                            EmptyDataText="No records Found" PageSize="10" AllowPaging="True" OnPageIndexChanging="gvAgreementCard_PageIndexChanging"
                                            OnRowCommand="gvAgreementCard_RowCommand">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Agreement No" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblAgrementNo" Text='<%# Eval("agreementno") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Name" HeaderStyle-Width="100" ItemStyle-Width="300" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblName" Text='<%# Eval("name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Installment Amount" HeaderStyle-Width="100" ItemStyle-Width="200" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblInstallmentAmount" Text='<%# Eval("InstallmentAmount") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Currency" HeaderStyle-Width="50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblcurrencyid" Text='<%# Eval(("currencyid")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Next Due Date" HeaderStyle-Width="150" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblNextInstallmentDueDate" Text='<%# Eval(("NextInstallmentDueDate")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Contact Status" HeaderStyle-Width="150" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblcontractstatus" Text='<%# Eval(("contractstatus")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STEP" HeaderStyle-Width="50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblapplicationstep" Text='<%# Eval(("applicationstep")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center">
                                                    <HeaderTemplate>
                                                        <asp:Literal runat="server" ID="ltl_Grid_Action" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnPrint" runat="server" CommandName="Print" Text="Print" CommandArgument='<%# Eval("agrmnt_id") %>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlPrint" Visible="false">
                <table class="formTable">
                    <tr>
                        <td colspan="4" style="text-align: center;">
                            <h3>AGREEMENT CARD</h3>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdDesc" style="width: 20%;">Branch </td>
                        <td class="tdValue" style="width: 30%;">: &nbsp;
                            <asp:Label runat="server" ID="lblBranchName"></asp:Label></td>
                        <td class="tdDesc" style="width: 20%;"></td>
                        <td class="tdValue" style="width: 30%;"></td>
                    </tr>
                </table>
                <table class="formTable">
                    <tr>
                        <td class="tdDesc" style="width: 20%;">Agreement No</td>
                        <td class="tdValue" style="width: 30%;">:&nbsp;
                            <asp:Label runat="server" ID="lblAgreementNo"></asp:Label></td>
                        <td class="tdDesc" style="width: 20%;">Customer Name</td>
                        <td class="tdValue" style="width: 30%;">:&nbsp; 
                            <asp:Label runat="server" ID="lblCustomerName"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdDesc" style="width: 20%;">Contract Status</td>
                        <td class="tdValue" style="width: 30%;">:&nbsp;
                            <asp:Label runat="server" ID="lblContractStatus"></asp:Label></td>
                        <td class="tdDesc" style="width: 20%;">O/S LC (Denda) Installment</td>
                        <td class="tdValue" style="width: 30%;">:&nbsp; 
                            <asp:Label runat="server" ID="lblDenda"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdDesc" style="width: 20%;">Contract Prepaid</td>
                        <td class="tdValue" style="width: 30%;">:&nbsp;
                            <asp:Label runat="server" ID="lblContractPrepaid"></asp:Label></td>
                        <td class="tdDesc" style="width: 20%;">LC Paid</td>
                        <td class="tdValue" style="width: 30%;">:&nbsp;
                            <asp:Label runat="server" ID="lblLCPaid"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdDesc" style="width: 20%;">Currency</td>
                        <td class="tdValue" style="width: 30%;">:&nbsp;
                            <asp:Label runat="server" ID="lblCurrency"></asp:Label></td>
                        <td class="tdDesc" style="width: 20%;">O/S Charge Receivable</td>
                        <td class="tdValue" style="width: 30%;">:&nbsp;
                            <asp:Label runat="server" ID="lblChargeReceivable"></asp:Label>
                        </td>
                    </tr>
                </table>

                <br />

                <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div runat="server" id="Div1" class="form">
                            <div id="dGridView" style="width: 100%">
                                <asp:GridView runat="server" ID="gvPrintAgreementCard" AutoGenerateColumns="False" GridLines="None"
                                    CssClass="mGrid" AlternatingRowStyle-CssClass="alt" ShowHeaderWhenEmpty="True" ShowFooter="true" OnRowDataBound="gvPrintAgreementCard_RowDataBound"
                                    EmptyDataText="No records Found">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Due Date" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblDueDate" Text='<%# Eval("DUE_DT") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ins Seq" HeaderStyle-Width="100" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblInsSeq" Text='<%# Eval("INST_SEQ_NO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ins Amount" HeaderStyle-Width="50" ItemStyle-Width="200" ItemStyle-HorizontalAlign="center" FooterStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblInsAmount" Text='<%# Decimal.Parse(Eval("INST_AMT").ToString()).ToString("N2") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label runat="server" ID="lblTotalInsAmount"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Paid Date" HeaderStyle-Width="150" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center" FooterStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblPaidDate" Text='<%# Eval("INST_PAID_DATE") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label runat="server" ID="lblTotalPaidDate"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Paid Amount" HeaderStyle-Width="150" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblPaidAmount" Text='<%# Decimal.Parse(Eval("INST_PAID_AMT").ToString()).ToString("N2") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label runat="server" ID="lblTotalPaidAmount"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Outstanding" HeaderStyle-Width="50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblOutstanding" Text='<%# Decimal.Parse(Eval("OUTSTANDING").ToString()).ToString("N2") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label runat="server" ID="lblTotalOutstanding"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="LC Amount" HeaderStyle-Width="50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblLCAmount" Text='<%# Decimal.Parse(Eval("LC_INST_PAID_AMT").ToString()).ToString("N2") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label runat="server" ID="lblTotaLCAmount"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="LC Days" HeaderStyle-Width="50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblLCDays" Text='<%# Eval("LC_DAY") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>

                            <div id="dGridView1" style="width: 40%">
                                <h3>Info O/S Charge Receivable</h3>
                                <asp:GridView runat="server" ID="gvReceivable" AutoGenerateColumns="False" GridLines="None"
                                    CssClass="mGrid" AlternatingRowStyle-CssClass="alt" ShowHeaderWhenEmpty="True" ShowFooter="true" OnRowDataBound="gvReceivable_RowDataBound"
                                    EmptyDataText="No records Found">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Allocation Name" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="200">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblAllocationName" Text='<%# Eval("PAYMENT_ALLOCATION_NAME") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label runat="server" ID="lblTotal" Font-Bold="true">Total</asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amount" HeaderStyle-Width="50" ItemStyle-Width="100" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblAmount" Text='<%# Decimal.Parse(Eval("AMOUNT").ToString()).ToString("N2") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label runat="server" ID="lblTotalAmount" Font-Bold="true"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
        </div>
    </form>
</body>
</html>

