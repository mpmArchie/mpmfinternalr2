﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.Web;

namespace MPMFInternalR2.Web.ReportPages.MPMFCust_ReportSold
{
    //public partial class MPMFCust_ReportSold : System.Web.UI.Page
    public partial class MPMFCust_ReportSold : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "49680921";

            if (!IsPostBack)
            {
                loadArea();
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                //loadData();
            }
        }

        private void loadData()
        {
            loadbranch();
        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            //ds = qry.QueryBranch(Session["sesBranchId"].ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if (Session["sesBranchId"].ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbranch.SelectedIndex = 0;
        }

        private void loadArea()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            //ds = qry.QueryAreaWithBranch(Session["sesBranchId"].ToString());
            ds = qry.QueryAreaWithBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlArea.DataSource = ds;
            ddlArea.DataTextField = "areafullname";
            ddlArea.DataValueField = "AreaID";
            ddlArea.DataBind();
            ddlArea.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlArea.SelectedIndex = 0;
        }

        protected void ddlArea_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            LoadBranch(ddlArea.SelectedValue.Trim());
        }

        private void LoadBranch(string strArea)
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranchArea(strArea);
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbranch.SelectedIndex = 0;
        }

        protected void lb_Print_Sync_OnClick(object sender, EventArgs e)
        {
            try
            {
                string strStartDate = "";

                if (ddlJenisNilai.SelectedValue == "")
                {
                    lblwarddlJenisNilai.Visible = true;
                    return;
                }
                else
                {
                    lblwarddlJenisNilai.Visible = false;
                }

                //if (dtStart.Text == "")
                //    strStartDate = DateTime.Now.ToString("dd/MM/yyyy");
                //else
                //    strStartDate = dtStart.Text;

                if ((dtStart.Text == string.Empty) | (dtEnd.Text == string.Empty))
                {
                    this.lblwardate.Text = "Please Fill Period From And Period To";
                    lblwardate.Visible = true; return;
                }

                if (DateTime.ParseExact(dtStart.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(dtEnd.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture))
                {
                    lblwardate.Visible = true; return;
                }
                else
                {
                    lblwardate.Visible = false;
                }

                Page.ClientScript.RegisterStartupScript(
                    this.GetType(), "OpenWindow", "window.open('MPMFCust_ReportSoldView.aspx?jenis=" + ddlJenisNilai.SelectedValue.ToString().Trim() + "&area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&date=" + dtStart.Text.ToString() + "&dateEnd=" + dtEnd.Text.ToString() + "','_newtab');", true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        protected void lbReset_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("MPMFCust_ReportSold.aspx");
        }

        protected void lb_Dowmload_OnClick(object sender, EventArgs e)
        {
            try
            {
                string strStartDate = "";

                if (ddlJenisNilai.SelectedValue == "")
                {
                    lblwarddlJenisNilai.Visible = true;
                    return;
                }
                else
                {
                    lblwarddlJenisNilai.Visible = false;
                }

                //if (dtStart.Text == "")
                //    strStartDate = DateTime.Now.ToString("dd/MM/yyyy");
                //else
                //    strStartDate = dtStart.Text;

                if ((dtStart.Text == string.Empty) | (dtEnd.Text == string.Empty))
                {
                    this.lblwardate.Text = "Please Fill Period From And Period To";
                    lblwardate.Visible = true; return;
                }

                if (DateTime.ParseExact(dtStart.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(dtEnd.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture))
                {
                    lblwardate.Visible = true; return;
                }
                else
                {
                    lblwardate.Visible = false;
                }

                Response.Redirect("MPMFCust_ReportSoldDownload.aspx?jenis=" + ddlJenisNilai.SelectedValue.ToString().Trim() + "&area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&date=" + dtStart.Text.ToString() + "&dateEnd=" + dtEnd.Text.ToString() );

                //Page.ClientScript.RegisterStartupScript(
                //    this.GetType(), "OpenWindow", "window.open('MPMFCust_ReportSoldDownload.aspx?jenis=" + ddlJenisNilai.SelectedValue.ToString().Trim() + "&area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&date=" + dtStart.Text.ToString() + "&dateEnd=" + dtEnd.Text.ToString() + "','_newtab');", true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
    }
}