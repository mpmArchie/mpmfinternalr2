﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;

namespace MPMFInternalR2.Web.ReportPages.MPMFCust_ReportSold
{
    public partial class MPMFCust_ReportSoldDownload : System.Web.UI.Page
    {
        private string jenis, area, areaname, cabang, cabangname, date, dateEnd;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            string nameFIle = "";
            jenis = Request.QueryString["jenis"].ToString().Trim();
            area = Request.QueryString["area"].ToString().Trim();
            areaname = Request.QueryString["areaname"].ToString().Trim();
            cabang = Request.QueryString["branch"].ToString().Trim();
            cabangname = Request.QueryString["branchname"].ToString().Trim();
            date = Request.QueryString["date"].ToString().Trim();
            dateEnd = Request.QueryString["dateEnd"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            dt = qry.QueryReportSOLDLORGOR(jenis, area, cabang, date, dateEnd, "sp_RPT_SOLDLORGOR");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;

            if (jenis == "LOR")
            {
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportSOLDLOR.rdlc");
                nameFIle = "ReportSOLDLOR";
            }
            else
            {
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportSOLDGOR.rdlc");
                nameFIle = "ReportSOLDGOR";
            }

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("jenis", jenis);
            ReportParameter p2 = new ReportParameter("area", areaname);
            ReportParameter p3 = new ReportParameter("branch", cabangname);
            ReportParameter p4 = new ReportParameter("date", date);
            ReportParameter p5 = new ReportParameter("dateEnd", dateEnd);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);

            GenerateToExcel(ReportViewer1, nameFIle);
        }

        private void GenerateToExcel(ReportViewer reportViewer1, string nameFile)
        {
            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = reportViewer1.LocalReport.Render("EXCEL", null, out contentType, out encoding, out extension, out streamIds, out warnings);

            //Download the RDLC Report in Word, Excel, PDF and Image formats.
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename="+ nameFile + "." + extension);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
        }
    }
}