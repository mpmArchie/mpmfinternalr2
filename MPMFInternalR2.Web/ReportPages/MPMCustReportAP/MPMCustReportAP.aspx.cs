﻿using Confins.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.MPMCustReportAP
{
    //public partial class MPMCustReportAP : System.Web.UI.Page
    public partial class MPMCustReportAP : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Session["sesBranchId"] = "999";
            //Session["loginid"] = "49680921";
            if (!IsPostBack)
            {
                loadArea();
                loadAPType();
                if (base.CurrentUserContext.OfficeCode.ToString() != "999")
                //if (Session["sesBranchId"].ToString() != "999")
                {
                    LoadBranchN();
                }
                else
                {
                    this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                }
                //loadData();
            }
        }

        private void loadAPType()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryGetAPType();
            ddlAPType.DataSource = ds;
            ddlAPType.DataTextField = "ACC_PAYABLE_TYPE_NAME";
            ddlAPType.DataValueField = "REF_ACC_PAYABLE_TYPE_ID";
            ddlAPType.DataBind();

        }

        private void LoadBranchN()
        {
            DataSet set = new DataSet();
            //set = new QueryConnection().QueryBranch(Session["sesBranchId"].ToString());
            set = new QueryConnection().QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            this.ddlbranch.DataSource = set;
            this.ddlbranch.DataTextField = "branchfullname";
            this.ddlbranch.DataValueField = "branchid";
            this.ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if (Session["sesBranchId"].ToString() == "999")
            {
                this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            this.ddlbranch.SelectedIndex = 0;
            set.Dispose();

            
        }

        private void loadArea()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            //ds = qry.QueryAreaWithBranch(Session["sesBranchId"].ToString());
            ds = qry.QueryAreaWithBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlArea.DataSource = ds;
            ddlArea.DataTextField = "areafullname";
            ddlArea.DataValueField = "AreaID";
            ddlArea.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if (Session["sesBranchId"].ToString() == "999")
            {
                ddlArea.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlArea.SelectedValue != "")
            {
                LoadBranch(ddlArea.SelectedValue.Trim());
            }
        }

        private void LoadBranch(string strArea)
        {
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if (Session["sesBranchId"].ToString() == "999")
            {
                DataSet ds = new DataSet();
                QueryConnection qry = new QueryConnection();
                ds = qry.QueryBranchArea(strArea);
                ddlbranch.DataSource = ds;
                ddlbranch.DataTextField = "branchfullname";
                ddlbranch.DataValueField = "branchid";
                ddlbranch.DataBind();
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                ddlbranch.SelectedIndex = 0;
            }
            else
            {
                DataSet set = new DataSet();
                // set = new QueryConnection().QueryBranch(Session["sesBranchId"].ToString());
                set = new QueryConnection().QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
                this.ddlbranch.DataSource = set;
                this.ddlbranch.DataTextField = "branchfullname";
                this.ddlbranch.DataValueField = "branchid";
                this.ddlbranch.DataBind();
                if (base.CurrentUserContext.OfficeCode.ToString() == "999")
                //if (Session["sesBranchId"].ToString() == "999")
                {
                    this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                }
                this.ddlbranch.SelectedIndex = 0;
                set.Dispose();
            }

        }


        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            try
            {
                if (DTEndGolive.Text == "")
                {
                    lblwarEndGolive.Visible = true;
                    DTEndGolive.Focus();
                    return;
                }
                else
                {
                    lblwarEndGolive.Visible = false;
                }

                Page.ClientScript.RegisterStartupScript(
                    this.GetType(), "OpenWindow", "window.open('MPMCustReportAPView.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&APTypeId=" + ddlAPType.SelectedValue.ToString() + "&APTypeName=" + ddlAPType.SelectedItem.Text.ToString() + "&DTEndGolive=" + DTEndGolive.Text.ToString() + "','_blank');", true);
            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void Lb_DownloadPDF_Click(object sender, EventArgs e)
        {
            try
            {
                if (DTEndGolive.Text == "")
                {
                    lblwarEndGolive.Visible = true;
                    DTEndGolive.Focus();
                    return;
                }
                else
                {
                    lblwarEndGolive.Visible = false;
                }

                Page.ClientScript.RegisterStartupScript(
                    this.GetType(), "OpenWindow", "window.open('MPMCustReportAPDownload.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&APTypeId=" + ddlAPType.SelectedValue.ToString() + "&APTypeName=" + ddlAPType.SelectedItem.Text.ToString() + "&DTEndGolive=" + DTEndGolive.Text.ToString() + "','_blank');", true);
            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void lb_Dowmload_Click(object sender, EventArgs e)
        {
            try
            {
                if (DTEndGolive.Text == "")
                {
                    lblwarEndGolive.Visible = true;
                    DTEndGolive.Focus();
                    return;
                }
                else
                {
                    lblwarEndGolive.Visible = false;
                }

                Page.ClientScript.RegisterStartupScript(
                    this.GetType(), "OpenWindow", "window.open('MPMCustReportAPDownloadExcel.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&APTypeId=" + ddlAPType.SelectedValue.ToString() + "&APTypeName=" + ddlAPType.SelectedItem.Text.ToString() + "&DTEndGolive=" + DTEndGolive.Text.ToString() + "','_blank');", true);
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}