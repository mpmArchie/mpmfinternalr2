﻿using AdIns.Applicationbloks.Data;
using AdIns.Service;
using Confins.BusinessService.Common.Org;
using Confins.BusinessService.RefCommon.Setting;
using Confins.DataModel.RefCommonModel;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages
{
    public partial class AgreementRpt : WebFormBase
    {
        #region PAGE CONTROL
        protected global::RefCommon.UserControl.Report.UCReportSearch ucReportSearch;
        #endregion

        #region PROPERTIES
        private ReportControlParams scParam
        {
            get { return (ReportControlParams)ViewState["scParam"]; }
            set { ViewState["scParam"] = value; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                scParam = new ReportControlParams();
                string mainTmpltCode = "RPTAGRMNTRIKI";

                //inisialisasi Field-Field yang akan di tampilkan di ASPX, format sama seperti penggunaan UCSearch
                //ltl_Agrmnt_GoLiveDtGte ambil dari table MULTILANG_TEXT, rubah titik menjadi _
                //parameter di SP

                #region Untuk prameter dll
                IList ListOffice = SpGet_GenericComboBox(CurrentUserContext.RefOfficeId);
                #endregion

                #region "INIT FIXED SEARCH VALUE"
                scParam.AddReportSearchInputParams(new FixedSearchPropSpec[] {
                    new FixedSearchPropSpec("ltl_Agrmnt_GoLiveDtGte", "GoLiveDateFrom", typeof(DateTime), true),
                    new FixedSearchPropSpec("ltl_Agrmnt_GoLiveDtLte", "GoLiveDateTo", typeof(DateTime),true),
                    // Penambahan ComboBox
                    new FixedSearchPropSpec("ltl_RefOffice_OfficeName_Search", "RefOfficeId", ListOffice, "Key", "Value", typeof(Int64), true,UCReference.AdditionalSelectionType.All)
                });
                #endregion

                //parameter di rdlc
                #region "INIT TEMPLATE REPORT VALUE"                

                scParam.AddReportTemplateParams(new ReportTemplateParam[]{
                    new ReportTemplateParam("CoyName", this.GetCompanyName()),
                    new ReportTemplateParam("OfficeName", CurrentUserContext.OfficeName),
                    new ReportTemplateParam("UserName",CurrentUserContext.FullName),
                    new ReportTemplateParam("SystemDate", DtFormatter.FormatToString(BusinessDate)),
                    new ReportTemplateParam("FilterBy", "Go Live Between {0} and {1}", new string[] {"GoLiveDateFrom", "GoLiveDateTo"})
                });
                #endregion

                #region SubReport
                string subTmpltCode = "SUBRPTAGRMNTRIKI";
                scParam.AddSubReportSearchInputParams(new SubReportControlParams[] {
                    new SubReportControlParams(subTmpltCode, new string[] {"GoliveDateFrom","GoLiveDateTo","RefOfficeId"})
                });
                #endregion

                ucReportSearch.SetAttribute(scParam, mainTmpltCode);

                UIDataHelper helper = new UIDataHelper();
                helper.WriteMultilangText(this, base.LanguageCode);
            }
        }

        private IList SpGet_GenericComboBox(Int64 RefofficeId)
        {
            String SpName;
            string connString;
            IList _list = new List<KeyValuePair<string, Int64>>();

            try
            {
                SpName = "spGet_ListOffice";
                connString = ConfigurationManager.ConnectionStrings["RefCommonSQLConn"].ToString();
                SqlConnection sqlConn = (SqlConnection)DBHelper.getConnection(connString);
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@RefofficeId", SqlDbType.BigInt);
                param[0].Value = RefofficeId;
                if (sqlConn.State == ConnectionState.Closed) sqlConn.Open();
                while (sqlConn.State == ConnectionState.Connecting) Thread.Sleep(1000);

                DataSet dr = new DataSet();
                dr = DBHelper.ExecuteDataset(connString, System.Data.CommandType.StoredProcedure, SpName, param);
                if (sqlConn != null)
                {
                    if (sqlConn.State == ConnectionState.Open)
                    {
                        sqlConn.Close();
                        sqlConn.Dispose();
                    }
                }
                for (int i = 0; i < dr.Tables[0].Rows.Count; i++)
                {
                    _list.Add(new KeyValuePair<string, Int64>(dr.Tables[0].Rows[i]["Key"].ToString(), Convert.ToInt64(dr.Tables[0].Rows[i]["Value"].ToString())));
                }

            }
            catch (Exception)
            {

                throw;
            }
            return _list;
        }
    }
}