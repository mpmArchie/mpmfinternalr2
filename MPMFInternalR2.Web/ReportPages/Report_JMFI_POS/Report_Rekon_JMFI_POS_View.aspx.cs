﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.Report_JMFI_POS
{
	
	public partial class Report_Rekon_JMFI_POS_View : System.Web.UI.Page
	{
		private string branch, branchname, date, date1, loginid;
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
            {
                bindreport();
            }
		}

		private void bindreport()
        {
            branch = Request.QueryString["branchID"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            date = Request.QueryString["period"].ToString().Trim();
            date1 = Request.QueryString["period1"].ToString().Trim();
            loginid = Request.QueryString["loginid"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            dt = qry.QueryReportRekon(branch, date, date1, "SP_Rekon_JMFI_POS");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/Report_Rekon_JMFI_POS.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
	}
}