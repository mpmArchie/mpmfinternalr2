﻿using Confins.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.Report_JMFI_POS
{
	//public partial class Report_Rekon_JMFI_POS : System.Web.UI.Page
	public partial class Report_Rekon_JMFI_POS : WebFormBase
   
	{
		protected void Page_Load(object sender, EventArgs e)
		{

			if (!IsPostBack)
            {
				//Session["sesBranchId"] = "999";
                //Session["loginid"] = "999";
                loadData();

            }
		}

		protected void loadData()
        {
            loadbranch();
            loadDate();

        }

		protected void loadDate()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryDate();
            //DatePicker.CalendarDateString = ds.Tables[0].Rows[0]["date"].ToString().Trim();
            //DatePicker

            ds.Dispose();
        }

		protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();

            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
             //ds = qry.QueryBranch(Session["sesBranchId"].ToString());

            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if(Session["sesBranchId"].ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbranch.SelectedIndex = 0;
        }

		protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string strDateStart = "";
            string strDateEnd = "";

            if (dtStart.Text == "")
			{
				strDateStart = "";
			}    
            else
			{
				strDateStart = dtStart.Text;
			}
            if (dtEnd.Text == "")
			{
				//strDateEnd = DateTime.Now.ToString("dd/MM/yyyy");
				strDateEnd = "";
			}   
            else
			{
				strDateEnd = dtEnd.Text;
			}

			if(strDateStart == "" && strDateEnd == "")
			{
				Response.Redirect("Report_Rekon_JMFI_POS_View.aspx?optionreport=" + "&branchId=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim() + "&period=" + strDateStart.ToString() + "&period1=" + strDateEnd.ToString() + "&loginid=" + base.CurrentUserContext.UserId.ToString() + "");
			}
			else
			{
				DateTime date1 = DateTime.ParseExact(strDateStart, "dd/MM/yyyy", CultureInfo.InvariantCulture);
				DateTime date2 = DateTime.ParseExact(strDateEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture);

				if (DateTime.ParseExact(strDateStart, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(strDateEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture))
				{
					lblwardate.Visible = true; return;
				}
				else if ((date2.Month - date1.Month) + 12 * (date2.Year - date1.Year) >= 6) 
				{
					lblwardate.Text = "* Period Date may not exceed 6 months";
					lblwardate.Visible = true; return;
				}
				else
				{
					lblwardate.Visible = false;
				}
				Response.Redirect("Report_Rekon_JMFI_POS_View.aspx?optionreport=" + "&branchId=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim() + "&period=" + strDateStart.ToString() + "&period1=" + strDateEnd.ToString() + "&loginid=" + base.CurrentUserContext.UserId.ToString() + "");
			}
        }

		protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("Report_Rekon_JMFI_POS.aspx");
        }
	}
}