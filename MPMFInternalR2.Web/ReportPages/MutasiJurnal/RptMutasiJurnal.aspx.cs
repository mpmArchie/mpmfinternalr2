﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.MutasiJurnal
{
    public partial class RptMutasiJurnal : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadData();
            }
        }

        protected void loadData()
        {
            loadbranch();
        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            ddlbranch.Items.Insert(0, new ListItem("Select One", "Select One"));
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(1, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            if (ddlbranch.SelectedIndex.ToString() == "0") { lblwarddlbranch.Visible = true; return; } else { lblwarddlbranch.Visible = false; }
            if (ddlBulan.SelectedIndex.ToString() == "0") { lblwarddlbulan.Visible = true; return; } else { lblwarddlbulan.Visible = false; }
            if (ddlTahun.SelectedIndex.ToString() == "0") { lblwarddltahun.Visible = true; return; } else { lblwarddltahun.Visible = false; }
            if (ddlReport.SelectedIndex.ToString() == "0") { lblwarddlreport.Visible = true; return; } else { lblwarddlreport.Visible = false; }
            Response.Redirect("RptMutasiJurnalView.aspx?optionreport=" + "&branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim() + "&bulan=" + ddlBulan.SelectedValue.ToString().Trim() + "&namaBulan=" + ddlBulan.SelectedItem.ToString().Trim() + "&tahun=" + ddlTahun.SelectedValue.ToString().Trim() + "&report=" + ddlReport.SelectedValue.ToString().Trim() + "&namaReport=" + ddlReport.SelectedItem.ToString().Trim() + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("RptMutasiJurnal.aspx");
        }
    }
}