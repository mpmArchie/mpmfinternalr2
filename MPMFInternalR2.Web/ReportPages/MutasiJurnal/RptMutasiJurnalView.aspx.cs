﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.MutasiJurnal
{
    public partial class RptMutasiJurnalView : System.Web.UI.Page
    {
        private string branch, branchname, bulan, namaBulan, tahun, report, namaReport;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branch = Request.QueryString["branchID"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            bulan = Request.QueryString["bulan"].ToString().Trim();
            namaBulan = Request.QueryString["namaBulan"].ToString().Trim();
            tahun = Request.QueryString["tahun"].ToString().Trim();
            report = Request.QueryString["report"].ToString().Trim();
            namaReport = Request.QueryString["namaReport"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            if (report == "Branch")
            {
                dt = qry.QueryReportMutasiJurnal(branch, bulan, tahun, report, "spMPMFCustRptMutasiJurnal");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptMutasiJurnalBranch.rdlc");
            }
            else
            {
                dt = qry.QueryReportMutasiJurnal(branch, bulan, tahun, report, "spMPMFCustRptMutasiJurnalCOA");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptMutasiJurnalCOA.rdlc");
            }
            
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("namaBranch", branchname);
            ReportParameter p2 = new ReportParameter("namaBulan", namaBulan);
            ReportParameter p3 = new ReportParameter("tahun", tahun);
            ReportParameter p4 = new ReportParameter("namaReport", namaReport);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4});
            ReportViewer1.LocalReport.DataSources.Add(datasource);

        }
    }
}