﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.AssetRepoGainLoss
{
    public partial class RptAssetRepoGainLossView : System.Web.UI.Page
    {
        private string branchid, BranchName, periodfrom, periodto;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branchid = Request.QueryString["branchid"].ToString().Trim();
            BranchName = Request.QueryString["BranchName"].ToString().Trim();
            periodfrom = Request.QueryString["periodfrom"].ToString().Trim();
            periodto = Request.QueryString["periodto"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryReportspAnjCustRptAssetRepoGainLoss(branchid, periodfrom, periodto, "spAnjCustRptAssetRepoGainLoss");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/Report_ReportANJF_AssetRepoGainLoss.rdlc");

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("BranchName", BranchName);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1});
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}