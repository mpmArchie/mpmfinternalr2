﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.MonthlySalesByBranch
{
    public partial class MonthlySalesByBranchView : System.Web.UI.Page
    {
        private string AreaId, Areafullname, branchid, branchname, PeriodMonth, PeriodeMonthName, PeriodeYear, ProdType, ProdTypeName, Currency, AssetType;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindReport();
            }
        }

        private void BindReport()
        {
            branchid = Request.QueryString["branchid"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            AreaId = Request.QueryString["AreaId"].ToString().Trim();
            Areafullname = Request.QueryString["AreaFullName"].ToString().Trim();
            PeriodMonth = Request.QueryString["PeriodMonth"].ToString().Trim();
            PeriodeMonthName = Request.QueryString["PeriodeMonthName"].ToString().Trim();
            PeriodeYear = Request.QueryString["PeriodeYear"].ToString().Trim();
            ProdType = Request.QueryString["ProdType"].ToString().Trim();
            ProdTypeName = Request.QueryString["ProdTypeName"].ToString().Trim();
            Currency = Request.QueryString["Currency"].ToString().Trim();
            AssetType = Request.QueryString["AssetType"].ToString().Trim();

            string strWhereCond = "";
            if (AreaId.Trim() != "ALL")
            {
                strWhereCond = strWhereCond + " and roa.area_code = '" + AreaId.Trim() + "' ";
            }
            else if (branchid.Trim() != "ALL")
            {
                strWhereCond = strWhereCond + " and ro.office_code = '" + branchid.Trim() + "' ";
            }
            else if (AssetType.Trim() != "ALL")
            {
                strWhereCond = strWhereCond + " and at.ASSET_TYPE_CODE = '" + AssetType.Trim() + "' ";
            }

            strWhereCond = strWhereCond + " and month(s.sales_dt) = '" + PeriodMonth.Trim() + "' and year(s.sales_dt) = '" + PeriodeYear.Trim() + "' and rc.CURR_CODE = '" + Currency.Trim() + "' and agr.PROD_TYPE = '" + ProdType.Trim() + "'";

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryReportspDailySales(strWhereCond, "spDailySales");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/SalesMonthlyReportByBranch.rdlc");

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("PeriodeMonth", PeriodeMonthName);
            ReportParameter p2 = new ReportParameter("PeriodeYear", PeriodeYear);
            ReportParameter p3 = new ReportParameter("Currency", Currency);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);

        }
    }
}