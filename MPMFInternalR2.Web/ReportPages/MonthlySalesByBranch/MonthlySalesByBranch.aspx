﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MonthlySalesByBranch.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.MonthlySalesByBranch.MonthlySalesByBranch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="Report/MonthlySalesByBranch"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">Monthly Sales By Branch</label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />

            <div id="dPDCReceive">
                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">Search</span>
                </div>
                <div id="dSearchForm">
                    <table id="ucReportSearch_tblFixedSearch" class="formTable">
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Area</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlArea" runat="server" OnSelectedIndexChanged="ddlArea_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Branch</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlBranch" runat="server" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">POS</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlPos" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Period</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="cboPeriodMonth" runat="server">
                                    <asp:ListItem Value="1">January</asp:ListItem>
                                    <asp:ListItem Value="2">February</asp:ListItem>
                                    <asp:ListItem Value="3">March</asp:ListItem>
                                    <asp:ListItem Value="4">April</asp:ListItem>
                                    <asp:ListItem Value="5">May</asp:ListItem>
                                    <asp:ListItem Value="6">June</asp:ListItem>
                                    <asp:ListItem Value="7">July</asp:ListItem>
                                    <asp:ListItem Value="8">August</asp:ListItem>
                                    <asp:ListItem Value="9">September</asp:ListItem>
                                    <asp:ListItem Value="10">October</asp:ListItem>
                                    <asp:ListItem Value="11">November</asp:ListItem>
                                    <asp:ListItem Value="12">Desember</asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox ID="txtPeriodYear" CssClass="inptype" runat="server" Width="56px" MaxLength="4"></asp:TextBox>
                                <asp:Label ID="lblSign" runat="server">
							    <font color="red">*)</font></asp:Label>
                                <asp:RequiredFieldValidator ID="rfvPeriodYear" Display="Dynamic" runat="server" ControlToValidate="txtPeriodYear" ErrorMessage="Please insert year!"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revPeriodYear" Display="Dynamic" runat="server" ControlToValidate="txtPeriodYear"
                                    ValidationExpression="\d{4}" ErrorMessage="Period Year is not Valid!"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Product Type</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:RadioButtonList ID="rdoproducttype" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="CF" Selected="True">Consumer Finance</asp:ListItem>
                                    <asp:ListItem Value="LS">Leasing Finance</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Currency</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlCurrency" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>                        
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Asset Type</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="cboAsset" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                            OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
