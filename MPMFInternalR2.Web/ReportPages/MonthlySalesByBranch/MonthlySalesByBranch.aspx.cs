﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.MonthlySalesByBranch
{
    //public partial class MonthlySalesByBranch : System.Web.UI.Page
    public partial class MonthlySalesByBranch : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["sesBranchId"] = "999";
            //HttpContext.Current.Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                loadArea();
                ddlBranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                LoadCurrency();
                LoadAssetType();
            }
        }

        private void LoadAssetType()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryAssetType();
            cboAsset.DataSource = ds;
            cboAsset.DataTextField = "description";
            cboAsset.DataValueField = "assettypeid";
            cboAsset.DataBind();
            cboAsset.Items.Insert(0, new ListItem("ALL", "ALL"));
            cboAsset.SelectedIndex = 0;
        }

        private void LoadCurrency()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryCurrency();
            ddlCurrency.DataSource = ds;
            ddlCurrency.DataTextField = "Currency";
            ddlCurrency.DataValueField = "CurrencyID";
            ddlCurrency.DataBind();
            ddlCurrency.SelectedIndex = 0;
        }

        private void loadArea()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryArea();
            ddlArea.DataSource = ds;
            ddlArea.DataTextField = "areafullname";
            ddlArea.DataValueField = "AreaID";
            ddlArea.DataBind();
            ddlArea.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlArea.SelectedIndex = 0;
        }

        private void LoadBranch(string strArea)
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranchArea(strArea);
            ddlBranch.DataSource = ds;
            ddlBranch.DataTextField = "branchfullname";
            ddlBranch.DataValueField = "branchid";
            ddlBranch.DataBind();
            ddlBranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlBranch.SelectedIndex = 0;
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadBranch(ddlArea.SelectedValue.Trim());
        }

        protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadPOS(ddlBranch.SelectedValue);
        }

        private void LoadPOS(string strBranch)
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryPOS(strBranch);
            ddlPos.DataSource = ds;
            ddlPos.DataTextField = "branchfullname";
            ddlPos.DataValueField = "branchid";
            ddlPos.DataBind();
            ddlPos.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlPos.SelectedIndex = 0;
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            Response.Redirect("MonthlySalesByBranchView.aspx?optionreport=" + "&AreaId=" + ddlArea.SelectedValue.ToString().Trim() + "&AreaFullName=" + ddlArea.SelectedItem.Text.Trim() + "&branchid=" + ddlBranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlBranch.SelectedItem.ToString().Trim() + "&PeriodMonth=" + cboPeriodMonth.SelectedValue.Trim() + "&PeriodeMonthName=" + cboPeriodMonth.SelectedItem.Text.Trim() + "&PeriodeYear=" + txtPeriodYear.Text.Trim() + "&ProdType=" + rdoproducttype.SelectedItem.Value.Trim() + "&ProdTypeName=" + rdoproducttype.SelectedItem.Text.Trim() + "&Currency=" + ddlCurrency.SelectedValue.Trim() + "&AssetType=" + cboAsset.SelectedValue.Trim() + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("MonthlySalesByBranch.aspx");
        }
    }
}