﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.InsuranceClaimMonitoring
{
    //public partial class InsuranceClaimMonitoring : System.Web.UI.Page
    public partial class InsuranceClaimMonitoring : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session.Add("loginID", "49680921");
            //Session.Add("sesBranchId", "999");
            if (!IsPostBack)
            {
                loadbranch();
                loadinscobranch();
            }
        }

        private void loadData()
        {
            loadbranch();
        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            //ds = qry.QueryBranch(Session["sesBranchId"].ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if (Session["sesBranchId"].ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbranch.SelectedIndex = 0;
        }

        protected void loadinscobranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryReportspMPMFInscoBranchList();
            ddlinscobranch.DataSource = ds;
            ddlinscobranch.DataTextField = "INSCO_BRANCH_NAME";
            ddlinscobranch.DataValueField = "INSCO_BRANCH_ID";
            ddlinscobranch.DataBind();
            ddlinscobranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlinscobranch.SelectedIndex = 0;
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            if (ddlrpttype.SelectedValue == "0")
            {
                lblwartype.Visible = true; return;
            }
            else
            {
                lblwartype.Visible = false;
            }

            if ((dtStart.Text == string.Empty) | (dtEnd.Text == string.Empty))
            {
                this.lblwardate.Text = "Please Fill Report Period";
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            if (DateTime.ParseExact(dtStart.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(dtEnd.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate0.Visible = true; return;
            }

            else if ((DateTime.ParseExact(dtEnd.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture).Month - DateTime.ParseExact(dtStart.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture).Month) + 12 * (DateTime.ParseExact(dtEnd.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture).Year - DateTime.ParseExact(dtStart.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture).Year) >= 6)
            {
                lblwardate0.Text = "* Period Date may not exceed 6 months";
                lblwardate0.Visible = true; return;
            }
            else
            {
                lblwardate0.Visible = false;
            }
            Response.Redirect("InsuranceClaimMonitoringView.aspx?branchid=" + ddlbranch.SelectedValue.ToString().Trim()
               + "&inscobranch=" + ddlinscobranch.SelectedValue.ToString().Trim()
               + "&claimstatus=" + ddlclaimstat.SelectedItem.ToString().Trim()
               + "&StartDate=" + dtStart.Text.ToString() + "&EndDate=" + dtEnd.Text.ToString()
               + "&type=" + ddlrpttype.SelectedValue.ToString()
               );

        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("InsuranceClaimMonitoring.aspx");
        }
    }
}