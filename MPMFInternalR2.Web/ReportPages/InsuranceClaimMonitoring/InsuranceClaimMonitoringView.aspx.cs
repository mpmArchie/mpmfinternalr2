﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.InsuranceClaimMonitoring
{
    public partial class InsuranceClaimMonitoringView : System.Web.UI.Page
    {
        private string type, branchid, inscobranchid, claimstat, startdate, enddate;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            type = Request.QueryString["type"].ToString().Trim();
            branchid = Request.QueryString["branchid"].ToString().Trim();
            inscobranchid = Request.QueryString["inscobranch"].ToString().Trim();
            startdate = Request.QueryString["StartDate"].ToString().Trim();
            enddate = Request.QueryString["EndDate"].ToString().Trim();
            claimstat = Request.QueryString["claimstatus"].ToString().Trim();

            if(type == "ASSET INSURANCE")
            { 
                DataTable dt = new DataTable();
                QueryConnection qry = new QueryConnection();
                
                dt = qry.QueryReportspAssetInsurance(branchid, inscobranchid, startdate, enddate, claimstat, "sp_RPT_Asset_Insurance");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportAssetInsurance.rdlc");

                ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
                ReportParameter p1 = new ReportParameter("branchid", branchid);
                ReportParameter p2 = new ReportParameter("inscobranchid", inscobranchid);
                ReportParameter p3 = new ReportParameter("startdate", startdate);
                ReportParameter p4 = new ReportParameter("enddate", enddate);
                ReportParameter p5 = new ReportParameter("claimstat", claimstat);
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5 });
                ReportViewer1.LocalReport.DataSources.Add(datasource);
            }
            else if (type == "LIFE INSURANCE")
            {
                DataTable dt = new DataTable();
                QueryConnection qry = new QueryConnection();

                dt = qry.QueryReportspAssetInsurance(branchid, inscobranchid, startdate, enddate, claimstat, "sp_RPT_Life_Insurance");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportLifeInsurance.rdlc");

                ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
                ReportParameter p1 = new ReportParameter("branchid", branchid);
                ReportParameter p2 = new ReportParameter("inscobranchid", inscobranchid);
                ReportParameter p3 = new ReportParameter("startdate", startdate);
                ReportParameter p4 = new ReportParameter("enddate", enddate);
                ReportParameter p5 = new ReportParameter("claimstat", claimstat);
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5 });
                ReportViewer1.LocalReport.DataSources.Add(datasource);
            }
        }
    }
}