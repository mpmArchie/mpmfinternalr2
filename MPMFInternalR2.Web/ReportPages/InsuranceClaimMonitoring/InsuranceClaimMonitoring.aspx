﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InsuranceClaimMonitoring.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.InsuranceClaimMonitoring.InsuranceClaimMonitoring" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({
                dateFormat: 'dd/mm/yy'
            });
        });
    </script>
    <style type="text/css">
        .auto-style1 {
            width: 316px;
        }
    </style>
    <style type="text/css">

        .auto-style1 {
            width: 316px;
            height: 28px;
        }
        .auto-style2 {
            width: 20%;
            height: 26px;
        }
        .auto-style3 {
            width: 30%;
            height: 26px;
        }
        .auto-style4 {
            height: 28px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="Report Claim Insurance Monitoring"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span><label id="pageTitle"> Report Calim Insurance </label>
                        </span>
                        <span id="toolMenuContainer">Monitoring</span></div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />

            <div id="dPDCReceive">
                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">Search</span>
                </div>
                <div id="dSearchForm">
                    <table id="ucReportSearch_tblFixedSearch" class="formTable">
                        <tr>
                            <td class="auto-style1">Report Type
                                    <asp:Label ID="lbltype" runat="server" Font-Names="verdana"
                                        Font-Size="X-Small" ForeColor="Red"
                                        Style="font-family: verdana, Arial, tahoma, san-serif; font-size: 11px;">*</asp:Label>
                                    </td>
                            <td class="auto-style4">
                                <asp:DropDownList ID="ddlrpttype" runat="server">
                                    <asp:ListItem Value="0">SELECT ONE</asp:ListItem>
                                    <asp:ListItem>ASSET INSURANCE</asp:ListItem>
                                    <asp:ListItem>LIFE INSURANCE</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblwartype" runat="server" Font-Size="X-Small" ForeColor="Red" Visible="False">*) Required field</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1">Office Name</td>
                            <td class="auto-style4">
                                <asp:DropDownList ID="ddlbranch" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style2">Insco Branch Name</td>
                            <td class="auto-style3">
                                <asp:DropDownList ID="ddlinscobranch" runat="server">
                                </asp:DropDownList>
                                </td>
                        </tr>

                        <tr>
                            <td class="tdDesc" style="width: 20%;">Claim Status</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlclaimstat" runat="server">
                                    <asp:ListItem Value="ALL">ALL</asp:ListItem>
                                    <asp:ListItem Value="REQUESTED">REQUESTED</asp:ListItem>
                                    <asp:ListItem Value="ON PROGRESS">ON PROGRESS</asp:ListItem>
                                    <asp:ListItem Value="ACCEPTED">ACCEPTED</asp:ListItem>
                                    <asp:ListItem Value="REJECTED">REJECTED</asp:ListItem>
                                    <asp:ListItem Value="CANCELED">CANCELED</asp:ListItem>
                                    <asp:ListItem Value="DONE">DONE</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style2">Periode Report
                                    <asp:Label ID="lbldate" runat="server" Font-Names="verdana"
                                        Font-Size="X-Small" ForeColor="Red"
                                        Style="font-family: verdana, Arial, tahoma, san-serif; font-size: 11px;">*</asp:Label>
                                    </td>
                             <td class="auto-style3">
                                    <asp:TextBox runat="server" AutoCompleteType="Enabled" ID="dtStart" CssClass="datepicker"></asp:TextBox>
                                    &nbsp;s/d
                                    <asp:TextBox runat="server" AutoCompleteType="Enabled" ID="dtEnd" CssClass="datepicker"></asp:TextBox>
                                <asp:Label ID="lblwardate" runat="server" Font-Size="X-Small" ForeColor="Red" Visible="False">*) Required field</asp:Label>
                                    <asp:Label ID="lblwardate0" runat="server" Font-Names="verdana"
                                        Font-Size="X-Small" ForeColor="Red"
                                        Style="font-family: verdana, Arial, tahoma, san-serif; font-size: 11px;"
                                        Visible="False">* Please check start and end dates</asp:Label>
                                    </td>
                        </tr>
                    </table>
                    <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                            OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
