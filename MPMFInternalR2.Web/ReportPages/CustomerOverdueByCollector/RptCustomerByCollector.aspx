﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RptCustomerByCollector.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.CustomerOverdueByCollector.RptCustomerByCollector" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({
                dateFormat: 'dd/mm/yy'
            });
        });
    </script>
</head>
<body>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="Report/CustomerOverdueByCollector"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">Customer Overdue By Collector </label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />

            <div id="dPDCReceive">
                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">Search</span>
                </div>
                <div id="dSearchForm">
                    <table id="ucReportSearch_tblFixedSearch" class="formTable">
                        <tr>
                            <td class="tdDesc" style="width: 20%;">
                                <asp:Literal ID="lblarea" runat="server" Text="Area" /></td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlarea" runat="server" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlarea_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">
                                <asp:Literal ID="lblBranch" runat="server" Text="Branch" /></td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlbranch" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">
                                <asp:Literal ID="lblDate" runat="server" Text="As of" /></td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:TextBox runat="server" AutoCompleteType="Enabled" ID="DatePicker" CssClass="datepicker"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">
                                <asp:Literal ID="Literal1" runat="server" Text="Beginning Bucket" /></td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlBucket" runat="server">
                                    <asp:ListItem Value="current">Current</asp:ListItem>
                                    <asp:ListItem Selected="True" Value="1">1 - 30 Hari</asp:ListItem>
                                    <asp:ListItem Value="2">31 - 60 Hari</asp:ListItem>
                                    <asp:ListItem Value="3">61 - 90 Hari</asp:ListItem>
                                    <asp:ListItem Value="4">91 - 120 Hari</asp:ListItem>
                                    <asp:ListItem Value="5">121 - 150 Hari</asp:ListItem>
                                    <asp:ListItem Value="6">151 - 180 Hari</asp:ListItem>
                                    <asp:ListItem Value="7">181 - 210 Hari</asp:ListItem>
                                    <asp:ListItem Value="8">211 - 240 Hari</asp:ListItem>
                                    <asp:ListItem Value="9">241 - 270 Hari</asp:ListItem>
                                    <asp:ListItem Value="10">> 270 Hari</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">
                                <asp:Literal ID="Literal2" runat="server" Text="Ending Bucket" /></td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlEndingBucket" runat="server">
                                    <asp:ListItem Value="current">Current</asp:ListItem>
                                    <asp:ListItem Selected="True" Value="1">1 - 30 Hari</asp:ListItem>
                                    <asp:ListItem Value="2">31 - 60 Hari</asp:ListItem>
                                    <asp:ListItem Value="3">61 - 90 Hari</asp:ListItem>
                                    <asp:ListItem Value="4">91 - 120 Hari</asp:ListItem>
                                    <asp:ListItem Value="5">121 - 150 Hari</asp:ListItem>
                                    <asp:ListItem Value="6">151 - 180 Hari</asp:ListItem>
                                    <asp:ListItem Value="7">181 - 210 Hari</asp:ListItem>
                                    <asp:ListItem Value="8">211 - 240 Hari</asp:ListItem>
                                    <asp:ListItem Value="9">241 - 270 Hari</asp:ListItem>
                                    <asp:ListItem Value="10">> 270 Hari</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Report Type</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlReportType" runat="server">
                                    <asp:ListItem Selected="True" Value="summary">Summary</asp:ListItem>
                                    <asp:ListItem Value="detail">Detail</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                            OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
