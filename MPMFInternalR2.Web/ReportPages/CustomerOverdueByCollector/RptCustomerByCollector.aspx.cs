﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.CustomerOverdueByCollector
{
    public partial class RptCustomerByCollector : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["sesBranchId"] = "999";
            //HttpContext.Current.Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                loadData();
            }
        }

        private void loadData()
        {
            loadarea();
            loadbranch();
            loadDate();
        }

        private void loadDate()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryDate();

            ds.Dispose();
        }

        private void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranchAreaCollection(ddlarea.SelectedValue.ToString().Trim());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbranch.SelectedIndex = 0;

            ds.Dispose();
        }

        private void loadarea()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryArea();
            ddlarea.DataSource = ds;
            ddlarea.DataTextField = "areafullname";
            ddlarea.DataValueField = "areaID";
            ddlarea.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlarea.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlarea.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string datepicker = "";

            if (DatePicker.Text == "")
            {
                datepicker = DateTime.Now.ToString("dd/MM/yyyy");
            }
            else
            {
                datepicker = DatePicker.Text;
            }

            Response.Redirect("RptCustomerByCollectorView.aspx?optionreport=" + "&branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim() + "&period=" + datepicker.ToString() + "&startBucket=" + ddlBucket.SelectedValue.ToString().Trim() + "&endingBucket=" + ddlEndingBucket.SelectedValue.ToString().Trim() + "&reportType=" + ddlReportType.SelectedValue.ToString().Trim() + "&areaid=" + ddlarea.SelectedValue.ToString().Trim() + "&areaname=" + ddlarea.SelectedItem.Text.ToString().Trim() + "&startBucketValue=" + ddlBucket.SelectedItem.Text.ToString().Trim() + "&endingBucketValue=" + ddlEndingBucket.SelectedItem.Text.ToString().Trim() + "&loginid=" + base.CurrentUserContext.UserId.ToString() + "");
        }

        protected void ddlarea_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadbranch();
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("RptCustomerByCollector.aspx");
        }
    }
}