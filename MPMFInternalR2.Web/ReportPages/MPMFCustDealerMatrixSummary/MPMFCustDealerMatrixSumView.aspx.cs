﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.MPMFCustDealerMatrixSummary
{
    public partial class MPMFCustDealerMatrixSumView : System.Web.UI.Page
    {

        private string branchid, branchname, RptTypeId, RptTypeName, AreaId, Areafullname, ProdId, ProdName, year, month, monthname;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branchid = Request.QueryString["branchid"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            RptTypeId = Request.QueryString["RptTypeId"].ToString().Trim();
            RptTypeName = Request.QueryString["RptTypeName"].ToString().Trim();
            AreaId = Request.QueryString["AreaId"].ToString().Trim();
            Areafullname = Request.QueryString["AreaFullName"].ToString().Trim();
            ProdId = Request.QueryString["ProdId"].ToString().Trim();
            ProdName = Request.QueryString["ProdName"].ToString().Trim();
            year = Request.QueryString["year"].ToString().Trim();
            month = Request.QueryString["month"].ToString().Trim();
            monthname = Request.QueryString["monthname"].ToString().Trim();



            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            QueryConnection qry = new QueryConnection();
            QueryConnection qry2 = new QueryConnection();

            if (RptTypeId.ToString() == "01")
            {
                dt = qry.QueryReportspMPMFCustRptDealerMatrixSummary(year, month, AreaId, branchid, ProdId, "uspMPMFDealerMatrixSumSupply");

                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptDealerMatrixSum01.rdlc");
                ReportDataSource datasource1 = new ReportDataSource("DT", dt);

                ReportParameter p1 = new ReportParameter("rpttype", RptTypeName);
                ReportParameter p2 = new ReportParameter("reg", Areafullname);
                ReportParameter p3 = new ReportParameter("brc", branchname);
                ReportParameter p4 = new ReportParameter("prod", ProdName);
                ReportParameter p5 = new ReportParameter("thn", year);
                ReportParameter p6 = new ReportParameter("bln", monthname);

                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6 });
                ReportViewer1.LocalReport.DataSources.Add(datasource1);


            }
            else if (RptTypeId.ToString() == "02")
            {
                if (ProdName == "ALL")
                {
                    DataTable dtNB = new DataTable();
                    DataTable dtNBGrp = new DataTable();
                    DataTable dtNC = new DataTable();
                    DataTable dtNCGrp = new DataTable();
                    DataTable dtUC = new DataTable();
                    DataTable dtUCGrp = new DataTable();

                    QueryConnection qryNB = new QueryConnection();
                    QueryConnection qryNBGrp = new QueryConnection();
                    QueryConnection qryNC = new QueryConnection();
                    QueryConnection qryNCGrp = new QueryConnection();
                    QueryConnection qryUC = new QueryConnection();
                    QueryConnection qryUCGrp = new QueryConnection();

                    dtNB = qryNB.QueryReportspMPMFCustRptDealerMatrixSummary(year, month, AreaId, branchid, "NB", "uspMPMFDealerMatrixSumKat");
                    dtNBGrp = qryNBGrp.QueryReportspMPMFCustRptDealerMatrixSummary(year, month, AreaId, branchid, "NB", "uspMPMFDealerMatrixSumKatGrph");

                    dtNC = qryNC.QueryReportspMPMFCustRptDealerMatrixSummary(year, month, AreaId, branchid, "NC", "uspMPMFDealerMatrixSumKat");
                    dtNCGrp = qryNCGrp.QueryReportspMPMFCustRptDealerMatrixSummary(year, month, AreaId, branchid, "NC", "uspMPMFDealerMatrixSumKatGrph");

                    dtUC = qryUC.QueryReportspMPMFCustRptDealerMatrixSummary(year, month, AreaId, branchid, "UC", "uspMPMFDealerMatrixSumKat");
                    dtUCGrp = qryUCGrp.QueryReportspMPMFCustRptDealerMatrixSummary(year, month, AreaId, branchid, "UC", "uspMPMFDealerMatrixSumKatGrph");

                    ReportViewer1.ProcessingMode = ProcessingMode.Local;
                    ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptDealerMatrixSum02All.rdlc");
                    ReportDataSource datasource1 = new ReportDataSource("DTNB", dtNB);
                    ReportDataSource datasource2 = new ReportDataSource("DTNBGRP", dtNBGrp);
                    ReportDataSource datasource3 = new ReportDataSource("DTNC", dtNC);
                    ReportDataSource datasource4 = new ReportDataSource("DTNCGRP", dtNCGrp);
                    ReportDataSource datasource5 = new ReportDataSource("DTUC", dtUC);
                    ReportDataSource datasource6 = new ReportDataSource("DTUCGRP", dtUCGrp);

                    ReportParameter p1 = new ReportParameter("rpttype", RptTypeName);
                    ReportParameter p2 = new ReportParameter("reg", Areafullname);
                    ReportParameter p3 = new ReportParameter("brc", branchname);
                    ReportParameter p4 = new ReportParameter("prod", ProdName);
                    ReportParameter p5 = new ReportParameter("thn", year);
                    ReportParameter p6 = new ReportParameter("bln", monthname);

                    ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6 });
                    ReportViewer1.LocalReport.DataSources.Add(datasource1);
                    ReportViewer1.LocalReport.DataSources.Add(datasource2);
                    ReportViewer1.LocalReport.DataSources.Add(datasource3);
                    ReportViewer1.LocalReport.DataSources.Add(datasource4);
                    ReportViewer1.LocalReport.DataSources.Add(datasource5);
                    ReportViewer1.LocalReport.DataSources.Add(datasource6);





                }
                else
                {
                    dt = qry.QueryReportspMPMFCustRptDealerMatrixSummary(year, month, AreaId, branchid, ProdId, "uspMPMFDealerMatrixSumKat");
                    dt2 = qry2.QueryReportspMPMFCustRptDealerMatrixSummary(year, month, AreaId, branchid, ProdId, "uspMPMFDealerMatrixSumKatGrph");

                    ReportViewer1.ProcessingMode = ProcessingMode.Local;
                    ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptDealerMatrixSum02.rdlc");
                    ReportDataSource datasource1 = new ReportDataSource("DT", dt);
                    ReportDataSource datasource2 = new ReportDataSource("DT2", dt2);

                    ReportParameter p1 = new ReportParameter("rpttype", RptTypeName);
                    ReportParameter p2 = new ReportParameter("reg", Areafullname);
                    ReportParameter p3 = new ReportParameter("brc", branchname);
                    ReportParameter p4 = new ReportParameter("prod", ProdName);
                    ReportParameter p5 = new ReportParameter("thn", year);
                    ReportParameter p6 = new ReportParameter("bln", monthname);

                    ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6 });
                    ReportViewer1.LocalReport.DataSources.Add(datasource1);
                    ReportViewer1.LocalReport.DataSources.Add(datasource2);
                }

            }
            else if (RptTypeId.ToString() == "03")
            {
                dt = qry.QueryReportspMPMFCustRptDealerMatrixSummary(year, month, AreaId, branchid, ProdId, "uspMPMFDealerMatrixSumKatReg");

                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptDealerMatrixSum03.rdlc");
                ReportDataSource datasource1 = new ReportDataSource("DT", dt);

                ReportParameter p1 = new ReportParameter("rpttype", RptTypeName);
                ReportParameter p2 = new ReportParameter("reg", Areafullname);
                ReportParameter p3 = new ReportParameter("brc", branchname);
                ReportParameter p4 = new ReportParameter("prod", ProdName);
                ReportParameter p5 = new ReportParameter("thn", year);
                ReportParameter p6 = new ReportParameter("bln", monthname);

                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5,p6 });
                ReportViewer1.LocalReport.DataSources.Add(datasource1);

            }

            //ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            //ReportParameter p1 = new ReportParameter("date", startDate);
            //ReportParameter p2 = new ReportParameter("date1", endDate);
            //ReportParameter p3 = new ReportParameter("Branch", branchfullname);
            //ReportParameter p4 = new ReportParameter("AreaName", Areafullname);
            ////ReportParameter p4 = new ReportParameter("assettype", RptType);
            //ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4 });
            //ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}