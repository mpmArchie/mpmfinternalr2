﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MPMFCustDealerMatrixSum.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.MPMFCustDealerMatrixSummary.MPMFCustDealerMatrixSum" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({
                dateFormat: 'dd/mm/yy'
            });
        });
    </script>
</head>
<body>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="ReportDealerMatrix"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">Report Dealer Matrix Summary</label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />

            <div id="dPDCReceive">
                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">Report Dealer Matrix Summary</span>
                </div>
                <div id="dSearchForm">
                    <table id="ucReportSearch_tblFixedSearch" class="formTable">
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Jenis Matrix</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlRptType" runat="server" OnSelectedIndexChanged="ddlRptType_SelectedIndexChanged" AutoPostBack="True">
                                    <asp:ListItem Value="00" Selected="True">Select One</asp:ListItem>
                                    <asp:ListItem Value="01">Supplier</asp:ListItem>
                                    <asp:ListItem Value="02">Kategori (Summary)</asp:ListItem>
                                    <asp:ListItem Value="03">Kategori by Regional</asp:ListItem>
                                </asp:DropDownList>
                                <td>
                                    <asp:Label ID="lblwarddlRptType" runat="server" Font-Size="X-Small" ForeColor="Red" Visible="False">*) Required field</asp:Label>
                                </td>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Product</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlProduct" runat="server" AutoPostBack="True">
                                    <asp:ListItem Value="ALL" Selected="True">ALL</asp:ListItem>
                                    <asp:ListItem Value="NC">New Car</asp:ListItem>
                                    <asp:ListItem Value="NB">New Bike</asp:ListItem>
                                    <asp:ListItem Value="UC">Used Car</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Tahun</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddltahun" runat="server" OnSelectedIndexChanged="ddltahun_SelectedIndexChanged" AutoPostBack="True">
                                    <%--<asp:ListItem Value="00" Selected="True">Select One</asp:ListItem>
                                    <asp:ListItem Value="2017">2017</asp:ListItem>
                                    <asp:ListItem Value="2018">2018</asp:ListItem>
                                    <asp:ListItem Value="2019">2019</asp:ListItem>
                                    <asp:ListItem Value="2020">2020</asp:ListItem>
                                    <asp:ListItem Value="2021">2021</asp:ListItem>
                                    <asp:ListItem Value="2022">2022</asp:ListItem>--%>
                                </asp:DropDownList>
                                <td>
                                    <asp:Label ID="lblwarddltahun" runat="server" Font-Size="X-Small" ForeColor="Red" Visible="False">*) Required field</asp:Label>
                                </td>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Bulan</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlbulan" runat="server" AutoPostBack="True">
                                    <asp:ListItem Value="ALL" Selected="True">ALL</asp:ListItem>
                                    <asp:ListItem Value="1">Januari</asp:ListItem>
                                    <asp:ListItem Value="2">Februari</asp:ListItem>
                                    <asp:ListItem Value="3">Maret</asp:ListItem>
                                    <asp:ListItem Value="4">April</asp:ListItem>
                                    <asp:ListItem Value="5">Mei</asp:ListItem>
                                    <asp:ListItem Value="6">Juni</asp:ListItem>
                                    <asp:ListItem Value="7">Juli</asp:ListItem>
                                    <asp:ListItem Value="8">Agustus</asp:ListItem>
                                    <asp:ListItem Value="9">September</asp:ListItem>
                                    <asp:ListItem Value="10">Oktober</asp:ListItem>
                                    <asp:ListItem Value="11">November</asp:ListItem>
                                    <asp:ListItem Value="12">Desember</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Area</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlArea" runat="server" OnSelectedIndexChanged="ddlArea_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Branch</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlbranch" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:Label ID="lblwarddlbranch" runat="server" Font-Size="X-Small" ForeColor="Red" Visible="False">*) Required field</asp:Label>
                            </td>
                        </tr>
                        
                    </table>
                    <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                            OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
