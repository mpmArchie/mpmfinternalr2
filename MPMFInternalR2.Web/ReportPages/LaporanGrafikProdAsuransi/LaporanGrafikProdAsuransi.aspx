﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LaporanGrafikProdAsuransi.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.LaporanGrafikProdAsuransi.LaporanGrafikProdAsuransi" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">INSURANCE GRAFIK PRODUCTION REPORT</label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />

            <div id="dPDCReceive">
                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">Search</span>
                </div>
                <div id="dSearchForm">
                    <table id="ucReportSearch_tblFixedSearch" class="formTable">
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Branch</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlbranch" runat="server">
                                </asp:DropDownList>
                                <asp:Label ID="lblWarBranch" runat="server" ForeColor="Red" Text="*) Pilihan Cabang wajib dipilih" Visible="False" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Report Type</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlGrafikType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrafikType_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlYearly" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlMonthly" runat="server" AutoPostBack="True">
                                    <asp:ListItem Value="0">Select Month</asp:ListItem>
                                    <asp:ListItem Value="1">JANUARI</asp:ListItem>
                                    <asp:ListItem Value="2">FEBRUARI</asp:ListItem>
                                    <asp:ListItem Value="3">MARET</asp:ListItem>
                                    <asp:ListItem Value="4">APRIL</asp:ListItem>
                                    <asp:ListItem Value="5">MEI</asp:ListItem>
                                    <asp:ListItem Value="6">JUNI</asp:ListItem>
                                    <asp:ListItem Value="7">JULI</asp:ListItem>
                                    <asp:ListItem Value="8">AGUSTUS</asp:ListItem>
                                    <asp:ListItem Value="9">SEPTEMBER</asp:ListItem>
                                    <asp:ListItem Value="10">OKTOBER</asp:ListItem>
                                    <asp:ListItem Value="11">NOVEMBER</asp:ListItem>
                                    <asp:ListItem Value="12">DESEMBER</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblWarGrafikType" runat="server" ForeColor="Red" Text="*) Pilihan Grafik Type wajib dipilih" Visible="False" />
                                <asp:Label ID="lblWarYear" runat="server" ForeColor="Red" Text="*) Pilihan Tahun wajib dipilih" Visible="False" />
                                <asp:Label ID="lblWarMonth" runat="server" ForeColor="Red" Text="*) Pilihan Bulan wajib dipilih" Visible="False" />
                            </td>
                        </tr>
                    </table>
                    <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                            OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
