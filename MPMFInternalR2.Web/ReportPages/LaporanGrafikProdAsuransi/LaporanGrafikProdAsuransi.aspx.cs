﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.LaporanGrafikProdAsuransi
{
    public partial class LaporanGrafikProdAsuransi : WebFormBase
    //public partial class LaporanGrafikProdAsuransi : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["sesBranchId"] = "999";
            //HttpContext.Current.Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                this.lblWarBranch.Visible = false;
                this.lblWarGrafikType.Visible = false;
                this.BindChooseBranch();
                this.bindGrafikType();
                this.bindYear();
            }
        }

        private void bindYear()
        {
            this.ddlYearly.ClearSelection();
            this.ddlYearly.Items.Insert(0, new ListItem("Select Year", "0"));
            int index = 1;
            int num2 = DateTime.Now.Year - 4;
            for (int i = num2 + 4; i >= num2; i--)
            {
                string text = i.ToString();
                this.ddlYearly.Items.Insert(index, new ListItem(text, i.ToString()));
                index++;
            }
        }

        private void bindGrafikType()
        {
            this.ddlGrafikType.ClearSelection();
            this.ddlGrafikType.Items.Insert(0, new ListItem("Select One", "0"));
            this.ddlGrafikType.Items.Insert(1, new ListItem("Year", "1"));
            this.ddlGrafikType.Items.Insert(2, new ListItem("Month", "2"));
            this.ddlGrafikType.SelectedIndex = 0;
        }

        private void BindChooseBranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;
        }

        protected void ddlGrafikType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.lblWarMonth.Visible = false;
            this.lblWarYear.Visible = false;
            if (this.ddlGrafikType.SelectedValue == "0")
            {
                this.ddlYearly.Visible = false;
                this.ddlMonthly.Visible = false;
                this.ddlYearly.ClearSelection();
                this.ddlMonthly.ClearSelection();
                this.lblWarGrafikType.Visible = true;
            }
            else if (this.ddlGrafikType.SelectedValue == "1")
            {
                this.ddlYearly.Visible = true;
                this.ddlMonthly.Visible = false;
                this.ddlMonthly.ClearSelection();
                this.ddlYearly.Focus();
                this.lblWarGrafikType.Visible = false;
            }
            else if (this.ddlGrafikType.SelectedValue == "2")
            {
                this.ddlYearly.Visible = true;
                this.ddlMonthly.Visible = true;
                this.ddlYearly.ClearSelection();
                this.ddlMonthly.Focus();
                this.lblWarGrafikType.Visible = false;
            }
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            this.lblWarBranch.Visible = false;
            this.lblWarGrafikType.Visible = false;
            this.lblWarYear.Visible = false;
            this.lblWarMonth.Visible = false;
            string str = "1";
            if (this.ddlbranch.SelectedValue == "0")
            {
                this.ddlbranch.Focus();
                this.lblWarBranch.Visible = true;
            }
            else if (this.ddlGrafikType.SelectedValue == "0")
            {
                this.lblWarGrafikType.Focus();
                this.lblWarGrafikType.Visible = true;
            }
            else
            {
                if (this.ddlGrafikType.SelectedValue == "1")
                {
                    if (this.ddlYearly.SelectedValue == "0")
                    {
                        this.lblWarYear.Focus();
                        this.lblWarYear.Visible = true;
                        return;
                    }
                    str = "2";
                }
                else if (this.ddlGrafikType.SelectedValue == "2")
                {
                    if (this.ddlMonthly.SelectedValue == "0")
                    {
                        this.lblWarMonth.Focus();
                        this.lblWarMonth.Visible = true;
                        return;
                    }
                    str = "3";
                }
                if (str == "2")
                {
                    base.Response.Redirect("LaporanGrafikProduksiAsuransiView.aspx?year=" + this.ddlYearly.SelectedItem.ToString().Trim() + "&branchid=" + this.ddlbranch.SelectedValue + "&branchdesc=" + this.ddlbranch.SelectedItem.ToString().Trim() + "&reportType=" + this.ddlGrafikType.SelectedValue + "&reportTypedesc=" + this.ddlGrafikType.SelectedItem.Text.ToString().Trim() + "&month=" + this.ddlMonthly.SelectedValue + "&monthdesc=" + this.ddlMonthly.SelectedItem.ToString().Trim());
                }
                else if (str == "3")
                {
                    base.Response.Redirect("LaporanGrafikProduksiAsuransiView.aspx?year=" + this.ddlYearly.SelectedItem.ToString().Trim() + "&branchid=" + this.ddlbranch.SelectedValue + "&branchdesc=" + this.ddlbranch.SelectedItem.ToString().Trim() + "&reportType=" + this.ddlGrafikType.SelectedValue + "&month="+ this.ddlMonthly.SelectedValue + "&monthdesc=" + this.ddlMonthly.SelectedItem.ToString().Trim());
                }
            }
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            base.Response.Redirect("LaporanGrafikProdAsuransi.aspx"); 
        }
    }
}