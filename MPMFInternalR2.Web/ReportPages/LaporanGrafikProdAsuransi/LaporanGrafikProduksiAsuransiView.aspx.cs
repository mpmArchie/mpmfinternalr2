﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.LaporanGrafikProdAsuransi
{
    public partial class LaporanGrafikProduksiAsuransiView : System.Web.UI.Page
    {
        private string year, branchid, branchdesc, reportType, reportTypedesc, month, monthdesc;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindReport();
            }
        }

        private void BindReport()
        {
            year = base.Request.QueryString["year"];
            branchid = base.Request.QueryString["branchid"];
            branchdesc = base.Request.QueryString["branchdesc"];
            reportType = base.Request.QueryString["reportType"];
            reportTypedesc = base.Request.QueryString["reportTypedesc"];
            month = base.Request.QueryString["month"];
            monthdesc = base.Request.QueryString["monthdesc"];

            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            DataTable dt4 = new DataTable();
            QueryConnection qry1 = new QueryConnection();
            QueryConnection qry2 = new QueryConnection();
            QueryConnection qry3 = new QueryConnection();
            QueryConnection qry4 = new QueryConnection();

            if (reportType == "1")
            {
                dt1 = qry1.QueryReportspMPMFCustRptProductionYear(branchid, year, "spMPMFCustRptProductionYearUnit");
                dt2 = qry2.QueryReportspMPMFCustRptProductionYear(branchid, year, "spMPMFCustRptProductionYearIDR");
                dt3 = qry3.QueryReportspMPMFCustRptProductionYear(branchid, year, "spMPMFCustRptProductionYearGrafikUnit");
                dt4 = qry4.QueryReportspMPMFCustRptProductionYear(branchid, year, "spMPMFCustRptProductionYearGrafikIDR");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptLapGrafikProduksiAsMain.rdlc");
                ReportDataSource datasource1 = new ReportDataSource("UNIT", dt1);
                ReportDataSource datasource2 = new ReportDataSource("IDR", dt2);
                ReportDataSource datasource3 = new ReportDataSource("GFUNIT", dt3);
                ReportDataSource datasource4 = new ReportDataSource("GFIDR", dt4);

                ReportParameter p1 = new ReportParameter("branchid", branchid);
                ReportParameter p2 = new ReportParameter("year", year);
                ReportParameter p3 = new ReportParameter("branchdesc", branchdesc);
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3 });
                ReportViewer1.LocalReport.DataSources.Add(datasource1);
                ReportViewer1.LocalReport.DataSources.Add(datasource2);
                ReportViewer1.LocalReport.DataSources.Add(datasource3);
                ReportViewer1.LocalReport.DataSources.Add(datasource4);
            }
            else
            {
                dt1 = qry1.QueryReportspMPMFCustRptProductionMonth(branchid, year, month, "spMPMFCustRptProductionMonthUnit");
                dt2 = qry2.QueryReportspMPMFCustRptProductionMonth(branchid, year, month, "spMPMFCustRptProductionMonthIDR");
                dt3 = qry3.QueryReportspMPMFCustRptProductionMonth(branchid, year, month, "spMPMFCustRptProductionMonthGrafikUnit");
                dt4 = qry4.QueryReportspMPMFCustRptProductionMonth(branchid, year, month, "spMPMFCustRptProductionMonthGrafikIDR");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptLapGrafikProduksiAsMainMonth.rdlc");
                ReportDataSource datasource1 = new ReportDataSource("UNIT", dt1);
                ReportDataSource datasource2 = new ReportDataSource("IDR", dt2);
                ReportDataSource datasource3 = new ReportDataSource("GFUNIT", dt3);
                ReportDataSource datasource4 = new ReportDataSource("GFIDR", dt4);

                ReportParameter p1 = new ReportParameter("branchid", branchid);
                ReportParameter p2 = new ReportParameter("year", year);
                ReportParameter p3 = new ReportParameter("month", month);
                ReportParameter p4 = new ReportParameter("monthdesc", monthdesc);
                ReportParameter p5 = new ReportParameter("branchdesc", branchdesc);
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5 });
                ReportViewer1.LocalReport.DataSources.Add(datasource1);
                ReportViewer1.LocalReport.DataSources.Add(datasource2);
                ReportViewer1.LocalReport.DataSources.Add(datasource3);
                ReportViewer1.LocalReport.DataSources.Add(datasource4);

            }



        }
    }
}