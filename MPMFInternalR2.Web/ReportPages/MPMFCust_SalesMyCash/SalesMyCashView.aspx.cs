﻿using Microsoft.Reporting.WebForms;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.MPMFCust_SalesMyCash
{
    public partial class SalesMyCashView : System.Web.UI.Page
    {
        private string branchid, branchfullname, tglfrom, tglto, Assettypeid, Assettype, reportType, assettype;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branchid = Request.QueryString["branchid"].ToString().Trim();
            branchfullname = Request.QueryString["branchname"].ToString().Trim();
            tglfrom = Request.QueryString["tglfrom"].ToString().Trim();
            tglto = Request.QueryString["tglto"].ToString().Trim();
            reportType = Request.QueryString["reportType"].ToString().Trim();
            Assettypeid = Request.QueryString["assettypeid"].ToString().Trim();
            Assettype = Request.QueryString["assettype"].ToString().Trim();
            


            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryReportspAnjCustRptSalesMyCashMotor(tglfrom, tglto, branchid, reportType, Assettypeid, "spAnjCustRptSalesMyCashMotor");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptSalesMyCashMotor.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("date", tglfrom);
            ReportParameter p2 = new ReportParameter("date1", tglto);
            ReportParameter p3 = new ReportParameter("Branch", branchfullname);
            ReportParameter p4 = new ReportParameter("assettype", Assettype);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);

        }

    }
}