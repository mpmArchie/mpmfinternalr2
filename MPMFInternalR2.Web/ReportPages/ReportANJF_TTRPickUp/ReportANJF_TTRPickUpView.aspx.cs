﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.ReportANJF_TTRPickUp
{
    public partial class ReportANJF_TTRPickUpView : System.Web.UI.Page
    {
        private string branch, branchname, date, date1, assettypeid, assettype;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branch = Request.QueryString["branchid"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            date= Request.QueryString["tglfrom"].ToString().Trim();
            date1 = Request.QueryString["tglto"].ToString().Trim();
            assettypeid = Request.QueryString["assettypeid"].ToString().Trim();
            assettype = Request.QueryString["assettype"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryTTRPickUp(date, date1, branch, assettypeid, "spAnjCustRptTTRPickUp");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportANJF_TTRPickUp.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("Branch", branchname);
            ReportParameter p2 = new ReportParameter("date", date);
            ReportParameter p3 = new ReportParameter("date1", date1);
            ReportParameter p4 = new ReportParameter("assettype", assettype);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }

    }
}