﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.Rpt_CIS_CIT
{
    public partial class CISdanCITView : System.Web.UI.Page
    {
        private string branchid, branchfullname, RptType, startDate, endDate, AreaId, Areafullname;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }

        }
        private void bindreport()
        {
            branchid = Request.QueryString["branchid"].ToString().Trim();
            branchfullname = Request.QueryString["branchname"].ToString().Trim();
            startDate = Request.QueryString["StartDate"].ToString().Trim();
            endDate = Request.QueryString["EndDate"].ToString().Trim();
            AreaId = Request.QueryString["AreaId"].ToString().Trim();
            Areafullname = Request.QueryString["AreaFullName"].ToString().Trim();
            RptType = Request.QueryString["tipereport"].ToString().Trim();
            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            //[uspRptCIS]
            //uspRptCIT

            if (RptType.ToString() == "CIS")
            {
                
                dt = qry.QueryReportspPhdp(AreaId, branchid, startDate, endDate, "uspRptCIS");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportCIS.rdlc");
            }
            else if (RptType.ToString() == "CIT")
            {
                    
                dt = qry.QueryReportspPhdp(AreaId, branchid, startDate, endDate, "uspRptCIT");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportCIT.rdlc");
            }

           


            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("date", startDate);
            ReportParameter p2 = new ReportParameter("date1", endDate);
            ReportParameter p3 = new ReportParameter("Branch", branchfullname);
            ReportParameter p4 = new ReportParameter("AreaName", Areafullname);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}