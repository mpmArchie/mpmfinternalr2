﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.PhysicPolicyReceive
{
    public partial class RptInsurancePhysicPolicyReceive : WebFormBase
    //public partial class RptInsurancePhysicPolicyReceive : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["sesBranchId"] = "999";
            //HttpContext.Current.Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {

                loadData();
            }
        }
        protected void loadData()
        {
            loadbranch();
        }
        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbranch.SelectedIndex = 0;

            ds.Dispose();
        }
        protected void ddlReportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlReportType.SelectedValue.ToString() == "1")
            {
                panel1.Visible = true;
                panel2.Visible = false;
            }
            else
            {
                panel1.Visible = false;
                panel2.Visible = true;
            }
        }
        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string datepicker = "";
            string datepicker1 = "";
            string reportType = "";

            if (dtStart.Text == "")
                datepicker = DateTime.Now.ToString("dd/MM/yyyy");
            else
                datepicker = dtStart.Text;

            if (dtEnd.Text == "")
                datepicker1 = DateTime.Now.ToString("dd/MM/yyyy");
            else
                datepicker1 = dtEnd.Text;

            if (DateTime.ParseExact(datepicker, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(datepicker1, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }
            if (ddlReportType.SelectedValue.ToString().Trim() != null)
            {
                reportType = ddlReportType.SelectedValue.ToString().Trim();
            }

            Response.Redirect("RptInsurancePhysicPolicyReceiveView.aspx?optionreport=" + "&branchId=" + ddlbranch.SelectedValue.ToString().Trim()
                 + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim()
                 + "&startDate=" + datepicker.ToString()
                 + "&endDate=" + datepicker1.ToString()
                 + "" + "&reportType=" + reportType);
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("RptInsurancePhysicPolicyReceive.aspx");
        }
    }
}

