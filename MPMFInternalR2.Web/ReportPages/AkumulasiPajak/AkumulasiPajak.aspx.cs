﻿using Confins.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.AkumulasiPajak
{
    //public partial class AkumulasiPajak : System.Web.UI.Page
    public partial class AkumulasiPajak : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                loadData();
            }
        }

        private void loadData()
        {
            loadbranch();
            loadsupplier();
            loadYear();
        }

        private void loadYear()
        {
            DataSet set = new DataSet();
            set = new QueryConnection().QueryGetYear();
            this.ddlyear.DataSource = set;
            this.ddlyear.DataTextField = "year";
            this.ddlyear.DataValueField = "year";
            this.ddlyear.DataBind();
            this.ddlyear.SelectedIndex = 0;
            set.Dispose();

        }

        private void loadsupplier()
        {
            if ((ddlbranch.SelectedValue != "Select One" & ddlbranch.SelectedValue != "All") & ddlbranch.SelectedValue != "All")
            {
                DataSet ds = new DataSet();
                QueryConnection qry = new QueryConnection();
                ds = qry.QuerySupplierByBranch(ddlbranch.SelectedValue.Trim(), ddlyear.SelectedValue.Trim());
                ddlsupplier.DataSource = ds;
                ddlsupplier.DataTextField = "suppliername";
                ddlsupplier.DataValueField = "supplierid";
                ddlsupplier.DataBind();
                //if (HttpContext.Current.Session["sesBranchId"].ToString() == "999")
                if (base.CurrentUserContext.OfficeCode.ToString() == "999")
                {
                    ddlsupplier.Items.Insert(0, new ListItem("ALL", "ALL"));
                }
                ddlsupplier.SelectedIndex = 0;
            }
        }

        private void loadbranch()
        {
            DataSet set = new DataSet();
            set = new QueryConnection().QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            this.ddlbranch.DataSource = set;
            this.ddlbranch.DataTextField = "branchfullname";
            this.ddlbranch.DataValueField = "branchid";
            this.ddlbranch.DataBind();
            //if (HttpContext.Current.Session["sesBranchId"].ToString() == "999")
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            this.ddlbranch.SelectedIndex = 0;
            set.Dispose();

        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("AkumulasiPajak.aspx");
        }

        protected void gvRefTableTax_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (this.txtmarketing.Text.ToString().Trim() == "")
            {
                this.lblwarmarketing.Visible = true;
            }
            else
            {
                this.lblwarmarketing.Visible = false;
                this.gvRefTableTax.PageIndex = e.NewPageIndex;
                DataSet set = new DataSet();
                DataTable dt = new DataTable();
                set = new QueryConnection().spMPMFCust_TaxGrid(this.ddlbranch.SelectedValue.ToString(), this.txtmarketing.Text.ToString().Trim(), this.ddlyear.SelectedValue.ToString().Trim(), this.ddlsupplier.SelectedValue.ToString().Trim(), "spMPMFCust_TaxGrid");
                dt = set.Tables[0];
                this.gvRefTableTax.DataSource = dt;
                this.gvRefTableTax.DataBind();
                set.Dispose();
            }

        }

        protected void gvRefTableTax_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Print")
            {
                //string marketing = Convert.ToString(e.CommandArgument.ToString());
                int rowIndex = ((e.CommandSource as LinkButton).NamingContainer as GridViewRow).RowIndex;
                var supplierKtpId = ((Label)gvRefTableTax.Rows[rowIndex].FindControl("lblsupplierktpid")).Text.Trim();

                Response.Redirect("AkumulasiPajakView.aspx?supplierktpid=" + supplierKtpId.Trim() + "&branchid=" + ddlbranch.SelectedValue.Trim() + "&year=" + ddlyear.SelectedValue.Trim() + "");
            }
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtmarketing.Text.Trim() == "")
                {
                    lblwarmarketing.Visible = true;
                }
                else
                {
                    lblwarmarketing.Visible = false;
                    DataSet set = new DataSet();
                    DataTable dt = new DataTable();
                    set = new QueryConnection().spMPMFCust_TaxGrid(this.ddlbranch.SelectedValue.ToString(), this.txtmarketing.Text.ToString().Trim(), this.ddlyear.SelectedValue.ToString().Trim(), this.ddlsupplier.SelectedValue.ToString().Trim(), "spMPMFCust_TaxGrid");
                    dt = set.Tables[0];
                    this.gvRefTableTax.DataSource = dt;
                    this.gvRefTableTax.DataBind();
                    set.Dispose();

                }
            }
            catch (Exception ex)
            {
                Response.Write(" <script language=\"javascript\" type=\"text/javascript\"> alert('Kesalahan pada sistem!' " + ex.ToString() + "'); </script>");
            }
        }

        protected void ddlyear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((this.ddlbranch.SelectedItem.Value != "Select One") & (this.ddlbranch.SelectedItem.Value != "All")) & (this.ddlbranch.SelectedItem.Value != "All"))
            {
                DataSet set = new DataSet();
                set = new QueryConnection().QuerySupplierByBranch(this.ddlbranch.SelectedValue.ToString().Trim(), this.ddlyear.SelectedValue.ToString().Trim());
                this.ddlsupplier.DataSource = set;
                this.ddlsupplier.DataTextField = "suppliername";
                this.ddlsupplier.DataValueField = "supplierid";
                this.ddlsupplier.DataBind();
                //if (HttpContext.Current.Session["sesBranchId"].ToString() == "999")
                if (base.CurrentUserContext.OfficeCode.ToString() == "999")
                {
                    this.ddlsupplier.Items.Insert(0, new ListItem("ALL", "ALL"));
                }
                this.ddlsupplier.SelectedIndex = 0;
                set.Dispose();
            }
        }

        protected void ddlbranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadsupplier();
        }
    }
}