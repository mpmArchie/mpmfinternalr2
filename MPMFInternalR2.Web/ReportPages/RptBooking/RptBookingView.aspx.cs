﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.RptBooking
{
    public partial class RptBookingView : System.Web.UI.Page
    {
        private string branch, branchname, date1, date2;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branch = Request.QueryString["branchid"].ToString().Trim();
            date1 = Request.QueryString["date1"].ToString().Trim();
            date2 = Request.QueryString["date2"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            dt = qry.QueryReportBooking(branch, date1, date2, "spMPMFCust_RptBooking");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptBooking.rdlc");

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("OFFICE_CODE", branch);
            ReportParameter p2 = new ReportParameter("date1", date1);
            ReportParameter p3 = new ReportParameter("date2", date2);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3});
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}