﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DailyNPL.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.DailyNPL.DailyNPL" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({
                dateFormat: 'dd/mm/yy'
            });
        });
    </script>
</head>
<body>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="Report/Daily NPL"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">Daily NPL</label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />

            <div id="dPDCReceive">
                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">Search</span>
                </div>
                <div id="dSearchForm">
                    <table id="ucReportSearch_tblFixedSearch" class="formTable">
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Reposses Date From</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:TextBox runat="server" AutoCompleteType="Enabled" ID="dtStart" CssClass="datepicker"></asp:TextBox>
                                <asp:Label ID="Label2" runat="server" Font-Names="verdana" Font-Size="8pt" ForeColor="Red"
                                    Visible="False">*) required field</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Branch</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlbranch" runat="server">
                                </asp:DropDownList>
                                <asp:Label ID="Label1" runat="server" Font-Names="verdana" Font-Size="8pt" ForeColor="Red"
                                    Visible="False">*) required field</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Product Type</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="dlProd" runat="server" Font-Names="Verdana" Font-Size="10pt">
                                    <asp:ListItem Value="All">All</asp:ListItem>
                                    <asp:ListItem Value="1">CF - Mobil</asp:ListItem>
                                    <asp:ListItem Value="2">CF - Motor</asp:ListItem>
                                    <asp:ListItem Value="3">FL</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="Label3" runat="server" Font-Names="verdana" Font-Size="8pt" ForeColor="Red"
                                    Visible="False">*) required field</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Report Type</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlReportType" runat="server" Font-Names="Verdana" Font-Size="10pt">
                                    <asp:ListItem Value="1">Summary</asp:ListItem>
                                    <asp:ListItem Value="2">Detail</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="Label4" runat="server" Font-Names="verdana" Font-Size="8pt" ForeColor="Red"
                                    Visible="False">*) required field</asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                            OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
