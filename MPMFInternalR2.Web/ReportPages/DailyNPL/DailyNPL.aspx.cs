﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.DailyNPL
{
    public partial class DailyNPL : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["sesBranchId"] = "999";
            //HttpContext.Current.Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                loadbranch();
            }
        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();

            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            if (dtStart.Text != "")
            {
                Response.Redirect("rptDailyNPLView.aspx?AgingDate=" + dtStart.Text.Trim() + "&Branch=" + ddlbranch.SelectedItem.ToString().Trim() + "&Product=" + dlProd.SelectedValue.ToString() + "&ReportType=" + ddlReportType.SelectedValue.ToString() + "");
                Label2.Visible = false;
            }
            else
            {
                Label2.Visible = true;
            }
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("DailyNPL.aspx");
        }
    }
}