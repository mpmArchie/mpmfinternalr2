﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.InquiryDisbursement
{
    public partial class RptBookingDanaView : System.Web.UI.Page
    {
        private string branch;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branch = Request.QueryString["branchID"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryReportBookingDana(branch, "spMPMFCustRptBookingDana");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptBookingDana.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}