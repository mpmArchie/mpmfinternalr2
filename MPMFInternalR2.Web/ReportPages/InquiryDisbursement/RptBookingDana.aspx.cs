﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.InquiryDisbursement
{
    public partial class RptBookingDana : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["sesBranchId"] = "999";
            //HttpContext.Current.Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                loadData();
            }
        }

        protected void loadData()
        {
            loadbranch();
        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            ddlbranch.Items.Insert(0, new ListItem("Select One", "Select One"));
            //if (HttpContext.Current.Session["sesBranchId"].ToString() == "999")
            //{
            //    ddlbranch.Items.Insert(1, new ListItem("ALL", "ALL"));
            //}
            ddlbranch.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("RptBookingDana.aspx");
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            if (ddlbranch.SelectedIndex.ToString() == "0")
            {
                lblwarddlbranch.Visible = true;
                return;
            }
            else
            {
                lblwarddlbranch.Visible = false;
            }
            Response.Redirect("RptBookingDanaView.aspx?optionreport=" + "&branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim() + "");
        }
    }
}