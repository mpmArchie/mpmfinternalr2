﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.Monitoring
{
    public partial class vLaporanMonitoringSrtTgs : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            loaddata();
        }

        private void loaddata()
        {
            string tglfrom = Request.QueryString["tglfrom"];
            string tglto = Request.QueryString["tglto"];
            string pilihan = Request.QueryString["pilihan"];
            string pilihn = Request.QueryString["pilihn"];
            string report = Request.QueryString["rpt"];
            string lblreports = Request.QueryString["lblrpt"];
            string choose = Request.QueryString["choose"];
            
            lbltglfrom.Text = Request.QueryString["tglfrom"];
            lbltglto.Text = Request.QueryString["tglto"];
            lblpilihan.Text = Request.QueryString["pilihan"];
            lblpilihn.Text = Request.QueryString["pilihn"];
            lblreport.Text = Request.QueryString["rpt"];
            txtreport.Text = Request.QueryString["lblrpt"];
            lblchoose.Text = Request.QueryString["choose"];

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryReportspMPMFCustRptMonitoringSuratTugas(pilihan.ToString().Trim(), tglfrom.ToString().Trim(), tglto.ToString().Trim(), "spMPMFCustRptMonitoringSuratTugas");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptMonitoringSuratTugas.rdlc");

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("From", tglfrom.ToString().Trim());
            ReportParameter p2 = new ReportParameter("End", tglto.ToString().Trim());
            ReportParameter p3 = new ReportParameter("pilih", pilihan.ToString().Trim());
            ReportParameter p4 = new ReportParameter("pilihn", pilihn.ToString().Trim());
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}