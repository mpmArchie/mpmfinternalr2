﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.Monitoring
{
    public partial class vLaporanMonitoringSTNKProses : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            loaddata();
        }

        private void loaddata()
        {
            string tglfrom = Request.QueryString["tglfrom"];
            string tglto = Request.QueryString["tglto"];
            string regional = Request.QueryString["regional"];
            string branch = Request.QueryString["branch"];
            string service = Request.QueryString["service"];
            string regionaldesc = Request.QueryString["regionaldesc"];
            string branchdesc = Request.QueryString["branchdesc"];
            string servicedesc = Request.QueryString["servicedesc"];
            
            lbltglfrom.Text = Request.QueryString["tglfrom"];
            lbltglto.Text = Request.QueryString["tglto"];
            lblregional.Text = Request.QueryString["regional"];
            lblbranch.Text = Request.QueryString["branch"];
            lblservice.Text = Request.QueryString["service"];
            lblregionaldesc.Text = Request.QueryString["regionaldesc"];
            lblbranchdesc.Text = Request.QueryString["branchdesc"];
            lblservicedesc.Text = Request.QueryString["servicedesc"];
            lblreport.Text = Request.QueryString["rpt"];
            txtreport.Text = Request.QueryString["lblrpt"];
            lblchoose.Text = Request.QueryString["choose"];

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryReportspMPMFCustRptMonitoringProsesFidusia(branch.ToString().Trim(), service.ToString().Trim(), tglfrom.ToString().Trim(), tglto.ToString().Trim(), "spMPMFCustRptMonitoringSTNKProses");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptMonitoringStnkProses.rdlc");

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("From", tglfrom.ToString().Trim());
            ReportParameter p2 = new ReportParameter("End", tglto.ToString().Trim());
            ReportParameter p3 = new ReportParameter("regionaldesc", branchdesc.ToString().Trim());
            ReportParameter p4 = new ReportParameter("branchdesc", branchdesc.ToString().Trim());
            ReportParameter p5 = new ReportParameter("servicedesc", servicedesc.ToString().Trim());
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}