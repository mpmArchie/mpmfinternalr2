﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.Web;

namespace MPMFInternalR2.Web.ReportPages.Monitoring
{
    public partial class LaporanMonitoring : WebFormBase
    //public partial class LaporanMonitoring : System.Web.UI.Page

    {
        string pilih = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["sesBranchId"] = "999";
            //HttpContext.Current.Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                this.ddlPilihan.SelectedIndex = ddlPilihan.Items.IndexOf(ddlPilihan.Items.FindByText("Select One"));

                lblWarPilihRpt.Visible = false;
                lblWarPilihStatus.Visible = false;
                lblWarPilih.Visible = false;

                BindChooseBranch();

                if (!IsPostBack)
                {
                    this.ddlPilihan.SelectedIndex = ddlPilihan.Items.IndexOf(ddlPilihan.Items.FindByText("Select One"));

                    lblWarPilihRpt.Visible = false;
                    lblWarPilihStatus.Visible = false;
                    lblWarPilih.Visible = false;

                    BindChooseBranch();
                    BIndChooseArea();

                    //ini kalau back lagi ke report
                    if (Request.QueryString["tglfrom"] != null)
                    {
                        lbltglfrom.Text = Request.QueryString["tglfrom"];
                        lbltglto.Text = Request.QueryString["tglto"];
                        lblpilihan.Text = Request.QueryString["pilihan"];
                        lblpilihn.Text = Request.QueryString["pilihn"];
                        lblreport.Text = Request.QueryString["rpt"];
                        txtreport.Text = Request.QueryString["lblrpt"];
                        lblStatusID.Text = Request.QueryString["statusid"];
                        lblstatusname.Text = Request.QueryString["statusname"];
                        lblchoose.Text = Request.QueryString["choose"];
                        this.ddlPilihan.SelectedIndex = ddlPilihan.Items.IndexOf(ddlPilihan.Items.FindByText(lblchoose.Text.ToString().Trim()));

                        /*FF - ganti pake datepicker
                        this.txtFromDate.Text = lbltglfrom.Text.ToString().Trim();
                        this.txtEndDate.Text = lbltglto.Text.ToString().Trim();
                        */

                        //ini kalau back lagi ke report
                        if (ddlPilihan.SelectedValue == "0")
                        {
                            pilih = "";
                            ddlPilihan.Focus();
                            ddlCabang.Visible = false;
                            ddlRegional.Visible = false;
                            lblWarPilih.Visible = true;
                            return;
                        }

                        if (lblchoose.Text == "Cabang")
                        {
                            ddlCabang.ClearSelection();
                            pilih = "";
                            BindAllCabang();
                            ddlCabang.Visible = true;
                            ddlCabang.SelectedIndex = ddlCabang.Items.IndexOf(ddlCabang.Items.FindByValue(lblpilihan.Text.ToString().Trim()));
                            ddlCabang.Focus();
                            ddlRegional.Visible = false;
                            lblWarPilih.Visible = false;
                        }
                        if (lblchoose.Text == "Regional")
                        {
                            pilih = "";
                            ddlCabang.Visible = false;
                            ddlRegional.Visible = true;
                            ddlRegional.SelectedIndex = ddlPilihan.Items.IndexOf(ddlPilihan.Items.FindByValue(lblpilihan.Text.ToString().Trim()));
                            ddlRegional.Focus();
                            lblWarPilih.Visible = false;
                        }

                        if (lblreport.Text != null)
                        {
                            if (lblreport.Text == "1")
                            {
                                lblWarPilihRpt.Visible = false;
                                BindProsesFidusia();
                                ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                                ddlStatus.Visible = true;
                                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                                ddlPilihRpt.Focus();
                            }
                            else if (lblreport.Text == "2")
                            {
                                lblWarPilihRpt.Visible = false;
                                BindProsesAsuransi();
                                ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                                ddlStatus.Visible = true;
                                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                                ddlPilihRpt.Focus();
                            }
                            else if (lblreport.Text == "3")
                            {
                                lblWarPilihRpt.Visible = false;
                                BindTTR();
                                ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                                ddlStatus.Visible = true;
                                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                                ddlPilihRpt.Focus();
                            }
                            else if (lblreport.Text == "4")
                            {
                                lblWarPilihRpt.Visible = false;
                                BindDokKontrak();
                                ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                                ddlStatus.Visible = true;
                                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                                ddlPilihRpt.Focus();
                            }
                            else if (lblreport.Text == "5")
                            {
                                lblWarPilihRpt.Visible = false;
                                BindClaimAsuransi();
                                ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                                ddlStatus.Visible = true;
                                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                                ddlPilihRpt.Focus();
                            }
                            else if (lblreport.Text == "6")
                            {
                                lblWarPilihRpt.Visible = false;
                                BindSuspend();
                                ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                                ddlStatus.Visible = true;
                                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                                ddlPilihRpt.Focus();
                            }
                            else if (lblreport.Text == "7")
                            {
                                lblWarPilihRpt.Visible = false;
                                BindVoucher();
                                ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                                ddlStatus.Visible = true;
                                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                                ddlPilihRpt.Focus();
                            }
                            else if (lblreport.Text == "8")
                            {
                                lblWarPilihRpt.Visible = false;
                                BindAplikasi();
                                ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                                ddlStatus.Visible = true;
                                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                                ddlPilihRpt.Focus();
                            }
                            else if (lblreport.Text == "9")
                            {
                                lblWarPilihRpt.Visible = false;
                                BindBPKBBorrow();
                                ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                                ddlStatus.Visible = true;
                                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                                ddlPilihRpt.Focus();
                            }
                            else if (lblreport.Text == "10")
                            {
                                lblWarPilihRpt.Visible = false;
                                BindSPBPKB();
                                ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                                ddlStatus.Visible = true;
                                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                                ddlPilihRpt.Focus();
                            }
                            else if (lblreport.Text == "11")
                            {
                                lblWarPilihRpt.Visible = false;
                                BindSPPrinting();
                                ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                                ddlStatus.Visible = true;
                                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                                ddlPilihRpt.Focus();
                            }
                            else if (lblreport.Text == "12")
                            {
                                lblWarPilihRpt.Visible = false;
                                BindSPPrinting();
                                ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                                ddlStatus.Visible = true;
                                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                                ddlPilihRpt.Focus();
                            }
                            else if (lblreport.Text == "13")
                            {
                                ddlStatus.Visible = false;
                                lblstatustd.Text = "";
                                lblWarPilihRpt.Visible = false;
                                //ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                                //lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                                dtStart.Focus();
                            }
                            else if (lblreport.Text == "14")
                            {
                                lblWarPilihRpt.Visible = false;
                                lblstatustd.Text = "Report By :";
                                BindChooseServiceType();
                                ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                                ddlStatus.Visible = true;
                                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                                ddlPilihRpt.Focus();
                            }
                        }
                        else
                        {
                            ddlPilihRpt.Focus();
                            ddlStatus.Visible = false;
                        }
                    }

                }

                lblWarPilihRpt.Visible = false;
                lblWarPilihStatus.Visible = false;
                lblWarPilih.Visible = false;

            }
        }

        private void BindChooseServiceType()
        {
            SqlConnection cnc = new SqlConnection(WebConfigurationManager.ConnectionStrings["THFCONFINS_UAT"].ConnectionString);
            if (cnc.State == ConnectionState.Closed)
            {
                cnc.Open();
            }
            else
            {
                cnc.Close();
                cnc.Dispose();
                cnc.Open();
            }
            String strCmd = "SELECT Id, Description FROM  confinsr1.safdb.dbo.TblSTNKServiceType WITH(NOLOCK)";
            SqlDataAdapter adapter = new SqlDataAdapter(strCmd, cnc);
            DataSet dr1c = new DataSet();
            adapter.Fill(dr1c, "ServiceType");

            this.ddlStatus.DataSource = dr1c;
            this.ddlStatus.DataTextField = "Description";
            this.ddlStatus.DataValueField = "Id";
            this.ddlStatus.DataBind();

            dr1c.Dispose();
            cnc.Close();
            cnc.Dispose();

            this.ddlStatus.Items.Insert(0, new ListItem("Select One", "0"));
            this.ddlStatus.Items.Insert(1, new ListItem("All", "All"));
            this.ddlStatus.SelectedIndex = 0;
        }

        private void BindSPPrinting()
        {
            this.ddlStatus.Items.Clear();
            this.ddlStatus.Items.Insert(0, new ListItem("Select One", "000"));
            this.ddlStatus.Items.Insert(1, new ListItem("All", "1"));
            this.ddlStatus.Items.Insert(2, new ListItem("SP1", "2"));
            this.ddlStatus.Items.Insert(3, new ListItem("SP2", "3"));
            this.ddlStatus.Items.Insert(4, new ListItem("SP3", "4"));
            this.ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText("Select One"));
        }

        private void BindSPBPKB()
        {
            SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["THFCONFINS_UAT"].ConnectionString);
            if (cn.State == ConnectionState.Closed)
            {
                cn.Open();
            }
            else
            {
                cn.Close();
                cn.Dispose();
                cn.Open();
            }
            SqlCommand cmd1 = new SqlCommand("spMPMFCustRptMonitoringStatus9", cn);
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.CommandTimeout = 14400;
            SqlDataReader dr1 = cmd1.ExecuteReader();
            while (dr1.Read())
            {
                this.ddlStatus.DataSource = dr1;
                this.ddlStatus.DataTextField = "StatusName";
                this.ddlStatus.DataValueField = "StatusId";
                this.ddlStatus.DataBind();

                this.ddlStatus.Items.Insert(0, new ListItem("Select One", "000"));
                this.ddlStatus.Items.Insert(1, new ListItem("All", "All"));

                this.ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText("Select One"));
            }
            dr1.Close();
            cmd1.Dispose();
            dr1.Dispose();

            cn.Close();
            cn.Dispose();
        }

        private void BindBPKBBorrow()
        {
            SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["THFCONFINS_UAT"].ConnectionString);
            if (cn.State == ConnectionState.Closed)
            {
                cn.Open();
            }
            else
            {
                cn.Close();
                cn.Dispose();
                cn.Open();
            }
            SqlCommand cmd1 = new SqlCommand("spMPMFCustRptMonitoringStatus8", cn);
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.CommandTimeout = 14400;
            SqlDataReader dr1 = cmd1.ExecuteReader();
            while (dr1.Read())
            {
                this.ddlStatus.DataSource = dr1;
                this.ddlStatus.DataTextField = "StatusName";
                this.ddlStatus.DataValueField = "StatusId";
                this.ddlStatus.DataBind();

                this.ddlStatus.Items.Insert(0, new ListItem("Select One", "000"));
                this.ddlStatus.Items.Insert(1, new ListItem("All", "All"));

                this.ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText("Select One"));
            }
            dr1.Close();
            cmd1.Dispose();
            dr1.Dispose();

            cn.Close();
            cn.Dispose();
        }

        private void BindAplikasi()
        {
            SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["THFCONFINS_UAT"].ConnectionString);
            if (cn.State == ConnectionState.Closed)
            {
                cn.Open();
            }
            else
            {
                cn.Close();
                cn.Dispose();
                cn.Open();
            }
            SqlCommand cmd1 = new SqlCommand("spMPMFCustRptMonitoringStatus7", cn);
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.CommandTimeout = 14400;
            SqlDataReader dr1 = cmd1.ExecuteReader();
            while (dr1.Read())
            {
                this.ddlStatus.DataSource = dr1;
                this.ddlStatus.DataTextField = "StatusName";
                this.ddlStatus.DataValueField = "StatusId";
                this.ddlStatus.DataBind();

                this.ddlStatus.Items.Insert(0, new ListItem("Select One", "000"));
                this.ddlStatus.Items.Insert(1, new ListItem("All", "All"));

                this.ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText("Select One"));
            }
            dr1.Close();
            cmd1.Dispose();
            dr1.Dispose();

            cn.Close();
            cn.Dispose();
        }

        private void BindVoucher()
        {
            SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["THFCONFINS_UAT"].ConnectionString);
            if (cn.State == ConnectionState.Closed)
            {
                cn.Open();
            }
            else
            {
                cn.Close();
                cn.Dispose();
                cn.Open();
            }
            SqlCommand cmd1 = new SqlCommand("spMPMFCustRptMonitoringStatus6", cn);
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.CommandTimeout = 14400;
            SqlDataReader dr1 = cmd1.ExecuteReader();
            while (dr1.Read())
            {
                this.ddlStatus.DataSource = dr1;
                this.ddlStatus.DataTextField = "StatusName";
                this.ddlStatus.DataValueField = "StatusId";
                this.ddlStatus.DataBind();

                this.ddlStatus.Items.Insert(0, new ListItem("Select One", "000"));
                this.ddlStatus.Items.Insert(1, new ListItem("All", "All"));

                this.ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText("Select One"));
            }
            dr1.Close();
            cmd1.Dispose();
            dr1.Dispose();

            cn.Close();
            cn.Dispose();
        }

        private void BindSuspend()
        {
            SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["THFCONFINS_UAT"].ConnectionString);
            if (cn.State == ConnectionState.Closed)
            {
                cn.Open();
            }
            else
            {
                cn.Close();
                cn.Dispose();
                cn.Open();
            }
            SqlCommand cmd1 = new SqlCommand("spMPMFCustRptMonitoringStatus5", cn);
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.CommandTimeout = 14400;
            SqlDataReader dr1 = cmd1.ExecuteReader();
            while (dr1.Read())
            {
                this.ddlStatus.DataSource = dr1;
                this.ddlStatus.DataTextField = "StatusName";
                this.ddlStatus.DataValueField = "StatusId";
                this.ddlStatus.DataBind();

                this.ddlStatus.Items.Insert(0, new ListItem("Select One", "000"));
                this.ddlStatus.Items.Insert(1, new ListItem("All", "All"));

                this.ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText("Select One"));
            }
            dr1.Close();
            cmd1.Dispose();
            dr1.Dispose();

            cn.Close();
            cn.Dispose();
        }

        private void BindClaimAsuransi()
        {
            SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["THFCONFINS_UAT"].ConnectionString);
            if (cn.State == ConnectionState.Closed)
            {
                cn.Open();
            }
            else
            {
                cn.Close();
                cn.Dispose();
                cn.Open();
            }
            SqlCommand cmd1 = new SqlCommand("spMPMFCustRptMonitoringStatus4", cn);
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.CommandTimeout = 14400;
            SqlDataReader dr1 = cmd1.ExecuteReader();
            while (dr1.Read())
            {
                this.ddlStatus.DataSource = dr1;
                this.ddlStatus.DataTextField = "StatusName";
                this.ddlStatus.DataValueField = "StatusId";
                this.ddlStatus.DataBind();

                this.ddlStatus.Items.Insert(0, new ListItem("Select One", "000"));
                this.ddlStatus.Items.Insert(1, new ListItem("All", "All"));

                this.ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText("Select One"));
            }
            dr1.Close();
            cmd1.Dispose();
            dr1.Dispose();

            cn.Close();
            cn.Dispose();
        }

        private void BindDokKontrak()
        {
            SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["THFCONFINS_UAT"].ConnectionString);
            if (cn.State == ConnectionState.Closed)
            {
                cn.Open();
            }
            else
            {
                cn.Close();
                cn.Dispose();
                cn.Open();
            }
            SqlCommand cmd1 = new SqlCommand("spMPMFCustRptMonitoringStatus3", cn);
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.CommandTimeout = 14400;
            SqlDataReader dr1 = cmd1.ExecuteReader();
            while (dr1.Read())
            {
                this.ddlStatus.DataSource = dr1;
                this.ddlStatus.DataTextField = "StatusName";
                this.ddlStatus.DataValueField = "StatusId";
                this.ddlStatus.DataBind();

                this.ddlStatus.Items.Insert(0, new ListItem("Select One", "000"));
                this.ddlStatus.Items.Insert(1, new ListItem("All", "All"));

                this.ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText("Select One"));
            }
            dr1.Close();
            cmd1.Dispose();
            dr1.Dispose();

            cn.Close();
            cn.Dispose();
        }

        private void BindTTR()
        {
            SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["THFCONFINS_UAT"].ConnectionString);
            if (cn.State == ConnectionState.Closed)
            {
                cn.Open();
            }
            else
            {
                cn.Close();
                cn.Dispose();
                cn.Open();
            }
            SqlCommand cmd1 = new SqlCommand("spMPMFCustRptMonitoringStatus2", cn);
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.CommandTimeout = 14400;
            SqlDataReader dr1 = cmd1.ExecuteReader();
            while (dr1.Read())
            {
                this.ddlStatus.DataSource = dr1;
                this.ddlStatus.DataTextField = "StatusName";
                this.ddlStatus.DataValueField = "StatusId";
                this.ddlStatus.DataBind();

                this.ddlStatus.Items.Insert(0, new ListItem("Select One", "000"));
                this.ddlStatus.Items.Insert(1, new ListItem("All", "All"));

                this.ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText("Select One"));
            }
            dr1.Close();
            cmd1.Dispose();
            dr1.Dispose();

            cn.Close();
            cn.Dispose();
        }

        private void BindProsesAsuransi()
        {
            SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["THFCONFINS_UAT"].ConnectionString);
            if (cn.State == ConnectionState.Closed)
            {
                cn.Open();
            }
            else
            {
                cn.Close();
                cn.Dispose();
                cn.Open();
            }
            SqlCommand cmd1 = new SqlCommand("spMPMFCustRptMonitoringStatus1", cn);
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.CommandTimeout = 14400;
            SqlDataReader dr1 = cmd1.ExecuteReader();
            while (dr1.Read())
            {
                this.ddlStatus.DataSource = dr1;
                this.ddlStatus.DataTextField = "StatusName";
                this.ddlStatus.DataValueField = "StatusId";
                this.ddlStatus.DataBind();

                this.ddlStatus.Items.Insert(0, new ListItem("Select One", "000"));
                this.ddlStatus.Items.Insert(1, new ListItem("All", "All"));

                this.ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText("Select One"));
            }
            dr1.Close();
            cmd1.Dispose();
            dr1.Dispose();

            cn.Close();
            cn.Dispose();
        }

        private void BindProsesFidusia()
        {
            SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["THFCONFINS_UAT"].ConnectionString);
            if (cn.State == ConnectionState.Closed)
            {
                cn.Open();
            }
            else
            {
                cn.Close();
                cn.Dispose();
                cn.Open();
            }
            SqlCommand cmd1 = new SqlCommand("spMPMFCustRptMonitoringStatus1", cn);
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.CommandTimeout = 14400;
            SqlDataReader dr1 = cmd1.ExecuteReader();
            while (dr1.Read())
            {
                this.ddlStatus.DataSource = dr1;
                this.ddlStatus.DataTextField = "StatusName";
                this.ddlStatus.DataValueField = "StatusId";
                this.ddlStatus.DataBind();

                this.ddlStatus.Items.Insert(0, new ListItem("Select One", "000"));
                this.ddlStatus.Items.Insert(1, new ListItem("All", "All"));

                this.ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText("Select One"));
            }
            dr1.Close();
            cmd1.Dispose();
            dr1.Dispose();

            cn.Close();
            cn.Dispose();
        }

        private void BindAllCabang()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlCabang.DataSource = ds;
            ddlCabang.DataTextField = "branchfullname";
            ddlCabang.DataValueField = "branchid";
            ddlCabang.DataBind();
            ddlCabang.SelectedIndex = 0;

            ds.Dispose();
        }

        private void BIndChooseArea()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryArea();
            ddlRegional.DataSource = ds;
            ddlRegional.DataTextField = "areafullname";
            ddlRegional.DataValueField = "AreaID";
            ddlRegional.DataBind();
            ddlRegional.SelectedIndex = 0;
            ds.Dispose();
        }

        private void BindChooseBranch()
        {
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                this.ddlPilihan.Items.Clear();
                this.ddlPilihan.Items.Insert(0, new ListItem("Select One", "0"));
                this.ddlPilihan.Items.Insert(1, new ListItem("Cabang", "1"));
                this.ddlPilihan.Items.Insert(2, new ListItem("Regional", "2"));

                this.ddlPilihan.SelectedIndex = 0;
            }
            else
            {
                DataSet ds = new DataSet();
                QueryConnection qry = new QueryConnection();
                ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
                ddlCabang.DataSource = ds;
                ddlCabang.DataTextField = "branchfullname";
                ddlCabang.DataValueField = "branchid";
                ddlCabang.DataBind();
                if (base.CurrentUserContext.OfficeCode.ToString() == "999")
                {
                    ddlCabang.Items.Insert(0, new ListItem("ALL", "ALL"));
                }
                ddlCabang.Items.Insert(0, new ListItem("ALL", "ALL"));
                ddlCabang.SelectedIndex = 0;
                

                //this.ddlPilihan.DataSource = dr1c;
                //this.ddlPilihan.DataTextField = "BranchFullName";
                //this.ddlPilihan.DataValueField = "BranchId";
                //dr1c.Dispose();
                //cnc.Close();
                //this.ddlPilihan.DataBind();

                //this.ddlPilihan.SelectedIndex = 0;

                //cnc.Dispose();
            }
        }

        protected void ddlPilihan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPilihan.SelectedValue == "0")
            {
                pilih = "";
                ddlPilihan.Focus();
                ddlCabang.Visible = false;
                ddlRegional.Visible = false;
                ddlCabang.ClearSelection();
                ddlRegional.ClearSelection();
                lblWarPilih.Visible = true;
            }

            if (ddlPilihan.SelectedValue == "1")
            {
                ddlCabang.ClearSelection();
                ddlRegional.ClearSelection();
                pilih = "";
                BindAllCabang();
                ddlCabang.Visible = true;
                ddlCabang.Focus();
                ddlRegional.Visible = false;
                lblWarPilih.Visible = false;

            }
            if (ddlPilihan.SelectedValue == "2")
            {
                ddlCabang.ClearSelection();
                ddlRegional.ClearSelection();
                pilih = "";
                ddlCabang.Visible = false;
                ddlRegional.Visible = true;
                ddlRegional.Focus();
                lblWarPilih.Visible = false;
            }
        }

        protected void ddlRegional_SelectedIndexChanged(object sender, EventArgs e)
        {
            pilih = "";
            lblWarPilih.Visible = false;
            pilih = ddlRegional.SelectedValue.ToString().Trim();
        }

        protected void ddlPilihRpt_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPilihan.SelectedValue == "0")
            {
                pilih = "";
                lblstatustd.Text = "Pilihan Status :";
                ddlPilihan.Focus();
                ddlCabang.Visible = false;
                ddlRegional.Visible = false;
                lblWarPilih.Visible = true;
                ddlPilihRpt.ClearSelection();
                ddlRegional.ClearSelection();
                ddlCabang.ClearSelection();
                lblWarPilihRpt.Visible = false;
                ddlStatus.Visible = false;
                return;
            }
            else
            { lblWarPilih.Visible = false; }

            if (ddlPilihRpt.SelectedValue == "0")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = true;
                ddlStatus.Visible = false;
                this.ddlStatus.DataTextField = "";
                this.ddlStatus.DataValueField = "";
                lblPilihRpt.Text = "";
                ddlPilihRpt.Focus();
                return;
            }
            else if (ddlPilihRpt.SelectedValue == "1")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindProsesFidusia();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "2")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindProsesAsuransi();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "3")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindTTR();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "4")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindDokKontrak();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "5")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindClaimAsuransi();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "6")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindSuspend();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "7")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindVoucher();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "8")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindAplikasi();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "9")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindBPKBBorrow();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "10")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindSPBPKB();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "11")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindSPPrinting();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "12")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindSPPrinting();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "13")
            {
                lblstatustd.Text = "";
                ddlStatus.Visible = false;
                lblWarPilihRpt.Visible = false;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "14")
            {
                lblstatustd.Text = "Report By :";
                lblWarPilihRpt.Visible = false;
                BindChooseServiceType();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();

            }
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string datepicker = "";
            string datepicker1 = "";
            string pilihn = "";

            if (ddlPilihan.SelectedValue == "0")
            {
                pilih = "";
                ddlPilihan.Focus();
                ddlCabang.Visible = false;
                ddlRegional.Visible = false;
                lblWarPilih.Visible = true;
                return;
            }
            else
            { lblWarPilih.Visible = false; }

            if (ddlPilihRpt.SelectedValue == "0")
            {
                lblWarPilihRpt.Visible = true;
                ddlStatus.Visible = false;
                this.ddlStatus.DataTextField = "";
                this.ddlStatus.DataValueField = "";
                lblPilihRpt.Text = "";
                ddlPilihRpt.Focus();
                return;
            }
            else
            {
                lblWarPilihRpt.Visible = false;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }

            if (ddlStatus.SelectedValue == "000" && lblPilihRpt.Text != "LAPORAN MONITORING SURAT TUGAS/KUASA")
            {
                lblWarPilihStatus.Visible = true;
                ddlStatus.Focus();
                lblStatus.Text = "";
                lblStatusID.Text = "";
                return;
            }
            else
            {
                if (lblPilihRpt.Text == "LAPORAN MONITORING SURAT TUGAS/KUASA")
                {
                    lblWarPilihStatus.Visible = false;
                    lblStatus.Text = "";
                    lblStatusID.Text = "";
                }
                else
                {
                    lblWarPilihStatus.Visible = false;
                    lblStatus.Text = ddlStatus.SelectedItem.Text.ToString().Trim();
                    lblStatusID.Text = ddlStatus.SelectedValue.ToString().Trim();
                }

            }

            if (dtStart.Text == "")
            {
                datepicker = DateTime.Now.ToString("dd/MM/yyyy");
            }
            else
            {
                datepicker = dtStart.Text;
            }

            if (dtEnd.Text == "")
            {
                datepicker1 = DateTime.Now.ToString("dd/MM/yyyy");
            }
            else
            {
                datepicker1 = dtEnd.Text;
            }

            if (DateTime.ParseExact(datepicker, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(datepicker1, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                Response.Write(" <script language=\"javascript\" type=\"text/javascript\"> alert('Tanggal Awal tidak boleh lebih besar dari Tanggal Akhir... !'); </script>");
                dtStart.Focus();
                return;
            }

            if (ddlPilihan.SelectedValue == "1")
            {
                Response.Redirect("vLaporanMonitoring.aspx?tglfrom=" + datepicker.ToString() + "&tglto=" + datepicker1.ToString() + "&choose=" + ddlPilihan.SelectedItem.Text.ToString().Trim() + "&pilihan=" + ddlCabang.SelectedValue.ToString().Trim() + "&pilihn=" + ddlCabang.SelectedItem.Text.ToString().Trim() + "&rpt=" + ddlPilihRpt.SelectedValue.ToString().Trim() + "&lblrpt=" + lblPilihRpt.Text.ToString().Trim().ToUpper() + "&statusname=" + lblStatus.Text.ToString().Trim() + "&statusid=" + lblStatusID.Text.ToString().Trim() + "");
            }

            else if (ddlPilihan.SelectedValue == "2")
            {
                pilihn = "-";
                Response.Redirect("vLaporanMonitoring.aspx?tglfrom=" + datepicker.ToString() + "&tglto=" + datepicker1.ToString() + "&choose=" + ddlPilihan.SelectedItem.Text.ToString().Trim() + "&pilihan=" + "R" + ddlRegional.SelectedValue.ToString().Trim() + "&pilihn=" + ddlRegional.SelectedItem.ToString().Trim() + "&rpt=" + ddlPilihRpt.SelectedValue.ToString().Trim() + "&lblrpt=" + lblPilihRpt.Text.ToString().Trim().ToUpper() + "&statusname=" + lblStatus.Text.ToString().Trim() + "&statusid=" + ddlStatus.SelectedValue.ToString().Trim() + "");
            }

            else
            {
                pilihn = "-";
                Response.Redirect("vLaporanMonitoring.aspx?tglfrom=" + datepicker.ToString() + "&tglto=" + datepicker1.ToString() + "&choose=" + ddlPilihan.SelectedItem.Text.ToString().Trim() + "&pilihan=" + ddlPilihan.SelectedValue.ToString().Trim() + "&pilihn=" + ddlPilihan.SelectedItem.Text.ToString().Trim() + "&rpt=" + ddlPilihRpt.SelectedValue.ToString().Trim() + "&lblrpt=" + lblPilihRpt.Text.ToString().Trim().ToUpper() + "&statusname=" + lblStatus.Text.ToString().Trim() + "&statusid=" + ddlStatus.SelectedValue.ToString().Trim() + "");
            }
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("LaporanMonitoring.aspx");
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlStatus.SelectedValue == "000")
            {
                lblWarPilihStatus.Visible = true;
                ddlStatus.Focus();
                lblStatus.Text = "";
                lblStatusID.Text = "";
                return;
            }
            else
            {
                lblWarPilihStatus.Visible = false;
                lblStatus.Text = ddlStatus.SelectedItem.Text.ToString().Trim();
                lblStatusID.Text = ddlStatus.SelectedValue.ToString().Trim();
            }
        }
    }
}