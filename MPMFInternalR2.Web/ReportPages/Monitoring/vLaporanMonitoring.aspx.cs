﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.Monitoring
{
    public partial class vLaporanMonitoring : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            loaddata();
        }

        private void loaddata()
        {
            string report = Request.QueryString["rpt"];
            lbltglfrom.Text = Request.QueryString["tglfrom"];
            lbltglto.Text = Request.QueryString["tglto"];
            lblpilihan.Text = Request.QueryString["pilihan"];
            lblpilihn.Text = Request.QueryString["pilihn"];
            lblreport.Text = Request.QueryString["rpt"];
            txtreport.Text = Request.QueryString["lblrpt"];
            lblstatusid.Text = Request.QueryString["statusid"];
            lblstatusname.Text = Request.QueryString["statusname"];
            lblchoose.Text = Request.QueryString["choose"];

            string aa = lblchoose.Text.ToString().Trim();
            string bb = lblpilihan.Text.ToString().Trim();
            string cc = lblpilihn.Text.ToString().Trim();

            if (report == "1")
            {
                Response.Redirect("vLaporanMonitoringPF.aspx?tglfrom=" + lbltglfrom.Text + "&tglto=" + lbltglto.Text + "&choose=" + lblchoose.Text.ToString().Trim() + "&pilihan=" + lblpilihan.Text.ToString().Trim() + "&pilihn=" + lblpilihn.Text.ToString().Trim() + "&rpt=" + lblreport.Text.ToString().Trim() + "&lblrpt=" + txtreport.Text.ToString().Trim() + "&statusname=" + lblstatusname.Text.ToString().Trim() + "&statusid=" + lblstatusid.Text.ToString().Trim() + "");
            }

            else if (report == "2")
            {
                Response.Redirect("vLaporanMonitoringPA.aspx?tglfrom=" + lbltglfrom.Text + "&tglto=" + lbltglto.Text + "&choose=" + lblchoose.Text.ToString().Trim() + "&pilihan=" + lblpilihan.Text.ToString().Trim() + "&pilihn=" + lblpilihn.Text.ToString().Trim() + "&rpt=" + lblreport.Text.ToString().Trim() + "&lblrpt=" + txtreport.Text.ToString().Trim() + "&statusname=" + lblstatusname.Text.ToString().Trim() + "&statusid=" + lblstatusid.Text.ToString().Trim() + "");
            }

            else if (report == "3")
            {
                Response.Redirect("vLaporanMonitoringTTR.aspx?tglfrom=" + lbltglfrom.Text + "&tglto=" + lbltglto.Text + "&choose=" + lblchoose.Text.ToString().Trim() + "&pilihan=" + lblpilihan.Text.ToString().Trim() + "&pilihn=" + lblpilihn.Text.ToString().Trim() + "&rpt=" + lblreport.Text.ToString().Trim() + "&lblrpt=" + txtreport.Text.ToString().Trim() + "&statusname=" + lblstatusname.Text.ToString().Trim() + "&statusid=" + lblstatusid.Text.ToString().Trim() + "");
            }

            else if (report == "4")
            {
                Response.Redirect("vLaporanMonitoringPPK.aspx?tglfrom=" + lbltglfrom.Text + "&tglto=" + lbltglto.Text + "&choose=" + lblchoose.Text.ToString().Trim() + "&pilihan=" + lblpilihan.Text.ToString().Trim() + "&pilihn=" + lblpilihn.Text.ToString().Trim() + "&rpt=" + lblreport.Text.ToString().Trim() + "&lblrpt=" + txtreport.Text.ToString().Trim() + "&statusname=" + lblstatusname.Text.ToString().Trim() + "&statusid=" + lblstatusid.Text.ToString().Trim() + "");
            }

            else if (report == "5")
            {
                Response.Redirect("vLaporanMonitoringCI.aspx?tglfrom=" + lbltglfrom.Text + "&tglto=" + lbltglto.Text + "&choose=" + lblchoose.Text.ToString().Trim() + "&pilihan=" + lblpilihan.Text.ToString().Trim() + "&pilihn=" + lblpilihn.Text.ToString().Trim() + "&rpt=" + lblreport.Text.ToString().Trim() + "&lblrpt=" + txtreport.Text.ToString().Trim() + "&statusname=" + lblstatusname.Text.ToString().Trim() + "&statusid=" + lblstatusid.Text.ToString().Trim() + "");
            }

            else if (report == "6")
            {
                Response.Redirect("vLaporanMonitoringSus.aspx?tglfrom=" + lbltglfrom.Text + "&tglto=" + lbltglto.Text + "&choose=" + lblchoose.Text.ToString().Trim() + "&pilihan=" + lblpilihan.Text.ToString().Trim() + "&pilihn=" + lblpilihn.Text.ToString().Trim() + "&rpt=" + lblreport.Text.ToString().Trim() + "&lblrpt=" + txtreport.Text.ToString().Trim() + "&statusname=" + lblstatusname.Text.ToString().Trim() + "&statusid=" + lblstatusid.Text.ToString().Trim() + "");
            }

            else if (report == "7")
            {
                Response.Redirect("vLaporanMonitoringAV.aspx?tglfrom=" + lbltglfrom.Text + "&tglto=" + lbltglto.Text + "&choose=" + lblchoose.Text.ToString().Trim() + "&pilihan=" + lblpilihan.Text.ToString().Trim() + "&pilihn=" + lblpilihn.Text.ToString().Trim() + "&rpt=" + lblreport.Text.ToString().Trim() + "&lblrpt=" + txtreport.Text.ToString().Trim() + "&statusname=" + lblstatusname.Text.ToString().Trim() + "&statusid=" + lblstatusid.Text.ToString().Trim() + "");
            }

            else if (report == "8")
            {
                Response.Redirect("vLaporanMonitoringAI.aspx?tglfrom=" + lbltglfrom.Text + "&tglto=" + lbltglto.Text + "&choose=" + lblchoose.Text.ToString().Trim() + "&pilihan=" + lblpilihan.Text.ToString().Trim() + "&pilihn=" + lblpilihn.Text.ToString().Trim() + "&rpt=" + lblreport.Text.ToString().Trim() + "&lblrpt=" + txtreport.Text.ToString().Trim() + "&statusname=" + lblstatusname.Text.ToString().Trim() + "&statusid=" + lblstatusid.Text.ToString().Trim() + "");
            }

            else if (report == "9")
            {
                Response.Redirect("vLaporanMonitoringBB.aspx?tglfrom=" + lbltglfrom.Text + "&tglto=" + lbltglto.Text + "&choose=" + lblchoose.Text.ToString().Trim() + "&pilihan=" + lblpilihan.Text.ToString().Trim() + "&pilihn=" + lblpilihn.Text.ToString().Trim() + "&rpt=" + lblreport.Text.ToString().Trim() + "&lblrpt=" + txtreport.Text.ToString().Trim() + "&statusname=" + lblstatusname.Text.ToString().Trim() + "&statusid=" + lblstatusid.Text.ToString().Trim() + "");
            }

            else if (report == "10")
            {
                Response.Redirect("vLaporanMonitoringSB.aspx?tglfrom=" + lbltglfrom.Text + "&tglto=" + lbltglto.Text + "&choose=" + lblchoose.Text.ToString().Trim() + "&pilihan=" + lblpilihan.Text.ToString().Trim() + "&pilihn=" + lblpilihn.Text.ToString().Trim() + "&rpt=" + lblreport.Text.ToString().Trim() + "&lblrpt=" + txtreport.Text.ToString().Trim() + "&statusname=" + lblstatusname.Text.ToString().Trim() + "&statusid=" + lblstatusid.Text.ToString().Trim() + "");
            }

            else if (report == "11")
            {
                Response.Redirect("vLaporanMonitoringSPPrinting.aspx?tglfrom=" + lbltglfrom.Text + "&tglto=" + lbltglto.Text + "&choose=" + lblchoose.Text.ToString().Trim() + "&pilihan=" + lblpilihan.Text.ToString().Trim() + "&pilihn=" + lblpilihn.Text.ToString().Trim() + "&rpt=" + lblreport.Text.ToString().Trim() + "&lblrpt=" + txtreport.Text.ToString().Trim() + "&statusname=" + lblstatusname.Text.ToString().Trim() + "&statusid=" + lblstatusid.Text.ToString().Trim() + "");
            }
            else if (report == "12")
            {
                Response.Redirect("vLaporanMonitoringSPCollection.aspx?tglfrom=" + lbltglfrom.Text + "&tglto=" + lbltglto.Text + "&choose=" + lblchoose.Text.ToString().Trim() + "&pilihan=" + lblpilihan.Text.ToString().Trim() + "&pilihn=" + lblpilihn.Text.ToString().Trim() + "&rpt=" + lblreport.Text.ToString().Trim() + "&lblrpt=" + txtreport.Text.ToString().Trim() + "&statusname=" + lblstatusname.Text.ToString().Trim() + "&statusid=" + lblstatusid.Text.ToString().Trim() + "");
            }
            else if (report == "13")
            {
                Response.Redirect("vLaporanMonitoringSrtTgs.aspx?tglfrom=" + lbltglfrom.Text + "&tglto=" + lbltglto.Text + "&choose=" + lblchoose.Text.ToString().Trim() + "&pilihan=" + lblpilihan.Text.ToString().Trim() + "&pilihn=" + lblpilihn.Text.ToString().Trim() + "&rpt=" + lblreport.Text.ToString().Trim() + "&lblrpt=" + txtreport.Text.ToString().Trim() + "");
            }
            else if (report == "14")
            {
                Response.Redirect("vLaporanMonitoringSTNKProses.aspx?tglfrom=" + lbltglfrom.Text + "&tglto=" + lbltglto.Text + "&choose=" + lblchoose.Text.ToString().Trim() + "&rpt=" + lblreport.Text.ToString().Trim() + "&lblrpt=" + txtreport.Text.ToString().Trim() + "&branch=" + lblpilihan.Text.ToString().Trim() + "&branchdesc=" + lblpilihn.Text.ToString().Trim() + "&service=" + lblstatusid.Text.ToString().Trim() + "&servicedesc=" + lblstatusname.Text.ToString().Trim());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}