﻿using Confins.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.ReportEmployeeSupplier
{
    public partial class ReportEmployeeSupplier : WebFormBase
    //public partial class ReportEmployeeSupplier : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "49680921";
            if (!IsPostBack)
            {
                loadArea();
                //loadStatSupp();
                //loadStatEmpSupp();

                if (base.CurrentUserContext.OfficeCode.ToString() != "999")
                //if (Session["sesBranchId"].ToString() != "999")
                {
                    LoadBranchN();
                }
                else
                {
                    this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                }
                //loadData();
            }
        }

        private void LoadBranchN()
        {
            DataSet set = new DataSet();
            //set = new QueryConnection().QueryBranch(Session["sesBranchId"].ToString());
            set = new QueryConnection().QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            this.ddlbranch.DataSource = set;
            this.ddlbranch.DataTextField = "branchfullname";
            this.ddlbranch.DataValueField = "branchid";
            this.ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if (Session["sesBranchId"].ToString() == "999")
            {
                this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            this.ddlbranch.SelectedIndex = 0;
            set.Dispose();


        }

        private void loadArea()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            //ds = qry.QueryAreaWithBranch(Session["sesBranchId"].ToString());
            ds = qry.QueryAreaWithBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlArea.DataSource = ds;
            ddlArea.DataTextField = "areafullname";
            ddlArea.DataValueField = "AreaID";
            ddlArea.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if (Session["sesBranchId"].ToString() == "999")
            {
                ddlArea.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlArea.SelectedValue != "")
            {
                LoadBranch(ddlArea.SelectedValue.Trim());
            }
        }

        //private void loadStatSupp()
        //{
        //    DataSet ds = new DataSet();
        //    QueryConnection qry = new QueryConnection();
        //    ds = qry.QueryGetStatSupplier();
        //    ddlStatSupp.DataSource = ds;
        //    ddlStatSupp.DataTextField = "IS_ACTIVE";
        //    ddlStatSupp.DataValueField = "IS_ACTIVE";
        //    ddlStatSupp.DataBind();

        //}

        //private void loadStatEmpSupp()
        //{
        //    DataSet ds = new DataSet();
        //    QueryConnection qry = new QueryConnection();
        //    ds = qry.QueryGetStatEmpSupplier();
        //    ddlStatEmpSupp.DataSource = ds;
        //    ddlStatEmpSupp.DataTextField = "IS_ACTIVE";
        //    ddlStatEmpSupp.DataValueField = "IS_ACTIVE";
        //    ddlStatEmpSupp.DataBind();

        //}

        private void LoadBranch(string strArea)
        {
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if (Session["sesBranchId"].ToString() == "999")
            {
                DataSet ds = new DataSet();
                QueryConnection qry = new QueryConnection();
                ds = qry.QueryBranchArea(strArea);
                ddlbranch.DataSource = ds;
                ddlbranch.DataTextField = "branchfullname";
                ddlbranch.DataValueField = "branchid";
                ddlbranch.DataBind();
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                ddlbranch.SelectedIndex = 0;
            }
            else
            {
                DataSet set = new DataSet();
                //set = new QueryConnection().QueryBranch(Session["sesBranchId"].ToString());
                set = new QueryConnection().QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
                this.ddlbranch.DataSource = set;
                this.ddlbranch.DataTextField = "branchfullname";
                this.ddlbranch.DataValueField = "branchid";
                this.ddlbranch.DataBind();
                if (base.CurrentUserContext.OfficeCode.ToString() == "999")
                //if (Session["sesBranchId"].ToString() == "999")
                {
                    this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                }
                this.ddlbranch.SelectedIndex = 0;
                set.Dispose();
            }

        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            if (txtNamaSupplier.Text == "")
            {
                lblwartxtNamaSupplier.Visible = true;
                txtNamaSupplier.Focus();
                return;
            }
            else
            {
                lblwartxtNamaSupplier.Visible = false;
            }

            if (txtNamaEmployeeSupplier.Text == "")
            {
                lblwartxtNamaEmployeeSupplier.Visible = true;
                txtNamaEmployeeSupplier.Focus();
                return;
            }
            else
            {
                lblwartxtNamaEmployeeSupplier.Visible = false;
            }


            Page.ClientScript.RegisterStartupScript(
                            this.GetType(), "OpenWindow", "window.open('ReportEmployeeSupplierView.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&NamaSupplier=" + txtNamaSupplier.Text.ToString() + "&StatEmployeeSupplier=" + txtNamaEmployeeSupplier.Text.ToString() + "&StatSupplier=" + ddlStatSupp.SelectedValue + "&StatSupplierName=" + ddlStatSupp.SelectedItem.ToString().Trim() + "&StatEmpSupplier=" + ddlStatEmpSupp.SelectedValue + "&StatEmpSupplierName=" + ddlStatEmpSupp.SelectedItem.ToString().Trim() + "','_blank');", true);

        }

        protected void Lb_DownloadPDF_Click(object sender, EventArgs e)
        {
            if (txtNamaSupplier.Text == "")
            {
                lblwartxtNamaSupplier.Visible = true;
                txtNamaSupplier.Focus();
                return;
            }
            else
            {
                lblwartxtNamaSupplier.Visible = false;
            }

            if (txtNamaEmployeeSupplier.Text == "")
            {
                lblwartxtNamaEmployeeSupplier.Visible = true;
                txtNamaEmployeeSupplier.Focus();
                return;
            }
            else
            {
                lblwartxtNamaEmployeeSupplier.Visible = false;
            }
            Page.ClientScript.RegisterStartupScript(
                           this.GetType(), "OpenWindow", "window.open('ReportEmployeeSupplierPDF.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&NamaSupplier=" + txtNamaSupplier.Text.ToString() + "&StatEmployeeSupplier=" + txtNamaEmployeeSupplier.Text.ToString() + "&StatSupplier=" + ddlStatSupp.SelectedValue + "&StatSupplierName=" + ddlStatSupp.SelectedItem.ToString().Trim() + "&StatEmpSupplier=" + ddlStatEmpSupp.SelectedValue + "&StatEmpSupplierName=" + ddlStatEmpSupp.SelectedItem.ToString().Trim() + "','_blank');", true);

        }

        protected void lb_Dowmload_Click(object sender, EventArgs e)
        {
            if (txtNamaSupplier.Text == "")
            {
                lblwartxtNamaSupplier.Visible = true;
                txtNamaSupplier.Focus();
                return;
            }
            else
            {
                lblwartxtNamaSupplier.Visible = false;
            }

            if (txtNamaEmployeeSupplier.Text == "")
            {
                lblwartxtNamaEmployeeSupplier.Visible = true;
                txtNamaEmployeeSupplier.Focus();
                return;
            }
            else
            {
                lblwartxtNamaEmployeeSupplier.Visible = false;
            }
            Page.ClientScript.RegisterStartupScript(
                           this.GetType(), "OpenWindow", "window.open('ReportEmployeeSupplierEXCEL.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&NamaSupplier=" + txtNamaSupplier.Text.ToString() + "&StatEmployeeSupplier=" + txtNamaEmployeeSupplier.Text.ToString() + "&StatSupplier=" + ddlStatSupp.SelectedValue+ "&StatSupplierName=" + ddlStatSupp.SelectedItem.ToString().Trim() + "&StatEmpSupplier=" + ddlStatEmpSupp.SelectedValue + "&StatEmpSupplierName=" + ddlStatEmpSupp.SelectedItem.ToString().Trim() + "','_blank');", true);

        }

    }
}