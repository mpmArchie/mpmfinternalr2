﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.ReportEmployeeSupplier
{
    public partial class ReportEmployeeSupplierEXCEL : System.Web.UI.Page
    {
        private string area, areaname, branch, branchname, NamaSupplier, NamaEmpSupplier, StatSupplier, StatSupplierName, StatEmpSupplier, StatEmpSupplierName;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }
        private void bindreport()
        {
            string nameFIle = "";
            area = Request.QueryString["area"].ToString().Trim();
            areaname = Request.QueryString["areaname"].ToString().Trim();
            branch = Request.QueryString["branch"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            NamaSupplier = Request.QueryString["NamaSupplier"].ToString().Trim();
            NamaEmpSupplier = Request.QueryString["StatEmployeeSupplier"].ToString().Trim();
            StatSupplier = Request.QueryString["StatSupplier"].ToString().Trim();
            StatSupplierName = Request.QueryString["StatSupplierName"].ToString().Trim();
            StatEmpSupplier = Request.QueryString["StatEmpSupplier"].ToString().Trim();
            StatEmpSupplierName = Request.QueryString["StatEmpSupplierName"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            //dt = qry.QueryReportAppInJMOrder(APTypeId, APTypeName "spMPMFGetAPType");
            dt = qry.QueryGetReportDataSupplier(area, branch, NamaSupplier, NamaEmpSupplier, StatSupplier, StatEmpSupplier, "SP_RPT_DataSupplier");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;

            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportDataSupplier.rdlc");

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);

            ReportParameter p1 = new ReportParameter("area", areaname);
            ReportParameter p2 = new ReportParameter("branch", branchname);
            ReportParameter p3 = new ReportParameter("StatSupplierName", StatSupplierName);
            ReportParameter p4 = new ReportParameter("StatEmpSupplierName", StatEmpSupplierName);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);

            nameFIle = "Report Employee Supplier";
            GenerateToPDF(ReportViewer1, nameFIle);
        }

            private void GenerateToPDF(ReportViewer reportViewer1, string nameFile)
        {
            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = reportViewer1.LocalReport.Render("EXCEL", null, out contentType, out encoding, out extension, out streamIds, out warnings);

            //Download the RDLC Report in Word, Excel, PDF and Image formats.
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + nameFile + "." + extension);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
        }
    }
}