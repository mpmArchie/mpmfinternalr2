﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.Web;

namespace MPMFInternalR2.Web.ReportPages.PenerimaanARBulanan
{
    //public partial class PenerimaanArBulanan : System.Web.UI.Page
    public partial class PenerimaanArBulanan : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["sesBranchId"] = "999";
            //HttpContext.Current.Session["loginid"] = "NikoP";
            if (!this.IsPostBack)
            {
                //this.fillcbo();
                loadArea();
                cboBranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
        }

        private void fillcbo()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            cboBranch.DataSource = ds;
            cboBranch.DataTextField = "branchfullname";
            cboBranch.DataValueField = "branchid";
            cboBranch.DataBind();
            cboBranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            cboBranch.SelectedIndex = 0;

            ds.Dispose();
        }


        private void loadArea()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryArea();
            ddlArea.DataSource = ds;
            ddlArea.DataTextField = "areafullname";
            ddlArea.DataValueField = "AreaID";
            ddlArea.DataBind();
            ddlArea.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlArea.SelectedIndex = 0;
        }

        private void LoadBranch(string strArea)
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranchArea(strArea);
            cboBranch.DataSource = ds;
            cboBranch.DataTextField = "branchfullname";
            cboBranch.DataValueField = "branchid";
            cboBranch.DataBind();
            cboBranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            cboBranch.SelectedIndex = 0;
        }


        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadBranch(ddlArea.SelectedValue.Trim());
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            if ((dtStart.Text == string.Empty) | (dtEnd.Text == string.Empty))
            {
                this.lblwardate.Text = "Please Fill Period From And Period To";
                lblwardate.Visible = true; return;
            }

            if (DateTime.ParseExact(dtStart.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(dtEnd.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Response.Redirect("PenerimaanARBulananViewer.aspx?optionreport=" + "&branchId=" + cboBranch.SelectedValue.ToString().Trim()
                + "&branchname=" + cboBranch.SelectedItem.ToString().Trim()
                + "&startDate=" + dtStart.Text.ToString()
                + "&endDate=" + dtEnd.Text.ToString()
                + "&AreaId=" + ddlArea.SelectedValue.ToString().Trim() + "&AreaFullName=" + ddlArea.SelectedItem.Text.Trim()
                );
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("PenerimaanARBulanan.aspx");
        }
    }
}