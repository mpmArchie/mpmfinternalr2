﻿using Confins.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages
{
    //public partial class Report_SLA_PO : System.Web.UI.Page
    public partial class Report_SLA_PO : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session.Add("loginID", "49680921");
            //Session.Add("sesBranchId", "999");

            if (!IsPostBack)
            {
                string BranchId = base.CurrentUserContext.OfficeCode.ToString();
                //string BranchId = Session["sesBranchId"].ToString();
                loadArea(BranchId);
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));


                if (BranchId != "999")
                {
                    loadbranch();
                }
            }
        }

        private void loadArea(string BranchId)
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryAreaByBranch(BranchId);
            //ds = qry.QueryAreaByBranch(Session["sesBranchId"].ToString());
            ddlArea.DataSource = ds;
            ddlArea.DataTextField = "areafullname";
            ddlArea.DataValueField = "AreaID";
            ddlArea.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
                //if (Session["sesBranchId"].ToString() == "999")
            {
                ddlArea.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlArea.SelectedIndex = 0;

        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            //ds = qry.QueryBranch(Session["sesBranchId"].ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
                //if (Session["sesBranchId"].ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;
        }


        private void LoadBranchbyArea(string strArea)
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranchArea(strArea);
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            //if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            if (Session["sesBranchId"].ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadBranchbyArea(ddlArea.SelectedValue.Trim());
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string strStartDate = "";
            string strEndDate = "";

            if (dtStart.Text == "")
                strStartDate = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strStartDate = dtStart.Text;

            if (dtEnd.Text == "")
                strEndDate = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strEndDate = dtEnd.Text;

            DateTime date1 = DateTime.ParseExact(strStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime date2 = DateTime.ParseExact(strEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            if (DateTime.ParseExact(strStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(strEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }

            else if ((date2.Month - date1.Month) + 12 * (date2.Year - date1.Year) >= 6)
            {
                lblwardate.Text = "* Period Date may not exceed 6 months";
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }



            Response.Redirect("Report_SLA_PO_View.aspx?branchid=" + ddlbranch.SelectedValue.ToString().Trim()
                + "&ProdId=" + ddlProduct.SelectedValue.ToString().Trim() + "&ProdName=" + ddlProduct.SelectedItem.Text.Trim()
                + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim()
                + "&StartDate=" + strStartDate.ToString() + "&EndDate=" + strEndDate.ToString()
                + "&AreaId=" + ddlArea.SelectedValue.ToString().Trim() + "&AreaFullName=" + ddlArea.SelectedItem.Text.Trim()
                );




        }



        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("Report_SLA_PO.aspx");
        }
    }
}