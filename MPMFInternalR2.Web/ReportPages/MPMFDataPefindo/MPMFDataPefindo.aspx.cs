﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.Web;
using System.Globalization;

namespace MPMFInternalR2.Web.ReportPages.MPMFDataPefindo
{
   //public partial class MPMFDataPefindo : System.Web.UI.Page
   public partial class MPMFDataPefindo : WebFormBase
    {

        // Session["sesBranchId"] = "999";
            //Session["loginid"] = "49680921";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
            }
        }

        protected void lb_Print_Sync_OnClick(object sender, EventArgs e)
        {
            
            string strStartDate = "";
            string strEndDate = "";

            //if (dtStart.Text == "")
            //    strStartDate = DateTime.Now.ToString("dd/MM/yyyy");
            //else
                strStartDate = dtStart.Text;

            //if (dtEnd.Text == "")
            //    strEndDate = DateTime.Now.ToString("dd/MM/yyyy");
            //else
                strEndDate = dtEnd.Text;

            if (strEndDate != "" && strStartDate != "")
            {
                DateTime date1 = DateTime.ParseExact(strStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime date2 = DateTime.ParseExact(strEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime maxDate = date1.AddMonths(6);
                if (DateTime.ParseExact(strStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(strEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture))
                {
                    lblwardate.Visible = true; return;
                }
                else if (date2 >= maxDate)
                {
                    lblwardate.Text = "* Period Date may not exceed 6 months";
                    lblwardate.Visible = true; return;
                }
                else
                {
                    lblwardate.Visible = false;
                }
            }
            

            //if (dtStart.Text == "")
            //{
            //    lblwardate.Visible = true;
            //    dtStart.Focus();
            //    return;
            //}
            //else
            //{
            //    lblwardate.Visible = false;
            //}



            //if (dtEnd.Text == "")
            //{
            //    lblwardate.Visible = true;
            //    dtStart.Focus();
            //    return;
            //}
            //else
            //{
            //    lblwardate.Visible = false;
            //}

            //if (txtKtp.Text == "")
            //{
            //    lblwartxtKtp.Visible = true;
            //    txtKtp.Focus();
            //    return;
            //}
            //else
            //{
            //    lblwartxtKtp.Visible = false;
            //}


            //Page.ClientScript.RegisterStartupScript(
            //    this.GetType(), "OpenWindow", "window.open('MPMFDataPefindoView.aspx?search=" + txtKtp.Text.Trim() + "&period=" + strStartDate.ToString() + "&period1=" + strEndDate.ToString() + "','_blank');", true);

            Response.Redirect("MPMFDataPefindoView.aspx?search=" + txtKtp.Text.Trim() + "&period=" + strStartDate.ToString() + "&period1=" + strEndDate.ToString() + "&custid=" + txtCustId.Text.Trim() + "&custname=" + txtCustName.Text.ToString() + "&appno=" + txtAppNo.Text.ToString());
        }

        protected void lbReset_OnClick(object sender, EventArgs e)
        {

            Response.Redirect("MPMFDataPefindo.aspx");
        }
    }
}