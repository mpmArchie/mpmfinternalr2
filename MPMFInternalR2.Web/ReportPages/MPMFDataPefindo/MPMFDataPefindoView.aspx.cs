﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;

namespace MPMFInternalR2.Web.ReportPages.MPMFDataPefindo
{
    public partial class MPMFDataPefindoView : System.Web.UI.Page
    {
        private string period, period1;

        private string search, custid, custname, appno;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //this.search = base.Request.QueryString["search"].ToString().Trim();
                bindreport();
               
            }
        }

        //private void BindReport(string strSearch)
        //{
        //    DataTable dt = new DataTable();
        //    QueryConnection qry = new QueryConnection();
        //    dt = qry.QueryRptPefindo(strSearch, "spMPMFRPTDataPefindo");
        //    ReportViewer1.ProcessingMode = ProcessingMode.Local;
        //    ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/Report_Pefindo.rdlc");
        //    ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
        //    ReportViewer1.LocalReport.DataSources.Add(datasource);
        //    ReportViewer1.LocalReport.Refresh();
        //}

        private void bindreport()
        {
            
            
            search = Request.QueryString["search"].ToString().Trim();
            period = Request.QueryString["period"].ToString().Trim();
            period1 = Request.QueryString["period1"].ToString().Trim();
            custid = Request.QueryString["custid"].ToString().Trim();
            custname = Request.QueryString["custname"].ToString().Trim();
            appno = Request.QueryString["appno"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            //dt = qry.QueryReportAppInJMOrder(APTypeId, APTypeName "spMPMFGetAPType");
            dt = qry.QueryRptPefindo1(search, period, period1, custid, custname, appno, "spMPMFRPTDataPefindo");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;

            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/Report_Pefindo.rdlc");

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);

            ReportViewer1.LocalReport.DataSources.Add(datasource);
          

          

        }
    }
}