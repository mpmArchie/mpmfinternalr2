﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;

namespace MPMFInternalR2.Web.ReportPages.ARDetailClosingEOM
{
    public partial class ARDetailClosingEOMPdf : System.Web.UI.Page
    {
        private string area, areaname, cabang, cabangname, assettypeid, month, month_name, year, date;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            string nameFIle = "";
            area = Request.QueryString["area"].ToString().Trim();
            areaname = Request.QueryString["areaname"].ToString().Trim();
            cabang = Request.QueryString["branch"].ToString().Trim();
            cabangname = Request.QueryString["branchname"].ToString().Trim();
            assettypeid = Request.QueryString["assettypeid"].ToString().Trim();
            month = Request.QueryString["month"].ToString().Trim();
            month_name = Request.QueryString["month_name"].ToString().Trim();
            year = Request.QueryString["year"].ToString().Trim();
            date = DateTime.Now.ToString("dd/MM/yyyy");

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            dt = qry.QueryReportARDetailCosingEom(area, cabang, assettypeid, month, year, "sp_RPT_AR_DETAIL_CLOSING_EOM");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;


            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportARDetailClosingEOM.rdlc");
            nameFIle = "ReportARDetailClosingEOM";


            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("area", areaname);
            ReportParameter p2 = new ReportParameter("branch", cabangname);
            ReportParameter p3 = new ReportParameter("month", month_name);
            ReportParameter p4 = new ReportParameter("year", year);
            ReportParameter p5 = new ReportParameter("date", date);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);

            GenerateToPDF(ReportViewer1, nameFIle);
        }

        private void GenerateToPDF(ReportViewer reportViewer1, string nameFile)
        {
            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = reportViewer1.LocalReport.Render("PDF", null, out contentType, out encoding, out extension, out streamIds, out warnings);

            //Download the RDLC Report in Word, Excel, PDF and Image formats.
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + nameFile + "." + extension);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
        }
    }
}