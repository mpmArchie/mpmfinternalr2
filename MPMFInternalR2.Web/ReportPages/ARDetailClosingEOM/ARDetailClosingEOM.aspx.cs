﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.Web;
using System.Collections;

namespace MPMFInternalR2.Web.ReportPages.ARDetailClosingEOM
{
    //public partial class ARDetailClosingEOM : System.Web.UI.Page
    public partial class ARDetailClosingEOM : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "49680921";

            if (!IsPostBack)
            {
                loadArea();
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                loadassettype();
                LoadTahun();
                ddlTahun.SelectedValue = DateTime.Now.Year.ToString();
            }
        }

        private void loadArea()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            //ds = qry.QueryAreaWithBranch(Session["sesBranchId"].ToString());
            ds = qry.QueryAreaWithBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlArea.DataSource = ds;
            ddlArea.DataTextField = "areafullname";
            ddlArea.DataValueField = "AreaID";
            ddlArea.DataBind();
            ddlArea.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlArea.SelectedIndex = 0;
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadBranch(ddlArea.SelectedValue.Trim());
        }

        private void LoadBranch(string strArea)
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranchArea(strArea);
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbranch.SelectedIndex = 0;
        }

        protected void loadassettype()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryAssetType();
            ddlassettype.DataSource = ds;
            ddlassettype.DataTextField = "description";
            ddlassettype.DataValueField = "assettypeid";
            ddlassettype.DataBind();
            //if (Session["sesBranchId"].ToString() == "999")
            //{
            //    ddlassettype.Items.Insert(0, new ListItem("ALL", "ALL"));
            //}

            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlassettype.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlassettype.SelectedIndex = 0;

            ds.Dispose();
        }

        private void LoadTahun()
        {
            ArrayList ayear = new ArrayList();
            int yearnow = Convert.ToInt32(DateTime.Now.Year.ToString());
            for (int i = 2010; i <= yearnow; i++)
            {
                ayear.Add(i);
            }
            ddlTahun.DataSource = ayear;
            ddlTahun.DataBind();
        }

        protected void lb_Print_Sync_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (ddlTahun.SelectedValue == "")
                {
                    lblwardate.Visible = true;
                    ddlTahun.Focus();
                    return;
                }
                else
                {
                    lblwardate.Visible = false;
                }

                if (ddlMonth.SelectedValue == "")
                {
                    lblwardate.Visible = true;
                    ddlTahun.Focus();
                    return;
                }
                else
                {
                    lblwardate.Visible = false;
                }

                Page.ClientScript.RegisterStartupScript(
                    this.GetType(), "OpenWindow", "window.open('ARDetailClosingEOMView.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&assettypeid=" + ddlassettype.SelectedValue.ToString().Trim() + "&month=" + ddlMonth.SelectedValue.ToString().Trim() + "&month_name=" + ddlMonth.SelectedItem.ToString().Trim() + "&year=" + ddlTahun.SelectedValue.ToString().Trim() + "','_newtab');", true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        protected void lbReset_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("ARDetailClosingEOM.aspx");
        }

        protected void lb_Download_OnClick(object sender, EventArgs e)
        {
            try
            {

                if (ddlTahun.SelectedValue == "")
                {
                    lblwardate.Visible = true;
                    ddlTahun.Focus();
                    return;
                }
                else
                {
                    lblwardate.Visible = false;
                }

                if (ddlMonth.SelectedValue == "")
                {
                    lblwardate.Visible = true;
                    ddlTahun.Focus();
                    return;
                }
                else
                {
                    lblwardate.Visible = false;
                }

                Response.Redirect("ARDetailClosingEOMExcel.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&assettypeid=" + ddlassettype.SelectedValue.ToString().Trim() + "&month=" + ddlMonth.SelectedValue.ToString().Trim() + "&month_name=" + ddlMonth.SelectedItem.ToString().Trim() + "&year=" + ddlTahun.SelectedValue.ToString().Trim());

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        protected void lb_File_Click(object sender, EventArgs e)
        {
            try
            {

                if (ddlTahun.SelectedValue == "")
                {
                    lblwardate.Visible = true;
                    ddlTahun.Focus();
                    return;
                }
                else
                {
                    lblwardate.Visible = false;
                }

                if (ddlMonth.SelectedValue == "")
                {
                    lblwardate.Visible = true;
                    ddlTahun.Focus();
                    return;
                }
                else
                {
                    lblwardate.Visible = false;
                }

                Response.Redirect("ARDetailClosingEOMPdf.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&assettypeid=" + ddlassettype.SelectedValue.ToString().Trim() + "&dateEnd=" + "&month=" + ddlMonth.SelectedValue.ToString().Trim() + "&month_name=" + ddlMonth.SelectedItem.ToString().Trim() + "&year=" + ddlTahun.SelectedValue.ToString().Trim());

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
    }
}