﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ARDetailClosingEOM.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.ARDetailClosingEOM.ARDetailClosingEOM" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script>
        $(function () {
            $(".datepicker").datepicker({
                dateFormat: 'dd/mm/yy'
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#mySpinner').addClass('spinner');
            $(window).on("load", function () {
                setTimeout(function () {
                    $('#mySpinner').remove();
                }, 2000)
            })

        });
    </script>
    <style type="text/css">
        .auto-style1 {
            width: 316px;
        }
    </style>
    <style>
        #mySpinner {
            position: fixed;
            z-index: 999999;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: white;
            display: flex;
            justify-content: center;
            align-items: center;
            opacity: 0.8;
        }

        @keyframes spinner {
            from {
                transform: rotate(0deg);
            }

            to {
                transform: rotate(360deg);
            }
        }

        .spinner:before {
            content: '';
            box-sizing: border-box;
            position: absolute;
            top: 50%;
            left: 50%;
            width: 30px;
            height: 30px;
            margin-top: -15px;
            margin-left: -15px;
            border-radius: 50%;
            border: 1px solid #ccc;
            border-top-color: #07d;
            animation: spinner .6s linear infinite;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
        </asp:ScriptManager>
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="AR Detail Closing EOM"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">AR Detail Closing EOM</label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />

            <div id="dPDCReceive">
                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">AR Detail Closing EOM</span>
                </div>
                <div id="dSearchForm">
                    <table id="ucReportSearch_tblFixedSearch" class="formTable">
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Area</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlArea" runat="server" OnSelectedIndexChanged="ddlArea_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>

                        <tr>
                            <td class="tdDesc" style="width: 20%;">Branch</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlbranch" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:Label ID="lblwarddlbranch" runat="server" Font-Size="X-Small" ForeColor="Red" Visible="False">*) Required field</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc">Asset Type</td>
                            <td class="tdValue">
                                <asp:DropDownList ID="ddlassettype" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Periode *</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList runat="server" ID="ddlMonth" >
                                    <asp:ListItem Value="">Select One</asp:ListItem>
                                    <asp:ListItem Value="01">Januari</asp:ListItem>
                                    <asp:ListItem Value="02">Februari</asp:ListItem>
                                    <asp:ListItem Value="03">Maret</asp:ListItem>
                                    <asp:ListItem Value="04">April</asp:ListItem>
                                    <asp:ListItem Value="05">Mei</asp:ListItem>
                                    <asp:ListItem Value="06">Juni</asp:ListItem>
                                    <asp:ListItem Value="07">Juli</asp:ListItem>
                                    <asp:ListItem Value="08">Agustus</asp:ListItem>
                                    <asp:ListItem Value="09">September</asp:ListItem>
                                    <asp:ListItem Value="10">Oktober</asp:ListItem>
                                    <asp:ListItem Value="11">November</asp:ListItem>
                                    <asp:ListItem Value="12">Desember</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label runat="server"> Tahun</asp:Label>
                                <asp:DropDownList runat="server" ID="ddlTahun" ></asp:DropDownList>
                                <asp:Label ID="lblwardate" runat="server" Font-Names="verdana"
                                    Font-Size="X-Small" ForeColor="Red"
                                    Visible="False">*) Required field</asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" ToolTip="Print Synchronously"
                                    OnClick="lb_Print_Sync_OnClick"><i class="fa fa-search"></i> </asp:LinkButton>
                                <asp:LinkButton runat="server" ID="lb_File" CssClass="btnForm" ToolTip="Download to PDF" AutoPostBack="True"
                                    OnClick="lb_File_Click"><i class="fas fa-file-alt"></i> </asp:LinkButton>
                                <asp:LinkButton runat="server" ID="lb_Download" CssClass="btnForm" ToolTip="Download to Excel" AutoPostBack="True"
                                    OnClick="lb_Download_OnClick"><i class="fa fa-download"></i> </asp:LinkButton>
                                <asp:LinkButton runat="server" ID="lbReset" ToolTip="RESET" CssClass="btnForm" OnClick="lbReset_OnClick"
                                    CausesValidation="false"><i class="fa fa-refresh"></i> </asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
