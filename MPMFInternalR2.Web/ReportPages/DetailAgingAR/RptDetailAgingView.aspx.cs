﻿using Microsoft.Reporting.WebForms;
using MPMFInternalR2.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.DetailAgingAR
{
    public partial class RptDetailAgingView : System.Web.UI.Page
    {
        private string branchid, branchname, categoryid, categoryname, brand, brandname, period, status, paid, loginid;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "NikoP";
            if (!IsPostBack)

            {

                branchid = Request.QueryString["branchid"].ToString().Trim();
                branchname = Request.QueryString["branchname"].ToString().Trim();
                categoryid = Request.QueryString["categoryid"].ToString().Trim();
                categoryname = Request.QueryString["categoryname"].ToString().Trim();
                brand = Request.QueryString["brand"].ToString().Trim();
                brandname = Request.QueryString["brandname"].ToString().Trim();
                period = Request.QueryString["period"].ToString().Trim();
                status = Request.QueryString["status"].ToString().Trim();
                paid = Request.QueryString["paid"].ToString().Trim();
                loginid = Request.QueryString["loginid"].ToString().Trim();

                ReportViewer1.ProcessingMode = ProcessingMode.Local;

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptDetailAgingAR.rdlc");
                //ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/Report1.rdlc");

                DataSet ds = GetReport();

                ReportDataSource datasource = new ReportDataSource("DataSet1", ds.Tables[0]);

                ReportViewer1.LocalReport.DataSources.Clear();
                ReportParameter p1 = new ReportParameter("branchname", branchname);
                ReportParameter p2 = new ReportParameter("brandname", brandname);
                ReportParameter p3 = new ReportParameter("categoryname", categoryname);
                ReportParameter p4 = new ReportParameter("period", period);
                ReportParameter p5 = new ReportParameter("status", status);
                ReportParameter p6 = new ReportParameter("paid", paid);
                ReportParameter p7 = new ReportParameter("loginid", loginid);
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6, p7 });
                ReportViewer1.LocalReport.DataSources.Add(datasource);

            }
        }

        private DataSet GetReport()
        {
            branchid = Request.QueryString["branchid"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            categoryid = Request.QueryString["categoryid"].ToString().Trim();
            categoryname = Request.QueryString["categoryname"].ToString().Trim();
            brand = Request.QueryString["brand"].ToString().Trim();
            brandname = Request.QueryString["brandname"].ToString().Trim();
            period = Request.QueryString["period"].ToString().Trim();
            status = Request.QueryString["status"].ToString().Trim();
            paid = Request.QueryString["paid"].ToString().Trim();

            //DataTable dt = new DataTable();
            //QueryConnection qry = new QueryConnection();
            //dt = qry.QueryReportDetailAgingAR(branchid, categoryid, brand, period, status, paid, "spSAFARPergerakanRpt");

            string conString = ConfigurationManager.ConnectionStrings["THFCONFINS_UAT"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 14400;
            cmd.CommandText = "spSAFARPergerakanRpt";
            cmd.Parameters.Add("@branchID", SqlDbType.VarChar, 8000).Value = branchid;
            cmd.Parameters.Add("@assetCategory", SqlDbType.VarChar, 8000).Value = categoryid;
            cmd.Parameters.Add("@assetBrand", SqlDbType.VarChar, 8000).Value = brand;
            cmd.Parameters.Add("@period", SqlDbType.VarChar, 8000).Value = period;
            cmd.Parameters.Add("@ddlStatus", SqlDbType.VarChar, 8000).Value = status;
            cmd.Parameters.Add("@ddlPaid", SqlDbType.VarChar, 8000).Value = paid;
            using (SqlConnection con = new SqlConnection(conString))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    
                    sda.Fill(ds, "DataTable1");
                    return ds;
                    
                }
            }
        }
    }
}