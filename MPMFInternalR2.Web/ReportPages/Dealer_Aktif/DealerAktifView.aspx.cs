﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/*<03022022> - created
 * 
 * 
 * */

namespace MPMFInternalR2.Web.ReportPages.Dealer_Aktif
{
    public partial class DealerAktif_view : System.Web.UI.Page
    {

        private string area, bulan, bulanname;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }

        }

        private void bindreport()
        {
            area = Request.QueryString["area"].ToString().Trim();
            bulan = Request.QueryString["bulan"].ToString().Trim();
            bulanname = Request.QueryString["bulanname"].ToString().Trim();
           

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

                dt = qry.QueryReportDealerAktif(area, bulan, "spMPMFDealer_Aktif");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptDealerAktif.rdlc");
          

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportParameter p1 = new ReportParameter("bulan", bulan);
            ReportParameter p2 = new ReportParameter("area", area);
            ReportParameter p3 = new ReportParameter("bulanname", bulanname);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}