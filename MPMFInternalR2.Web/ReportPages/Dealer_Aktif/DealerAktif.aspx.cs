﻿using Confins.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.Dealer_Aktif
{
     //public partial class DealerAktif : System.Web.UI.Page
       public partial class DealerAktif : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //Session.Add("loginID", "MellysaK");
            //Session.Add("sesBranchId", "999");
            if (!IsPostBack)
            {
                loadData();
               
            }

        }

        private void loadData()
        {
           
            loadbulan();
            loadArea();
        }

        private void loadArea()
        {
            ddlArea.Items.Insert(0, new ListItem("Select One", "000"));
            ddlArea.Items.Insert(1, new ListItem("Regional", "regional"));
            ddlArea.Items.Insert(1, new ListItem("Branch", "branch"));
        }

        private void loadbulan()
        {
            ddlbulan.Items.Insert(0, new ListItem("Select One", "000"));
            ddlbulan.Items.Insert(1, new ListItem("January", "01"));
            ddlbulan.Items.Insert(2, new ListItem("February", "02"));
            ddlbulan.Items.Insert(3, new ListItem("Maret", "03"));
            ddlbulan.Items.Insert(4, new ListItem("April", "04"));
            ddlbulan.Items.Insert(5, new ListItem("Mei", "05"));
            ddlbulan.Items.Insert(6, new ListItem("Juni", "06"));
            ddlbulan.Items.Insert(7, new ListItem("Juli", "07"));
            ddlbulan.Items.Insert(8, new ListItem("Agustus", "08"));
            ddlbulan.Items.Insert(9, new ListItem("September", "09"));
            ddlbulan.Items.Insert(10, new ListItem("Oktober", "10"));
            ddlbulan.Items.Insert(11, new ListItem("November", "11"));
            ddlbulan.Items.Insert(12, new ListItem("Desember", "12"));

          
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            if (ddlbulan.SelectedValue.ToString() == "000")
            {
                lblwarbulan.Visible = true;
                return;
            }
            else
            {
                lblwarbulan.Visible = false;
            }

            if (ddlArea.SelectedValue.ToString() == "000")
            {
                lblwararea.Visible = true;
                return;
            }
            else
            {
                lblwararea.Visible = false;
            }



            Response.Redirect("DealerAktifView.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&bulan=" + ddlbulan.SelectedValue.ToString().Trim() + "&bulanname=" + ddlbulan.SelectedItem.ToString() + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("DealerAktif.aspx");
        }
    }
}