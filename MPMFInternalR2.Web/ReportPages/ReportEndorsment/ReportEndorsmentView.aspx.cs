﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.ReportEndorsment
{
    public partial class ReportEndorsmentView : System.Web.UI.Page
    {
        private string branch, startDate, endDate, status;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branch = Request.QueryString["branchid"].ToString().Trim();
            startDate = Request.QueryString["StartDate"].ToString().Trim();
            endDate = Request.QueryString["EndDate"].ToString().Trim();
            status = Request.QueryString["status"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            dt = qry.QueryReportspEndorsment(branch, startDate, endDate, status, "sp_RPT_Endorsment");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportEndorsment.rdlc");


            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("branchid", branch);
            ReportParameter p2 = new ReportParameter("startDate", startDate);
            ReportParameter p3 = new ReportParameter("endDate", endDate);
            ReportParameter p4 = new ReportParameter("status", status);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}