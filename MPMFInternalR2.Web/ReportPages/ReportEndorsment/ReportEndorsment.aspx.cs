﻿using Confins.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.ReportPolisEndorsment
{
    //public partial class ReportEndorsment : System.Web.UI.Page
    public partial class ReportEndorsment : WebFormBase
    { 
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session.Add("loginID", "49680921");
            //Session.Add("sesBranchId", "999");

            if (!IsPostBack)
            {
                //ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                loadData();
            }
        }

        protected void loadData()
        {
            loadbranch();
        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            //ds = qry.QueryBranch(Session["sesBranchId"].ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if(Session["sesBranchId"].ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {

            if ((dtStart.Text == string.Empty) | (dtEnd.Text == string.Empty))
            {
                this.lblwardate.Text = "Please Fill RMP Date From And RMP Date To";
                lblwardate.Visible = true; return;
            }

            if (DateTime.ParseExact(dtStart.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(dtEnd.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }

            else if ((DateTime.ParseExact(dtEnd.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture).Month - DateTime.ParseExact(dtStart.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture).Month) + 12 * (DateTime.ParseExact(dtEnd.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture).Year - DateTime.ParseExact(dtStart.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture).Year) >= 12)
            {
                lblwardate.Text = "* Period Date may not exceed 12 months";
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Response.Redirect("ReportEndorsmentView.aspx?branchid=" + ddlbranch.SelectedValue.ToString().Trim()
                + "&StartDate=" + dtStart.Text.ToString() + "&EndDate=" + dtEnd.Text.ToString()
                + "&status=" + ddlstatus.SelectedValue.ToString().Trim()
                );
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("ReportEndorsment.aspx");
        }
    }
}