﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.RptInvalidDataIntelix
{
    public partial class RptInvalidDataIntelixView : System.Web.UI.Page
    {
        private string branch, branchname, date1, date2, statusid, statusname;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            this.branch = base.Request.QueryString["branchid"].ToString().Trim();
            this.branchname = base.Request.QueryString["branchname"].ToString().Trim();
            this.date1 = base.Request.QueryString["date1"].ToString().Trim();
            this.date2 = base.Request.QueryString["date2"].ToString().Trim();
            this.statusid = base.Request.QueryString["statusid"].ToString().Trim();
            this.statusname = base.Request.QueryString["statusname"].ToString().Trim();

            DataTable dataTable = new DataTable();
            dataTable = new QueryConnection().QueryRptInvalidDataIntelix(this.branch, this.date1, this.date2, this.statusid, "spMPMFCust_RptUpdateIntelix");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptInvalidDataIntelix.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dataTable);
            ReportParameter p1 = new ReportParameter("Branch", branch);
            ReportParameter p2 = new ReportParameter("BranchName", branchname);
            ReportParameter p3 = new ReportParameter("date1", date1);
            ReportParameter p4 = new ReportParameter("date2", date2);
            ReportParameter p5 = new ReportParameter("statusname", statusname);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}