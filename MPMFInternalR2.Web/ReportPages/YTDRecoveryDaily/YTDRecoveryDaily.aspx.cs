﻿using Confins.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.YTDRecoveryDaily
{
    public partial class YTDRecoveryDaily : WebFormBase
   // public partial class YTDRecoveryDaily : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // Session["sesBranchId"] = "999";
           // Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                loadData();
            }
        }

        private void loadData()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            //ds = qry.QueryBranch(Session["sesBranchId"].ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            //if (Session["sesBranchId"].ToString() == "999")
            //{
            //    ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            //}
            ddlbranch.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {

            if (txtYear.Text == "")
            {
                lblwardate.Visible = true;
                txtYear.Focus();
                return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Response.Redirect("YTDRecoveryDailyView.aspx?optionreport=&branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim() + "&year=" + txtYear.Text + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("YTDRecoveryDaily.aspx");
        }
    }
}