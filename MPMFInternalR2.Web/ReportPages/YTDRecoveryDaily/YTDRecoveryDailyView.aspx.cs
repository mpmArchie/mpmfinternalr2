﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.YTDRecoveryDaily
{
    public partial class YTDRecoveryDailyView : System.Web.UI.Page
    {
        private string branch, branchname, year;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            this.branch = base.Request.QueryString["branchid"].ToString().Trim();
            this.branchname = base.Request.QueryString["branchname"].ToString().Trim();
            this.year = base.Request.QueryString["year"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryRptYTDRecoveryDaily(branch, year, "spYTDRecoveryrpt");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptYTDRecoveryDaily.rdlc");

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("Branch", branch);
            ReportParameter p2 = new ReportParameter("BranchName", branchname);
            ReportParameter p3 = new ReportParameter("Year", year);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3});
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}