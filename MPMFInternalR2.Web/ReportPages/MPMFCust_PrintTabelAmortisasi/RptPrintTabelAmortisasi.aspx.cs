﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;


namespace MPMFInternalR2.Web.ReportPages.MPMFCust_PrintTabelAmortisasi
{
    public partial class RptPrintTabelAmortisasi : WebFormBase
    //public partial class RptPrintTabelAmortisasi : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                loadData();
            }
        }

        protected void loadData()
        {
            loadbranch();
        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;
        }

        protected void lb_view_Sync_Click(object sender, EventArgs e)
        {
            lblwarddlbranch.Visible = false;
            if (ddlbranch.SelectedValue.ToString() == "000")
            {
                lblwarddlbranch.Visible = true;
                ddlbranch.Focus();
                return;
            }
            else { lblwarddlbranch.Visible = false; }
            //gdAgr.Visible = true;
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryAgreementCardListSet(ddlbranch.SelectedValue.ToString().Trim(), txtcari.Text.ToString().Trim(), "spMPMFCustAgrementCardList");

           
            DataTable dt = ds.Tables[0];

            if (ds.Tables != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    gvRefTableAmortisasi.DataSource = dt;
                    gvRefTableAmortisasi.DataBind();
                }

            }
           
        }
        

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("RptPrintTabelAmortisasi.aspx");
        }

        protected void gvRefTableAmortisasi_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Print")
            {
                string applicationid = Convert.ToString(e.CommandArgument.ToString());

                Response.Redirect("RptPrintTabelAmortisasiView.aspx?optionreport=" + "&applicationid=" + applicationid.ToString().Trim() + "");
            }
        }

        protected void gvRefTableAmortisasi_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvRefTableAmortisasi.PageIndex = e.NewPageIndex;
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryAgreementCardListSet(ddlbranch.SelectedValue.ToString().Trim(), txtcari.Text.ToString().Trim(), "spMPMFCustAgrementCardList");
            gvRefTableAmortisasi.DataSource = ds;
            gvRefTableAmortisasi.DataBind();
        }
    }
}