﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.ARByJT
{
    public partial class RptARByJTView : System.Web.UI.Page
    {
        private string branch, branchname, asof, areaid, areaname, loginid;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branch = Request.QueryString["branchid"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            asof = Request.QueryString["asof"].ToString().Trim();
            areaid = Request.QueryString["areaid"].ToString().Trim();
            areaname = Request.QueryString["areaname"].ToString().Trim();
            loginid = Request.QueryString["loginid"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            dt = qry.QueryReportARByJT(branch, asof, areaid, "spSAFARbyJT");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptARByJT.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("branch", branch);
            ReportParameter p2 = new ReportParameter("branchname", branchname);
            ReportParameter p3 = new ReportParameter("asof", asof);
            ReportParameter p4 = new ReportParameter("loginid", loginid);
            ReportParameter p5 = new ReportParameter("areaname", areaname);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5});
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}