﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.ARByJT
{
    public partial class RptARByJT : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["sesBranchId"] = "999";
            //HttpContext.Current.Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                loadData();
            }
        }

        private void loadData()
        {
            loadarea();
            loadbranch();
        }

        protected void ddlarea_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadbranch();
        }

        private void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranchAreaCollection(ddlarea.SelectedValue.ToString().Trim());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbranch.SelectedIndex = 0;

            ds.Dispose();
        }

        private void loadarea()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryAreaCollection(base.CurrentUserContext.OfficeCode.ToString());
            ddlarea.DataSource = ds;
            ddlarea.DataTextField = "collareafullname";
            ddlarea.DataValueField = "collareaid";
            ddlarea.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlarea.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlarea.SelectedIndex = 0;

            ds.Dispose();
        }



        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string datepicker = "";

            if (dtStart.Text == "")
                datepicker = DateTime.Now.ToString("dd/MM/yyyy");
            else
                datepicker = dtStart.Text;

            Response.Redirect("RptARByJTView.aspx?branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim() + "&asof=" + datepicker.ToString() + "&areaid=" + ddlarea.SelectedValue.ToString().Trim() + "&areaname=" + ddlarea.SelectedItem.Text.ToString().Trim() + "&loginid=" + base.CurrentUserContext.UserId.ToString() + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("RptARByJT.aspx");
        }
    }
}