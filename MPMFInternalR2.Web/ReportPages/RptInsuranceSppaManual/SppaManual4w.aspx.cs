﻿using Confins.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.RptInsuranceSppaManual
{
    //public partial class SppaManual4w : System.Web.UI.Page
         public partial class SppaManual4w : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // Session.Add("loginID", "MellysaK");
           //  Session.Add("sesBranchId", "999");

            if (!IsPostBack)
            {
                loadbranch();
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                //loadData();
            }
        }

        private void loadData()
        {
            loadbranch();
        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            //ds = qry.QueryBranch(Session["sesBranchId"].ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if(Session["sesBranchId"].ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbranch.SelectedIndex = 0;
        }

      

        private void LoadBranch(string strArea)
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranchArea(strArea);
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbranch.SelectedIndex = 0;
        }

      

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string strStartDate = "";
            string strEndDate = "";
        

            if (dtStart.Text == "")
                strStartDate = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strStartDate = dtStart.Text;

            if (dtEnd.Text == "")
                strEndDate = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strEndDate = dtEnd.Text;

            DateTime date1 = DateTime.ParseExact(strStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime date2 = DateTime.ParseExact(strEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            if (DateTime.ParseExact(strStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(strEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }

            else if ((date2.Month - date1.Month) + 12 * (date2.Year - date1.Year) >= 6)
            {
                lblwardate.Text = "* Period Date may not exceed 6 months";
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            string noAgreement = txtnoagreement.Text.ToString().Trim();
            if(string.IsNullOrEmpty(noAgreement))
            {
                noAgreement = "ALL";
            }


            Response.Redirect("SppaManual4wView.aspx?branchid=" + ddlbranch.SelectedValue.ToString().Trim()
                + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim()
                + "&StartDate=" + strStartDate.ToString() + "&EndDate=" + strEndDate.ToString()
                + "&noAgreement=" + noAgreement
                );




        }



        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("SppaManual4w.aspx");
        }
    }
}