﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.LapPenyelesaianPengaduanKonsumen
{
    public partial class LaporanPerBulanView : System.Web.UI.Page
    {
        private string BranchID, Tahun;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            Tahun = Request.QueryString["Tahun"].ToString().Trim();
            BranchID = "000";            

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            dt = qry.QueryspMPMFCustRekapLaporanKeluhanPerBulanCS(BranchID, Tahun, "spMPMFCustRekapLaporanKeluhanPerBulanCS");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/COMRptKeluhanPerBulan.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("Tahun", Tahun);
            ReportParameter p2 = new ReportParameter("BranchID", BranchID);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}