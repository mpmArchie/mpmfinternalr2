﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.LaporanProduksiAsuransi
{
    public partial class LaporanProduksiAsuransiView : System.Web.UI.Page
    {
        private string year, branch, reportType, branchdesc, reportTypedesc;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindReport();
            }
        }

        private void bindReport()
        {
            year = base.Request.QueryString["year"];
            branch = base.Request.QueryString["branch"];
            reportType = base.Request.QueryString["reportType"];
            branchdesc = base.Request.QueryString["branchdesc"];
            reportTypedesc = base.Request.QueryString["reportTypedesc"];

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            if (reportType == "4")
            {
                dt = qry.QueryRptLapProduksiAsuransi(branch, reportType, year, "spMPMFCustRptProductionInsLife");
            }
            else
            {
                dt = qry.QueryRptLapProduksiAsuransi(branch, reportType, year, "spMPMFCustRptProductionIns_new");
            }

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            if (this.reportType == "1")
            {
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptLapProduksiAsuransi1.rdlc");
            }
            else if ((this.reportType == "2") || (this.reportType == "3"))
            {
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptLapProduksiAsuransi23.rdlc");
            }
            else if (this.reportType == "4")
            {
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptLapProduksiAsuransi4.rdlc");
            }
            else if (this.reportType == "5")
            {
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptLapProduksiAsuransi5.rdlc");
            }

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("year", year);
            ReportParameter p2 = new ReportParameter("branchdesc", branchdesc);
            ReportParameter p3 = new ReportParameter("reportTypedesc", reportTypedesc);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}