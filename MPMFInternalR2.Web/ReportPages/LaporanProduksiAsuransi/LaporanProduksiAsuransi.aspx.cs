﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.Web;

namespace MPMFInternalR2.Web.ReportPages.LaporanProduksiAsuransi
{
    //public partial class LaporanProduksiAsuransi : System.Web.UI.Page
    public partial class LaporanProduksiAsuransi : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["loginid"] = "NikoP";
            //Session["sesBranchId"] = "999";
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            loadbranch();
            loadyear();
        }

        private void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();

            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;
        }

        private void loadyear()
        {
            this.ddlYear.Items.Insert(0, new ListItem("Select Year", "0"));
            int index = 1;
            int num2 = DateTime.Now.Year - 4;
            for (int i = num2 + 4; i >= num2; i--)
            {
                string text = i.ToString();
                this.ddlYear.Items.Insert(index, new ListItem(text, i.ToString()));
                index++;
            }
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            this.lblWarBranch.Visible = false;
            this.lblWarReportType.Visible = false;
            this.lblYear.Visible = false;
            if (this.ddlbranch.SelectedValue == "0")
            {
                this.ddlbranch.Focus();
                this.lblWarBranch.Visible = true;
                this.lblWarReportType.Visible = false;
                this.lblYear.Visible = false;
            }
            else if (this.ddlYear.SelectedValue == "0")
            {
                this.lblYear.Focus();
                this.lblYear.Visible = true;
                this.lblWarBranch.Visible = false;
                this.lblWarReportType.Visible = false;
            }
            else if (this.ddlReportType.SelectedValue == "0")
            {
                this.lblWarReportType.Focus();
                this.lblWarReportType.Visible = true;
                this.lblWarBranch.Visible = false;
                this.lblYear.Visible = false;
            }
            else
            {
                base.Response.Redirect("LaporanProduksiAsuransiView.aspx?year=" + this.ddlYear.SelectedItem.ToString().Trim() + "&branch=" + this.ddlbranch.SelectedValue + "&branchdesc=" + this.ddlbranch.SelectedItem.ToString().Trim() + "&reportType=" + this.ddlReportType.SelectedValue + "&reportTypedesc=" + this.ddlReportType.SelectedItem.Text.ToString().Trim());
            }

        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("LaporanProduksiAsuransi.aspx");
        }
    }
}