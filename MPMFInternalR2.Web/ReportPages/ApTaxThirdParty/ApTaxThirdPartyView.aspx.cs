﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.ApTaxThirdParty
{
    public partial class ApTaxThirdPartyView : System.Web.UI.Page
    {
        private string branch, branchname, period1, period2; 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }

        }

        protected void bindreport()
        {
            branch = base.Request.QueryString["branchid"].ToString().Trim();
            branchname = base.Request.QueryString["branchname"].ToString().Trim();
            period1 = base.Request.QueryString["period1"].ToString().Trim();
            period2 = base.Request.QueryString["period2"].ToString().Trim();

            DataTable dataTable = new DataTable();
            //dataTable = new QueryConnection().QueryRptApTaxFiduciaFee(branch, period1, period2, "spAnjCustRptFiducia");
            dataTable = new QueryConnection().QueryRptApTaxFiduciaFee(branch, period1, period2, "USPMPMFApTaxThirdParty");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptApTaxThirdParty.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dataTable);
            ReportParameter p1 = new ReportParameter("branchid", branchname);
            ReportParameter p2 = new ReportParameter("period1", period1);
            ReportParameter p3 = new ReportParameter("period2", period2);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
        
    }
}