﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.RptInvalidNumberIntelix
{
    //public partial class RptInvalidNumberIntelix : System.Web.UI.Page
    public partial class RptInvalidNumberIntelix : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //Session.Add("loginID", "MellysaK");
                //Session.Add("sesBranchId", "'999'");
                this.loadbranch();
            }
        }

        private void loadbranch()
        {
            DataSet set = new DataSet();
            set = new QueryConnection().QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            this.ddlbranch.DataSource = set;
            this.ddlbranch.DataTextField = "branchfullname";
            this.ddlbranch.DataValueField = "branchid";
            this.ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            this.ddlbranch.SelectedIndex = 0;
            set.Dispose();

        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string str = "";
            string str2 = "";

            if (dtStart.Text == "")
                str = DateTime.Now.ToString("dd/MM/yyyy");
            else
                str = dtStart.Text;

            if (dtEnd.Text == "")
                str2 = DateTime.Now.ToString("dd/MM/yyyy");
            else
                str2 = dtEnd.Text;

            if (DateTime.ParseExact(str, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(str2, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Response.Redirect("RptInvalidNumberIntelixView.aspx?optionreport=&branchid=" + this.ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + this.ddlbranch.SelectedItem.ToString().Trim() + "&date1=" + str.ToString() + "&date2=" + str2.ToString() + "&statusid=" + this.ddlstatus.SelectedValue.ToString().Trim() + "&statusname=" + this.ddlstatus.SelectedItem.ToString().Trim() + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("RptInvalidNumberIntelix.aspx");
        }
    }
}