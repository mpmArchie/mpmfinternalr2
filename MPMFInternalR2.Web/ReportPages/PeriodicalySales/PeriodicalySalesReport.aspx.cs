﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.PeriodicalySales
{
    //public partial class PeriodicalySalesReport : System.Web.UI.Page
    public partial class PeriodicalySalesReport : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["sesBranchId"] = "999";
            //HttpContext.Current.Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                loadbranch();
                loadSupplier();
            }
        }

        private void loadSupplier()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QuerySupplier();
            ddlSupplier.DataSource = ds;
            ddlSupplier.DataTextField = "suppl_name";
            ddlSupplier.DataValueField = "suppl_code";
            ddlSupplier.DataBind();
            ddlSupplier.Items.Insert(0, new ListItem("", ""));
            ddlSupplier.SelectedIndex = 0;
        }

        private void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;
        }

        protected void ddlbranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadPOS(ddlbranch.SelectedValue);
        }

        private void LoadPOS(string strBranch)
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryPOS(strBranch);
            ddlPos.DataSource = ds;
            ddlPos.DataTextField = "branchfullname";
            ddlPos.DataValueField = "branchid";
            ddlPos.DataBind();
            ddlPos.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlPos.SelectedIndex = 0;
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            if (ddlbranch.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('branch belum dipilih !!');", true);
            }
            else if (cboPeriod.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Periode belum dipilih !!');", true);
            }
            else if (txtYear.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Tahun belum diisi !!');", true);
            }
            else
            {
                Response.Redirect("PeriodicalySalesView.aspx?optionreport=" + "&branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim() + "&SupplierID=" + ddlSupplier.SelectedValue.Trim() + "&Month=" + cboPeriod.SelectedValue.Trim() + "&MonthName=" + cboPeriod.SelectedItem.Text.Trim() + "&Year=" + txtYear.Text + "&PosID=" + ddlPos.SelectedValue.Trim() + "");
            }
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("PeriodicalySales.aspx");
        }
    }
}