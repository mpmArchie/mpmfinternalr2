﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.PeriodicalySales
{
    public partial class PeriodicalySalesView : System.Web.UI.Page
    {
        private string branchid, branchname, SupplierID, Month, MonthName, Year, PosID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindReport();
            }
        }

        private void BindReport()
        {
            branchid = Request.QueryString["branchid"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            SupplierID = Request.QueryString["SupplierID"].ToString().Trim();
            Month = Request.QueryString["Month"].ToString().Trim();
            MonthName = Request.QueryString["MonthName"].ToString().Trim();
            Year = Request.QueryString["Year"].ToString().Trim();
            PosID = Request.QueryString["PosID"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryReportSpSalesReportPeriodic(branchid, SupplierID, Month, Year, PosID, "SpSalesReportPeriodic");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptDealerReport.rdlc");

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("BranchName", branchname);
            ReportParameter p2 = new ReportParameter("Periode", MonthName);
            ReportParameter p3 = new ReportParameter("Year", Year);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);

        }
    }
}