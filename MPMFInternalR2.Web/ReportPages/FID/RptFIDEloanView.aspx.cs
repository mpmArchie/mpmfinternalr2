﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.FID
{
    public partial class RptFIDEloanView : System.Web.UI.Page
    {
        private string branchid, branchname, assettype, assetname, period, reporttype, reportname;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branchid = Request.QueryString["branchid"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            assettype = Request.QueryString["assetCode"].ToString().Trim();
            assetname = Request.QueryString["assetName"].ToString().Trim();
            period = Request.QueryString["period"].ToString().Trim();
            reporttype = Request.QueryString["reportType"].ToString().Trim();
            reportname = Request.QueryString["reportName"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            //dt = qry.QueryReportFIDEloan(branchid, period, assettype, reporttype, "spMPMFCustRptFID");

            if (reporttype == "1")
            {
                dt = qry.QueryReportFIDEloan(branchid, period, assettype, reporttype, "spMPMFCustRptFID1");
            }
            else if (reporttype == "2")
            {
                dt = qry.QueryReportFIDEloan(branchid, period, assettype, reporttype, "spMPMFCustRptFID2");
            }
            else if (reporttype == "3")
            {
                dt = qry.QueryReportFIDEloan(branchid, period, assettype, reporttype, "spMPMFCustRptFID3");
            }
            else if (reporttype == "4")
            {
                dt = qry.QueryReportFIDEloan(branchid, period, assettype, reporttype, "spMPMFCustRptFID4");
            }
            else if (reporttype == "5")
            {
                if (branchid == "ALL")
                {
                    dt = qry.QueryReportFIDEloan(branchid, period, assettype, reporttype, "spMPMFCustRptFID5ALL");
                }
                else
                {
                    dt = qry.QueryReportFIDEloan(branchid, period, assettype, reporttype, "spMPMFCustRptFID5");
                }
                
            }

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            if (reporttype == "1")
            {
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RPtFIDBranchEloan.rdlc");
            }
            else if (reporttype == "4")
            {
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptFIDMOEloan.rdlc");
            }
            else
            {
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptFIDOthersEloan.rdlc");
            }
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("branchname", branchname);
            ReportParameter p2 = new ReportParameter("agdate", period);
            ReportParameter p3 = new ReportParameter("assetname", assetname);
            ReportParameter p4 = new ReportParameter("reportname", reportname);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}