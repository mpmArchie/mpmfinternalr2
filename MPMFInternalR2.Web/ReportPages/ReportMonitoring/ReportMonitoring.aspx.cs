﻿using System;
using Confins.Web;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace MPMFInternalR2.Web.ReportPages.ReportMonitoring
{
    //public partial class ReportMonitoring : System.Web.UI.Page
    public partial class ReportMonitoring : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            //Session.Add("sesBranchId", "503");

            //if (!IsPostBack)
            //{
            //    //string BranchId = base.CurrentUserContext.OfficeCode.ToString();
            //    string BranchId = Session["sesBranchId"].ToString();
            //    loadArea(BranchId);
            //    ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));


            //    if (BranchId != "999")
            //    {
            //        loadbranch();
            //    }
            //}


            //if (!IsPostBack)
            //{
            //    //string BranchId = base.CurrentUserContext.OfficeCode.ToString();
            //    string BranchId = Session["sesBranchId"].ToString();
            //    loadArea(BranchId);
            //    ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));


            //    if (BranchId != "999")
            //    {
            //        loadbranch();
            //    }
            //}

            //if (!IsPostBack)
            //{
            //    //loadArea();
            //    loadbranch();
            //    //string BranchId = base.CurrentUserContext.OfficeCode.ToString();
            //    string BranchId = Session["sesBranchId"].ToString();
            //    loadArea(BranchId);
            //    ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));


            //    if (BranchId != "999")
            //    {
            //        loadbranch();
            //    }
            //}

            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "49680921";
            if (!IsPostBack)
            {
                loadArea();

                if (base.CurrentUserContext.OfficeCode.ToString() != "999")
                //if (Session["sesBranchId"].ToString() != "999")
                {
                    loadbranch();
                }
                else
                {
                    this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                }
                //loadData();
            }
        }

        private void loadArea()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            //ds = qry.QueryAreaWithBranch(Session["sesBranchId"].ToString());
            ds = qry.QueryAreaWithBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlArea.DataSource = ds;
            ddlArea.DataTextField = "areafullname";
            ddlArea.DataValueField = "AreaID";
            ddlArea.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if (Session["sesBranchId"].ToString() == "999")
            {
                ddlArea.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
        }

        private void loadArea(string BranchId)
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryAreaByBranch(BranchId);
           // ds = qry.QueryAreaByBranch(Session["sesBranchId"].ToString());
            ddlArea.DataSource = ds;
            ddlArea.DataTextField = "areafullname";
            ddlArea.DataValueField = "AreaID";
            ddlArea.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
              //  if (Session["sesBranchId"].ToString() == "999")
            {
                ddlArea.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlArea.SelectedIndex = 0;

        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            //ds = qry.QueryBranch(Session["sesBranchId"].ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
                //if (Session["sesBranchId"].ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;
        }


        private void LoadBranchbyArea(string strArea)
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranchArea(strArea);
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
                //if (Session["sesBranchId"].ToString() == "999")
                {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadBranchbyArea(ddlArea.SelectedValue.Trim());
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string strStartDate = "";
            string strEndDate = "";

            if (dtStart.Text == "")
            //strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
            strStartDate = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strStartDate = dtStart.Text;

            if (dtEnd.Text == "")
                //strEndDate = DateTime.Now.ToString("yyyy-MM-dd");
                strEndDate = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strEndDate = dtEnd.Text;

            //DateTime date;
            //IFormatProvider culture = new CultureInfo("en-US", true);

            //DateTime date1 = (DateTime.TryParseExact(strStartDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date));
            //DateTime date2 = DateTime.ParseExact(strEndDate, "yyyy-MM-dd", culture);

            //DateTime date1 = DateTime.ParseExact(strStartDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            //DateTime date2 = DateTime.ParseExact(strEndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);

            DateTime date1 = DateTime.ParseExact(strStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime date2 = DateTime.ParseExact(strEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);


            //if (DateTime.ParseExact(strStartDate, "yyyy-MM-dd", CultureInfo.InvariantCulture) > DateTime.ParseExact(strEndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture))
             if (DateTime.ParseExact(strStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(strEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }

            else if ((date2.Month - date1.Month) + 12 * (date2.Year - date1.Year) >= 6)
            {
                lblwardate.Text = "* Period Date may not exceed 6 months";
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            if (ddljenisreport.SelectedValue == "00")
            {
                lblwarjenisreport.Visible = true;
                ddljenisreport.Focus();
                return;
            }
            else
            {
                lblwarjenisreport.Visible = false;
            }

            Response.Redirect("ReportMonitoringView.aspx?branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim()
                        + "&StartDate=" + strStartDate.ToString() + "&EndDate=" + strEndDate.ToString()
                        + "&AreaId=" + ddlArea.SelectedValue.ToString().Trim() + "&AreaFullName=" + ddlArea.SelectedItem.Text.Trim()
                        + "&JenisReport=" + ddljenisreport.SelectedValue.ToString().Trim() + "&JenisReportName=" + ddljenisreport.SelectedItem.ToString().Trim()
                        );
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("ReportMonitoring.aspx");
        }


    }
}