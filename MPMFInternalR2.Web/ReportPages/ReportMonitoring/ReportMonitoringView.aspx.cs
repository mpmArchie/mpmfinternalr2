﻿using System;
using Microsoft.Reporting.WebForms;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace MPMFInternalR2.Web.ReportPages.ReportMonitoring
{
    public partial class ReportMonitoringView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            string StartDate = Request.QueryString["StartDate"].ToString().Trim();
            string EndDate = Request.QueryString["EndDate"].ToString().Trim();
            string AreaId = Request.QueryString["AreaId"].ToString().Trim();
            string AreaName = Request.QueryString["AreaFullName"].ToString().Trim();
            string BranchId = Request.QueryString["branchid"].ToString().Trim();
            string BranchName = Request.QueryString["branchname"].ToString().Trim();
            string JenisReport = Request.QueryString["JenisReport"].ToString().Trim();
            string JenisReportName = Request.QueryString["JenisReportName"].ToString().Trim();
            string Tanggal = DateTime.Today.ToString("dd/MM/yyyy");

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            if (JenisReport == "01")
            {

                dt = qry.QueryReportMonitoring(StartDate,EndDate, AreaId, BranchId, "sp_RPT_MonitoringPO");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportMonitoringPO_DO.rdlc");
            }

            if (JenisReport == "02")
            {

                dt = qry.QueryReportMonitoring(StartDate, EndDate, AreaId, BranchId, "sp_RPT_MonitoringFiducia");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportMonitoringFiducia.rdlc");
            }

            if (JenisReport == "03")
            {

                dt = qry.QueryReportMonitoring(StartDate, EndDate, AreaId, BranchId, "sp_RPT_MonitoringCashAdvance");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportMonitoringCashAdvance.rdlc");
            }

            if (JenisReport == "04")
            {

                dt = qry.QueryReportMonitoring(StartDate, EndDate, AreaId, BranchId, "sp_RPT_MonitoringDocumentWaiting");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportMonitoringDocumentWaiting.rdlc");
            }

            if (JenisReport == "05")
            {

                dt = qry.QueryReportMonitoring(StartDate, EndDate, AreaId, BranchId, "sp_RPT_MonitoringDocumentBorrow");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportMonitoringDocumentBorrow.rdlc");
            }

            if (JenisReport == "06")
            {

                dt = qry.QueryReportMonitoring(StartDate, EndDate, AreaId, BranchId, "sp_RPT_MonitoringPDC");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportMonitoringPDC.rdlc");
            }

            if (JenisReport == "07")
            {

                dt = qry.QueryReportMonitoring(StartDate, EndDate, AreaId, BranchId, "sp_RPT_MonitoringTTR");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportMonitoringTTR.rdlc");
            }

            if (JenisReport == "08")
            {

                dt = qry.QueryReportMonitoring(StartDate, EndDate, AreaId, BranchId, "sp_RPT_MonitoringPrintKontrak");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportMonitoringPrintKontrak.rdlc");
            }

            if (JenisReport == "09")
            {

                dt = qry.QueryReportMonitoring(StartDate, EndDate, AreaId, BranchId, "sp_RPT_MonitoringPrintSP");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportMonitoringPrintSP.rdlc");
            }

            if (JenisReport == "10")
            {

                dt = qry.QueryReportMonitoring(StartDate, EndDate, AreaId, BranchId, "sp_RPT_MonitoringLapKeu");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportMonitoringLapKeu.rdlc");
            }

            if (JenisReport == "11")
            {

                dt = qry.QueryReportMonitoring(StartDate, EndDate, AreaId, BranchId, "sp_RPT_MonitoringSelishLapKeu");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportMonitoringSelisihLapKeu.rdlc");
            }

            if (JenisReport == "12")
            {

                dt = qry.QueryReportMonitoring(StartDate, EndDate, AreaId, BranchId, "sp_RPT_MonitoringStockOpnAssetDoc");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportMonitoringSOAssetDoc.rdlc");
            }

            if (JenisReport == "13")
            {

                dt = qry.QueryReportMonitoring(StartDate, EndDate, AreaId, BranchId, "sp_RPT_MonitoringStockOpnTTR");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportMonitoringSOTTR.rdlc");
            }

            if (JenisReport == "14")
            {

                dt = qry.QueryReportMonitoring(StartDate, EndDate, AreaId, BranchId, "sp_RPT_MonitoringStockOpnPDC");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportMonitoringSOPDC.rdlc");
            }

            if (JenisReport == "15")
            {

                dt = qry.QueryReportMonitoring(StartDate, EndDate, AreaId, BranchId, "sp_RPT_MonitoringSOMaterai");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportMonitoringSOMaterai.rdlc");
            }

            if (JenisReport == "16")
            {

                dt = qry.QueryReportMonitoring(StartDate, EndDate, AreaId, BranchId, "sp_RPT_MonitoringAssetInsurance");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportMonitoringAssetInsurance.rdlc");
            }

            if (JenisReport == "17")
            {

                dt = qry.QueryReportMonitoring(StartDate, EndDate, AreaId, BranchId, "sp_RPT_MonitoringLifeInsurance");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportMonitoringLifeInsurance.rdlc");
            }

            if (JenisReport == "18")
            {

                dt = qry.QueryReportMonitoring(StartDate, EndDate, AreaId, BranchId, "sp_RPT_MonitoringSuratKonfirmasiAYDA");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportMonitoringSuratKonfirmasiAYDA.rdlc");
            }

            if (JenisReport == "19")
            {

                dt = qry.QueryReportMonitoring(StartDate, EndDate, AreaId, BranchId, "sp_RPT_MonitoringBASTAYDA");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportMonitoringBestAyda.rdlc");
            }

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("StartDate", StartDate);
            ReportParameter p2 = new ReportParameter("EndDate", EndDate);
            ReportParameter p3 = new ReportParameter("AreaName", AreaName);
            ReportParameter p4 = new ReportParameter("BranchName", BranchName);
            ReportParameter p5 = new ReportParameter("JenisReport", JenisReportName);
            ReportParameter p6 = new ReportParameter("Tanggal", Tanggal);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);

        }
    }
}