﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportMonitoring.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.ReportMonitoring.ReportMonitoring" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({
                dateFormat: 'dd/mm/yy'
                //dateFormat: 'yy-mm-dd'
            });
        });
    </script>
    <style type="text/css">
        .auto-style1 {
            width: 20%;
            height: 28px;
        }
        .auto-style2 {
            width: 30%;
            height: 28px;
        }
    </style>
</head>
<body>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="ReportMonitoring"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">Report Monitoring</label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />

            <div id="dPDCReceive">
                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">Report Monitoring</span>
                </div>
                <div id="dSearchForm">
                    <table id="ucReportSearch_tblFixedSearch" class="formTable">
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Periode</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:TextBox runat="server" AutoCompleteType="Enabled" ID="dtStart" CssClass="datepicker"></asp:TextBox>
                                &nbsp;s/d
                                <asp:TextBox runat="server" AutoCompleteType="Enabled" ID="dtEnd" CssClass="datepicker"></asp:TextBox>
                                <asp:Label ID="lblwardate" runat="server" Font-Names="verdana"
                                        Font-Size="X-Small" ForeColor="Red"
                                        Style="font-family: verdana, Arial, tahoma, san-serif; font-size: 11px;"
                                        Visible="False">* Please check start and end dates</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1">Regional</td>
                            <td class="auto-style2">
                                <asp:DropDownList ID="ddlArea" runat="server" OnSelectedIndexChanged="ddlArea_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Cabang</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlbranch" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Jenis Report </td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddljenisreport" runat="server">
                                    <asp:ListItem Value="00">Select One</asp:ListItem>
                                    <asp:ListItem Value="01" >PO DO</asp:ListItem>
                                    <asp:ListItem Value="02">Fiducia</asp:ListItem>
                                    <asp:ListItem Value="03">Cash Advance</asp:ListItem>
                                    <asp:ListItem Value="04">Document Waiting</asp:ListItem>
                                    <asp:ListItem Value="05">Document Borrow</asp:ListItem>
                                    <asp:ListItem Value="06">PDC</asp:ListItem>
                                    <asp:ListItem Value="07">TTR</asp:ListItem>
                                    <asp:ListItem Value="08">Print Kontrak</asp:ListItem>
                                    <asp:ListItem Value="09">Print SP</asp:ListItem>
                                    <asp:ListItem Value="10">Laporan Keuangan</asp:ListItem>
                                    <asp:ListItem Value="11">Ket Selisih Lap Keuangan</asp:ListItem>
                                    <asp:ListItem Value="12">Stock Opname Asset Doc</asp:ListItem>
                                    <asp:ListItem Value="13">Stock Opname TTR</asp:ListItem>
                                    <asp:ListItem Value="14">Stock Opname PDC</asp:ListItem>
                                    <asp:ListItem Value="15">SO Materai</asp:ListItem>
                                    <asp:ListItem Value="16">Asset Asuransi</asp:ListItem>
                                    <asp:ListItem Value="17">Life Insurance</asp:ListItem>
                                    <asp:ListItem Value="18">Surat Konfirmasi Pelunasan AYDA</asp:ListItem>
                                    <asp:ListItem Value="19">Bast AYDA</asp:ListItem>

                                </asp:DropDownList>
                                <asp:Label ID="lblwarjenisreport" runat="server" Font-Size="X-Small" ForeColor="Red" Visible="False" Font-Names="Verdana">*) Required field</asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                            OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
