﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.AssetDocumentStatus
{
    
    public partial class AssetDocumentStatusView : System.Web.UI.Page
    {
        private string branchid, branchname, docstat, docstatname, ismaidoc, ismaidocname, officeName, printby, coyname, date;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.branchid = base.Request.QueryString["branchid"].ToString().Trim();
                this.branchname = base.Request.QueryString["branchname"].ToString().Trim();
                this.docstat = base.Request.QueryString["docstat"].ToString().Trim();
                this.docstatname = base.Request.QueryString["docstatname"].ToString().Trim();
                this.ismaidoc = base.Request.QueryString["ismaidoc"].ToString().Trim();
                this.ismaidocname = base.Request.QueryString["ismaidocname"].ToString().Trim();
                this.officeName = base.Request.QueryString["officeName"].ToString().Trim();
                this.printby = base.Request.QueryString["printby"].ToString().Trim();
                this.coyname = base.Request.QueryString["coyname"].ToString().Trim();

                DataSet ds = new DataSet();
                QueryConnection qry = new QueryConnection();
                ds = qry.QueryDate();
                this.date = ds.Tables[0].Rows[0]["date"].ToString();

                BindReport(branchid, branchname, docstat, docstatname, ismaidoc, ismaidocname, officeName, printby, coyname, date);
            }
        }

        private void BindReport(string branchid, string branchname, string docstat, string docstatname, string ismaidoc, string ismaidocname, string officeName, string printby, string coyname, string date)
        {
            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryRptAssetDocumentStatus(branchid, docstat, ismaidoc, "spMPMFRPTAssetDocumentStatus");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/AssetDocumentStatus.rdlc");
            ReportDataSource datasource = new ReportDataSource("DT_AssetDocStat", dt);
            ReportParameter p1 = new ReportParameter("CoyName", coyname);
            ReportParameter p2 = new ReportParameter("OfficeName", officeName);
            ReportParameter p3 = new ReportParameter("FilterBy", "Office Name: " + branchname + " AND Document Status : " + docstatname + " AND  Is Main Doc Only : " + ismaidocname);
            ReportParameter p4 = new ReportParameter("UserName", printby);
            ReportParameter p5 = new ReportParameter("SystemDate", date);
            ReportParameter p6 = new ReportParameter("AsOfDate", date);
            ReportParameter p7 = new ReportParameter("DocStat", docstat);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6, p7 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
            ReportViewer1.LocalReport.Refresh();
        }
    }
}