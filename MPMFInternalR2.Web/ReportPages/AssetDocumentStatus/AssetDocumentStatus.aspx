<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AssetDocumentStatus.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.AssetDocumentStatus.AssetDocumentStatus" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'M-yy',
                onClose: function (dateText, inst) {
                    $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                }
            });
        });
        function isnumerickey(e) {

            var txt = window.event.keyCode;
            if (txt > 31 && (txt < 48 || txt > 57)) {
                if (txt == 46) {
                    return true;
                }
                else {
                    return false;
                }
            }
            return true;
        }
    </script>
</head>
<body>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="Report/AssetDocumentStatus"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">ASSET DOCUMENT STATUS</label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />

            <div id="dPDCReceive">
                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">Search</span>
                </div>

                <div id="dSearchForm">
                    <table id="ucReportSearch_tblFixedSearch" class="formTable">
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Agreement Office Name</td>
                            <td class="tdValue" style="width: 80%;">
                                <asp:DropDownList ID="ddlbranch" runat="server">
                                </asp:DropDownList>
                                <asp:Label ID="lblWarBranch" runat="server" Font-Size="X-Small" ForeColor="Red" Visible="False">* This field is required</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Document Status</td>
                            <td class="tdValue" style="width: 80%;">
                               <asp:DropDownList ID="ddlDocumentStatus" runat="server">
                                </asp:DropDownList>
                                <asp:Label ID="lblwarStatus" runat="server" Font-Size="X-Small" ForeColor="Red" Visible="False">*) Required field</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">IsMainDocOnly</td>
                            <td class="tdValue" style="width: 80%;">
                               <asp:DropDownList ID="ddlIsMainDoc" runat="server">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="Label1" runat="server" Font-Size="X-Small" ForeColor="Red" Visible="False">*) Required field</asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                            OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
