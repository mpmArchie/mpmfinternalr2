﻿using Confins.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.AssetDocumentStatus
{
    //public partial class AssetDocumentStatus : System.Web.UI.Page
    public partial class AssetDocumentStatus : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                loadData();
                loadDocStat();
            }
        }

        private void loadDocStat()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryDocStat();
            ddlDocumentStatus.DataSource = ds;
            ddlDocumentStatus.DataTextField = "DESCR";
            ddlDocumentStatus.DataValueField = "MASTER_CODE";
            ddlDocumentStatus.DataBind();
            ddlDocumentStatus.Items.Insert(0, new ListItem("All", ""));
            ddlDocumentStatus.SelectedIndex = 0;

            ds.Dispose();
        }

        private void loadData()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            //ds = qry.QueryBranch(Session["sesBranchId"].ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("Select One", ""));
            }
            //if (Session["sesBranchId"].ToString() == "999")
            //{
            //    ddlbranch.Items.Insert(0, new ListItem("Select One", ""));
            //}
            ddlbranch.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            if (ddlbranch.SelectedValue == "")
            {
                lblWarBranch.Visible = true;
                ddlbranch.Focus();
                return;
            }
            else
            {
                lblWarBranch.Visible = false;
            }

            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryCoyName();

            string strCoyName = ds.Tables[0].Rows[0]["FULL_NAME"].ToString();
            //string strPrintBy = Session["loginid"].ToString();
            string strPrintBy = base.CurrentUserContext.FullName.ToString();
            //string strOfficeName = Session["sesBranchId"].ToString();
            string strOfficeName = base.CurrentUserContext.OfficeName.ToString();

            Response.Redirect("AssetDocumentStatusView.aspx?optionreport=&branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim() + "&docstat=" + ddlDocumentStatus.SelectedValue + "&docstatname=" + ddlDocumentStatus.SelectedItem.ToString() + "&ismaidoc=" + ddlIsMainDoc.SelectedValue + "&ismaidocname=" + ddlIsMainDoc.SelectedItem.ToString() + "&officeName=" + strOfficeName + "&printby=" + strPrintBy + "&coyname=" + strCoyName + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("AssetDocumentStatus.aspx");
        }
    }
}