﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages
{
    public partial class RptFiduciaList : WebFormBase
    //public partial class RptFiduciaList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            if (!IsPostBack)
            {
                //HttpContext.Current.Session["sesBranchId"] = "999";
                //HttpContext.Current.Session["loginid"] = "NikoP";
                loadData();
            }
        }

        private void loadData()
        {
            loadbranch();
            loadNotaris();
        }

        private void loadNotaris()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryNotary();
            ddlnotary.DataSource = ds;
            ddlnotary.DataTextField = "notaryname";
            ddlnotary.DataValueField = "notaryname";
            ddlnotary.DataBind();
            ddlnotary.Items.Insert(000, "Select One");
            ddlnotary.SelectedIndex = 0;

            ds.Dispose();
        }

        private void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            ddlbranch.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string strDateStart = "";
            string strDateEnd = "";
            if (ddlnotary.SelectedIndex.ToString() == "0")
            {
                lblwarnotary.Visible = true; return;
            }
            else
            {
                lblwarnotary.Visible = false;
            }

            if (dtStart.Text == "")
                strDateStart = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strDateStart = dtStart.Text;

            if (dtEnd.Text == "")
                strDateEnd = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strDateEnd = dtEnd.Text;


            if (DateTime.ParseExact(strDateStart, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(strDateEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Response.Redirect("RptFiduciaListView.aspx?optionreport=" + "&branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim() + "&period=" + strDateStart + "&period1=" + strDateEnd + "&notaryid=" + ddlnotary.SelectedValue.ToString() + "&notaryname=" + ddlnotary.SelectedItem.Text.ToString() + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("RptFiduciaList.aspx");
        }
    }
}