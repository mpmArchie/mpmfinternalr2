﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.Web;

namespace MPMFInternalR2.Web.ReportPages.YTDWriteOffReport
{
    //public partial class YTDWriteOffReport : System.Web.UI.Page
    public partial class YTDWriteOffReport : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
            }
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            Response.Redirect("OpenYTDReportViewer.aspx?optionreport=" + "&Year=" + txtYear.Text.ToString().Trim());
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("YTDWriteOffReport.aspx");
        }
    }
}