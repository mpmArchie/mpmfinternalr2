﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.YTDWriteOffReport
{
    public partial class OpenYTDReportViewer : System.Web.UI.Page
    {
        string strYear, strBranch;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindReport();
            }
        }

        private void bindReport()
        {
            strYear = Request.QueryString["Year"].ToString().Trim();
            //strBranch = Request.QueryString["branchid"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryReportYTDLossReport(strYear, "ALL", "spYTDLossReport");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptYTDWriteOffReport.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            //ReportParameter p1 = new ReportParameter("branchname", branchname);
            //ReportParameter p2 = new ReportParameter("startDate", startDate);
            //ReportParameter p3 = new ReportParameter("endDate", endDate);
            //ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}