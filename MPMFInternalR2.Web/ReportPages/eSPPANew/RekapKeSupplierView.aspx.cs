﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.eSPPANew
{
    public partial class RekapKeSupplierView : System.Web.UI.Page
    {
        private string inscoid, date, date1, branch;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindReport();
            }
        }

        private void BindReport()
        {
            date = base.Request.QueryString["tglfrom"].ToString();
            date1 = base.Request.QueryString["tglto"].ToString();
            inscoid = base.Request.QueryString["inscoid"].ToString();
            branch = base.Request.QueryString["branchid"].ToString();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryRekapToInsCo(branch, inscoid, date, date1, "spMPMFCusteSPPARptRekapToInsCo");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RkpInsCo.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("From", date);
            ReportParameter p2 = new ReportParameter("End", date1);
            ReportParameter p3 = new ReportParameter("Cabang", branch);
            ReportParameter p4 = new ReportParameter("InsuranceCoyBranchID", inscoid);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);

        }
    }
}