﻿using Confins.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.ReportB2BAAB
{
    public partial class ReportB2BAAB : WebFormBase
    //public partial class ReportB2BAAB : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                loadData();
            }
        }

        private void loadData()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            //ds = qry.QueryBranch(Session["sesBranchId"].ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            //if (Session["sesBranchId"].ToString() == "999")
            //{
            //    ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            //}
            ddlbranch.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            if (txtPeriodeGolive.Text == "")
            {
                lblwarPeriodeGolive.Visible = true;
                txtPeriodeGolive.Focus();
                return;
            }
            else
            {
                txtPeriodeGolive.Visible = false;
            }

            Response.Redirect("ReportB2BAABView.aspx?optionreport=&branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim() + "&periode=" + txtPeriodeGolive.Text + "&status=" + ddlStatus.SelectedValue + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("ReportB2BAAB.aspx");
        }
    }
}