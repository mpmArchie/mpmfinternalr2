﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.ReportB2BAAB
{
    public partial class ReportB2BAABView : System.Web.UI.Page
    {
        private string branch, branchname, periode, status;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            this.branch = base.Request.QueryString["branchid"].ToString().Trim();
            this.branchname = base.Request.QueryString["branchname"].ToString().Trim();
            this.periode = base.Request.QueryString["periode"].ToString().Trim();
            this.status = base.Request.QueryString["status"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            if (status == "ALL")
            {
                dt = qry.QueryRptRpB2BAAB(branch, periode, "spRptB2BAABALL");
            }
            else
            {
                dt = qry.QueryRptRpB2BAAB(branch, periode, "spRptB2BAABRekon");
            }
            

            ReportViewer1.ProcessingMode = ProcessingMode.Local;

            if (status == "ALL")
            {
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptB2BAABALL.rdlc");
            }
            else
            {
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptB2BAABRekon.rdlc");
            }

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("Branch", branch);
            ReportParameter p2 = new ReportParameter("BranchName", branchname);
            ReportParameter p3 = new ReportParameter("Periode", periode);
            ReportParameter p4 = new ReportParameter("Status", status);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);

            GenerateToExcel(ReportViewer1, status);
        }

        private void GenerateToExcel(ReportViewer reportViewer1, string status)
        {
            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = reportViewer1.LocalReport.Render("EXCEL", null, out contentType, out encoding, out extension, out streamIds, out warnings);

            //Download the RDLC Report in Word, Excel, PDF and Image formats.
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            if (status == "ALL")
            {
                Response.AppendHeader("Content-Disposition", "attachment; filename=ReportB2BAABStatusAll." + extension);
            }
            else
            {
                Response.AppendHeader("Content-Disposition", "attachment; filename=ReportB2BAABRekon." + extension);
            }
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
        }
    }
}