﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.HotProspekTelesales
{
    public partial class HotProspekTelesalesView : System.Web.UI.Page
    {
        private string branchid, date1, date2, branchname;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branchid = Request.QueryString["branchid"].ToString().Trim();
            date1 = Request.QueryString["date1"].ToString().Trim();
            date2 = Request.QueryString["date2"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();

            DataTable dataTable = new DataTable();
            dataTable = new QueryConnection().QueryMPMFHotProspekTelesales(branchid, date1, date2, "spMPMFHotProspekTelesales");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptHotProspekTelesales.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dataTable);
            ReportParameter p1 = new ReportParameter("branchname", branchname);
            ReportParameter p2 = new ReportParameter("date1", date1);
            ReportParameter p3 = new ReportParameter("date2", date2);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}