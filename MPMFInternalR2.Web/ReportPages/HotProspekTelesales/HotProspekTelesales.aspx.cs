﻿using Confins.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.HotProspekTelesales
{
    //public partial class HotProspekTelesales : System.Web.UI.Page
    public partial class HotProspekTelesales : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["sesBranchId"] = "502";
            //HttpContext.Current.Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                loadBranch();
            }
        }

        private void loadBranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            //ds = qry.QueryBranch(Session["sesBranchId"].ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            //if (Session["sesBranchId"].ToString() == "999")
            //{
            //    ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            //}
            ddlbranch.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            if (dtStart.Text != "")
            {
                if (dtEnd.Text == "")
                {
                    Response.Write(" <script language=\"javascript\" type=\"text/javascript\"> alert('Tanggal date akhir harus diisi !'); </script>");
                    dtEnd.Focus();
                    return;
                }
            }

            if (dtEnd.Text != "")
            {
                if (dtStart.Text == "")
                {
                    Response.Write(" <script language=\"javascript\" type=\"text/javascript\"> alert('Tanggal date awal harus diisi !'); </script>");
                    dtStart.Focus();
                    return;
                }
            }

            if (DateTime.ParseExact(dtStart.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(dtEnd.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Response.Redirect("HotProspekTelesalesView.aspx?branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&date1=" + dtStart.Text.ToString().Trim() + "&date2=" + dtEnd.Text.ToString().Trim() + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("HotProspekTelesales.aspx");
        }
    }
}