﻿using Confins.Web;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.MPMFReportApiPos
{
    public partial class WebForm1 : WebFormBase
    //public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "49680921";
            if (!IsPostBack)
            {
                loadArea();
                loadRate();
                LoadTahun();
                ddlTahun.SelectedValue = DateTime.Now.Year.ToString();

                if (base.CurrentUserContext.OfficeCode.ToString() != "999")
                //if (Session["sesBranchId"].ToString() != "999")
                {
                    LoadBranchN();
                }
                else
                {
                    this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                }
                //loadData();
            }
        }

        private void LoadTahun()
        {
            ArrayList ayear = new ArrayList();
            int yearnow = Convert.ToInt32(DateTime.Now.Year.ToString()) + 1;
            for (int i = 2010; i <= yearnow; i++)
            {
                ayear.Add(i);
            }
            ddlTahun.DataSource = ayear;
            ddlTahun.DataBind();
        }

        private void LoadBranchN()
        {
            DataSet set = new DataSet();
            //set = new QueryConnection().QueryBranch(Session["sesBranchId"].ToString());
           set = new QueryConnection().QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            this.ddlbranch.DataSource = set;
            this.ddlbranch.DataTextField = "branchfullname";
            this.ddlbranch.DataValueField = "branchid";
            this.ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if (Session["sesBranchId"].ToString() == "999")
            {
                this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            this.ddlbranch.SelectedIndex = 0;
            set.Dispose();


        }

        private void loadArea()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
           // ds = qry.QueryAreaWithBranch(Session["sesBranchId"].ToString());
            ds = qry.QueryAreaWithBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlArea.DataSource = ds;
            ddlArea.DataTextField = "areafullname";
            ddlArea.DataValueField = "AreaID";
            ddlArea.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if (Session["sesBranchId"].ToString() == "999")
            {
                ddlArea.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlArea.SelectedValue != "")
            {
                LoadBranch(ddlArea.SelectedValue.Trim());
            }
        }

        private void LoadBranch(string strArea)
        {
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if (Session["sesBranchId"].ToString() == "999")
            {
                DataSet ds = new DataSet();
                QueryConnection qry = new QueryConnection();
                ds = qry.QueryBranchArea(strArea);
                ddlbranch.DataSource = ds;
                ddlbranch.DataTextField = "branchfullname";
                ddlbranch.DataValueField = "branchid";
                ddlbranch.DataBind();
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                ddlbranch.SelectedIndex = 0;
            }
            else
            {
                DataSet set = new DataSet();
               // set = new QueryConnection().QueryBranch(Session["sesBranchId"].ToString());
                set = new QueryConnection().QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
                this.ddlbranch.DataSource = set;
                this.ddlbranch.DataTextField = "branchfullname";
                this.ddlbranch.DataValueField = "branchid";
                this.ddlbranch.DataBind();
                if (base.CurrentUserContext.OfficeCode.ToString() == "999")
                //if (Session["sesBranchId"].ToString() == "999")
                {
                    this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                }
                this.ddlbranch.SelectedIndex = 0;
                set.Dispose();
            }

        }

        private void loadRate()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryGetRate();
            ddlRate.DataSource = ds;
            ddlRate.DataTextField = "SERVICENAME";
            ddlRate.DataValueField = "SERVICENAME";
            ddlRate.DataBind();

        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            try
            {
                string strStartDate = "";

                if (ddlTahun.SelectedValue == "")
                {
                    lblwardate.Visible = true;
                    ddlTahun.Focus();
                    return;
                }
                else
                {
                    lblwardate.Visible = false;
                }

                if (ddlMonth.SelectedValue == "")
                {
                    lblwardate.Visible = true;
                    ddlTahun.Focus();
                    return;
                }
                else
                {
                    lblwardate.Visible = false;
                }

                //if (dtStart.Text == "")
                //    strStartDate = DateTime.Now.ToString("dd/MM/yyyy");
                //else
                //    strStartDate = dtStart.Text;

                Page.ClientScript.RegisterStartupScript(
                            this.GetType(), "OpenWindow", "window.open('MPMFReportApiPosView.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&Rate=" + ddlRate.SelectedValue.ToString() + "&RateName=" + ddlRate.SelectedItem.Text.ToString() + "&JenisReport=" + ddlJenisReport.SelectedValue.ToString() + "&JenisReportName=" + ddlJenisReport.SelectedItem.Text.ToString() + "&date=" + ddlMonth.SelectedValue.ToString() + "&datename=" + ddlMonth.SelectedItem.Text.ToString() + "&dateEnd=" + ddlTahun.SelectedValue.ToString() + "','_blank');", true);


                //Page.ClientScript.RegisterStartupScript(
                //    this.GetType(), "OpenWindow", "window.open('MPMFCust_SalesRankingMOView.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&date=" + ddlMonth.SelectedValue.ToString() + "&datename=" + ddlMonth.SelectedItem.Text.ToString() + "&dateEnd=" + ddlTahun.SelectedValue.ToString() + "&assettype=" + ddlAssetType.SelectedValue + "&assettypename=" + ddlAssetType.SelectedItem.Text + "&product=" + ddlProduct.SelectedValue + "&productname=" + ddlProduct.SelectedItem.Text + "','_blank');", true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        protected void lb_Dowmload_OnClick(object sender, EventArgs e)
        {
            try
            {
                string strStartDate = "";

                //if (dtStart.Text == "")
                //    strStartDate = DateTime.Now.ToString("dd/MM/yyyy");
                //else
                //    strStartDate = dtStart.Text;

                if (ddlTahun.SelectedValue == "")
                {
                    lblwardate.Visible = true;
                    ddlTahun.Focus();
                    return;
                }
                else
                {
                    lblwardate.Visible = false;
                }

                if (ddlMonth.SelectedValue == "")
                {
                    lblwardate.Visible = true;
                    ddlTahun.Focus();
                    return;
                }
                else
                {
                    lblwardate.Visible = false;
                }

                Response.Redirect("MPMFReportAPiPosDownloadPDF.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&Rate=" + ddlRate.SelectedValue.ToString() + "&RateName=" + ddlRate.SelectedItem.Text.ToString() + "&JenisReport=" + ddlJenisReport.SelectedValue.ToString() + "&JenisReportName=" + ddlJenisReport.SelectedItem.Text.ToString() + "&date=" + ddlMonth.SelectedValue.ToString() + "&datename=" + ddlMonth.SelectedItem.Text.ToString() + "&dateEnd=" + ddlTahun.SelectedValue.ToString(), true);

                //Page.ClientScript.RegisterStartupScript(
                //    this.GetType(), "OpenWindow", "window.open('MPMFCust_ReportSoldDownload.aspx?jenis=" + ddlJenisNilai.SelectedValue.ToString().Trim() + "&area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&date=" + dtStart.Text.ToString() + "&dateEnd=" + dtEnd.Text.ToString() + "','_newtab');", true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        protected void lb_Dowmload_OnClickExcel(object sender, EventArgs e)
        {
            try
            {
                string strStartDate = "";

                //if (dtStart.Text == "")
                //    strStartDate = DateTime.Now.ToString("dd/MM/yyyy");
                //else
                //    strStartDate = dtStart.Text;

                if (ddlTahun.SelectedValue == "")
                {
                    lblwardate.Visible = true;
                    ddlTahun.Focus();
                    return;
                }
                else
                {
                    lblwardate.Visible = false;
                }

                if (ddlMonth.SelectedValue == "")
                {
                    lblwardate.Visible = true;
                    ddlTahun.Focus();
                    return;
                }
                else
                {
                    lblwardate.Visible = false;
                }

                Response.Redirect("MPMFReportAPiPosDownloadExcel.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&Rate=" + ddlRate.SelectedValue.ToString() + "&RateName=" + ddlRate.SelectedItem.Text.ToString() + "&JenisReport=" + ddlJenisReport.SelectedValue.ToString() + "&JenisReportName=" + ddlJenisReport.SelectedItem.Text.ToString() + "&date=" + ddlMonth.SelectedValue.ToString() + "&datename=" + ddlMonth.SelectedItem.Text.ToString() + "&dateEnd=" + ddlTahun.SelectedValue.ToString() , true);

                //Page.ClientScript.RegisterStartupScript(
                //    this.GetType(), "OpenWindow", "window.open('MPMFCust_ReportSoldDownload.aspx?jenis=" + ddlJenisNilai.SelectedValue.ToString().Trim() + "&area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&date=" + dtStart.Text.ToString() + "&dateEnd=" + dtEnd.Text.ToString() + "','_newtab');", true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }






    }
}
