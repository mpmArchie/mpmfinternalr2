﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MPMFReportAPiPosDownloadPDF.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.MPMFReportApiPos.MPMFReportAPiPosDownloadPDF" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
<form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False"></asp:ScriptManager>
    <div>
        <rsweb:ReportViewer ID="ReportViewer1" Height="800px" Width="100%" runat="server" ShowExportControls="False" ShowPrintButton="False"></rsweb:ReportViewer>
    </div>
</form>
</body>
</html>

