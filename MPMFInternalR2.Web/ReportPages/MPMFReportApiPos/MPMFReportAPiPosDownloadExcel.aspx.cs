﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.MPMFReportApiPos
{
    public partial class MPMFReportAPiPosDownloadExcel : System.Web.UI.Page
    {
        private string area, areaname, branch, branchname, Rate, RateName, JenisReport, date, datename, dateEnd;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }
        private void bindreport()
        {
            string nameFIle = "";
            area = Request.QueryString["area"].ToString().Trim();
            areaname = Request.QueryString["areaname"].ToString().Trim();
            branch = Request.QueryString["branch"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            Rate = Request.QueryString["Rate"].ToString().Trim();
            RateName = Request.QueryString["RateName"].ToString().Trim();
            JenisReport = Request.QueryString["JenisReport"].ToString().Trim();
            date = Request.QueryString["date"].ToString().Trim();
            datename = Request.QueryString["datename"].ToString().Trim();
            dateEnd = Request.QueryString["dateEnd"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            //dt = qry.QueryReportAppInJMOrder(APTypeId, APTypeName "spMPMFGetAPType");
            if (JenisReport == "Rekap")
            {
                dt = qry.QueryGetReportApiPos(area, branch, Rate, JenisReport, date, dateEnd, "sp_RPT_REPORTRekap");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RPTReportRekapToExcel.rdlc");
                ReportDataSource datasource = new ReportDataSource("DataSet1", dt);

                /* ReportParameter p1 = new ReportParameter("RateName", RateName);
                 ReportParameter p2 = new ReportParameter("date", dateEnd);

                 ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2 });*/
                ReportViewer1.LocalReport.DataSources.Add(datasource);

                nameFIle = "Report API POS";
                GenerateToPDF(ReportViewer1, nameFIle);
            }

            else
            {
                dt = qry.QueryGetReportApiPos(area, branch, Rate, JenisReport, date, dateEnd, "sp_RPT_REPORTDetail");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RPTReportDetailDownload.rdlc");
                ReportDataSource datasource = new ReportDataSource("DataSet1", dt);

                ReportParameter p1 = new ReportParameter("RateName", RateName);
                ReportParameter p2 = new ReportParameter("date", date);
                ReportParameter p3 = new ReportParameter("dateEnd", dateEnd);
                ReportParameter p4 = new ReportParameter("datename", datename);

                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4 });
                ReportViewer1.LocalReport.DataSources.Add(datasource);

                nameFIle = "Report API POS";
                GenerateToPDF(ReportViewer1, nameFIle);
            }


          

        }

        private void GenerateToPDF(ReportViewer reportViewer1, string nameFile)
        {
            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = reportViewer1.LocalReport.Render("EXCEL", null, out contentType, out encoding, out extension, out streamIds, out warnings);

            //Download the RDLC Report in Word, Excel, PDF and Image formats.
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + nameFile + "." + extension);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
        }
    }
}