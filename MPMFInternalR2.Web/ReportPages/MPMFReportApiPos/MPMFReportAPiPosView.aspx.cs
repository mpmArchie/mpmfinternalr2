﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.MPMFReportApiPos
{
    public partial class MPMFReportAPiPosView : System.Web.UI.Page
    {
        private string area, areaname, branch, branchname, Rate, RateName, JenisReport, date, datename, dateEnd;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            string nameFIle = "";
            area = Request.QueryString["area"].ToString().Trim();
            areaname = Request.QueryString["areaname"].ToString().Trim();
            branch = Request.QueryString["branch"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            Rate = Request.QueryString["Rate"].ToString().Trim();
            RateName = Request.QueryString["RateName"].ToString().Trim();
            JenisReport = Request.QueryString["JenisReport"].ToString().Trim();
            date = Request.QueryString["date"].ToString().Trim();
            datename = Request.QueryString["datename"].ToString().Trim();
            dateEnd = Request.QueryString["dateEnd"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            //dt = qry.QueryReportAppInJMOrder(APTypeId, APTypeName "spMPMFGetAPType");
            if (JenisReport == "Rekap")
            {
                dt = qry.QueryGetReportApiPos(area, branch, Rate, JenisReport, date, dateEnd, "sp_RPT_REPORTRekap");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RPTReportRekap.rdlc");
                ReportDataSource datasource = new ReportDataSource("DataSet1", dt);

                /* ReportParameter p1 = new ReportParameter("RateName", RateName);
                 ReportParameter p2 = new ReportParameter("date", dateEnd);

                 ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2 });*/
                ReportViewer1.LocalReport.DataSources.Add(datasource);

                nameFIle = "Report API POS";

            }

            else
            {
                dt = qry.QueryGetReportApiPos(area, branch, Rate, JenisReport, date, dateEnd, "sp_RPT_REPORTDetail");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RPTReportDetailDownload.rdlc");
                ReportDataSource datasource = new ReportDataSource("DataSet1", dt);

                 ReportParameter p1 = new ReportParameter("RateName", RateName);
                 ReportParameter p2 = new ReportParameter("date", date);
                 ReportParameter p3 = new ReportParameter("dateEnd", dateEnd);
                 ReportParameter p4 = new ReportParameter("datename", datename);

                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4 });
                ReportViewer1.LocalReport.DataSources.Add(datasource);

                nameFIle = "Report API POS";
            }
                

            

        }
    }
    }
