﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.Disburse_List
{
    public partial class DisburseList : WebFormBase
    //public partial class DisburseList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["sesBranchId"] = "999";
            //HttpContext.Current.Session["loginid"] = "NikoP";
            if (this.IsPostBack || (base.CurrentUserContext.OfficeCode.ToString() != "999"))
            {
                return;
            }
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            if (cboAptype.SelectedIndex.ToString() == "0")
            {
                lblwar.Visible = true; return;
            }
            else
            {
                lblwar.Visible = false;
            }

            Response.Redirect("DisburseListView.aspx?APType=" + cboAptype.SelectedValue.ToString() + "");

        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("DisburseList.aspx");
        }
    }
}
