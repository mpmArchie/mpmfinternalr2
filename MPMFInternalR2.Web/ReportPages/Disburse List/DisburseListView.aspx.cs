﻿using Microsoft.Reporting.WebForms;
using MPMFInternalR2.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.Disburse_List
{
    public partial class DisburseListView : System.Web.UI.Page
    {
        private string disvalue;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindReport();
            }
        }

        private void bindReport()
        {
            string str = Request.QueryString["APType"].ToString();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryMPMFCustRptDisburseList(str, "spMPMFCustRptDisburseList");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptDisburseListView.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("aptype", str);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}