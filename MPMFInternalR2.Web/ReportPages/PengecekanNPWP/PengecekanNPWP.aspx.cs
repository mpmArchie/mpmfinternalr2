﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;
using System.Globalization;
using System.Data;

namespace MPMFInternalR2.Web.ReportPages.PengecekanNPWP
{
    //public partial class PengecekanNPWP : System.Web.UI.Page
    public partial class PengecekanNPWP : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Session["sesBranchId"] = "999";
                //Session["loginid"] = "999";

            }

        }

        private void BindDataGrid()
        {
           
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryGetNPWP(txtNama.Text,txtAgrmnt.Text,txtNPWP.Text, "spMPMFgetNPWP");
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                gvdata.DataSource = dt;
                gvdata.DataBind();
            }
            else
            {
                gvdata.DataSource = null;
                gvdata.DataBind();
            }
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {

           if(string.IsNullOrEmpty(txtAgrmnt.Text) && string.IsNullOrEmpty(txtNama.Text) && string.IsNullOrEmpty(txtNPWP.Text))
            {
                lblwarning.Visible = true;
                lblwarning.Text = "Semua field tidak boleh kosong !";
                return;

            }
            BindDataGrid();
        }
        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("PengecekanNPWP.aspx");
        }
    }
}