﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PengecekanNPWP.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.PengecekanNPWP.PengecekanNPWP" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({
                dateFormat: 'dd/mm/yy'
            });
        });
    </script>

</head>
<body>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text=""></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">NPWP RESULT</label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />

            <div id="dPDCReceive">

                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">Search</span>
                </div>
                <div id="dSearchForm">
                    <table id="ucReportSearch_tblFixedSearch" class="formTable">
                        <asp:Label ID="lblwarning" runat="server" Text="Label" Visible="false" ForeColor="Red"></asp:Label>
                        <tr>
                            <td class="tdDesc">Name</td>
                            <td class="tdValue">
                                <asp:TextBox runat="server" AutoCompleteType="None" ID="txtNama"></asp:TextBox>

                            </td>
                            <td class="tdDesc">Agreement No</td>
                            <td class="tdValue">
                                <asp:TextBox runat="server" AutoCompleteType="None" ID="txtAgrmnt"></asp:TextBox>

                            </td>
                        </tr>
                         <tr>
                            <td class="tdDesc">NPWP</td>
                            <td class="tdValue">
                                <asp:TextBox runat="server" AutoCompleteType="None" ID="txtNPWP"></asp:TextBox>

                            </td>
                            <td class="tdDesc"></td>
                            <td class="tdValue">
                             
                            </td>
                        </tr>
                    </table>
                    <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                            OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>

                    <asp:GridView runat="server" ID="gvdata" AutoGenerateColumns="False" GridLines="None"
                                            CssClass="mGrid" AlternatingRowStyle-CssClass="alt" ShowHeaderWhenEmpty="True"
                                            EmptyDataText="No records Found" PageSize="10" AllowPaging="True">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Agreement No" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblAgrementNo" Text='<%# Eval("AGRMNT_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Name" HeaderStyle-Width="100" ItemStyle-Width="300" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblName" Text='<%# Eval("NAMA") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="NIK" HeaderStyle-Width="100" ItemStyle-Width="200" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblInstallmentAmount" Text='<%# Eval("NIK") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="NPWP" HeaderStyle-Width="50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblcurrencyid" Text='<%# Eval(("NPWP_NO")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="TAX RESULT" HeaderStyle-Width="150" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblNextInstallmentDueDate" Text='<%# Eval(("TAX_RESULT")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="GRADE RESULT" HeaderStyle-Width="150" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblcontractstatus" Text='<%# Eval(("GRADE_RESULT")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                               
                                                
                                            </Columns>
                                        </asp:GridView>

                </div>
            </div>
        </div>

    </form>
</body>
</html>

