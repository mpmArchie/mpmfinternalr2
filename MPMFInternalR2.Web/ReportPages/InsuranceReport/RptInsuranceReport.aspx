﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RptInsuranceReport.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.InsuranceReport.RptInsuranceReport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
        </asp:ScriptManager>
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="Report/InsuranceReport"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">Insurance Report </label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />

            <div id="dPDCReceive">
                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">Search</span>
                </div>
                <div id="dSearchForm">
                    <table id="ucReportSearch_tblFixedSearch" class="formTable">
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Branch</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlbranch" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc">Report Based On</td>
                            <td class="tdValue">
                                <asp:DropDownList ID="ddlReportType" runat="server" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlReportType_SelectedIndexChanged">
                                    <asp:ListItem Value="1">Based On Go Live Date </asp:ListItem>
                                    <asp:ListItem Value="2">Based On Invoice Number </asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <asp:Panel ID="panelReport1" runat="server">
                            <tr>
                                <td class="tdDesc">Report Type</td>
                                <td class="tdValue">
                                    <asp:DropDownList ID="ddlReportType2" runat="server" AutoPostBack="True">
                                        <asp:ListItem Value="Detail">Detail</asp:ListItem>
                                        <asp:ListItem Value="Rekap">Rekap</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdDesc" style="width: 20%;">Date</td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:TextBox runat="server" AutoCompleteType="Enabled" ID="dtStart" CssClass="datepicker"></asp:TextBox>
                                </td>
                                <td class="tdDesc" style="width: 20%;">To</td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:TextBox runat="server" AutoCompleteType="Enabled" ID="dtEnd" CssClass="datepicker"></asp:TextBox>
                                    <asp:Label ID="lblwardate" runat="server" Font-Names="verdana"
                                        Font-Size="X-Small" ForeColor="Red"
                                        Style="font-family: verdana, Arial, tahoma, san-serif; font-size: 11px;"
                                        Visible="False">* Please cek date awal dan akhir</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdDesc">Order Condition</td>
                                <td class="tdValue">
                                    <asp:DropDownList ID="dllOrderCond" runat="server" AutoPostBack="True">
                                        <asp:ListItem Value="ALL">All</asp:ListItem>
                                        <asp:ListItem Value="1">Ordered</asp:ListItem>
                                        <asp:ListItem Value="2">Not Ordered</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdDesc">Policy Condition</td>
                                <td class="tdValue">
                                    <asp:DropDownList ID="ddlPolicyCond" runat="server" AutoPostBack="True">
                                        <asp:ListItem Value="ALL">All</asp:ListItem>
                                        <asp:ListItem Value="1">Have Policy Number</asp:ListItem>
                                        <asp:ListItem Value="2">Haven't Policy Number</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdDesc">Grouping Condition</td>
                                <td class="tdValue">
                                    <asp:DropDownList ID="dllGroupingCond" runat="server" AutoPostBack="True">
                                        <asp:ListItem Value="ALL">All</asp:ListItem>
                                        <asp:ListItem Value="1">Paid</asp:ListItem>
                                        <asp:ListItem Value="2">Not Paid</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdDesc">Insurance Type</td>
                                <td class="tdValue">
                                    <asp:DropDownList ID="ddlTypeCond" runat="server" AutoPostBack="True">
                                        <asp:ListItem Value="ALL">All</asp:ListItem>
                                        <asp:ListItem Value="MPM">MPM</asp:ListItem>
                                        <asp:ListItem Value="MPM07">MPM07</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="panelReport2" runat="server" Visible="False">
                            <tr>
                                <td class="tdDesc">InvoiceNo</td>
                                <td class="tdValue">
                                    <asp:TextBox ID="txtInvoiceNo" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtInvoiceNo" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Invoice Number Must be Filled!" ForeColor="Red"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </asp:Panel>
                    </table>
                    <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                            OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
