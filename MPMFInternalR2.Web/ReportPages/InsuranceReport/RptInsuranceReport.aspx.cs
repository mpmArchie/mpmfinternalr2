﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.InsuranceReport
{
    public partial class RptInsuranceReport : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["sesBranchId"] = "999";
            //HttpContext.Current.Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {

                loadData();
            }
        }
        protected void loadData()
        {
            loadbranch();
        }
        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbranch.SelectedIndex = 0;

            ds.Dispose();
        }
       

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string strDateStart = "";
            string strDateEnd = "";
            string reportType = "";
            string orderCond = "";
            string policyCond = "";
            string groupingCond = "";
            string typeCond = "";
            string invoiceNo = "";
            string rekapDetail = "";


            if (dtStart.Text == "")
                strDateStart = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strDateStart = dtStart.Text;

            if (dtEnd.Text == "")
                strDateEnd = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strDateEnd = dtEnd.Text;
            if (ddlReportType.SelectedValue.ToString().Trim() != null)
            {
                reportType = ddlReportType.SelectedValue.ToString().Trim();
            }

            if (dllOrderCond.SelectedValue.ToString().Trim() != null)
            {
                orderCond = dllOrderCond.SelectedValue.ToString().Trim();
            }

            if (ddlPolicyCond.SelectedValue.ToString().Trim() != null)
            {
                policyCond = ddlPolicyCond.SelectedValue.ToString().Trim();
            }

            if (dllGroupingCond.SelectedValue.ToString().Trim() != null)
            {
                groupingCond = dllGroupingCond.SelectedValue.ToString().Trim();
            }

            if (ddlTypeCond.SelectedValue.ToString().Trim() != null)
            {
                typeCond = ddlTypeCond.SelectedValue.ToString().Trim();
            }

            if (txtInvoiceNo.Text != null)
            {
                invoiceNo = txtInvoiceNo.Text;
            }

            if (ddlReportType2.SelectedValue.ToString().Trim() != null)
            {
                rekapDetail = ddlReportType2.SelectedValue.ToString().Trim();
            }

            if (DateTime.ParseExact(strDateStart, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(strDateEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Response.Redirect("RptInsuranceReportView.aspx?optionreport=" + "&branchId=" + ddlbranch.SelectedValue.ToString().Trim()
                + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim()
                + "&startDate=" + strDateStart.ToString()
                + "&endDate=" + strDateEnd.ToString()
                + "" + "&reportType=" + reportType
                + "" + "&orderCond=" + orderCond
                + "" + "&policyCond=" + policyCond
                + "" + "&groupingCond=" + groupingCond
                + "" + "&typeCond=" + typeCond
                + "" + "&invoiceNo=" + invoiceNo
                + "" + "&rekapDetail=" + rekapDetail);

        }

        protected void ddlReportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlReportType.SelectedValue.ToString() == "1")
            {
                panelReport1.Visible = true;
                panelReport2.Visible = false;
            }
            else
            {
                panelReport1.Visible = false;
                panelReport2.Visible = true;
            }
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("RptInsuranceReport.aspx");
        }
    }
}