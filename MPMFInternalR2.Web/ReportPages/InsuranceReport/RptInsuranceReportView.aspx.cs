﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.InsuranceReport
{
    public partial class RptInsuranceReportView : System.Web.UI.Page
    {
        private string branch, branchname, startDate, endDate, reportType, orderCond, policyCond, groupingCond, typeCond, invoiceNo, rekapDetail;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }
        protected void bindreport()
        {
            branch = Request.QueryString["branchId"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            reportType = Request.QueryString["reportType"].ToString().Trim();
            startDate = Request.QueryString["startDate"].ToString().Trim();
            endDate = Request.QueryString["endDate"].ToString().Trim();
            orderCond = Request.QueryString["orderCond"].ToString().Trim();
            policyCond = Request.QueryString["policyCond"].ToString().Trim();
            groupingCond = Request.QueryString["groupingCond"].ToString().Trim();
            typeCond = Request.QueryString["typeCond"].ToString().Trim();
            invoiceNo = Request.QueryString["invoiceNo"].ToString().Trim();
            rekapDetail = Request.QueryString["rekapDetail"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            dt = qry.QueryReportInsurance(branch, startDate, endDate, reportType, orderCond, policyCond, groupingCond, typeCond, invoiceNo, rekapDetail, "spSAFInsuranceReport");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;

            if (rekapDetail == "Rekap")
            {
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptInsuranceReportRekap.rdlc");
            }
            else
            {
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptInsuranceReport.rdlc");
            }
            //begin parameter

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);

            ReportViewer1.LocalReport.DataSources.Clear();
            ReportParameter p1 = new ReportParameter("branchname", branchname);
            ReportParameter p2 = new ReportParameter("startDate", startDate);
            ReportParameter p3 = new ReportParameter("endDate", endDate);
            ReportParameter p4 = new ReportParameter("reportType", reportType);
            ReportParameter p5 = new ReportParameter("orderCond", orderCond);
            ReportParameter p6 = new ReportParameter("policyCond", policyCond);
            ReportParameter p7 = new ReportParameter("groupingCond", groupingCond);
            ReportParameter p8 = new ReportParameter("typeCond", typeCond);
            ReportParameter p9 = new ReportParameter("invoiceNo", invoiceNo);
            ReportParameter p10 = new ReportParameter("rekapDetail", rekapDetail);
            ReportParameter p11 = new ReportParameter("branchID", branch);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6, p7, p8, p9,p10 ,p11});
            ReportViewer1.LocalReport.DataSources.Add(datasource);
            //end parameter



        }

    }
}