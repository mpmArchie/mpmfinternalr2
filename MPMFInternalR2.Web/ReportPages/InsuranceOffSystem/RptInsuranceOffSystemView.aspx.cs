﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.InsuranceOffSystem
{
    public partial class RptInsuranceOffSystemView : System.Web.UI.Page
    {
        private string branch, branchname, date1, date2;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branch = Request.QueryString["branchId"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            date1 = Request.QueryString["period"].ToString().Trim();
            date2 = Request.QueryString["period1"].ToString().Trim();

            DataTable dtin = new DataTable();
            QueryConnection qryin = new QueryConnection();
            dtin = qryin.QueryReportInsOffSystem(branch, date1, date2, "spMPMFInsuranceOffSystem");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptInsuranceOffSystem.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dtin);
            ReportParameter p1 = new ReportParameter("branchId", branch);
            ReportParameter p2 = new ReportParameter("branchname", branchname);
            ReportParameter p3 = new ReportParameter("date1", date1);
            ReportParameter p4 = new ReportParameter("date2", date2);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
} 
