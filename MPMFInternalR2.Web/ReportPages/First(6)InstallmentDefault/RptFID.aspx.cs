﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.First_6_InstallmentDefault
{
    public partial class RptFID : WebFormBase
    //public partial class RptFID : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Session["sesBranchId"] = "999";
                //Session["loginid"] = "999";
                loadData();
            }
        }

        protected void loadData()
        {
            loadddlarea();
            loadcategory();
            ddlbrand.Items.Insert(0, new ListItem("ALL", "ALL"));
            loadyear();
        }

        private void loadddlarea()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryArea();
            ddlarea.DataSource = ds;
            ddlarea.DataTextField = "areafullname";
            ddlarea.DataValueField = "AreaID";
            ddlarea.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlarea.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlarea.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void loadcategory()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryCategory();
            ddlcategory.DataSource = ds;
            ddlcategory.DataTextField = "DESCRIPTIONPRODUCT";
            ddlcategory.DataValueField = "InitialProductCode";
            ddlcategory.DataBind();
            ddlcategory.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlcategory.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void loadbrand()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBrand();
            ddlbrand.DataSource = ds;
            ddlbrand.DataTextField = "AssetCode";
            ddlbrand.DataValueField = "AssetCode";
            ddlbrand.DataBind();
            ddlbrand.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbrand.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void loadyear()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryYear();
            txtYear.Text = ds.Tables[0].Rows[0]["year"].ToString().Trim();
            ddlMonth.SelectedValue = ds.Tables[0].Rows[0]["month"].ToString().Trim();

            ds.Dispose();
        }

        protected void ddlOption_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlOption.SelectedValue == "Surveyor" || ddlOption.SelectedValue == "Dealer" || ddlOption.SelectedValue == "Type")
            {
                PanelArea.Visible = false;
                PanelBranch.Visible = true;
                PanelDP.Visible = false;
                loadbranch();
            }
            else if (ddlOption.SelectedValue == "Downpayment")
            {
                PanelArea.Visible = false;
                PanelBranch.Visible = true;
                PanelDP.Visible = true;
                loadbranch();
            }
            else
            {
                PanelArea.Visible = true;
                PanelBranch.Visible = false;
                PanelDP.Visible = false;
            }
        }

        protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            string WhereCond = "InitialProductCode='" + ddlcategory.SelectedValue.ToString() + "'";

            ds = qry.QueryFID(WhereCond, "spBrandCategory_Vera");
            ddlbrand.DataSource = ds;
            ddlbrand.DataTextField = "AssetCode";
            ddlbrand.DataValueField = "AssetCode";
            ddlbrand.DataBind();
            ddlbrand.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbrand.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            Response.Redirect("RptFIDView.aspx?optionreport=" + ddlOption.SelectedValue.ToString().Trim() + "&branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&areaid=" + ddlarea.SelectedValue.ToString().Trim() + "&areaname=" + ddlarea.SelectedItem.ToString().Trim() + "&categoryid=" + ddlcategory.SelectedValue.ToString().Trim() + "&categoryname=" + ddlcategory.SelectedItem.ToString().Trim() + "&brand=" + ddlbrand.SelectedValue.ToString().Trim() + "&brandname=" + ddlbrand.SelectedItem.ToString().Trim() + "&month=" + ddlMonth.SelectedValue.ToString().Trim() + "&year=" + txtYear.Text.ToString().Trim() + "&optionbase=" + ddloptionbase.SelectedValue.ToString().Trim() + "&baseon=" + ddlBaseOn.SelectedValue.ToString().Trim() + "&dp1=" + txtdp1.Text.ToString().Trim() + "&dp2=" + txtdp2.Text.ToString().Trim() + "&dp3=" + txtdp3.Text.ToString().Trim() + "&dp4=" + txtdp4.Text.ToString().Trim() + "&dp5=" + txtdp5.Text.ToString().Trim() + "&monthName=" + ddlMonth.SelectedItem.ToString().Trim() + "&loginid=" + base.CurrentUserContext.UserId.ToString() + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("RptFID.aspx");
        }
    }
}