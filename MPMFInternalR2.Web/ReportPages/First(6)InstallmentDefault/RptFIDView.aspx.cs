﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.First_6_InstallmentDefault
{
    public partial class RptFIDView : System.Web.UI.Page
    {
        private string optionreport, branch, areaid, areaname, categoryid, categoryname, brand, brandname, month, year, optionbase, baseon, dp1, dp2, dp3, dp4, dp5, monthName, loginid;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            optionreport = Request.QueryString["optionreport"];
            branch = Request.QueryString["branchid"].ToString().Trim();
            areaid = Request.QueryString["areaid"].ToString().Trim();
            areaname = Request.QueryString["areaname"].ToString().Trim();
            categoryid = Request.QueryString["categoryid"].ToString().Trim();
            categoryname = Request.QueryString["categoryname"].ToString().Trim();
            brand = Request.QueryString["brand"].ToString().Trim();
            brandname = Request.QueryString["brandname"].ToString().Trim();
            month = Request.QueryString["month"].ToString().Trim();
            year = Request.QueryString["year"].ToString().Trim();
            optionbase = Request.QueryString["optionbase"].ToString().Trim();
            baseon = Request.QueryString["baseon"].ToString().Trim();
            dp1 = Request.QueryString["dp1"].ToString().Trim();
            dp2 = Request.QueryString["dp2"].ToString().Trim();
            dp3 = Request.QueryString["dp3"].ToString().Trim();
            dp4 = Request.QueryString["dp4"].ToString().Trim();
            dp5 = Request.QueryString["dp5"].ToString().Trim();
            monthName = Request.QueryString["monthName"].ToString().Trim();
            loginid = Request.QueryString["loginid"].ToString().Trim();


            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            if (optionreport == "Branch")
            {
                dt = qry.QueryReportFID(areaid, categoryid, brand, month, year, optionbase, baseon, optionreport, "spSAFFIDRptBranch");

                ReportViewer1.ProcessingMode = ProcessingMode.Local;

                if (optionreport == "Branch")
                {
                    ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptFIDBranch.rdlc");
                }

                ReportDataSource datasource = new ReportDataSource("DataSet1", dt);

                ReportViewer1.LocalReport.DataSources.Clear();
                ReportParameter p1 = new ReportParameter("areaname", areaname);
                ReportParameter p2 = new ReportParameter("brandname", brandname);
                ReportParameter p3 = new ReportParameter("categoryname", categoryname);
                ReportParameter p4 = new ReportParameter("month", month);
                ReportParameter p5 = new ReportParameter("year", year);
                ReportParameter p6 = new ReportParameter("optionreport", optionreport);
                ReportParameter p7 = new ReportParameter("optionbase", optionbase);
                ReportParameter p8 = new ReportParameter("loginid", loginid);
                ReportParameter p9 = new ReportParameter("baseon", baseon);
                ReportParameter p10 = new ReportParameter("monthName", monthName);
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6, p7, p8, p9, p10 });
                ReportViewer1.LocalReport.DataSources.Add(datasource);
            }

            if (optionreport == "Dealer" || optionreport == "Surveyor")
            {
                //dt = qry.QueryReportFIDwBranch(areaid, categoryid, brand, month, year, optionbase, baseon, optionreport, branch, "spSAFFIDRpt");

                if (optionreport == "Dealer")
                {
                    dt = qry.QueryReportFIDwBranch(areaid, categoryid, brand, month, year, optionbase, baseon, optionreport, branch, "spSAFFIDRptDealer");
                    ReportViewer1.ProcessingMode = ProcessingMode.Local;
                    ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptFIDDealer.rdlc");
                }
                else if (optionreport == "Surveyor")
                {
                    dt = qry.QueryReportFIDwBranch(areaid, categoryid, brand, month, year, optionbase, baseon, optionreport, branch, "spSAFFIDRptSurveyor");
                    ReportViewer1.ProcessingMode = ProcessingMode.Local;
                    ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptFIDSurveyor.rdlc");
                }

                ReportDataSource datasource = new ReportDataSource("DataSet1", dt);

                ReportViewer1.LocalReport.DataSources.Clear();
                ReportParameter p1 = new ReportParameter("areaname", areaname);
                ReportParameter p2 = new ReportParameter("brandname", brandname);
                ReportParameter p3 = new ReportParameter("categoryname", categoryname);
                ReportParameter p4 = new ReportParameter("month", month);
                ReportParameter p5 = new ReportParameter("year", year);
                ReportParameter p6 = new ReportParameter("optionreport", optionreport);
                ReportParameter p7 = new ReportParameter("optionbase", optionbase);
                ReportParameter p8 = new ReportParameter("loginid", loginid);
                ReportParameter p9 = new ReportParameter("baseon", baseon);
                ReportParameter p10 = new ReportParameter("monthName", monthName);
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6, p7, p8, p9, p10 });
                ReportViewer1.LocalReport.DataSources.Add(datasource);
            }

            if (optionreport == "Downpayment" || optionreport == "Type")
            {

                if (optionreport == "Downpayment")
                {
                    dt = qry.QueryReportFIDDownpayment(areaid, categoryid, brand, month, year, optionbase, baseon, optionreport, branch, Convert.ToInt32(dp1), Convert.ToInt32(dp2), Convert.ToInt32(dp3), Convert.ToInt32(dp4), Convert.ToInt32(dp5), "spSAFFIDRptDownpayment");
                    ReportViewer1.ProcessingMode = ProcessingMode.Local;
                    ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptFIDDownpayment.rdlc");
                }
                else if (optionreport == "Type")
                {
                    dt = qry.QueryReportFIDDownpayment(areaid, categoryid, brand, month, year, optionbase, baseon, optionreport, branch, Convert.ToInt32(dp1), Convert.ToInt32(dp2), Convert.ToInt32(dp3), Convert.ToInt32(dp4), Convert.ToInt32(dp5), "spSAFFIDRptType");
                    ReportViewer1.ProcessingMode = ProcessingMode.Local;
                    ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptFIDType.rdlc");
                }

                ReportDataSource datasource = new ReportDataSource("DataSet1", dt);

                ReportViewer1.LocalReport.DataSources.Clear();
                ReportParameter p1 = new ReportParameter("areaname", areaname);
                ReportParameter p2 = new ReportParameter("brandname", brandname);
                ReportParameter p3 = new ReportParameter("categoryname", categoryname);
                ReportParameter p4 = new ReportParameter("month", month);
                ReportParameter p5 = new ReportParameter("year", year);
                ReportParameter p6 = new ReportParameter("optionreport", optionreport);
                ReportParameter p7 = new ReportParameter("optionbase", optionbase);
                ReportParameter p8 = new ReportParameter("loginid", loginid);
                ReportParameter p9 = new ReportParameter("baseon", baseon);
                ReportParameter p10 = new ReportParameter("monthName", monthName);
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6, p7, p8, p9, p10 });
                ReportViewer1.LocalReport.DataSources.Add(datasource);
            }
        }
    }
}