﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RptFID.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.First_6_InstallmentDefault.RptFID" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="Report/First(6)InstallmentDefault"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">FIRST (6) INSTALLMENT DEFAULT</label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />


            <div id="dPDCReceive">
                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">Search</span>
                </div>
                <div id="dSearchForm">
                    <asp:Panel ID="PanelReport" runat="server">
                        <table id="tblFixedSearch" class="formTable">
                            <tr>
                                <td class="tdDesc" style="width: 20%;">Report By</td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:DropDownList ID="ddlOption"
                                        OnSelectedIndexChanged="ddlOption_SelectedIndexChanged" runat="server"
                                        AutoPostBack="true">
                                        <asp:ListItem Value="Branch" Selected="True">Branch</asp:ListItem>
                                        <asp:ListItem Value="Dealer">Dealer</asp:ListItem>
                                        <asp:ListItem Value="Downpayment">Downpayment</asp:ListItem>
                                        <asp:ListItem Value="Surveyor">Surveyor</asp:ListItem>
                                        <asp:ListItem Value="Type">Type</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="PanelArea" runat="server">
                        <table id="tblFixedSearch1" class="formTable">
                            <tr>
                                <td class="tdDesc" style="width: 20%;">Area</td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:DropDownList ID="ddlarea" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="PanelBranch" runat="server" Visible="False">
                        <table id="tblFixedSearch2" class="formTable">
                            <tr>
                                <td class="tdDesc" style="width: 20%;">Branch</td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:DropDownList ID="ddlbranch" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="PanelOption" runat="server">
                        <table id="tblFixedSearch3" class="formTable">
                            <tr>
                                <td class="tdDesc" style="width: 20%;">Category</td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:DropDownList ID="ddlcategory" runat="server" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlcategory_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdDesc" style="width: 20%;">Brand</td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:DropDownList ID="ddlbrand" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdDesc" style="width: 20%;">Period</td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:DropDownList ID="ddlMonth" runat="server">
                                        <asp:ListItem Value="1">January</asp:ListItem>
                                        <asp:ListItem Value="2">February</asp:ListItem>
                                        <asp:ListItem Value="3">March</asp:ListItem>
                                        <asp:ListItem Value="4">April</asp:ListItem>
                                        <asp:ListItem Value="5">May</asp:ListItem>
                                        <asp:ListItem Value="6">June</asp:ListItem>
                                        <asp:ListItem Value="7">July</asp:ListItem>
                                        <asp:ListItem Value="8">August</asp:ListItem>
                                        <asp:ListItem Value="9">September</asp:ListItem>
                                        <asp:ListItem Value="10">October</asp:ListItem>
                                        <asp:ListItem Value="11">November</asp:ListItem>
                                        <asp:ListItem Value="12">December</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:TextBox ID="txtYear" runat="server" CssClass="inptype" MaxLength="4"
                                        Width="56px"></asp:TextBox>
                                    <span style="font-size: 11pt; text-align: left;"><span style="font-size: 11pt">
                                        <span style="font-family: Tahoma"><font size="2">
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                    ControlToValidate="txtYear" Display="Dynamic" ErrorMessage="*) Numeric" 
                                    ForeColor="Red" ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>
                                </font></span></span></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdDesc" style="width: 20%;">Option</td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:DropDownList ID="ddloptionbase" runat="server">
                                        <asp:ListItem Selected="True" Value="0">Base on A/R</asp:ListItem>
                                        <asp:ListItem Value="1">Base on Overdue</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdDesc" style="width: 20%;">Base On</td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:DropDownList ID="ddlBaseOn" runat="server">
                                        <asp:ListItem Selected="True" Value="Amount">Amount</asp:ListItem>
                                        <asp:ListItem Value="Unit">Unit</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="PanelDP" runat="server" Visible="False">
                        <table id="tblFixedSearch4" class="formTable">
                            <tr>
                                <td class="tdDesc" style="width: 20%;">Range Satu</td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:TextBox ID="txtdp1" runat="server" CssClass="inptype" MaxLength="4"
                                        Text="10" Width="56px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdDesc" style="width: 20%;">Range Dua</td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:TextBox ID="txtdp2" runat="server" CssClass="inptype" MaxLength="4"
                                        Text="12" Width="56px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdDesc" style="width: 20%;">Range Tiga</td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:TextBox ID="txtdp3" runat="server" CssClass="inptype" MaxLength="4"
                                        Text="14" Width="56px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdDesc" style="width: 20%;">Range Empat</td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:TextBox ID="txtdp4" runat="server" CssClass="inptype" MaxLength="4"
                                        Text="16" Width="56px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdDesc" style="width: 20%;">Range Lima</td>
                                <td class="tdValue" style="width: 30%;">
                                    <asp:TextBox ID="txtdp5" runat="server" CssClass="inptype" MaxLength="4"
                                        Text="18" Width="56px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                            OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
