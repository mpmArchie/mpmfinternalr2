﻿using Confins.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.RptBpkb
{
    //public partial class RptBlokirBpkb : System.Web.UI.Page
    public partial class RptBlokirBpkb : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session.Add("loginID", "MellysaK");
            //Session.Add("sesBranchId", "999");

            if (!IsPostBack)
            {
                loadArea();
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                //loadData();
            }
        }

        private void loadData()
        {
            loadbranch();
        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            //ds = qry.QueryBranch(Session["sesBranchId"].ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if (Session["sesBranchId"].ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbranch.SelectedIndex = 0;
        }

        private void loadArea()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryArea();
            ddlArea.DataSource = ds;
            ddlArea.DataTextField = "areafullname";
            ddlArea.DataValueField = "AreaID";
            ddlArea.DataBind();
            ddlArea.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlArea.SelectedIndex = 0;
        }

        private void LoadBranch(string strArea)
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranchArea(strArea);
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbranch.SelectedIndex = 0;
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadBranch(ddlArea.SelectedValue.Trim());
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string strStartDate = "";
            string strEndDate = "";
            string strStockDate = "";

            if (dtStart.Text == "")
                strStartDate = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strStartDate = dtStart.Text;

            if (dtEnd.Text == "")
                strEndDate = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strEndDate = dtEnd.Text;

            DateTime date1 = DateTime.ParseExact(strStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime date2 = DateTime.ParseExact(strEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            if (DateTime.ParseExact(strStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(strEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }

            else if ((date2.Month - date1.Month) + 12 * (date2.Year - date1.Year) >= 6)
            {
                lblwardate.Text = "* Period Date may not exceed 6 months";
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }



            Response.Redirect("RptBlokirBpkbView.aspx?branchid=" + ddlbranch.SelectedValue.ToString().Trim()
                + "&ProdId=" + ddlProduct.SelectedValue.ToString().Trim() + "&ProdName=" + ddlProduct.SelectedItem.Text.Trim()
                + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim()
                + "&StartDate=" + strStartDate.ToString() + "&EndDate=" + strEndDate.ToString()
                + "&AreaId=" + ddlArea.SelectedValue.ToString().Trim() + "&AreaFullName=" + ddlArea.SelectedItem.Text.Trim()
                );




        }



        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("MPMFRptAssetStockSettlement.aspx");
        }
    }
}