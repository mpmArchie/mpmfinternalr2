﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.RptBpkb
{
    public partial class RptBlokirBpkbview : System.Web.UI.Page
    {
        private string prodid, prodname, branchid, branchfullname, RptType, startDate, endDate, AreaId, Areafullname;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }

        }
        private void bindreport()
        {
            branchid = Request.QueryString["branchid"].ToString().Trim();
            branchfullname = Request.QueryString["branchname"].ToString().Trim();
            prodid = Request.QueryString["prodid"].ToString().Trim();
            prodname = Request.QueryString["prodname"].ToString().Trim();
            startDate = Request.QueryString["StartDate"].ToString().Trim();
            endDate = Request.QueryString["EndDate"].ToString().Trim();
            AreaId = Request.QueryString["AreaId"].ToString().Trim();
            Areafullname = Request.QueryString["AreaFullName"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();



            dt = qry.QueryReportspBlokirBPKB(AreaId, branchid, startDate, endDate, prodid, "uspBlokirBpkb");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptBlokirBpkb.rdlc");


            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("date", startDate);
            ReportParameter p2 = new ReportParameter("date1", endDate);
            ReportParameter p3 = new ReportParameter("Branch", branchfullname);
            ReportParameter p4 = new ReportParameter("AreaName", Areafullname);
            ReportParameter p5 = new ReportParameter("prodid", prodid);
            ReportParameter p6 = new ReportParameter("prodname", prodname);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4,p5,p6 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}