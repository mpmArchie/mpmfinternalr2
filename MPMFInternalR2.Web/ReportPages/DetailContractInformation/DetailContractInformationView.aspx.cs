﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.DetailContractInformation
{
    public partial class DetailContractInformationView : System.Web.UI.Page
    {
        private string branch, branchname, startDate, endDate;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branch = Request.QueryString["branchId"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            startDate = Request.QueryString["startDate"].ToString().Trim();
            endDate = Request.QueryString["endDate"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            dt = qry.QueryReportDetailContract(branch, startDate, endDate, "spDetailContract");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptDetailContract.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportParameter p1 = new ReportParameter("branchname", branchname);
            ReportParameter p2 = new ReportParameter("startDate", startDate);
            ReportParameter p3 = new ReportParameter("endDate", endDate);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}