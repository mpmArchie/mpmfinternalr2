﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.Web;

namespace MPMFInternalR2.Web.ReportPages.DetailContractInformation
{
    //public partial class DetailContractInformation : System.Web.UI.Page
    public partial class DetailContractInformation : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.fillcbo();
            }
        }

        private void fillcbo()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            cboBranch.DataSource = ds;
            cboBranch.DataTextField = "branchfullname";
            cboBranch.DataValueField = "branchid";
            cboBranch.DataBind();
            cboBranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            cboBranch.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            if ((dtStart.Text == string.Empty) | (dtEnd.Text == string.Empty))
            {
                this.lblwardate.Text = "Please Fill Period From And Period To";
                lblwardate.Visible = true; return;
            }

            if (DateTime.ParseExact(dtStart.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(dtEnd.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Response.Redirect("DetailContractInformationView.aspx?optionreport=" + "&branchId=" + cboBranch.SelectedValue.ToString().Trim()
                + "&branchname=" + cboBranch.SelectedItem.ToString().Trim()
                + "&startDate=" + dtStart.Text.ToString()
                + "&endDate=" + dtEnd.Text.ToString());
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("DetailContractInformation.aspx");
        }
    }
}