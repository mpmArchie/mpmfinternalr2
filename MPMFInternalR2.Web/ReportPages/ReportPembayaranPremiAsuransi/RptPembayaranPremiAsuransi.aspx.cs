﻿using Confins.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.ReportPembayaranPremiAsuransi
{
    //public partial class RptPembayaranPremiAsuransi : System.Web.UI.Page
    public partial class RptPembayaranPremiAsuransi : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "NikoP";

            if (!IsPostBack)
            {
                BindRegional();
            }
        }

        private void BindRegional()
        {

            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryArea();
            ddlRegional.DataSource = ds;
            ddlRegional.DataTextField = "areafullname";
            ddlRegional.DataValueField = "AreaID";
            ddlRegional.DataBind();
            ddlRegional.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlRegional.SelectedIndex = 0;
        }

        protected void ddlInsuranceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlInsuranceType.SelectedValue == "")
            {
                ddlInsco.Items.Clear();
                ddlInsco.Items.Insert(0, new ListItem("- Pilih -", ""));
            }
            else
            {
                LoadInsco(ddlInsuranceType.SelectedValue);
            }
        }

        private void LoadInsco(string strInsuranceType)
        {
            ddlInsco.Items.Clear();
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            if (strInsuranceType == "1")
            {
                ds = qry.QueryAssetIns();
            }
            else if (strInsuranceType == "2")
            {
                ds = qry.QueryLifeIns();
            }
            ddlInsco.DataSource = ds;
            ddlInsco.DataTextField = "INSCONAME";
            ddlInsco.DataValueField = "INSCOID";
            ddlInsco.DataBind();
            ddlInsco.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlInsco.SelectedIndex = 0;
        }

        protected void ddlRegional_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadBranch(ddlRegional.SelectedValue.Trim());
        }

        private void LoadBranch(string strArea)
        {
            ddlBranch.Items.Clear();
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranchArea(strArea);
            ddlBranch.DataSource = ds;
            ddlBranch.DataTextField = "branchfullname";
            ddlBranch.DataValueField = "branchid";
            ddlBranch.DataBind();
            ddlBranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlBranch.SelectedIndex = 0;
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            if (ddlInsuranceType.SelectedValue == "")
            {
                Response.Write(" <script language=\"javascript\" type=\"text/javascript\"> alert('Silahkan pilih Insurance Type !'); </script>");
                ddlInsuranceType.Focus();
                return;
            }

            if (txtGoLiveStart.Text != "")
            {
                if (txtGoLiveEnd.Text == "")
                {
                    Response.Write(" <script language=\"javascript\" type=\"text/javascript\"> alert('Tanggal Go Live Date End harus diisi !'); </script>");
                    txtGoLiveEnd.Focus();
                    return;
                }
            }

            if (txtGoLiveEnd.Text != "")
            {
                if (txtGoLiveStart.Text == "")
                {
                    Response.Write(" <script language=\"javascript\" type=\"text/javascript\"> alert('Tanggal Go Live Date Start harus diisi !'); </script>");
                    txtGoLiveStart.Focus();
                    return;
                }
            }

            if (txtPaidDateStart.Text != "")
            {
                if (txtPaidDateEnd.Text == "")
                {
                    Response.Write(" <script language=\"javascript\" type=\"text/javascript\"> alert('Tanggal Paid Date End harus diisi !'); </script>");
                    txtPaidDateEnd.Focus();
                    return;
                }
            }

            if (txtPaidDateEnd.Text != "")
            {
                if (txtPaidDateStart.Text == "")
                {
                    Response.Write(" <script language=\"javascript\" type=\"text/javascript\"> alert('Tanggal Paid Date Start harus diisi !'); </script>");
                    txtPaidDateStart.Focus();
                    return;
                }
            }

            if (txtGoLiveStart.Text != "" || txtGoLiveEnd.Text != "")
            {
                if (DateTime.ParseExact(txtGoLiveStart.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(txtGoLiveEnd.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture))
                {
                    Response.Write(" <script language=\"javascript\" type=\"text/javascript\"> alert('Tanggal Go Live Date tidak sesuai !'); </script>");
                    txtGoLiveStart.Focus();
                    return;
                }
            }

            if (txtPaidDateStart.Text != "" || txtPaidDateEnd.Text != "")
            {
                if (DateTime.ParseExact(txtPaidDateStart.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(txtPaidDateEnd.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture))
                {
                    Response.Write(" <script language=\"javascript\" type=\"text/javascript\"> alert('Tanggal Paid Date tidak sesuai !'); </script>");
                    txtPaidDateStart.Focus();
                    return;
                }
            }

            Response.Redirect("RptPembayaranPremiAsuransiView.aspx?InsuranceTypeId=" + ddlInsuranceType.SelectedValue.ToString().Trim() +
                              "&InsuranceTypeName=" + ddlInsuranceType.SelectedItem.Text.Trim() +
                              "&InscoId=" + ddlInsco.SelectedValue +
                              "&InscoName=" + ddlInsco.SelectedItem.Text +
                              "&regionalId=" + ddlRegional.SelectedValue +
                              "&regionalName=" + ddlRegional.SelectedItem.Text +
                              "&branchId=" + ddlBranch.SelectedValue +
                              "&branchName=" + ddlBranch.SelectedItem.Text +
                              "&golivestart=" + txtGoLiveStart.Text +
                              "&goliveend=" + txtGoLiveEnd.Text +
                              "&paiddatestart=" + txtPaidDateStart.Text +
                              "&paiddateend=" + txtPaidDateEnd.Text + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("RptPembayaranPremiAsuransi.aspx");
        }
    }
}