﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RptPembayaranPremiAsuransi.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.ReportPembayaranPremiAsuransi.RptPembayaranPremiAsuransi" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({
                dateFormat: 'dd/mm/yy'
            });
        });
    </script>
</head>
<body>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
    <form id="form1" runat="server">
        <div>
            <asp:UpdatePanel runat="server" ID="upPath" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="linkPath">
                        <asp:Label runat="server" ID="lblPath" Text="Report/ReportRincianPremi"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="upToolbar" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="toolbar">
                        <span>
                            <label id="pageTitle">Report Rincian Premi</label>
                        </span>
                        <span id="toolMenuContainer"></span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />


            <div id="dPDCReceive">
                <div class="subSection">
                    <a href="javascript:ExpandUnexpandMenu('minSearchForm','dSearchForm', 'ucReportSearch_UCSubSectionContainer1_subSectionID')" id="minSearchForm" style="color: rgb(0, 0, 0);">[-]</a>
                    <span id="ucReportSearch_UCSubSectionContainer1_subSectionID" style="color: rgb(0, 0, 0);">Search</span>
                </div>
                <div id="dSearchForm">
                    <table id="ucReportSearch_tblFixedSearch" class="formTable">
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Insurance Type</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlInsuranceType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlInsuranceType_SelectedIndexChanged">
                                    <asp:ListItem Value="">- Pilih -</asp:ListItem>
                                    <asp:ListItem Value="1">Asset Insurance</asp:ListItem>
                                    <asp:ListItem Value="2">Life Insurance</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="tdDesc" style="width: 20%;">Go Live Date</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:TextBox runat="server" AutoCompleteType="Enabled" ID="txtGoLiveStart" CssClass="datepicker"></asp:TextBox>
                                To
                                <asp:TextBox runat="server" AutoCompleteType="Enabled" ID="txtGoLiveEnd" CssClass="datepicker"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Insco Name</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:DropDownList ID="ddlInsco" runat="server" >
                                    <asp:ListItem Value="">- Pilih -</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="tdDesc" style="width: 20%;">Paid Date</td>
                            <td class="tdValue" style="width: 30%;">
                                <asp:TextBox runat="server" AutoCompleteType="Enabled" ID="txtPaidDateStart" CssClass="datepicker"></asp:TextBox>
                                To
                                <asp:TextBox runat="server" AutoCompleteType="Enabled" ID="txtPaidDateEnd" CssClass="datepicker"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Regional</td>
                            <td class="tdValue" style="width: 30%;" colspan="3">
                                <asp:DropDownList ID="ddlRegional" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRegional_SelectedIndexChanged">
                                    <asp:ListItem Value="">- Pilih -</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDesc" style="width: 20%;">Branch</td>
                            <td class="tdValue" style="width: 30%;" colspan="3">
                                <asp:DropDownList ID="ddlBranch" runat="server" >
                                    <asp:ListItem Value="">ALL</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    <table class="formTable">
                        <tr>
                            <td align="right">
                                <asp:UpdatePanel runat="server" ID="upButton" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="lb_Print_Sync" CssClass="btnForm" Text="Print Synchronously"
                                            OnClick="lb_Print_Sync_Click"></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbReset" Text="RESET" CssClass="buttonComp" OnClick="lbReset_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
