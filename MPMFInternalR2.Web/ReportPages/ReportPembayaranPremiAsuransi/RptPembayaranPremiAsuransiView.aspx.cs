﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.ReportPembayaranPremiAsuransi
{
    public partial class RptPembayaranPremiAsuransiView : System.Web.UI.Page
    {
        private string InsuranceTypeId, InsuranceTypeName, InscoId, InscoName, regionalId, regionalName, branchId, branchName, golivestart, goliveend, paiddatestart, paiddateend;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            InsuranceTypeId = Request.QueryString["InsuranceTypeId"].ToString().Trim();
            InsuranceTypeName = Request.QueryString["InsuranceTypeName"].ToString().Trim();
            InscoId = Request.QueryString["InscoId"].ToString().Trim();
            InscoName = Request.QueryString["InscoName"].ToString().Trim();
            regionalId = Request.QueryString["regionalId"].ToString().Trim();
            regionalName = Request.QueryString["regionalName"].ToString().Trim();
            branchId = Request.QueryString["branchId"].ToString().Trim();
            branchName = Request.QueryString["branchName"].ToString().Trim();
            golivestart = Request.QueryString["golivestart"].ToString().Trim();
            goliveend = Request.QueryString["goliveend"].ToString().Trim();
            paiddatestart = Request.QueryString["paiddatestart"].ToString().Trim();
            paiddateend = Request.QueryString["paiddateend"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            dt = qry.QueryReportPembayaranPremiAsuransi(InsuranceTypeId, InscoId, regionalId, branchId, golivestart, goliveend, paiddatestart, paiddateend, "spReportPembayaranPremiAsuransi");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptPembayaranPremiAsuransi.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("InsuranceTypeName", InsuranceTypeName);
            ReportParameter p2 = new ReportParameter("InscoName", InscoName);
            ReportParameter p3 = new ReportParameter("regionalName", regionalName);
            ReportParameter p4 = new ReportParameter("branchName", branchName);
            ReportParameter p5 = new ReportParameter("golivestart", golivestart);
            ReportParameter p6 = new ReportParameter("goliveend", goliveend);
            ReportParameter p7 = new ReportParameter("paiddatestart", paiddatestart);
            ReportParameter p8 = new ReportParameter("paiddateend", paiddateend);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6, p7, p8 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}