﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.MPMFCust_RptAssetManagement
{
    public partial class MPMFRptAssetManagementStockView : System.Web.UI.Page
    {
        private string branchid, branchfullname, RptType, startDate, AreaId, Areafullname;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branchid = Request.QueryString["branchid"].ToString().Trim();
            branchfullname = Request.QueryString["branchname"].ToString().Trim();
            //RptType = Request.QueryString["RptType"].ToString().Trim();
            startDate = Request.QueryString["StartDate"].ToString().Trim();
            //endDate = Request.QueryString["EndDate"].ToString().Trim();
            AreaId = Request.QueryString["AreaId"].ToString().Trim();
            Areafullname = Request.QueryString["AreaFullName"].ToString().Trim();


            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();


            //dt = qry.QueryReportspMPMFAssetManagementStock(branchid, startDate, "spMPMFAmStock");
            dt = qry.QueryReportspMPMFAssetManagementStock(AreaId,branchid, startDate, "uspMPMFAmStock");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptAssetMgmntStock.rdlc");

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("date", startDate);
            ReportParameter p2 = new ReportParameter("Branch", branchfullname);
            ReportParameter p3 = new ReportParameter("AreaName", Areafullname);
            //ReportParameter p4 = new ReportParameter("assettype", RptType);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}