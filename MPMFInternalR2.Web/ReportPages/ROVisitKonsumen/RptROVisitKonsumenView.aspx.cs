﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.ROVisitKonsumen
{
    public partial class RptROVisitKonsumenView : System.Web.UI.Page
    {
        private string branch, branchname, maturitydate, maturitydate2, category, product, categoryName, productName, loginid;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branch = Request.QueryString["branchid"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            maturitydate = Request.QueryString["maturitydate"].ToString().Trim();
            maturitydate2 = Request.QueryString["maturitydate2"].ToString().Trim();
            category = Request.QueryString["category"].ToString().Trim();
            product = Request.QueryString["product"].ToString().Trim();
            categoryName = Request.QueryString["categoryName"].ToString().Trim();
            productName = Request.QueryString["productName"].ToString().Trim();
            loginid = Request.QueryString["loginid"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryReportROVisitKonsumen(branch, maturitydate, maturitydate2, category, product, "spMPMFCustRptROVisitKonsumen");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;

            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptROVisitKonsumenNew2.rdlc");

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);

            ReportViewer1.LocalReport.DataSources.Clear();
            ReportParameter p1 = new ReportParameter("Branch", branch);
            ReportParameter p2 = new ReportParameter("BranchName", branchname);
            ReportParameter p3 = new ReportParameter("MaturityDate", maturitydate);
            ReportParameter p4 = new ReportParameter("MaturityDate2", maturitydate2);
            ReportParameter p5 = new ReportParameter("CategoryCustomer", categoryName.Trim());
            ReportParameter p6 = new ReportParameter("Product", productName.Trim());
            ReportParameter p7 = new ReportParameter("LoginID", loginid);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6, p7 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}