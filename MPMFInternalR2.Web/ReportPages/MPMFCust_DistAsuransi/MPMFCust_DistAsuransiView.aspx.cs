﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.MPMFCust_DistAsuransi
{
    public partial class MPMFCust_DistAsuransiView : System.Web.UI.Page
    {
        private string branch, tglfrom, tglto;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branch = Request.QueryString["branchid"].ToString().Trim();
            tglfrom = Request.QueryString["tglfrom"].ToString().Trim();
            tglto = Request.QueryString["tglto"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryRptDistAsuransi(branch, tglfrom, tglto, "spMPMFCust_DistAsuransi");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptMPMFCust_DistAsuransi.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("BranchId", branch);
            ReportParameter p2 = new ReportParameter("tglfrom", tglfrom);
            ReportParameter p3 = new ReportParameter("tglto", tglto);
           
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3});
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}