﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MPMFInternalR2.Web.ReportPages.MPMFCust_AutoAssign
{
    public partial class AutoAssignView : System.Web.UI.Page
    {
        private string LoginID,tglfrom, tglto;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        protected void bindreport()
        {
            LoginID = Request.QueryString["LoginID"].ToString().Trim();
            tglfrom = Request.QueryString["tglfrom"].ToString().Trim();
            tglto = Request.QueryString["tglto"].ToString().Trim();

            DataTable dataTable = new DataTable();
            dataTable = new QueryConnection().QueryRptCustAutoAssign(LoginID, tglfrom, tglto, "SPMPMFCustAutoAssign");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptCustAutoAssign.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dataTable);
            ReportParameter p1 = new ReportParameter("LoginID", LoginID);
            ReportParameter p2 = new ReportParameter("tglfrom", tglfrom);
            ReportParameter p3 = new ReportParameter("tglto", tglto);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2,p3 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}