﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.LaporanMonitoring
{
    public partial class MPMFRptLapMonitoring : WebFormBase
    {
        string pilih = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            /*
           //Open for testing
           Session.Add("loginid", "OloanP");
           Session.Add("sesBranchId", "'999'");
           */

            //Tambahan AdIns utk report beda pool
            //if (SessionInvalid())
            //{
            //    Response.Redirect("~/am_sessionend.aspx");
            //}


            if (!IsPostBack)
            {
                this.ddlPilihan.SelectedIndex = ddlPilihan.Items.IndexOf(ddlPilihan.Items.FindByText("Select One"));

                lblWarPilihRpt.Visible = false;
                lblWarPilihStatus.Visible = false;
                lblWarPilih.Visible = false;

                BindChooseBranch();

                //ini kalau back lagi ke report
                if (Request.QueryString["tglfrom"] != null)
                {
                    lbltglfrom.Text = Request.QueryString["tglfrom"];
                    lbltglto.Text = Request.QueryString["tglto"];
                    lblpilihan.Text = Request.QueryString["pilihan"];
                    lblpilihn.Text = Request.QueryString["pilihn"];
                    lblreport.Text = Request.QueryString["rpt"];
                    txtreport.Text = Request.QueryString["lblrpt"];
                    lblStatusID.Text = Request.QueryString["statusid"];
                    lblstatusname.Text = Request.QueryString["statusname"];
                    lblchoose.Text = Request.QueryString["choose"];
                    this.ddlPilihan.SelectedIndex = ddlPilihan.Items.IndexOf(ddlPilihan.Items.FindByText(lblchoose.Text.ToString().Trim()));

                    /*FF - ganti pake datepicker
                    this.txtFromDate.Text = lbltglfrom.Text.ToString().Trim();
                    this.txtEndDate.Text = lbltglto.Text.ToString().Trim();
                    */

                    //ini kalau back lagi ke report
                    if (ddlPilihan.SelectedValue == "0")
                    {
                        pilih = "";
                        ddlPilihan.Focus();
                        ddlCabang.Visible = false;
                        ddlRegional.Visible = false;
                        lblWarPilih.Visible = true;
                        return;
                    }

                    if (lblchoose.Text == "Cabang")
                    {
                        ddlCabang.ClearSelection();
                        pilih = "";
                        BindAllCabang();
                        ddlCabang.Visible = true;
                        ddlCabang.SelectedIndex = ddlCabang.Items.IndexOf(ddlCabang.Items.FindByValue(lblpilihan.Text.ToString().Trim()));
                        ddlCabang.Focus();
                        ddlRegional.Visible = false;
                        lblWarPilih.Visible = false;
                    }
                    if (lblchoose.Text == "Regional")
                    {
                        pilih = "";
                        ddlCabang.Visible = false;
                        ddlRegional.Visible = true;
                        ddlRegional.SelectedIndex = ddlPilihan.Items.IndexOf(ddlPilihan.Items.FindByValue(lblpilihan.Text.ToString().Trim()));
                        ddlRegional.Focus();
                        lblWarPilih.Visible = false;
                    }

                    if (lblreport.Text != null)
                    {
                        if (lblreport.Text == "1")
                        {
                            lblWarPilihRpt.Visible = false;
                            BindProsesFidusia();
                            ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                            ddlStatus.Visible = true;
                            ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                            lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                            ddlPilihRpt.Focus();
                        }
                        else if (lblreport.Text == "2")
                        {
                            lblWarPilihRpt.Visible = false;
                            BindProsesAsuransi();
                            ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                            ddlStatus.Visible = true;
                            ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                            lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                            ddlPilihRpt.Focus();
                        }
                        else if (lblreport.Text == "3")
                        {
                            lblWarPilihRpt.Visible = false;
                            BindTTR();
                            ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                            ddlStatus.Visible = true;
                            ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                            lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                            ddlPilihRpt.Focus();
                        }
                        else if (lblreport.Text == "4")
                        {
                            lblWarPilihRpt.Visible = false;
                            BindDokKontrak();
                            ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                            ddlStatus.Visible = true;
                            ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                            lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                            ddlPilihRpt.Focus();
                        }
                        else if (lblreport.Text == "5")
                        {
                            lblWarPilihRpt.Visible = false;
                            BindClaimAsuransi();
                            ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                            ddlStatus.Visible = true;
                            ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                            lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                            ddlPilihRpt.Focus();
                        }
                        else if (lblreport.Text == "6")
                        {
                            lblWarPilihRpt.Visible = false;
                            BindSuspend();
                            ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                            ddlStatus.Visible = true;
                            ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                            lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                            ddlPilihRpt.Focus();
                        }
                        else if (lblreport.Text == "7")
                        {
                            lblWarPilihRpt.Visible = false;
                            BindVoucher();
                            ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                            ddlStatus.Visible = true;
                            ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                            lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                            ddlPilihRpt.Focus();
                        }
                        else if (lblreport.Text == "8")
                        {
                            lblWarPilihRpt.Visible = false;
                            BindAplikasi();
                            ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                            ddlStatus.Visible = true;
                            ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                            lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                            ddlPilihRpt.Focus();
                        }
                        else if (lblreport.Text == "9")
                        {
                            lblWarPilihRpt.Visible = false;
                            BindBPKBBorrow();
                            ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                            ddlStatus.Visible = true;
                            ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                            lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                            ddlPilihRpt.Focus();
                        }
                        else if (lblreport.Text == "10")
                        {
                            lblWarPilihRpt.Visible = false;
                            BindSPBPKB();
                            ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                            ddlStatus.Visible = true;
                            ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                            lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                            ddlPilihRpt.Focus();
                        }
                        else if (lblreport.Text == "11")
                        {
                            lblWarPilihRpt.Visible = false;
                            BindSPPrinting();
                            ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                            ddlStatus.Visible = true;
                            ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                            lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                            ddlPilihRpt.Focus();
                        }
                        else if (lblreport.Text == "12")
                        {
                            lblWarPilihRpt.Visible = false;
                            BindSPPrinting();
                            ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                            ddlStatus.Visible = true;
                            ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                            lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                            ddlPilihRpt.Focus();
                        }
                        else if (lblreport.Text == "13")
                        {
                            ddlStatus.Visible = false;
                            lblstatustd.Text = "";
                            lblWarPilihRpt.Visible = false;
                            //ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                            //lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                            DatePicker.Focus();
                        }
                        else if (lblreport.Text == "14")
                        {
                            lblWarPilihRpt.Visible = false;
                            lblstatustd.Text = "Report By :";
                            BindChooseServiceType();
                            ddlPilihRpt.SelectedIndex = ddlPilihRpt.Items.IndexOf(ddlPilihRpt.Items.FindByValue(lblreport.Text.ToString().Trim()));
                            ddlStatus.Visible = true;
                            ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(lblstatusname.Text.ToString().Trim()));
                            lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
                            ddlPilihRpt.Focus();
                        }
                    }
                    else
                    {
                        ddlPilihRpt.Focus();
                        ddlStatus.Visible = false;
                    }
                }

            }

            lblWarPilihRpt.Visible = false;
            lblWarPilihStatus.Visible = false;
            lblWarPilih.Visible = false;
        }

        private void loadData()
        {
            loadbranch();
        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch();
            
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (HttpContext.Current.Session["sesBranchId"].ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbranch.SelectedIndex = 0;
        }


        protected void ddlRptType_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlRptType.SelectedValue == "0")
            {
                pilih = "";
                lblstatustd.Text = "Pilihan Status :";
                ddlPilihan.Focus();
                ddlCabang.Visible = false;
                ddlRegional.Visible = false;
                lblWarPilih.Visible = true;
                ddlPilihRpt.ClearSelection();
                ddlRegional.ClearSelection();
                ddlCabang.ClearSelection();
                lblWarPilihRpt.Visible = false;
                ddlStatus.Visible = false;
                return;
            }
            else
            { lblWarPilih.Visible = false; }

            if (ddlPilihRpt.SelectedValue == "0")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = true;
                ddlStatus.Visible = false;
                this.ddlStatus.DataTextField = "";
                this.ddlStatus.DataValueField = "";
                lblPilihRpt.Text = "";
                ddlPilihRpt.Focus();
                return;
            }
            else if (ddlPilihRpt.SelectedValue == "1")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindProsesFidusia();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "2")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindProsesAsuransi();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "3")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindTTR();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "4")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindDokKontrak();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "5")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindClaimAsuransi();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "6")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindSuspend();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "7")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindVoucher();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "8")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindAplikasi();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "9")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindBPKBBorrow();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "10")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindSPBPKB();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "11")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindSPPrinting();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "12")
            {
                lblstatustd.Text = "Pilihan Status :";
                lblWarPilihRpt.Visible = false;
                BindSPPrinting();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "13")
            {
                lblstatustd.Text = "";
                ddlStatus.Visible = false;
                lblWarPilihRpt.Visible = false;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }
            else if (ddlPilihRpt.SelectedValue == "14")
            {
                lblstatustd.Text = "Report By :";
                lblWarPilihRpt.Visible = false;
                BindChooseServiceType();
                ddlStatus.Visible = true;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();

            }

        }
        
        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string datepicker = "";
            string datepicker1 = "";
            string pilihn = "";

            if (ddlPilihan.SelectedValue == "0")
            {
                pilih = "";
                ddlPilihan.Focus();
                ddlCabang.Visible = false;
                ddlRegional.Visible = false;
                lblWarPilih.Visible = true;
                return;
            }
            else
            { lblWarPilih.Visible = false; }


            if (ddlPilihRpt.SelectedValue == "0")
            {
                lblWarPilihRpt.Visible = true;
                ddlStatus.Visible = false;
                this.ddlStatus.DataTextField = "";
                this.ddlStatus.DataValueField = "";
                lblPilihRpt.Text = "";
                ddlPilihRpt.Focus();
                return;
            }
            else
            {
                lblWarPilihRpt.Visible = false;
                lblPilihRpt.Text = ddlPilihRpt.SelectedItem.Text.ToString().Trim().ToUpper();
            }

            if (ddlStatus.SelectedValue == "000")
            {
                lblWarPilihStatus.Visible = true;
                ddlStatus.Focus();
                lblStatus.Text = "";
                lblStatusID.Text = "";
                return;
            }
            else
            {
                if (lblPilihRpt.Text == "LAPORAN MONITORING SURAT TUGAS/KUASA")
                {
                    lblWarPilihStatus.Visible = false;
                    lblStatus.Text = "";
                    lblStatusID.Text = "";
                }
                else
                {
                    lblWarPilihStatus.Visible = false;
                    lblStatus.Text = ddlStatus.SelectedItem.Text.ToString().Trim();
                    lblStatusID.Text = ddlStatus.SelectedValue.ToString().Trim();
                }

            }

            if (DatePicker.CalendarDate.ToString("dd/MM/yyyy") == "01/01/0001")
            {
                DatePicker.Focus();
                return;
            }
            else
            {
                datepicker = DatePicker.CalendarDate.ToString("dd/MM/yyyy");
            }

            if (DatePicker1.CalendarDate.ToString("dd/MM/yyyy") == "01/01/0001")
            {
                DatePicker1.Focus();
                return;
            }
            else
            {
                datepicker1 = DatePicker1.CalendarDate.ToString("dd/MM/yyyy");
            }

            if (DatePicker.CalendarDate > DatePicker1.CalendarDate)
            {
                Response.Write(" <script language=\"javascript\" type=\"text/javascript\"> alert('Tanggal Awal tidak boleh lebih besar dari Tanggal Akhir... !'); </script>");
                DatePicker.Focus();
                return;
            }

            if (ddlPilihan.SelectedValue == "1")
            {
                Response.Redirect("vLaporanMonitoring.aspx?tglfrom=" + datepicker.ToString() + "&tglto=" + datepicker1.ToString() + "&choose=" + ddlPilihan.SelectedItem.Text.ToString().Trim() + "&pilihan=" + ddlCabang.SelectedValue.ToString().Trim() + "&pilihn=" + ddlCabang.SelectedItem.Text.ToString().Trim() + "&rpt=" + ddlPilihRpt.SelectedValue.ToString().Trim() + "&lblrpt=" + lblPilihRpt.Text.ToString().Trim().ToUpper() + "&statusname=" + lblStatus.Text.ToString().Trim() + "&statusid=" + lblStatusID.Text.ToString().Trim() + "");
            }

            else if (ddlPilihan.SelectedValue == "2")
            {
                pilihn = "-";
                Response.Redirect("vLaporanMonitoring.aspx?tglfrom=" + datepicker.ToString() + "&tglto=" + datepicker1.ToString() + "&choose=" + ddlPilihan.SelectedItem.Text.ToString().Trim() + "&pilihan=" + "R" + ddlRegional.SelectedValue.ToString().Trim() + "&pilihn=" + ddlRegional.SelectedItem.ToString().Trim() + "&rpt=" + ddlPilihRpt.SelectedValue.ToString().Trim() + "&lblrpt=" + lblPilihRpt.Text.ToString().Trim().ToUpper() + "&statusname=" + lblStatus.Text.ToString().Trim() + "&statusid=" + ddlStatus.SelectedValue.ToString().Trim() + "");
            }

            else
            {
                pilihn = "-";
                Response.Redirect("vLaporanMonitoring.aspx?tglfrom=" + datepicker.ToString() + "&tglto=" + datepicker1.ToString() + "&choose=" + ddlPilihan.SelectedItem.Text.ToString().Trim() + "&pilihan=" + ddlPilihan.SelectedValue.ToString().Trim() + "&pilihn=" + ddlPilihan.SelectedItem.Text.ToString().Trim() + "&rpt=" + ddlPilihRpt.SelectedValue.ToString().Trim() + "&lblrpt=" + lblPilihRpt.Text.ToString().Trim().ToUpper() + "&statusname=" + lblStatus.Text.ToString().Trim() + "&statusid=" + ddlStatus.SelectedValue.ToString().Trim() + "");
            }
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("MPMFRptAssetManagement.aspx");
        }
    }
}