﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.Web;

namespace MPMFInternalR2.Web.ReportPages.MPMFCust_ReportAppInJMOrder
{
    //public partial class MPMFCust_ReportAppInJMOrder : System.Web.UI.Page
    public partial class MPMFCust_ReportAppInJMOrder : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "49680921";

            if (!IsPostBack)
            {
                loadArea();
                if (base.CurrentUserContext.OfficeCode.ToString() != "999")
                    //if (Session["sesBranchId"].ToString() != "999")
                {
                    LoadBranchN();
                }
                else
                {
                    this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                }
                //loadData();
            }
        }

        private void LoadBranchN()
        {
            DataSet set = new DataSet();
            //set = new QueryConnection().QueryBranch(Session["sesBranchId"].ToString());
            set = new QueryConnection().QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            this.ddlbranch.DataSource = set;
            this.ddlbranch.DataTextField = "branchfullname";
            this.ddlbranch.DataValueField = "branchid";
            this.ddlbranch.DataBind();
            //if (HttpContext.Current.Session["sesBranchId"].ToString() == "999")
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
                //if (Session["sesBranchId"].ToString() == "999")
            {
                this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            this.ddlbranch.SelectedIndex = 0;
            set.Dispose();
        }

        private void loadArea()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            //ds = qry.QueryAreaWithBranch(Session["sesBranchId"].ToString());
            ds = qry.QueryAreaWithBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlArea.DataSource = ds;
            ddlArea.DataTextField = "areafullname";
            ddlArea.DataValueField = "AreaID";
            ddlArea.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
                //if (Session["sesBranchId"].ToString() == "999")
            {
                ddlArea.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
        }

        protected void lb_Print_Sync_OnClick(object sender, EventArgs e)
        {
            try
            {
                string strStartDate = "";
                
                //if (dtStart.Text == "")
                //    strStartDate = DateTime.Now.ToString("dd/MM/yyyy");
                //else
                //    strStartDate = dtStart.Text;
                string strAgreementno = "";
                if ((dtStart.Text == string.Empty) | (dtEnd.Text == string.Empty))
                {
                    this.lblwardate.Text = "Please Fill Period From And Period To";
                    lblwardate.Visible = true; return;
                }

                if (DateTime.ParseExact(dtStart.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(dtEnd.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture))
                {
                    lblwardate.Visible = true; return;
                }
                else
                {
                    lblwardate.Visible = false;
                }

                Page.ClientScript.RegisterStartupScript(
                    this.GetType(), "OpenWindow", "window.open('MPMFCust_ReportAppInJMOrderView.aspx?agreementno=" + strAgreementno.ToString().Trim() + "&area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&date=" + dtStart.Text.ToString() + "&dateEnd=" + dtEnd.Text.ToString() + "','_newtab');", true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        protected void lb_Dowmload_OnClick(object sender, EventArgs e)
        {
            try
            {
                string strStartDate = "";

                string strAgreementno = "";
                //if (dtStart.Text == "")
                //    strStartDate = DateTime.Now.ToString("dd/MM/yyyy");
                //else
                //    strStartDate = dtStart.Text;

                if ((dtStart.Text == string.Empty) | (dtEnd.Text == string.Empty))
                {
                    this.lblwardate.Text = "Please Fill Period From And Period To";
                    lblwardate.Visible = true; return;
                }

                if (DateTime.ParseExact(dtStart.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(dtEnd.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture))
                {
                    lblwardate.Visible = true; return;
                }
                else
                {
                    lblwardate.Visible = false;
                }

                Response.Redirect("MPMFCust_ReportAppInJMOrderDownload.aspx?agreementno=" + strAgreementno.ToString().Trim() + "&area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&date=" + dtStart.Text.ToString() + "&dateEnd=" + dtEnd.Text.ToString());

                //Page.ClientScript.RegisterStartupScript(
                //    this.GetType(), "OpenWindow", "window.open('MPMFCust_ReportSoldDownload.aspx?jenis=" + ddlJenisNilai.SelectedValue.ToString().Trim() + "&area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&date=" + dtStart.Text.ToString() + "&dateEnd=" + dtEnd.Text.ToString() + "','_newtab');", true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        protected void lbReset_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("MPMFCust_ReportAppInJMOrder.aspx");
        }

        protected void ddlArea_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlArea.SelectedValue != "")
            {
                LoadBranch(ddlArea.SelectedValue.Trim());
            }
        }

        private void LoadBranch(string strArea)
        {
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
                //if (Session["sesBranchId"].ToString() == "999")
            {
                DataSet ds = new DataSet();
                QueryConnection qry = new QueryConnection();
                ds = qry.QueryBranchArea(strArea);
                ddlbranch.DataSource = ds;
                ddlbranch.DataTextField = "branchfullname";
                ddlbranch.DataValueField = "branchid";
                ddlbranch.DataBind();
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                ddlbranch.SelectedIndex = 0;
            }
            else
            {
                DataSet set = new DataSet();
                //set = new QueryConnection().QueryBranch(Session["sesBranchId"].ToString());
                set = new QueryConnection().QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
                this.ddlbranch.DataSource = set;
                this.ddlbranch.DataTextField = "branchfullname";
                this.ddlbranch.DataValueField = "branchid";
                this.ddlbranch.DataBind();
                //if (HttpContext.Current.Session["sesBranchId"].ToString() == "999")
                if (base.CurrentUserContext.OfficeCode.ToString() == "999")
                    //if (Session["sesBranchId"].ToString() == "999")
                {
                    this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                }
                this.ddlbranch.SelectedIndex = 0;
                set.Dispose();
            }
        }
    }
}