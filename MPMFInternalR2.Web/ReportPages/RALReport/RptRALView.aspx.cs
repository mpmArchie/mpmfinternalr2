﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.RALReport
{
    public partial class RptRALView : System.Web.UI.Page
    {
        private string branch, branchname, date, date1, loginid, criteria, criteriaName;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branch = Request.QueryString["branchID"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            date = Request.QueryString["period"].ToString().Trim();
            date1 = Request.QueryString["period1"].ToString().Trim();
            criteria = Request.QueryString["criteria"].ToString().Trim();
            criteriaName = Request.QueryString["criteriaName"].ToString().Trim();
            loginid = Request.QueryString["loginid"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            dt = qry.QueryReportRAL(branch, date, date1, criteria, "spSAFRALReport");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptRAL.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("branchname", branchname);
            ReportParameter p2 = new ReportParameter("date", date);
            ReportParameter p3 = new ReportParameter("date1", date1);
            ReportParameter p4 = new ReportParameter("loginid", loginid);
            ReportParameter p5 = new ReportParameter("criteria", criteria);
            ReportParameter p6 = new ReportParameter("criteriaName", criteriaName);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}