﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.MonitoringRefundAyda
{
    public partial class Monitoring_Refund_Ayda_view : System.Web.UI.Page
    {       
        private string TerminationDate1, TerminationDate2, InsuranceCompanyName, AgreementNo, Status;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                bindreport();
            }
        }
        private void bindreport()
        {
            string nameFIle = "";
            TerminationDate1 = Request.QueryString["TerminationDate1"].ToString().Trim();
            TerminationDate2 = Request.QueryString["TerminationDate2"].ToString().Trim();
            InsuranceCompanyName = Request.QueryString["InsuranceCompanyName"].ToString().Trim();
            AgreementNo = Request.QueryString["AgreementNo"].ToString().Trim();
            Status = Request.QueryString["Status"].ToString().Trim();


            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            
            dt = qry.QueryReportMonitoringRefundAyda(TerminationDate1, TerminationDate2, InsuranceCompanyName, AgreementNo, Status, "sp_RPT_Monitoring_Refund_Ayda");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;

            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportMonitoringRefundAYDA.rdlc");

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("TerminationDate1", TerminationDate1);
            ReportParameter p2 = new ReportParameter("TerminationDate2", TerminationDate2);           
            ReportParameter p3 = new ReportParameter("InsuranceCompanyName", InsuranceCompanyName);
            ReportParameter p4 = new ReportParameter("AgreementNo", AgreementNo);
            ReportParameter p5 = new ReportParameter("Status", Status);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);

            nameFIle = "Report AP";


        }
    }
}