﻿using Confins.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.MonitoringRefundAyda
{
    public partial class Monitoring_Refund_Ayda : WebFormBase
    //public partial class Monitoring_Refund_Ayda : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            if (!IsPostBack)
            {
                loadCompanyName();
                QueryStatus();            
            }
        }

        private void loadCompanyName()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryCompanyName();
            ddlCompanyName.DataSource = ds;
            ddlCompanyName.DataTextField = "INSCO_NAME";
            ddlCompanyName.DataValueField = "INSCO_NAME";
            ddlCompanyName.DataBind();
            ddlCompanyName.Items.Insert(0, "Select One");
            ddlCompanyName.SelectedIndex = 0;
            ds.Dispose();
        }
        private void QueryStatus()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryStatus();
            ddlStatus.DataSource = ds;
            ddlStatus.DataTextField = "STATUS";
            ddlStatus.DataValueField = "STATUS";
            ddlStatus.DataBind();
            this.ddlStatus.Items.Insert(0, new ListItem("ALL", "ALL"));
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string strDateStart = "";
            string strDateEnd = "";

            if (txtTerminationDate1.Text == "")
            {
                lblWarDate.Visible = true; return;
            }
            else
                strDateStart = txtTerminationDate1.Text;

            if (txtTerminationDate2.Text == "")
            {
                lblWarDate.Visible = true; return;
            }
            else
                strDateEnd = txtTerminationDate2.Text;

            if (DateTime.ParseExact(strDateStart, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(strDateEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblWarDate.Visible = true; return;
            }
            else
            {
                lblWarDate.Visible = false;
            }
            if (txtAgreementNo.Text == "")
            {
                lblwartxtAgreementNo.Visible = true;
                txtAgreementNo.Focus();
                return;
            }
            else
            {
                lblwartxtAgreementNo.Visible = false;
            }
            if (ddlCompanyName.SelectedValue == "0")
            {
                lblddlInsuranceCompany.Visible = true;
                ddlCompanyName.Focus();
                return;
            }
            else
            {
                lblddlInsuranceCompany.Visible = false;
            }

            Response.Redirect("Monitoring_Refund_Ayda_view.aspx?TerminationDate1=" + strDateStart.ToString().Trim().ToString().Trim() + "&TerminationDate2=" + strDateEnd.ToString().Trim() + "&Status=" + this.ddlStatus.SelectedItem.Value.ToString().Trim() + "&InsuranceCompanyName=" + ddlCompanyName.SelectedValue.ToString().Trim() + "&AgreementNo=" + txtAgreementNo.Text.ToString().Trim());
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("Monitoring_Refund_Ayda.aspx");
        }
    }
}