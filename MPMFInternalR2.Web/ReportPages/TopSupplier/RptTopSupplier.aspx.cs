﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.TopSupplier
{
    //public partial class RptTopSupplier : System.Web.UI.Page
    public partial class RptTopSupplier : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["sesBranchId"] = "999";
            //HttpContext.Current.Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                loadbranch();
                txtPeriodYearS.Text = DateTime.Now.Year.ToString();
                txtPeriodYearE.Text = DateTime.Now.Year.ToString();
                txtNTop.Text = "20";
            }
        }

        private void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            ddlbranch.Items.Insert(0, new ListItem("Select One", ""));
            //if (HttpContext.Current.Session["sesBranchId"].ToString() == "999")
            //{
            //    ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            //}
            ddlbranch.SelectedIndex = 0;
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            
            if (ddlbranch.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('branch belum dipilih !!');", true);
            }
            else if (txtNTop.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('masukan select top !!');", true);
            }
            else
            {
                Response.Redirect("RptTopSupplierView.aspx?optionreport=" + "&branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim() + "&PeriodeMonthS=" + cboPeriodMonthS.SelectedValue.Trim() + "&PeriodeYearS=" + txtPeriodYearS.Text + "&PeriodeMonthE=" + cboPeriodMonthE.SelectedValue.Trim() + "&PeriodeYearE=" + txtPeriodYearE.Text + "&Currency=" + cboCurrency.SelectedValue.Trim() + "&ProdType=" + cboProductType.SelectedValue.Trim() + "&Ntop=" + txtNTop.Text.Trim() + "&PeriodeMonthNameS=" + cboPeriodMonthS.SelectedItem.Text.Trim() + "&PeriodeMonthNameE=" + cboPeriodMonthE.SelectedItem.Text.Trim() + "");
            }
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("RptTopSupplier.aspx");
        }

        protected void ddlbranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadPOS(ddlbranch.SelectedValue);
        }

        private void LoadPOS(string strBranch)
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryPOS(strBranch);
            ddlPos.DataSource = ds;
            ddlPos.DataTextField = "branchfullname";
            ddlPos.DataValueField = "branchid";
            ddlPos.DataBind();
            ddlPos.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlPos.SelectedIndex = 0;
        }
    }
}