﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.TopSupplier
{
    public partial class RptTopSupplierView : System.Web.UI.Page
    {
        private string branchid, branchname, PeriodeMonthS, PeriodeYearS, PeriodeMonthE, PeriodeYearE, Currency, ProdType, Ntop;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindReport();
            }
        }

        private void BindReport()
        {
            branchid = Request.QueryString["branchid"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            PeriodeMonthS = Request.QueryString["PeriodeMonthS"].ToString().Trim();
            PeriodeYearS = Request.QueryString["PeriodeYearS"].ToString().Trim();
            PeriodeMonthE = Request.QueryString["PeriodeMonthE"].ToString().Trim();
            PeriodeYearE = Request.QueryString["PeriodeYearE"].ToString().Trim();
            Currency = Request.QueryString["Currency"].ToString().Trim();
            ProdType = Request.QueryString["ProdType"].ToString().Trim();
            Ntop = Request.QueryString["Ntop"].ToString().Trim();

            string strWhereCond = "";

            strWhereCond = " ro.office_code = '" + branchid + "' and month(s.sales_dt) >= '" + PeriodeMonthS.Trim() + "' and year(s.sales_dt) >= '" + PeriodeYearS.Trim() + "' and month(s.sales_dt) = '" + PeriodeMonthE.Trim() + "' and year(s.sales_dt) = '" + PeriodeYearE.Trim() + "' and rc.curr_code = '" + Currency.Trim() + "' and agr.prod_type = '" + ProdType.Trim() + "' ";

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryReportspTopSupplierRpt(strWhereCond, Ntop, "spTopSupplierRpt");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptTopSupplierRpt.rdlc");

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("PeriodeMonthS", Request.QueryString["PeriodeMonthNameS"].ToString().Trim());
            ReportParameter p2 = new ReportParameter("PeriodeYearS", PeriodeYearS);
            ReportParameter p3 = new ReportParameter("PeriodeMonthE", Request.QueryString["PeriodeMonthNameE"].ToString().Trim());
            ReportParameter p4 = new ReportParameter("PeriodeYearE", PeriodeYearE);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}