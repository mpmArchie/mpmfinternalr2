﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;


namespace MPMFInternalR2.Web.ReportPages.SMSInbox
{
    public partial class CallCenterSMSInbox : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string str = "";
            string str2 = "";

            if (dtStart.Text == "")
                str = DateTime.Now.ToString("dd/MM/yyyy");
            else
                str = dtStart.Text;

            if (dtEnd.Text == "")
                str2 = DateTime.Now.ToString("dd/MM/yyyy");
            else
                str2 = dtEnd.Text;

            if (DateTime.ParseExact(str, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(str2, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Response.Redirect("CallCenterSMSInboxView.aspx?date1=" + str.ToString().Trim() + "&date2=" + str2.ToString().Trim() + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("CallCenterSMSInbox.aspx");
        }
    }
}