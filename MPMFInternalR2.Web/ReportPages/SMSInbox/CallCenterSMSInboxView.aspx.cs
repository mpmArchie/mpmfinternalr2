﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.SMSInbox
{
    public partial class CallCenterSMSInboxView : System.Web.UI.Page
    {
        private string date1, date2;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.bindreport();
            }
        }

        private void bindreport()
        {
            date1 = Request.QueryString["date1"].ToString().Trim();
            date2 = Request.QueryString["date2"].ToString().Trim();

            DataTable dataTable = new DataTable();
            dataTable = new QueryConnection().QueryReportspSAFCustCallCenterRptSMSInbox(base.Request.QueryString["date1"], base.Request.QueryString["date2"], "spSAFCustCallCenterRptSMSInbox");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptCallCenterSMSInbox.rdlc");

            ReportDataSource datasource = new ReportDataSource("DataSet1", dataTable);
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportParameter p1 = new ReportParameter("date1", date1);
            ReportParameter p2 = new ReportParameter("date2", date2);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2});
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}