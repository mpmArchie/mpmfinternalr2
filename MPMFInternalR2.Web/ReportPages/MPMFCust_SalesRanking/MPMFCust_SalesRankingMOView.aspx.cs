﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.Web;
using Microsoft.Reporting.WebForms;

namespace MPMFInternalR2.Web.ReportPages.MPMFCust_SalesRanking
{
    public partial class MPMFCust_SalesRankingMOView : System.Web.UI.Page
    //public partial class MPMFCust_SalesRankingMOView : WebFormBase
    {
        private string area, areaname, cabang, cabangname, date, datename, dateEnd, assettype, assettypename, product, productname;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            area = Request.QueryString["area"].ToString().Trim();
            areaname = Request.QueryString["areaname"].ToString().Trim();
            cabang = Request.QueryString["branch"].ToString().Trim();
            cabangname = Request.QueryString["branchname"].ToString().Trim();
            date = Request.QueryString["date"].ToString().Trim();
            datename = Request.QueryString["datename"].ToString().Trim();
            dateEnd = Request.QueryString["dateEnd"].ToString().Trim();
            assettype = Request.QueryString["assettype"].ToString().Trim();
            assettypename = Request.QueryString["assettypename"].ToString().Trim();
            product = Request.QueryString["product"].ToString().Trim();
            productname = Request.QueryString["productname"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            dt = qry.QueryReportRankingCabang(area, cabang, date, dateEnd, assettype, product, "sp_RPT_RANKINGMO");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportRANKINGMO.rdlc");

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);

            ReportParameter p1 = new ReportParameter("area", areaname);
            ReportParameter p2 = new ReportParameter("branch", cabangname);
            ReportParameter p3 = new ReportParameter("datename", datename);
            ReportParameter p4 = new ReportParameter("dateEnd", dateEnd);
            ReportParameter p5 = new ReportParameter("assettypename", assettypename);
            ReportParameter p6 = new ReportParameter("product", productname);


            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);

        }
    }
}