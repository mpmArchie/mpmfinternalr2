﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;

namespace MPMFInternalR2.Web.ReportPages.MPMFCust_SalesRanking
{
    public partial class MPMFCust_SalesRankingDownload : System.Web.UI.Page
    {
        private string area, areaname, cabang, cabangname, date, datename, dateEnd, assettype, assettypename, product, productname;
        protected void Page_Load(object sender, EventArgs e)
        {
            bindreport();
        }

        private void bindreport()
        {
            area = Request.QueryString["area"].ToString().Trim();
            areaname = Request.QueryString["areaname"].ToString().Trim();
            cabang = Request.QueryString["branch"].ToString().Trim();
            cabangname = Request.QueryString["branchname"].ToString().Trim();
            date = Request.QueryString["date"].ToString().Trim();
            datename = Request.QueryString["datename"].ToString().Trim();
            dateEnd = Request.QueryString["dateEnd"].ToString().Trim();
            assettype = Request.QueryString["assettype"].ToString().Trim();
            assettypename = Request.QueryString["assettypename"].ToString().Trim();
            product = Request.QueryString["product"].ToString().Trim();
            productname = Request.QueryString["productname"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryReportRankingCabang(area, cabang, date, dateEnd, assettype, product, "sp_RPT_RANKINGCABANG");

            DataTable dt2 = new DataTable();
            QueryConnection qry2 = new QueryConnection();
            dt2 = qry2.QueryReportRankingCabang(area, cabang, date, dateEnd, assettype, product, "sp_RPT_RANKINGMO");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportRANKINGCABANGDANMO.rdlc");

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportDataSource datasource2 = new ReportDataSource("DataSet2", dt2);


            ReportParameter p1 = new ReportParameter("area", areaname);
            ReportParameter p2 = new ReportParameter("branch", cabangname);
            ReportParameter p3 = new ReportParameter("datename", datename);
            ReportParameter p4 = new ReportParameter("dateEnd", dateEnd);
            ReportParameter p5 = new ReportParameter("assettypename", assettypename);
            ReportParameter p6 = new ReportParameter("product", productname);


            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
            ReportViewer1.LocalReport.DataSources.Add(datasource2);

            string nameFIle = "Report_Data_Ranking_Cabang_dan_MO";
            GenerateToExcel(ReportViewer1, nameFIle);

        }

        private void GenerateToExcel(ReportViewer reportViewer1, string nameFile)
        {
            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = reportViewer1.LocalReport.Render("EXCEL", null, out contentType, out encoding, out extension, out streamIds, out warnings);

            //Download the RDLC Report in Word, Excel, PDF and Image formats.
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + nameFile + "." + extension);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
        }
    }
}