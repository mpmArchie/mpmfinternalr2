﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.Web;
using Microsoft.Reporting.WebForms;

namespace MPMFInternalR2.Web.ReportPages.MPMFCust_SalesRanking
{
    public partial class MPMFCust_SalesRankingView : System.Web.UI.Page
    //public partial class MPMFCust_SalesRankingView : WebFormBase
    {
        private string area, areaname, cabang, cabangname, date, datename, dateEnd, assettype, assettypename, product, productname;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            area = Request.QueryString["area"].ToString().Trim();
            areaname = Request.QueryString["areaname"].ToString().Trim();
            cabang = Request.QueryString["branch"].ToString().Trim();
            cabangname = Request.QueryString["branchname"].ToString().Trim();
            date = Request.QueryString["date"].ToString().Trim();
            datename = Request.QueryString["datename"].ToString().Trim();
            dateEnd = Request.QueryString["dateEnd"].ToString().Trim();
            assettype = Request.QueryString["assettype"].ToString().Trim();
            assettypename = Request.QueryString["assettypename"].ToString().Trim();
            product = Request.QueryString["product"].ToString().Trim();
            productname = Request.QueryString["productname"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            dt = qry.QueryReportRankingCabang( area, cabang, date, dateEnd,  assettype, product, "sp_RPT_RANKINGCABANG");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportRANKINGCABANG.rdlc");
            
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);

            ReportParameter p1 = new ReportParameter("area", areaname);
            ReportParameter p2 = new ReportParameter("branch", cabangname);
            ReportParameter p3 = new ReportParameter("datename", datename);
            ReportParameter p4 = new ReportParameter("dateEnd", dateEnd);
            ReportParameter p5 = new ReportParameter("assettypename", assettypename);
            ReportParameter p6 = new ReportParameter("product", productname);
            

            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);

        }

        

        //protected void btnCabang_OnClick(object sender, EventArgs e)
        //{
        //    ReportViewer1.LocalReport.Refresh();
        //    pnlCabang.Visible = true;
        //    pnlMO.Visible = false;
        //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "tabnavCabang", "tabnavCabang();", true);
        //}

        //protected void btnMO_OnClick(object sender, EventArgs e)
        //{
        //    BindReportMO();
        //    pnlCabang.Visible = false;
        //    pnlMO.Visible = true;
        //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "tabnavMO", "tabnavMO();", true);
        //}

        //private void BindReportMO()
        //{
        //    area = Request.QueryString["area"].ToString().Trim();
        //    areaname = Request.QueryString["areaname"].ToString().Trim();
        //    cabang = Request.QueryString["branch"].ToString().Trim();
        //    cabangname = Request.QueryString["branchname"].ToString().Trim();
        //    date = Request.QueryString["date"].ToString().Trim();
        //    datename = Request.QueryString["datename"].ToString().Trim();
        //    dateEnd = Request.QueryString["dateEnd"].ToString().Trim();
        //    assettype = Request.QueryString["assettype"].ToString().Trim();
        //    assettypename = Request.QueryString["assettypename"].ToString().Trim();
        //    product = Request.QueryString["product"].ToString().Trim();
        //    productname = Request.QueryString["productname"].ToString().Trim();
            
        //    DataTable dt2 = new DataTable();
        //    QueryConnection qry2 = new QueryConnection();
            
        //    dt2 = qry2.QueryReportRankingCabang(area, cabang, date, dateEnd, assettype, product, "sp_RPT_RANKINGMO");
            
        //    ReportViewer2.ProcessingMode = ProcessingMode.Local;
        //    ReportViewer2.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportRANKINGMO.rdlc");
            
        //    ReportDataSource datasource2 = new ReportDataSource("DataSet1", dt2);

        //    ReportParameter p1 = new ReportParameter("area", areaname);
        //    ReportParameter p2 = new ReportParameter("branch", cabangname);
        //    ReportParameter p3 = new ReportParameter("datename", datename);
        //    ReportParameter p4 = new ReportParameter("dateEnd", dateEnd);
        //    ReportParameter p5 = new ReportParameter("assettypename", assettypename);
        //    ReportParameter p6 = new ReportParameter("product", productname);
            

        //    ReportViewer2.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6 });
        //    ReportViewer2.LocalReport.DataSources.Add(datasource2);
        //}
    }
}