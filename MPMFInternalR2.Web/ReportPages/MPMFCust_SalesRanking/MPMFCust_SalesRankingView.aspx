﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MPMFCust_SalesRankingView.aspx.cs" Inherits="MPMFInternalR2.Web.ReportPages.MPMFCust_SalesRanking.MPMFCust_SalesRankingView" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script type="text/javascript">
        $(function () {
            //$("#tabs").tabs();
            $("#MyAccordion").accordion();
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#mySpinner').addClass('spinner');
            $(window).on("load", function () {
                setTimeout(function () {
                    $('#mySpinner').remove();
                }, 2000)
            })

        });
    </script>
    <style type="text/css">
        .auto-style1 {
            width: 316px;
        }
    </style>
    <style>
        #mySpinner {
            position: fixed;
            z-index: 999999;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: white;
            display: flex;
            justify-content: center;
            align-items: center;
            opacity: 0.8;
        }

        @keyframes spinner {
            from {
                transform: rotate(0deg);
            }

            to {
                transform: rotate(360deg);
            }
        }

        .spinner:before {
            content: '';
            box-sizing: border-box;
            position: absolute;
            top: 50%;
            left: 50%;
            width: 30px;
            height: 30px;
            margin-top: -15px;
            margin-left: -15px;
            border-radius: 50%;
            border: 1px solid #ccc;
            border-top-color: #07d;
            animation: spinner .6s linear infinite;
        }

        ul li {
            display: inline;
        }
        ul li {
            display: inline;
            list-style-type: none;
        }

    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="False"></asp:ScriptManager>
        <div>
            <div id="mySpinner">
            </div>

                <rsweb:ReportViewer ID="ReportViewer1" Height="800px" Width="100%" runat="server" ShowExportControls="False" ShowPrintButton="False"></rsweb:ReportViewer>

        </div>
    </form>
</body>
</html>
