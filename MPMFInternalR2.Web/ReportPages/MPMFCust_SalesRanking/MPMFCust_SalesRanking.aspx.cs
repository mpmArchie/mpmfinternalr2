﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.Web;

namespace MPMFInternalR2.Web.ReportPages.MPMFCust_SalesRanking
{
    //public partial class MPMFCust_SalesRanking : System.Web.UI.Page
    public partial class MPMFCust_SalesRanking : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "49680921";
            if (!IsPostBack)
            {
                loadArea();
                LoasAssetType();
                LoadTahun();
                ddlTahun.SelectedValue = DateTime.Now.Year.ToString();
                if (base.CurrentUserContext.OfficeCode.ToString() != "999")
                //if (Session["sesBranchId"].ToString() != "999")
                {
                    LoadBranchN();
                }
                else
                {
                    this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                }
            }
        }

        private void LoadBranchN()
        {
            DataSet set = new DataSet();
            //set = new QueryConnection().QueryBranch(Session["sesBranchId"].ToString());
            set = new QueryConnection().QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            this.ddlbranch.DataSource = set;
            this.ddlbranch.DataTextField = "branchfullname";
            this.ddlbranch.DataValueField = "branchid";
            this.ddlbranch.DataBind();
            //if (HttpContext.Current.Session["sesBranchId"].ToString() == "999")
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if (Session["sesBranchId"].ToString() == "999")
            {
                this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            this.ddlbranch.SelectedIndex = 0;
            set.Dispose();
        }

        private void LoadTahun()
        {
            ArrayList ayear = new ArrayList();
            int yearnow = Convert.ToInt32(DateTime.Now.Year.ToString()) + 1;
            for (int i = 2010; i <= yearnow; i++)
            {
                ayear.Add(i);
            }
            ddlTahun.DataSource = ayear;
            ddlTahun.DataBind();
        }

        private void LoasAssetType()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryAssetType();
            ddlAssetType.DataSource = ds;
            ddlAssetType.DataTextField = "description";
            ddlAssetType.DataValueField = "assettypeid";
            ddlAssetType.DataBind();
            /*if(Session["sesBranchId"].ToString() == "999")
            {
                ddlassettype.Items.Insert(0, new ListItem("ALL", "ALL"));
            } */
            ddlAssetType.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlAssetType.SelectedIndex = 0;
            ds.Dispose();
        }

        private void loadArea()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            //ds = qry.QueryAreaWithBranch(Session["sesBranchId"].ToString());
            ds = qry.QueryAreaWithBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlArea.DataSource = ds;
            ddlArea.DataTextField = "areafullname";
            ddlArea.DataValueField = "AreaID";
            ddlArea.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if (Session["sesBranchId"].ToString() == "999")
            {
                ddlArea.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlArea.SelectedIndex = 0;
        }

        protected void ddlArea_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            LoadBranch(ddlArea.SelectedValue.Trim());
        }

        private void LoadBranch(string strArea)
        {
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if (Session["sesBranchId"].ToString() == "999")
            {
                DataSet ds = new DataSet();
                QueryConnection qry = new QueryConnection();
                ds = qry.QueryBranchArea(strArea);
                ddlbranch.DataSource = ds;
                ddlbranch.DataTextField = "branchfullname";
                ddlbranch.DataValueField = "branchid";
                ddlbranch.DataBind();
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                ddlbranch.SelectedIndex = 0;
            }
            else
            {
                DataSet set = new DataSet();
                //set = new QueryConnection().QueryBranch(Session["sesBranchId"].ToString());
                set = new QueryConnection().QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
                this.ddlbranch.DataSource = set;
                this.ddlbranch.DataTextField = "branchfullname";
                this.ddlbranch.DataValueField = "branchid";
                this.ddlbranch.DataBind();
                //if (HttpContext.Current.Session["sesBranchId"].ToString() == "999")
                if (base.CurrentUserContext.OfficeCode.ToString() == "999")
                //if (Session["sesBranchId"].ToString() == "999")
                {
                    this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                }
                this.ddlbranch.SelectedIndex = 0;
                set.Dispose();
            }

        }

        protected void lb_Print_Sync_OnClick(object sender, EventArgs e)
        {
            try
            {
                string strStartDate = "";

                if (ddlTahun.SelectedValue == "")
                {
                    lblwardate.Visible = true;
                    ddlTahun.Focus();
                    return;
                }
                else
                {
                    lblwardate.Visible = false;
                }

                if (ddlMonth.SelectedValue == "")
                {
                    lblwardate.Visible = true;
                    ddlTahun.Focus();
                    return;
                }
                else
                {
                    lblwardate.Visible = false;
                }

                //if (dtStart.Text == "")
                //    strStartDate = DateTime.Now.ToString("dd/MM/yyyy");
                //else
                //    strStartDate = dtStart.Text;

                Page.ClientScript.RegisterStartupScript(
                    this.GetType(), "OpenWindow", "window.open('MPMFCust_SalesRankingView.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&date=" + ddlMonth.SelectedValue.ToString() + "&datename=" + ddlMonth.SelectedItem.Text.ToString() + "&dateEnd=" + ddlTahun.SelectedValue.ToString() + "&assettype=" + ddlAssetType.SelectedValue + "&assettypename=" + ddlAssetType.SelectedItem.Text + "&product=" + ddlProduct.SelectedValue + "&productname=" + ddlProduct.SelectedItem.Text + "','_blank');window.open('MPMFCust_SalesRankingMOView.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&date=" + ddlMonth.SelectedValue.ToString() + "&datename=" + ddlMonth.SelectedItem.Text.ToString() + "&dateEnd=" + ddlTahun.SelectedValue.ToString() + "&assettype=" + ddlAssetType.SelectedValue + "&assettypename=" + ddlAssetType.SelectedItem.Text + "&product=" + ddlProduct.SelectedValue + "&productname=" + ddlProduct.SelectedItem.Text + "','_blank');", true);


                //Page.ClientScript.RegisterStartupScript(
                //    this.GetType(), "OpenWindow", "window.open('MPMFCust_SalesRankingMOView.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&date=" + ddlMonth.SelectedValue.ToString() + "&datename=" + ddlMonth.SelectedItem.Text.ToString() + "&dateEnd=" + ddlTahun.SelectedValue.ToString() + "&assettype=" + ddlAssetType.SelectedValue + "&assettypename=" + ddlAssetType.SelectedItem.Text + "&product=" + ddlProduct.SelectedValue + "&productname=" + ddlProduct.SelectedItem.Text + "','_blank');", true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        protected void lb_Dowmload_OnClick(object sender, EventArgs e)
        {
            try
            {
                string strStartDate = "";

                //if (dtStart.Text == "")
                //    strStartDate = DateTime.Now.ToString("dd/MM/yyyy");
                //else
                //    strStartDate = dtStart.Text;

                if (ddlTahun.SelectedValue == "")
                {
                    lblwardate.Visible = true;
                    ddlTahun.Focus();
                    return;
                }
                else
                {
                    lblwardate.Visible = false;
                }

                if (ddlMonth.SelectedValue == "")
                {
                    lblwardate.Visible = true;
                    ddlTahun.Focus();
                    return;
                }
                else
                {
                    lblwardate.Visible = false;
                }

                Response.Redirect("MPMFCust_SalesRankingDownload.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&date=" + ddlMonth.SelectedValue.ToString() + "&datename=" + ddlMonth.SelectedItem.Text.ToString() + "&dateEnd=" + ddlTahun.SelectedValue.ToString() + "&assettype=" + ddlAssetType.SelectedValue + "&assettypename=" + ddlAssetType.SelectedItem.Text + "&product=" + ddlProduct.SelectedValue + "&productname=" + ddlProduct.SelectedItem.Text);

                //Page.ClientScript.RegisterStartupScript(
                //    this.GetType(), "OpenWindow", "window.open('MPMFCust_ReportSoldDownload.aspx?jenis=" + ddlJenisNilai.SelectedValue.ToString().Trim() + "&area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&date=" + dtStart.Text.ToString() + "&dateEnd=" + dtEnd.Text.ToString() + "','_newtab');", true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        protected void lbReset_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("MPMFCust_SalesRanking.aspx");
        }

        protected void ddlAssetType_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlAssetType.SelectedValue != "ALL")
            {
                LoadProduct(ddlAssetType.SelectedValue);
            }
            else
            {
                ddlProduct.Items.Clear();
                ddlProduct.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
        }

        private void LoadProduct(string assetType)
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryCategoryWithCondition(assetType);
            ddlProduct.DataSource = ds;
            ddlProduct.DataTextField = "DESCRIPTIONPRODUCT";
            ddlProduct.DataValueField = "InitialProductCode";
            ddlProduct.DataBind();
            ddlProduct.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlProduct.SelectedIndex = 0;

            ds.Dispose();
        }
    }
}