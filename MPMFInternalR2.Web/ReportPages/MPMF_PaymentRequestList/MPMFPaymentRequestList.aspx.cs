﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.Web;


namespace MPMFInternalR2.Web.ReportPages.MPMF_PaymentRequestList
{
    //public partial class MPMFPaymentRequestList : System.Web.UI.Page

    public partial class MPMFPaymentRequestList : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "NikoP";

            if (!IsPostBack)
            {
                
            }
        }
        

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            if (this.ddlrequest.SelectedIndex.ToString().Trim() == "0")
            {
                this.lblwarrequest.Visible = true;
            }
            else
            {
                this.lblwarrequest.Visible = false;
                base.Response.Redirect("VRptANJF_paymentrequestlist.aspx?requesttype=" + this.ddlrequest.SelectedItem.Value.ToString().Trim());
            }

        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("MPMFPaymentRequestList.aspx");
        }
    }
}