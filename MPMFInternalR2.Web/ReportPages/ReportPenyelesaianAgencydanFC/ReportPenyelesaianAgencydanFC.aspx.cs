﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.ReportPenyelesaianAgencydanFC
{
    //public partial class Report_PenyelesaianAgencydanFC : System.Web.UI.Page
    public partial class Report_PenyelesaianAgencydanFC : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "49680921";

            if (!IsPostBack)
            {
                loadArea();
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                //loadData();
            }
        }

        private void loadData()
        {
            loadbranch();
        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            //ds = qry.QueryBranch(Session["sesBranchId"].ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if (Session["sesBranchId"].ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbranch.SelectedIndex = 0;
        }

        private void loadArea()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryArea();
            ddlArea.DataSource = ds;
            ddlArea.DataTextField = "areafullname";
            ddlArea.DataValueField = "AreaID";
            ddlArea.DataBind();
            ddlArea.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlArea.SelectedIndex = 0;
        }

        private void LoadBranch(string strArea)
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranchArea(strArea);
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlbranch.SelectedIndex = 0;
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadBranch(ddlArea.SelectedValue.Trim());
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string strStartDate = "";

            if (ddltipe.SelectedValue == "0")
            {
                lblwarddlType.Visible = true;
                return;
            }

            //if (dtStart.Text == "")
            //    strStartDate = DateTime.Now.ToString("dd/MM/yyyy");
            //else
            //    strStartDate = dtStart.Text;

            if ((dtStart.Text == string.Empty) | (dtEnd.Text == string.Empty))
            {
                this.lblwardate.Text = "Please Fill Period From And Period To";
                lblwardate.Visible = true; return;
            }

            if (DateTime.ParseExact(dtStart.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(dtEnd.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Page.ClientScript.RegisterStartupScript(
                       this.GetType(), "OpenWindow", "window.open('ReportPenyelesaianAgencydanFCView.aspx?type=" + ddltipe.SelectedValue.ToString().Trim() + "&area=" + ddlArea.SelectedItem.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&date=" + dtStart.Text.ToString() + "&dateEnd=" + dtEnd.Text.ToString() + "','_newtab');", true);
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("ReportPenyelesaianAgencydanFC.aspx");
        }
    }
}