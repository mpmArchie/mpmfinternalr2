﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.ReportPenyelesaianAgencydanFC
{
    public partial class ReportPenyelesaianAgencydanFCView : System.Web.UI.Page
    {
        private string type, area, cabang, date, dateEnd;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }

        }

        private void bindreport()
        {
            type = Request.QueryString["type"].ToString().Trim();
            area = Request.QueryString["area"].ToString().Trim();
            cabang = Request.QueryString["branch"].ToString().Trim();
            date = Request.QueryString["date"].ToString().Trim();
            dateEnd = Request.QueryString["dateEnd"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            if (type == "1")
            {

                dt = qry.QueryReport(type, area, cabang, date, dateEnd, "sp_RPT_PenyelesaianAgency");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportAgency.rdlc");
            }

            if (type == "2")
            {

                dt = qry.QueryReport(type, area, cabang, date, dateEnd, "sp_RPT_PenyelesaianFC");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ReportFC.rdlc");
            }

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("type", type);
            ReportParameter p2 = new ReportParameter("area", area);
            ReportParameter p3 = new ReportParameter("branch", cabang);
            ReportParameter p4 = new ReportParameter("date", date);
            ReportParameter p5 = new ReportParameter("dateEnd", dateEnd);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);

        }
    }
}