﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.Web;
namespace MPMFInternalR2.Web.ReportPages.ReportInvalidNumberTelesales
{
    //public partial class RptReportInvalidNumberTelesales : System.Web.UI.Page
    public partial class RptReportInvalidNumberTelesales : WebFormBase
    {        
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session.Add("loginID", "49680921");
            //Session.Add("sesBranchId", "999");
            if (!IsPostBack)
            {
                //hdnbranchid.Value = Session["sesBranchId"].ToString();
                hdnbranchid.Value = base.CurrentUserContext.OfficeCode.ToString();
                loadbranch();
                LoadBranchN();
                loadArea();
                ReasonCall();
                ProgramCategory();            
            }
        }
        private void LoadBranchN()
        {
            DataSet set = new DataSet();
            set = new QueryConnection().QueryBranch(hdnbranchid.Value);
            this.ddlbranch.DataSource = set;
            this.ddlbranch.DataTextField = "branchfullname";
            this.ddlbranch.DataValueField = "branchid";
            this.ddlbranch.DataBind();
            
            if (hdnbranchid.Value == "999")
            {
                this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            this.ddlbranch.SelectedIndex = 0;
            set.Dispose();


        }
        private void loadArea()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryAreaWithBranch(hdnbranchid.Value);
            ddlArea.DataSource = ds;
            ddlArea.DataTextField = "areafullname";
            ddlArea.DataValueField = "AreaID";
            ddlArea.DataBind();
            
            if (hdnbranchid.Value == "999")
            {
                ddlArea.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(hdnbranchid.Value);
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
        
            if (hdnbranchid.Value == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;
        }

        private void ReasonCall()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryReasonCall();
            ddlReasonCall.DataSource = ds;
            ddlReasonCall.DataTextField = "reason_text";
            ddlReasonCall.DataValueField = "reason_text";
            ddlReasonCall.DataBind();
            ddlReasonCall.Items.Insert(0, "ALL");
            ddlReasonCall.SelectedIndex = 0;
            ds.Dispose();
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlArea.SelectedValue != "")
            {
                LoadBranch(ddlArea.SelectedValue.Trim());
            }
        }
        private void ProgramCategory()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QuerryProgramCategory();
            ddlCategory.DataSource = ds;
            ddlCategory.DataTextField = "Program_Category";
            ddlCategory.DataValueField = "Program_Category";
            ddlCategory.DataBind();
            ddlCategory.Items.Insert(0, "ALL");
            ddlCategory.SelectedIndex = 0;
            ds.Dispose();
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string strDateStart = "";
            string strDateEnd = "";


            if (dtStart.Text == "")
                strDateStart = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strDateStart = dtStart.Text;

            if (dtEnd.Text == "")
                strDateEnd = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strDateEnd = dtEnd.Text;


            if (DateTime.ParseExact(strDateStart, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(strDateEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Page.ClientScript.RegisterStartupScript(
                   this.GetType(), "OpenWindow", "window.open('ReportInvalidNumberTelesalesView.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&reason_text=" + ddlReasonCall.SelectedValue.ToString() + "&Program_Category=" + ddlCategory.SelectedItem.Text.ToString() + "&period=" + strDateStart.ToString() + "&period1=" + strDateEnd.ToString() + "&StatusCall=" + ddlStatusCall.SelectedValue.ToString() + "','_blank');", true);
        }
   
        protected void ddlRegional_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlArea.SelectedValue != "")
            {
                LoadBranch(ddlArea.SelectedValue.Trim());
            }
        }
        private void LoadBranch(string strArea)
        {
            if (hdnbranchid.Value == "999")
            {
                DataSet ds = new DataSet();
                QueryConnection qry = new QueryConnection();
                ds = qry.QueryBranchArea(strArea);
                ddlbranch.DataSource = ds;
                ddlbranch.DataTextField = "branchfullname";
                ddlbranch.DataValueField = "branchid";
                ddlbranch.DataBind();
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                ddlbranch.SelectedIndex = 0;
            }
            else
            {
                DataSet set = new DataSet();
                set = new QueryConnection().QueryBranch(hdnbranchid.Value);
                this.ddlbranch.DataSource = set;
                this.ddlbranch.DataTextField = "branchfullname";
                this.ddlbranch.DataValueField = "branchid";
                this.ddlbranch.DataBind();
                
                if (hdnbranchid.Value == "999")
                {
                    this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                }
                this.ddlbranch.SelectedIndex = 0;
                set.Dispose();
            }

        }

        protected void lb_Dowmload_OnClick(object sender, EventArgs e)
        {
            string strDateStart = "";
            string strDateEnd = "";


            if (dtStart.Text == "")
                strDateStart = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strDateStart = dtStart.Text;

            if (dtEnd.Text == "")
                strDateEnd = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strDateEnd = dtEnd.Text;


            if (DateTime.ParseExact(strDateStart, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(strDateEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Page.ClientScript.RegisterStartupScript(
                   this.GetType(), "OpenWindow", "window.open('ReportInvalidNumberTelesalesPDF.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&reason_text=" + ddlReasonCall.SelectedValue.ToString() + "&Program_Category=" + ddlCategory.SelectedItem.Text.ToString() + "&period=" + strDateStart.ToString() + "&period1=" + strDateEnd.ToString() + "&StatusCall=" + ddlStatusCall.SelectedValue.ToString() + "','_blank');", true);
        }

        protected void lb_Dowmload_OnClickExcel(object sender, EventArgs e)
        {
            string strDateStart = "";
            string strDateEnd = "";


            if (dtStart.Text == "")
                strDateStart = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strDateStart = dtStart.Text;

            if (dtEnd.Text == "")
                strDateEnd = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strDateEnd = dtEnd.Text;


            if (DateTime.ParseExact(strDateStart, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(strDateEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Page.ClientScript.RegisterStartupScript(
                   this.GetType(), "OpenWindow", "window.open('ReportInvalidNumberTelesalesExcel.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&reason_text=" + ddlReasonCall.SelectedValue.ToString() + "&Program_Category=" + ddlCategory.SelectedItem.Text.ToString() + "&period=" + strDateStart.ToString() + "&period1=" + strDateEnd.ToString() + "&StatusCall=" + ddlStatusCall.SelectedValue.ToString() + "','_blank');", true);
        }
    }
}