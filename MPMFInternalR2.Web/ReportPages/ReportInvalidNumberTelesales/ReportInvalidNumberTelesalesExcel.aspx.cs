﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.ReportInvalidNumberTelesales
{
    public partial class ReportInvalidNumberTelesalesExcel : System.Web.UI.Page
    {
        private string area, areaname, branch, branchname, period, period1, reason_text, Program_Category, StatusCall;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }
        private void bindreport()
        {
            string nameFIle = "";
            period = Request.QueryString["period"].ToString().Trim();
            period1 = Request.QueryString["period1"].ToString().Trim();
            area = Request.QueryString["area"].ToString().Trim();
            areaname = Request.QueryString["areaname"].ToString().Trim();
            branch = Request.QueryString["branch"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            reason_text = Request.QueryString["reason_text"].ToString().Trim();
            StatusCall = Request.QueryString["StatusCall"].ToString().Trim();
            Program_Category = Request.QueryString["Program_Category"].ToString().Trim();
            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QuertyReportInvalidNumberTelesales(period, period1, area, branch, StatusCall, reason_text, Program_Category, "sp_RPT_Invalid_Telesales");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/Rpt_Invalid_Number_Tesales.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("period", period);
            ReportParameter p2 = new ReportParameter("period1", period1);
            ReportParameter p3 = new ReportParameter("StatusCall", StatusCall);
            ReportParameter p4 = new ReportParameter("branch", branchname);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
            nameFIle = "ReportInvalidTelesales";
            GenerateToExcel(ReportViewer1, nameFIle);
        }

        private void GenerateToExcel(ReportViewer reportViewer1, string nameFile)
        {
            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = reportViewer1.LocalReport.Render("EXCEL", null, out contentType, out encoding, out extension, out streamIds, out warnings);

            //Download the RDLC Report in Word, Excel, PDF and Image formats.
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + nameFile + "." + extension);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
        }
    }
}