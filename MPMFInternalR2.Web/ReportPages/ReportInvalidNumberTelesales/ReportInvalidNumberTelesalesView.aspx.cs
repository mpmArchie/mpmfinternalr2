﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;

namespace MPMFInternalR2.Web.ReportPages.ReportInvalidNumberTelesales
{
    public partial class ReportInvalidNumberTelesalesView : System.Web.UI.Page
    {        
        private string area, areaname, branch, branchname, period, period1, reason_text, Program_Category, StatusCall;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                bindreport();
            }
        }
        private void bindreport()
        {
            string nameFIle = "";           
            period = Request.QueryString["period"].ToString().Trim();
            period1 = Request.QueryString["period1"].ToString().Trim();
            area = Request.QueryString["area"].ToString().Trim();
            areaname = Request.QueryString["areaname"].ToString().Trim();
            branch = Request.QueryString["branch"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            reason_text = Request.QueryString["reason_text"].ToString().Trim();
            StatusCall = Request.QueryString["StatusCall"].ToString().Trim();
            Program_Category = Request.QueryString["Program_Category"].ToString().Trim();
            
            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();            
            dt = qry.QuertyReportInvalidNumberTelesales(period, period1, area, branch, StatusCall, reason_text, Program_Category, "sp_RPT_Invalid_Telesales");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/Rpt_Invalid_Number_Tesales.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("period", period);
            ReportParameter p2 = new ReportParameter("period1", period1);
            ReportParameter p3 = new ReportParameter("StatusCall", StatusCall);
            ReportParameter p4 = new ReportParameter("branch", branchname);           
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
            nameFIle = "ReportInvalidTelesales";
        }
    }
}