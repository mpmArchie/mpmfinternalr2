﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;


namespace MPMFInternalR2.Web.ReportPages.ARDetailClosing
{
    public partial class ARDetailBln : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                loadData();
            }
        }

        protected void loadData()
        {
            loadbranch();
            loadassettype();
            loadPeriod();
        }


        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;
        }

        protected void loadassettype()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryAssetType();
            ddlassettype.DataSource = ds;
            ddlassettype.DataTextField = "description";
            ddlassettype.DataValueField = "assettypeid";
            ddlassettype.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlassettype.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlassettype.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void loadPeriod()
        {
            ddlBulan.Items.Insert(0, new ListItem("Select One", "000"));
            ddlBulan.Items.Insert(1, new ListItem("Januari", "01"));
            ddlBulan.Items.Insert(2, new ListItem("Februari", "02"));
            ddlBulan.Items.Insert(3, new ListItem("Maret", "03"));
            ddlBulan.Items.Insert(4, new ListItem("April", "04"));
            ddlBulan.Items.Insert(5, new ListItem("Mei", "05"));
            ddlBulan.Items.Insert(6, new ListItem("Juni", "06"));
            ddlBulan.Items.Insert(7, new ListItem("Juli", "07"));
            ddlBulan.Items.Insert(8, new ListItem("Agustus", "08"));
            ddlBulan.Items.Insert(9, new ListItem("September", "09"));
            ddlBulan.Items.Insert(10, new ListItem("Oktober", "10"));
            ddlBulan.Items.Insert(11, new ListItem("November", "11"));
            ddlBulan.Items.Insert(12, new ListItem("Desember", "12"));


            DateTime today = DateTime.Today;
            DateTime Y = new DateTime(today.Year, today.Month, 1);
            ddlTahun.Items.Insert(0, new ListItem("Select One", "000"));
            for (int m = -2; m <= 2; m++)
            {
                DateTime previousmont = Y.AddYears(m); ;
                ListItem list = new ListItem();
                list.Text = previousmont.ToString("yyyy");
                list.Value = previousmont.ToString("yyyy");
                ddlTahun.Items.Add(list);

            }


        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string str = this.ddlTahun.SelectedValue.ToString().Trim() + this.ddlBulan.SelectedValue.ToString().Trim();
            Response.Redirect("ARDetailBlnView.aspx?optionreport=" + "&branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchfullname=" + ddlbranch.SelectedItem.ToString().Trim() + "&assettypeid=" + ddlassettype.SelectedValue.ToString().Trim() + "&agingbln=" + str + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("ARDetailBln.aspx");
        }
    }
}