﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;

namespace MPMFInternalR2.Web.ReportPages.ARDetailClosing
{
    public partial class ARDetailBlnView : System.Web.UI.Page
    {
        private string branchid, assettypeid, agingbln;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }


        private void bindreport()
        {
            branchid = Request.QueryString["branchid"].ToString().Trim();
            assettypeid = Request.QueryString["assettypeid"].ToString().Trim();
            agingbln = Request.QueryString["agingbln"].ToString().Trim();
            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryReportspANJCustGenARDetailBln(branchid, assettypeid, agingbln, "spANJCustGenARDetailBln");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/ARDetailBln.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("branchid", branchid);
            ReportParameter p2 = new ReportParameter("assettypeid", assettypeid);
            ReportParameter p3 = new ReportParameter("agingbln", agingbln);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1,p2,p3 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}