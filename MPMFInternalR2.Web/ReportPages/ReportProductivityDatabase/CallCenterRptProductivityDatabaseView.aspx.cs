﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.ReportProductivityDatabase
{
    public partial class CallCenterRptProductivityDatabaseView : System.Web.UI.Page
    {
        private string date1, date2, branchfullname, areaname, sourcename, datetypename;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.bindreport();
            }
        }

        private void bindreport()
        {
            date1 = Request.QueryString["date1"].ToString().Trim();
            date2 = Request.QueryString["date2"].ToString().Trim();
            branchfullname = Request.QueryString["branchfullname"].ToString().Trim();
            areaname = Request.QueryString["areaname"].ToString().Trim();
            sourcename = Request.QueryString["sourcename"].ToString().Trim();
            datetypename = Request.QueryString["datetypename"].ToString().Trim();

            DataTable dataTable = new DataTable();
            dataTable = new QueryConnection().QueryReportspSAFCustCallCenterRptProductivityDatabase(base.Request.QueryString["date1"], base.Request.QueryString["date2"], base.Request.QueryString["areaid"], base.Request.QueryString["branchid"].ToString(), base.Request.QueryString["sourceid"], base.Request.QueryString["datetype"], "spSAFCustCallCenterRptProductivityDatabase");
            
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptCallCenterProductivityDatabase.rdlc");

            ReportDataSource datasource = new ReportDataSource("DataSet1", dataTable);
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportParameter p1 = new ReportParameter("date1", date1);
            ReportParameter p2 = new ReportParameter("date2", date2);
            ReportParameter p3 = new ReportParameter("branchfullname", branchfullname);
            ReportParameter p4 = new ReportParameter("areaname", areaname);
            ReportParameter p5 = new ReportParameter("sourcename", sourcename);
            ReportParameter p6 = new ReportParameter("datetypename", datetypename);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}