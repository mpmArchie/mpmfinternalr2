﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.LapPengaduanPerMasaLaporan
{
    public partial class LapPengaduanPerMasaLaporan : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Session["sesBranchId"] = "999";
                //Session["loginid"] = "999";
            }
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {

            string datepicker = "";
            string datepicker1 = "";

            if (dtStart.Text == "")
            {
                datepicker = DateTime.Now.ToString("dd/MM/yyyy");
            }
            else
            {
                datepicker = dtStart.Text;
            }

            if (dtEnd.Text == "")
            {
                datepicker1 = DateTime.Now.ToString("dd/MM/yyyy");
            }
            else
            {
                datepicker1 = dtEnd.Text;
            }

            if (DateTime.ParseExact(datepicker, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(datepicker1, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Response.Redirect("LapPengaduanPerMasaLaporanView.aspx?optionreport=" + "&FromDate=" + datepicker.ToString().Trim() + "&EndDate=" + datepicker1.ToString().Trim() + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("LapPengaduanPerMasaLaporan.aspx");
        }
    }
}