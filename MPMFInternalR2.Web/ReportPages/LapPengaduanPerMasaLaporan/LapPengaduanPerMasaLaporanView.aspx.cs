﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.LapPengaduanPerMasaLaporan
{
    public partial class LapPengaduanPerMasaLaporanView : System.Web.UI.Page
    {
        private string BranchID, FromDate, EndDate;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            FromDate = Request.QueryString["FromDate"].ToString().Trim();
            EndDate = Request.QueryString["EndDate"].ToString().Trim();
            BranchID = "000";

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            dt = qry.QueryspMPMFCustLaporanKeluhanPerMasaCS(BranchID, FromDate, EndDate, "spMPMFCustLaporanKeluhanPerMasaCS");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/COMRptKeluhanPerMasa.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("FromDate", FromDate);
            ReportParameter p2 = new ReportParameter("EndDate", EndDate);
            ReportParameter p3 = new ReportParameter("BranchID", BranchID);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2,p3 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }

    }
}