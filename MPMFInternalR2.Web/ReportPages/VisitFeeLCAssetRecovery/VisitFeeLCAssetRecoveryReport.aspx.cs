﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.VisitFeeLCAssetRecovery
{
    public partial class VisitFeeLCAssetRecoveryReport : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["sesBranchId"] = "999";
            //HttpContext.Current.Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                loadarea();
                LoadBranch();
                LoadAssetType();
                ddlbranch.Focus();
            }
        }
        protected void ddlarea_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadBranch();
        }
        private void loadarea()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryAreaCollection(base.CurrentUserContext.OfficeCode.ToString());
            ddlarea.DataSource = ds;
            ddlarea.DataTextField = "collareafullname";
            ddlarea.DataValueField = "collareaid";
            ddlarea.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlarea.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlarea.SelectedIndex = 0;

            ds.Dispose();
        }
        protected void LoadBranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = ds = qry.QueryBranchAreaCollection(ddlarea.SelectedValue.ToString().Trim());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            ddlbranch.SelectedIndex = 0;

            ddlbranch.Items.Insert(000, "Select One");

            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(001, "All");
            }

            ds.Dispose();
        }

        protected void LoadAssetType()
        {
            ddlassettype.Items.Add(new ListItem("Select One", "00"));
            ddlassettype.Items.Add(new ListItem("Mobil", "1"));
            ddlassettype.Items.Add(new ListItem("Motor", "5"));
            ddlassettype.Items.Add(new ListItem("Elektronik", "4"));
            ddlassettype.Items.Add(new ListItem("Heavy Equipment", "2"));
            ddlassettype.Items.Add(new ListItem("KTA", "10"));
            ddlassettype.Items.Add(new ListItem("Property", "6"));
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            if (this.ddlbranch.SelectedIndex.ToString() == "0") { lblwarbranch.Visible = true; return; } else { lblwarbranch.Visible = false; }
            if (this.ddlperiod1.SelectedIndex.ToString() == "0" || this.ddlperiod2.SelectedIndex.ToString() == "0") { this.lblwarddlperiod.Visible = true; return; } else { this.lblwarddlperiod.Visible = false; }
            if (this.ddlassettype.SelectedIndex.ToString() == "0") { lblwarddlassettype.Visible = true; return; } else { lblwarddlassettype.Visible = false; }

            //Response.Write("<script>window.open('VisitFeeLCAssetRecoveryReportView.aspx?branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&period1=" + ddlperiod1.SelectedValue.ToString().Trim() + "&period2=" + ddlperiod2.SelectedValue.ToString().Trim() + "&assetid=" + ddlassettype.SelectedValue.ToString().Trim() + "&assettypename=" + ddlassettype.SelectedItem.Text.ToString().Trim() + "&period1name=" + ddlperiod1.SelectedItem.Text.ToString().Trim() + "&period2name=" + ddlperiod2.SelectedItem.Text.ToString().Trim() + "');</script>");
            Response.Redirect("VisitFeeLCAssetRecoveryReportView.aspx?branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&period1=" + ddlperiod1.SelectedValue.ToString().Trim() + "&period2=" + ddlperiod2.SelectedValue.ToString().Trim() + "&assetid=" + ddlassettype.SelectedValue.ToString().Trim() + "&assettypename=" + ddlassettype.SelectedItem.Text.ToString().Trim() + "&period1name=" + ddlperiod1.SelectedItem.Text.ToString().Trim() + "&period2name=" + ddlperiod2.SelectedItem.Text.ToString().Trim() + "");


        }
        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("VisitFeeLCAssetRecoveryReport.aspx");
        }
    }
}