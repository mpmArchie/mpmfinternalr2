﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.ReportDetailKirimKontrakbyPos
{
    public partial class ReportDetailKirimKontrakbyPosPDF : System.Web.UI.Page
    {
        private string area, areaname, branch, branchname, Product, ProductName, period, period1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }
        private void bindreport()
        {
            string nameFIle = "";
            area = Request.QueryString["area"].ToString().Trim();
            areaname = Request.QueryString["areaname"].ToString().Trim();
            branch = Request.QueryString["branch"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            Product = Request.QueryString["Product"].ToString().Trim();
            ProductName = Request.QueryString["ProductName"].ToString().Trim();
            period = Request.QueryString["period"].ToString().Trim();
            period1 = Request.QueryString["period1"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            //dt = qry.QueryReportAppInJMOrder(APTypeId, APTypeName "spMPMFGetAPType");
            dt = qry.QueryGetReportDetailKirimKontrak(area, branch, Product, period, period1, "sp_RPT_ReportDetailKirim");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;

            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RPTReportDetailKirim.rdlc");

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);

            ReportParameter p1 = new ReportParameter("area", areaname);
            ReportParameter p2 = new ReportParameter("branch", branchname);
            ReportParameter p3 = new ReportParameter("ProductName", ProductName);
            ReportParameter p4 = new ReportParameter("period", period);
            ReportParameter p5 = new ReportParameter("period1", period1);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);

            nameFIle = "Report Detail Kirim Kontrak";
            GenerateToPDF(ReportViewer1, nameFIle);



        }

        private void GenerateToPDF(ReportViewer reportViewer1, string nameFile)
        {
            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;

            //Export the RDLC Report to Byte Array.
            byte[] bytes = reportViewer1.LocalReport.Render("PDF", null, out contentType, out encoding, out extension, out streamIds, out warnings);

            //Download the RDLC Report in Word, Excel, PDF and Image formats.
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + nameFile + "." + extension);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
        }
    }
}