﻿using Confins.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.ReportDetailKirimKontrakbyPos
{
    public partial class ReportDetailKirimKontrakbyPos : WebFormBase
    //public partial class ReportDetailKirimKontrakbyPos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "49680921";
            if (!IsPostBack)
            {
                loadArea();
                loadGroupProduct();

                if (base.CurrentUserContext.OfficeCode.ToString() != "999")
                //if (Session["sesBranchId"].ToString() != "999")
                {
                    LoadBranchN();
                }
                else
                {
                    this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                }
                //loadData();
            }
        }

        private void LoadBranchN()
        {
            DataSet set = new DataSet();
            //set = new QueryConnection().QueryBranch(Session["sesBranchId"].ToString());
            set = new QueryConnection().QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            this.ddlbranch.DataSource = set;
            this.ddlbranch.DataTextField = "branchfullname";
            this.ddlbranch.DataValueField = "branchid";
            this.ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if (Session["sesBranchId"].ToString() == "999")
            {
                this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            this.ddlbranch.SelectedIndex = 0;
            set.Dispose();


        }

        private void loadArea()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            //ds = qry.QueryAreaWithBranch(Session["sesBranchId"].ToString());
            ds = qry.QueryAreaWithBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlArea.DataSource = ds;
            ddlArea.DataTextField = "areafullname";
            ddlArea.DataValueField = "AreaID";
            ddlArea.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if (Session["sesBranchId"].ToString() == "999")
            {
                ddlArea.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlArea.SelectedValue != "")
            {
                LoadBranch(ddlArea.SelectedValue.Trim());
            }
        }
        private void loadGroupProduct()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryGetProdType();
            ddlGroupProduct.DataSource = ds;
            ddlGroupProduct.DataTextField = "PROD_TYPE";
            ddlGroupProduct.DataValueField = "PROD_TYPE";
            ddlGroupProduct.DataBind();

        }

        private void LoadBranch(string strArea)
        {
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            //if (Session["sesBranchId"].ToString() == "999")
            {
                DataSet ds = new DataSet();
                QueryConnection qry = new QueryConnection();
                ds = qry.QueryBranchArea(strArea);
                ddlbranch.DataSource = ds;
                ddlbranch.DataTextField = "branchfullname";
                ddlbranch.DataValueField = "branchid";
                ddlbranch.DataBind();
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                ddlbranch.SelectedIndex = 0;
            }
            else
            {
                DataSet set = new DataSet();
                // set = new QueryConnection().QueryBranch(Session["sesBranchId"].ToString());
               set = new QueryConnection().QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
                this.ddlbranch.DataSource = set;
                this.ddlbranch.DataTextField = "branchfullname";
                this.ddlbranch.DataValueField = "branchid";
                this.ddlbranch.DataBind();
                if (base.CurrentUserContext.OfficeCode.ToString() == "999")
                //if (Session["sesBranchId"].ToString() == "999")
                {
                    this.ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
                }
                this.ddlbranch.SelectedIndex = 0;
                set.Dispose();
            }

        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string strDateStart = "";
            string strDateEnd = "";
            

            if (dtStart.Text == "")
                strDateStart = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strDateStart = dtStart.Text;

            if (dtEnd.Text == "")
                strDateEnd = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strDateEnd = dtEnd.Text;


            if (DateTime.ParseExact(strDateStart, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(strDateEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Page.ClientScript.RegisterStartupScript(
                            this.GetType(), "OpenWindow", "window.open('ReportDetailKirimKontrakbyPosView.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&Product=" + ddlGroupProduct.SelectedValue.ToString() + "&ProductName=" + ddlGroupProduct.SelectedItem.Text.ToString()  +  "&period=" + strDateStart + "&period1=" + strDateEnd + "','_blank');", true);
        }

        protected void Lb_DownloadPDF_Click(object sender, EventArgs e)
        {
            string strDateStart = "";
            string strDateEnd = "";


            if (dtStart.Text == "")
                strDateStart = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strDateStart = dtStart.Text;

            if (dtEnd.Text == "")
                strDateEnd = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strDateEnd = dtEnd.Text;


            if (DateTime.ParseExact(strDateStart, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(strDateEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Page.ClientScript.RegisterStartupScript(
                            this.GetType(), "OpenWindow", "window.open('ReportDetailKirimKontrakbyPosPDF.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&Product=" + ddlGroupProduct.SelectedValue.ToString() + "&ProductName=" + ddlGroupProduct.SelectedItem.Text.ToString() + "&period=" + strDateStart + "&period1=" + strDateEnd + "','_blank');", true);
        }

        protected void lb_Dowmload_Click(object sender, EventArgs e)
        {
            string strDateStart = "";
            string strDateEnd = "";


            if (dtStart.Text == "")
                strDateStart = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strDateStart = dtStart.Text;

            if (dtEnd.Text == "")
                strDateEnd = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strDateEnd = dtEnd.Text;


            if (DateTime.ParseExact(strDateStart, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(strDateEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Page.ClientScript.RegisterStartupScript(
                            this.GetType(), "OpenWindow", "window.open('ReportDetailKirimKontrakbyPosExcel.aspx?area=" + ddlArea.SelectedValue.ToString().Trim() + "&areaname=" + ddlArea.SelectedItem.Text.ToString().Trim() + "&branch=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.Text.ToString().Trim() + "&Product=" + ddlGroupProduct.SelectedValue.ToString() + "&ProductName=" + ddlGroupProduct.SelectedItem.Text.ToString() + "&period=" + strDateStart + "&period1=" + strDateEnd + "','_blank');", true);
        }

    }
}