﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.Web;

namespace MPMFInternalR2.Web.ReportPages.SalesBySupplier
{
    //public partial class ReportMPMF_SalesBySuplier : System.Web.UI.Page
    public partial class ReportMPMF_SalesBySuplier : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "NikoP";

            if (!IsPostBack)
            {
                loadData();
            }
        }

        private void loadData()
        {
            loadbranch();
            loadasset();
        }

        private void loadasset()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryAsset();
            ddlasset.DataSource = ds;
            ddlasset.DataTextField = "description";
            ddlasset.DataValueField = "assettypeid";
            ddlasset.DataBind();

            this.ddlasset.Items.Insert(000, "Select One");
            this.ddlasset.Items.Insert(001, "ALL");
        }

        private void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();

            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;

            loadsuplier();
        }

        private void loadsuplier()
        {
            DataSet ds1 = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds1 = qry.QuerySuplier(this.ddlbranch.SelectedValue.ToString());
            ddlsuplier.DataSource = ds1;
            ddlsuplier.DataTextField = "suppliername";
            ddlsuplier.DataValueField = "supplierid";
            ddlsuplier.DataBind();

            this.ddlsuplier.Items.Insert(000, "Select One");
            this.ddlsuplier.Items.Insert(001, "ALL");
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string datepicker = "";
            string datepicker1 = "";

            if (this.ddlsuplier.SelectedValue == "Select One")
            {
                lblwarningsupp.Visible = true;
                ddlsuplier.Focus();
                return;
            }
            else
            {
                lblwarningsupp.Visible = false;
            }

            if (this.ddlasset.SelectedValue == "Select One")
            {
                lblwarningasset.Visible = true;
                ddlasset.Focus();
                return;
            }
            else
            {
                lblwarningasset.Visible = false;
            }

            if (date1.Text == "")
            {
                datepicker = DateTime.Now.ToString("dd/MM/yyyy");
            }
            else
            {
                datepicker = date1.Text;
            }


            if (date2.Text == "")
            {
                datepicker1 = DateTime.Now.ToString("dd/MM/yyyy");
            }
            else
            {
                datepicker1 = date2.Text;
            }

            if (DateTime.ParseExact(datepicker, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(datepicker1, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true;
                return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Response.Redirect("ReportMPMF_SalesBySuplierView.aspx?optionreport=" + "&branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim() + "&supplierid=" + ddlsuplier.SelectedValue.ToString().Trim() + "&suppliername=" + ddlsuplier.SelectedItem.ToString().Trim() + "&date1=" + datepicker.ToString() + "&date2=" + datepicker1.ToString() + "&assettypeid=" + ddlasset.SelectedValue.ToString().Trim() + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("ReportMPMF_SalesBySuplier.aspx");
        }

        protected void ddlbranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadsuplier();
        }
    }
}