﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.SalesBySupplier
{
    public partial class ReportMPMF_SalesBySuplierView : System.Web.UI.Page
    {
        private string branch, branchname, supplierid, suppliername, date1, date2, assettypeid;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branch = Request.QueryString["branchid"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            supplierid = Request.QueryString["supplierid"].ToString().Trim();
            suppliername = Request.QueryString["suppliername"].ToString().Trim();
            date1 = Request.QueryString["date1"].ToString().Trim();
            date2 = Request.QueryString["date2"].ToString().Trim();
            assettypeid = Request.QueryString["assettypeid"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryRptSalesBySupplier(branch, date1, date2, supplierid, assettypeid, "spAnjCustRptSalesBySupplier");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/Report_ReportANJF_SalesBySupplier.rdlc");
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("branch", branch);
            ReportParameter p2 = new ReportParameter("branchname", branchname);
            ReportParameter p3 = new ReportParameter("date1", date1);
            ReportParameter p4 = new ReportParameter("date2", date2);
            ReportParameter p5 = new ReportParameter("suppliername", suppliername);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}