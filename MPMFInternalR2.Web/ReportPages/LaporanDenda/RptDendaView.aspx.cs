﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.LaporanDenda
{
    public partial class RptDendaView : System.Web.UI.Page
    {
        private string branch, branchname, year, type, agreementno, loginid;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            branch = Request.QueryString["branchid"].ToString().Trim();
            branchname = Request.QueryString["branchname"].ToString().Trim();
            type = Request.QueryString["type"].ToString().Trim();
            agreementno = Request.QueryString["agreementno"].ToString().Trim();
            year = Request.QueryString["year"].ToString().Trim();
            loginid = Request.QueryString["loginid"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();

            if (agreementno == "")
            {
                dt = qry.QueryReportDenda(agreementno, branch, type, year, "spSAFCust_RptLCAmount");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptDenda.rdlc");
                
            }
            else if (agreementno != "")
            {
                dt = qry.QueryReportDenda(agreementno, branch, type, year, "spSAFCust_RptLCAmountAgreementNo");
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptDendaAgreement.rdlc");
            }

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            ReportParameter p1 = new ReportParameter("Branch", branch);
            ReportParameter p2 = new ReportParameter("Type", type);
            ReportParameter p3 = new ReportParameter("Year", year);
            ReportParameter p4 = new ReportParameter("AgreementNo", agreementno);
            ReportParameter p5 = new ReportParameter("LoginID", loginid);
            ReportParameter p6 = new ReportParameter("BranchName", branchname);
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6 });
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}