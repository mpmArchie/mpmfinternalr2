﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;

namespace MPMFInternalR2.Web.ReportPages.LaporanDenda
{
    public partial class RptDenda : WebFormBase
    //public partial class RptDenda : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["sesBranchId"] = "999";
            //HttpContext.Current.Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                loadData();
            }
        }

        private void loadData()
        {
            loadbranch();
            loadYear();
        }

        protected void loadYear()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryYear();
            txtYear.Text = ds.Tables[0].Rows[0]["year"].ToString().Trim();

            ds.Dispose();

        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            //if (HttpContext.Current.Session["sesBranchId"].ToString() == "999")
            //{
            //    ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            //}
            ddlbranch.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            Response.Redirect("RptDendaView.aspx?optionreport=" + "&branchId=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim() + "&type=" + ddlType.SelectedValue.ToString().Trim() + "&year=" + txtYear.Text.ToString().Trim() + "&agreementno=" + txtAgreementNo.Text.ToString().Trim() + "&loginid=" + base.CurrentUserContext.UserId.ToString() + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("RptDenda.aspx");
        }
    }
}