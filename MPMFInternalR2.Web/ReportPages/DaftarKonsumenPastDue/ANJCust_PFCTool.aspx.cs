﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Confins.Web;

namespace MPMFInternalR2.Web.ReportPages.DaftarKonsumenPastDue
{
    //public partial class ANJCust_PFCTool : System.Web.UI.Page
    public partial class ANJCust_PFCTool : WebFormBase

    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["sesBranchId"] = "999";
            //HttpContext.Current.Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                loadbranch();
                loadOVD();
                loadColect();
            }
        }

        protected void loadColect()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryspCLActivityListCollector();
            ddlcollector.DataSource = ds;
            ddlcollector.DataTextField = "name";
            ddlcollector.DataValueField = "collectorid";
            ddlcollector.DataBind();

            ddlcollector.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlcollector.Items.Insert(1, new ListItem("UnAssign", "UnAssign"));
            //ddlcollector.Items.Insert(000, "All");
            //ddlcollector.Items.Insert(001, "UnAssign");



            ddlcollector.SelectedIndex = 0;


        }

        private void loadOVD()
        {
            ddlovd.Items.Add(new ListItem("All", "All"));
            ddlovd.Items.Add(new ListItem("0-30", "001"));
            ddlovd.Items.Add(new ListItem(">30", "002"));
        }

        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranchDue(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "cgname";
            ddlbranch.DataValueField = "cgid";
            ddlbranch.DataBind();

            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("Select One", "000"));
                ddlbranch.Items.Insert(1, new ListItem("ALL", "ALL"));
            }
            else {
                ddlbranch.Items.Insert(0, new ListItem("Select One", "000"));
            }
        
            ddlbranch.SelectedIndex = 0;
        }

        protected void ddlbranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlbranch.SelectedItem.Value.ToString().Trim() == "ALL" || this.ddlbranch.SelectedItem.Value.ToString().Trim() == "000")
            {
                ddlcollector.Items.Clear();
                ddlcollector.Items.Insert(000, "All");
                ddlcollector.Items.Insert(001, "UnAssign");
                lblwarddlbranch.Visible = false;
            }
            else
            {
                DataSet ds = new DataSet();
                QueryConnection qry = new QueryConnection();
                ds = qry.QueryspCLActivityListCollector();
                ddlcollector.DataSource = ds;
                ddlcollector.DataTextField = "name";
                ddlcollector.DataValueField = "collectorid";
                ddlcollector.DataBind();

                ddlcollector.Items.Insert(0, new ListItem("ALL", "ALL"));
                ddlcollector.Items.Insert(1, new ListItem("UnAssign", "UnAssign"));
                //ddlcollector.Items.Insert(000, "All");
                //ddlcollector.Items.Insert(001, "UnAssign");
                ddlcollector.SelectedIndex = 0;
            }
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            if (this.ddlbranch.SelectedIndex.ToString() == "0")
            {
                lblwarddlbranch.Visible = true;
                ddlbranch.Focus();
                return;
            }
            else
            {
                lblwarddlbranch.Visible = false;
            }

            string branchid = this.ddlbranch.SelectedItem.Value;
            string ovd = this.ddlovd.SelectedItem.Value;
            string collectorid = this.ddlcollector.SelectedItem.Value;
            Response.Redirect("RptANJCust_PFCToolView.aspx?optionreport=" + "&branchid=" + branchid.ToString().Trim() + "&ovd=" + ovd.ToString().Trim() + "&collectorid=" + collectorid.ToString().Trim() + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("RptANJCust_PFCTool.aspx");
        }
    }
}