﻿using Confins.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.AnjCustRptTTR
{
    //public partial class AnjCustTTR : System.Web.UI.Page
    public partial class AnjCustTTR : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Session["loginid"] = "NikoP";
            //Session["sesBranchId"] = "999";
            if (!IsPostBack)
            {
                loadbranch();
            }
        }

        private void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();

            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            string strDateStart = "";
            string strDateEnd = "";

            if (dtStart.Text == "")
                strDateStart = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strDateStart = dtStart.Text;

            if (dtEnd.Text == "")
                strDateEnd = DateTime.Now.ToString("dd/MM/yyyy");
            else
                strDateEnd = dtEnd.Text;

            if (DateTime.ParseExact(strDateStart, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(strDateEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true; return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Response.Redirect("RptANJF_TTRView.aspx?TglFrom=" + strDateStart.ToString().Trim().ToString().Trim() + "&TglTo=" + strDateEnd.ToString().Trim() + "&branchid=" + this.ddlbranch.SelectedItem.Value.ToString().Trim());
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("RptANJF_TTRView.aspx");
        }
    }
}