﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPMFInternalR2.Web.ReportPages.AnjCustRptTTR
{
    public partial class RptANJF_TTRView : System.Web.UI.Page
    {
        private string TglFrom, TglTo, branchid;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindreport();
            }
        }

        private void bindreport()
        {
            TglFrom = Request.QueryString["TglFrom"].ToString().Trim();
            TglTo = Request.QueryString["TglTo"].ToString().Trim();
            branchid = Request.QueryString["branchid"].ToString().Trim();

            DataTable dt = new DataTable();
            QueryConnection qry = new QueryConnection();
            dt = qry.QueryspAnjCustRptTTR(TglFrom, TglTo, branchid, "spAnjCustRptTTR");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportTemplate/RptReportANJF_TTR.rdlc");

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);

            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}