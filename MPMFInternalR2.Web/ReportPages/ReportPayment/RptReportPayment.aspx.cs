﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Confins.DataModel.RefCommonModel.CustomObj;
using Confins.Web;
using Confins.Web.WebUserControl;
using Confins.Web.WebUserControl.Search;
using Confins.WebLib.UIDataHelper;
using RefCommon.UserControl.Report;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;

namespace MPMFInternalR2.Web.ReportPages.ReportPayment
{
    public partial class RptReportPayment : WebFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["sesBranchId"] = "999";
            //Session["loginid"] = "NikoP";
            if (!IsPostBack)
            {
                loadData();
            }
        }

        protected void loadData()
        {
            loadbranch();
            loadassettype();
        }



        protected void loadbranch()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryBranch(base.CurrentUserContext.OfficeCode.ToString());
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "branchfullname";
            ddlbranch.DataValueField = "branchid";
            ddlbranch.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlbranch.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlbranch.SelectedIndex = 0;
        }

        protected void loadassettype()
        {
            DataSet ds = new DataSet();
            QueryConnection qry = new QueryConnection();
            ds = qry.QueryAssetType();
            ddlassettype.DataSource = ds;
            ddlassettype.DataTextField = "description";
            ddlassettype.DataValueField = "assettypeid";
            ddlassettype.DataBind();
            if (base.CurrentUserContext.OfficeCode.ToString() == "999")
            {
                ddlassettype.Items.Insert(0, new ListItem("ALL", "ALL"));
            }
            ddlassettype.SelectedIndex = 0;

            ds.Dispose();
        }

        protected void ddlperiodtype_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlperiodtype.SelectedValue.ToString() == "Daily")
            {
                Monthly.Visible = false;
                Daily.Visible = true;
            }
            else
            {
                Monthly.Visible = true;
                Daily.Visible = false;
                //loadYear();
            }
        }

        protected void lb_Print_Sync_Click(object sender, EventArgs e)
        {
            // string datepicker = "";
            string datepicker1 = "";
            string datepicker2 = "";

            if (date1.Text == "")
            {
                datepicker1 = DateTime.Now.ToString("dd/MM/yyyy");
            }

            else
            {
                datepicker1 = date1.Text;
            }


            if (date2.Text == "")
            {
                datepicker2 = DateTime.Now.ToString("dd/MM/yyyy");
            }

            else
            {
                datepicker2 = date2.Text;
            }

            //if (month.Text =="00")
            //{
            //    lblwarddlperiod.Visible = true;
            //    return;
            //}


            //if (year.Text == "00")
            //{
            //    lblwarddlperiod.Visible = true;
            //    return;
            //}


            if (DateTime.ParseExact(datepicker1, "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(datepicker2, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            {
                lblwardate.Visible = true;
                return;
            }
            else
            {
                lblwardate.Visible = false;
            }

            Response.Redirect("RptReportPaymentView.aspx?optionreport=" + "&branchid=" + ddlbranch.SelectedValue.ToString().Trim() + "&branchname=" + ddlbranch.SelectedItem.ToString().Trim() + "&month=" + month.SelectedValue.ToString().Trim() + "&year=" + year.SelectedValue.ToString().Trim() + "&date1=" + datepicker1.ToString() + "&date2=" + datepicker2.ToString() + "&reporttype=" + reporttype.SelectedValue.ToString().Trim() + "&assettypeid=" + ddlassettype.SelectedValue.ToString().Trim() + "&assettype=" + ddlassettype.SelectedItem.ToString().Trim() + "&periodtypeid=" + ddlperiodtype.SelectedValue.ToString().Trim() + "");
        }

        protected void lbReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("RptReportPayment.aspx");
        }
    }
}